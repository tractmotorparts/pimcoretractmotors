<?php

return [
    "default" => [
        "iconCls" => "pimcore_nav_icon_perspective",
        "elementTree" => [
            [
                "type" => "documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -3
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -2
            ],
            [
                "type" => "objects",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => -1
            ]
        ],
        "dashboards" => [
            "predefined" => [
                "welcome" => [
                    "positions" => [
                        [
                            [
                                "id" => 1,
                                "type" => "pimcore.layout.portlets.modificationStatistic",
                                "config" => null
                            ],
                            [
                                "id" => 2,
                                "type" => "pimcore.layout.portlets.modifiedAssets",
                                "config" => null
                            ],
                        ],
                        [
                            [
                                "id" => 3,
                                "type" => "pimcore.layout.portlets.modifiedObjects",
                                "config" => null
                            ],
                            [
                                "id" => 4,
                                "type" => "pimcore.layout.portlets.modifiedDocuments",
                                "config" => null
                            ]
                        ]
                    ],
                    ]
                ]
            ]
    ],

    "PIM" => [
        "iconCls" => "pimcore_nav_icon_object",
        "elementTree" => [
            [
                "type" => "documents",
                "position" => "left",
                "expanded" => false,
                "hidden" => true,
                "sort" => -3
            ],
            [
                "type" => "objects",
                "position" => "left",
                "expanded" => false,
                "hidden" => true,
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 1,
                "hidden" => false,
                "position" => "left",
                "sort" => -5
            ],
            [
                "type" => "customview",
                "id" => 2,
                "hidden" => false,
                "position" => "left",
                "sort" => -4
            ],

            [
                "type" => "customview",
                "id" => 4,
                "hidden" => false,
                "position" => "left",
                "sort" => 1
            ],
            [
                "type" => "customview",
                "id" => 5,
                "hidden" => false,
                "position" => "left",
                "sort" => 1
            ],
            [
                "type" => "assets",
                "position" => "left",
                "expanded" => false,
                "hidden" => false,
                "sort" => 5
            ],
            
        ],
    ],
];
