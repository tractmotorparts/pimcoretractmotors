<?php

return [
    "views" => [
        [
            "treetype" => "object",
            "name" => "Product",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/automotive.svg",
            "id" => 1,
            "showroot" => FALSE,
            "classes" => "tProduct",
            "position" => "left",
            "sort" => "1",
            "expanded" => TRUE,
            "rootfolder" => "/Product",
        ],
        [
            "treetype" => "object",
            "name" => "Parts",
            "condition" => NULL,
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-product.svg",
            "id" => 2,
            "showroot" => FALSE,
            "classes" => "Parts",
            "position" => "left",
            "sort" => "2",
            "expanded" => TRUE,
            "rootfolder" => "/Parts",
        ],
        [
            "treetype" => "object",
            "name" => "Category",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-category.svg",
            "id" => 4,
            "showroot" => false,
            "classes" => "tCategory",
            "position" => "right",
            "sort" => "5",
            "expanded" => TRUE,
            "rootfolder" => "/Category",
        ],
        [
            "treetype" => "object",
            "name" => "Master",
            "icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-category.svg",
            "id" => 5,
            "showroot" => false,
            "classes" => "tbrand,machineType",
            "position" => "right",
            "sort" => "5",
            "expanded" => TRUE,
            "rootfolder" => "/Master",
        ],


    ]
];
