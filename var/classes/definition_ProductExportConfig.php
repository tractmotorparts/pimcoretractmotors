<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- exportFor [select]
- baseAttribute [multiselect]
- mediaAttribute [multiselect]
- salesAttribute [multiselect]
- purchaseAttribute [multiselect]
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => 'prc',
   'name' => 'ProductExportConfig',
   'description' => '',
   'creationDate' => 0,
   'modificationDate' => 1620796515,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => NULL,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
         'fieldtype' => 'panel',
         'labelWidth' => 100,
         'layout' => '',
         'border' => false,
         'name' => 'Base',
         'type' => NULL,
         'region' => 'center',
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
             'fieldtype' => 'fieldcontainer',
             'labelWidth' => 100,
             'layout' => 'hbox',
             'fieldLabel' => '',
             'name' => 'Field Container',
             'type' => NULL,
             'region' => NULL,
             'title' => NULL,
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'fieldtype' => 'select',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'Product',
                    'value' => 'Product',
                  ),
                ),
                 'width' => '',
                 'defaultValue' => 'Product',
                 'optionsProviderClass' => '',
                 'optionsProviderData' => '',
                 'queryColumnType' => 'varchar',
                 'columnType' => 'varchar',
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'dynamicOptions' => false,
                 'name' => 'exportFor',
                 'title' => 'Export For he Class',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'defaultValueGenerator' => '',
              )),
            ),
             'locked' => false,
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
             'fieldtype' => 'fieldcontainer',
             'labelWidth' => 100,
             'layout' => 'hbox',
             'fieldLabel' => '',
             'name' => 'Field Container',
             'type' => NULL,
             'region' => NULL,
             'title' => NULL,
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                 'fieldtype' => 'multiselect',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SKU',
                    'value' => 'SKU',
                  ),
                  1 => 
                  array (
                    'key' => 'Product  Name',
                    'value' => 'ProductName',
                  ),
                  2 => 
                  array (
                    'key' => 'Model',
                    'value' => 'Model',
                  ),
                  3 => 
                  array (
                    'key' => 'Category',
                    'value' => 'Category',
                  ),
                  4 => 
                  array (
                    'key' => 'Brand Name',
                    'value' => 'BrandName',
                  ),
                  5 => 
                  array (
                    'key' => 'Machine Type',
                    'value' => 'MachineType',
                  ),
                  6 => 
                  array (
                    'key' => 'Country',
                    'value' => 'Country',
                  ),
                  7 => 
                  array (
                    'key' => 'Short Description',
                    'value' => 'ShortDescription',
                  ),
                  8 => 
                  array (
                    'key' => 'Color',
                    'value' => 'Color',
                  ),
                  9 => 
                  array (
                    'key' => 'Height (cm)',
                    'value' => 'Height',
                  ),
                  10 => 
                  array (
                    'key' => 'Gross Weight (kg)',
                    'value' => 'GrossWeight',
                  ),
                  11 => 
                  array (
                    'key' => 'Net Weight (kg)',
                    'value' => 'NetWeight',
                  ),
                  12 => 
                  array (
                    'key' => 'Packing Dimension Height (mm) ',
                    'value' => 'PackingDimensionHeight',
                  ),
                  13 => 
                  array (
                    'key' => 'Packing Dimension Length (mm) ',
                    'value' => 'PackingDimensionLength',
                  ),
                  14 => 
                  array (
                    'key' => 'Packing Dimension Width (mm) ',
                    'value' => 'PackingDimensionWidth',
                  ),
                  15 => 
                  array (
                    'key' => 'Description',
                    'value' => 'Description',
                  ),
                  16 => 
                  array (
                    'key' => 'Main Image',
                    'value' => 'Image',
                  ),
                  17 => 
                  array (
                    'key' => 'Image Gallery',
                    'value' => 'Gallery',
                  ),
                  18 => 
                  array (
                    'key' => 'Video Gallery',
                    'value' => 'Video',
                  ),
                  19 => 
                  array (
                    'key' => 'Currency',
                    'value' => 'currency',
                  ),
                  20 => 
                  array (
                    'key' => 'SST',
                    'value' => 'SST',
                  ),
                  21 => 
                  array (
                    'key' => 'Purchase Price',
                    'value' => 'PurchasePrice',
                  ),
                  22 => 
                  array (
                    'key' => 'Cost',
                    'value' => 'Cost',
                  ),
                  23 => 
                  array (
                    'key' => 'List Price',
                    'value' => 'ListPrice',
                  ),
                  24 => 
                  array (
                    'key' => 'Profit Margin',
                    'value' => 'profitMargin',
                  ),
                  25 => 
                  array (
                    'key' => 'Discount for EU ',
                    'value' => 'DiscountforEU',
                  ),
                  26 => 
                  array (
                    'key' => 'Additional  Discount For Eu',
                    'value' => 'addDiscountforEU',
                  ),
                  27 => 
                  array (
                    'key' => 'End User Price',
                    'value' => 'EndUserPrice',
                  ),
                  28 => 
                  array (
                    'key' => 'Discount for Dealer',
                    'value' => 'DiscountforDealer',
                  ),
                  29 => 
                  array (
                    'key' => 'Additional Discount For Dealer',
                    'value' => 'addDiscountforDealer',
                  ),
                  30 => 
                  array (
                    'key' => 'Dealer Price',
                    'value' => 'DealerPrice',
                  ),
                  31 => 
                  array (
                    'key' => 'Discount for Loose ',
                    'value' => 'DiscountforLoose',
                  ),
                  32 => 
                  array (
                    'key' => 'Additional Discount For Loose Price',
                    'value' => 'addDiscountforLoose',
                  ),
                  33 => 
                  array (
                    'key' => 'Loose Price',
                    'value' => 'LoosePrice',
                  ),
                  34 => 
                  array (
                    'key' => 'Discount for Bulk ',
                    'value' => 'DiscountforBulk',
                  ),
                  35 => 
                  array (
                    'key' => 'Additional Discount For Bulk Price',
                    'value' => 'addDiscountforBulk',
                  ),
                  36 => 
                  array (
                    'key' => 'Bulk Price',
                    'value' => 'BulkPrice',
                  ),
                  37 => 
                  array (
                    'key' => 'Technical Specification',
                    'value' => 'TechnicalSpecification',
                  ),
                ),
                 'width' => 500,
                 'height' => 300,
                 'maxItems' => '',
                 'renderType' => 'list',
                 'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\ProExportConfigOptionProvider',
                 'optionsProviderData' => '\\Pimcore\\Model\\DataObject\\Product',
                 'queryColumnType' => 'text',
                 'columnType' => 'text',
                 'phpdocType' => 'array',
                 'dynamicOptions' => false,
                 'name' => 'baseAttribute',
                 'title' => 'Base Attribute',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
             'fieldtype' => 'fieldcontainer',
             'labelWidth' => 100,
             'layout' => 'hbox',
             'fieldLabel' => '',
             'name' => 'Field Container',
             'type' => NULL,
             'region' => NULL,
             'title' => NULL,
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                 'fieldtype' => 'multiselect',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SKU',
                    'value' => 'SKU',
                  ),
                  1 => 
                  array (
                    'key' => 'Product  Name',
                    'value' => 'ProductName',
                  ),
                  2 => 
                  array (
                    'key' => 'Model',
                    'value' => 'Model',
                  ),
                  3 => 
                  array (
                    'key' => 'Category',
                    'value' => 'Category',
                  ),
                  4 => 
                  array (
                    'key' => 'Brand Name',
                    'value' => 'BrandName',
                  ),
                  5 => 
                  array (
                    'key' => 'Machine Type',
                    'value' => 'MachineType',
                  ),
                  6 => 
                  array (
                    'key' => 'Country',
                    'value' => 'Country',
                  ),
                  7 => 
                  array (
                    'key' => 'Short Description',
                    'value' => 'ShortDescription',
                  ),
                  8 => 
                  array (
                    'key' => 'Color',
                    'value' => 'Color',
                  ),
                  9 => 
                  array (
                    'key' => 'Height (cm)',
                    'value' => 'Height',
                  ),
                  10 => 
                  array (
                    'key' => 'Gross Weight (kg)',
                    'value' => 'GrossWeight',
                  ),
                  11 => 
                  array (
                    'key' => 'Net Weight (kg)',
                    'value' => 'NetWeight',
                  ),
                  12 => 
                  array (
                    'key' => 'Packing Dimension Height (mm) ',
                    'value' => 'PackingDimensionHeight',
                  ),
                  13 => 
                  array (
                    'key' => 'Packing Dimension Length (mm) ',
                    'value' => 'PackingDimensionLength',
                  ),
                  14 => 
                  array (
                    'key' => 'Packing Dimension Width (mm) ',
                    'value' => 'PackingDimensionWidth',
                  ),
                  15 => 
                  array (
                    'key' => 'Description',
                    'value' => 'Description',
                  ),
                  16 => 
                  array (
                    'key' => 'Main Image',
                    'value' => 'Image',
                  ),
                  17 => 
                  array (
                    'key' => 'Image Gallery',
                    'value' => 'Gallery',
                  ),
                  18 => 
                  array (
                    'key' => 'Video Gallery',
                    'value' => 'Video',
                  ),
                  19 => 
                  array (
                    'key' => 'Currency',
                    'value' => 'currency',
                  ),
                  20 => 
                  array (
                    'key' => 'SST',
                    'value' => 'SST',
                  ),
                  21 => 
                  array (
                    'key' => 'Purchase Price',
                    'value' => 'PurchasePrice',
                  ),
                  22 => 
                  array (
                    'key' => 'Cost',
                    'value' => 'Cost',
                  ),
                  23 => 
                  array (
                    'key' => 'List Price',
                    'value' => 'ListPrice',
                  ),
                  24 => 
                  array (
                    'key' => 'Profit Margin',
                    'value' => 'profitMargin',
                  ),
                  25 => 
                  array (
                    'key' => 'Discount for EU ',
                    'value' => 'DiscountforEU',
                  ),
                  26 => 
                  array (
                    'key' => 'Additional  Discount For Eu',
                    'value' => 'addDiscountforEU',
                  ),
                  27 => 
                  array (
                    'key' => 'End User Price',
                    'value' => 'EndUserPrice',
                  ),
                  28 => 
                  array (
                    'key' => 'Discount for Dealer',
                    'value' => 'DiscountforDealer',
                  ),
                  29 => 
                  array (
                    'key' => 'Additional Discount For Dealer',
                    'value' => 'addDiscountforDealer',
                  ),
                  30 => 
                  array (
                    'key' => 'Dealer Price',
                    'value' => 'DealerPrice',
                  ),
                  31 => 
                  array (
                    'key' => 'Discount for Loose ',
                    'value' => 'DiscountforLoose',
                  ),
                  32 => 
                  array (
                    'key' => 'Additional Discount For Loose Price',
                    'value' => 'addDiscountforLoose',
                  ),
                  33 => 
                  array (
                    'key' => 'Loose Price',
                    'value' => 'LoosePrice',
                  ),
                  34 => 
                  array (
                    'key' => 'Discount for Bulk ',
                    'value' => 'DiscountforBulk',
                  ),
                  35 => 
                  array (
                    'key' => 'Additional Discount For Bulk Price',
                    'value' => 'addDiscountforBulk',
                  ),
                  36 => 
                  array (
                    'key' => 'Bulk Price',
                    'value' => 'BulkPrice',
                  ),
                  37 => 
                  array (
                    'key' => 'Technical Specification',
                    'value' => 'TechnicalSpecification',
                  ),
                ),
                 'width' => 500,
                 'height' => 300,
                 'maxItems' => '',
                 'renderType' => 'list',
                 'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\ProExportConfigOptionProvider',
                 'optionsProviderData' => '\\Pimcore\\Model\\DataObject\\Product',
                 'queryColumnType' => 'text',
                 'columnType' => 'text',
                 'phpdocType' => 'array',
                 'dynamicOptions' => false,
                 'name' => 'mediaAttribute',
                 'title' => 'Media Attribute',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
          )),
          3 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
             'fieldtype' => 'fieldcontainer',
             'labelWidth' => 100,
             'layout' => 'hbox',
             'fieldLabel' => '',
             'name' => 'Field Container',
             'type' => NULL,
             'region' => NULL,
             'title' => NULL,
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                 'fieldtype' => 'multiselect',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SKU',
                    'value' => 'SKU',
                  ),
                  1 => 
                  array (
                    'key' => 'Product  Name',
                    'value' => 'ProductName',
                  ),
                  2 => 
                  array (
                    'key' => 'Model',
                    'value' => 'Model',
                  ),
                  3 => 
                  array (
                    'key' => 'Category',
                    'value' => 'Category',
                  ),
                  4 => 
                  array (
                    'key' => 'Brand Name',
                    'value' => 'BrandName',
                  ),
                  5 => 
                  array (
                    'key' => 'Machine Type',
                    'value' => 'MachineType',
                  ),
                  6 => 
                  array (
                    'key' => 'Country',
                    'value' => 'Country',
                  ),
                  7 => 
                  array (
                    'key' => 'Short Description',
                    'value' => 'ShortDescription',
                  ),
                  8 => 
                  array (
                    'key' => 'Color',
                    'value' => 'Color',
                  ),
                  9 => 
                  array (
                    'key' => 'Height (cm)',
                    'value' => 'Height',
                  ),
                  10 => 
                  array (
                    'key' => 'Gross Weight (kg)',
                    'value' => 'GrossWeight',
                  ),
                  11 => 
                  array (
                    'key' => 'Net Weight (kg)',
                    'value' => 'NetWeight',
                  ),
                  12 => 
                  array (
                    'key' => 'Packing Dimension Height (mm) ',
                    'value' => 'PackingDimensionHeight',
                  ),
                  13 => 
                  array (
                    'key' => 'Packing Dimension Length (mm) ',
                    'value' => 'PackingDimensionLength',
                  ),
                  14 => 
                  array (
                    'key' => 'Packing Dimension Width (mm) ',
                    'value' => 'PackingDimensionWidth',
                  ),
                  15 => 
                  array (
                    'key' => 'Description',
                    'value' => 'Description',
                  ),
                  16 => 
                  array (
                    'key' => 'Main Image',
                    'value' => 'Image',
                  ),
                  17 => 
                  array (
                    'key' => 'Image Gallery',
                    'value' => 'Gallery',
                  ),
                  18 => 
                  array (
                    'key' => 'Video Gallery',
                    'value' => 'Video',
                  ),
                  19 => 
                  array (
                    'key' => 'Currency',
                    'value' => 'currency',
                  ),
                  20 => 
                  array (
                    'key' => 'SST',
                    'value' => 'SST',
                  ),
                  21 => 
                  array (
                    'key' => 'Purchase Price',
                    'value' => 'PurchasePrice',
                  ),
                  22 => 
                  array (
                    'key' => 'Cost',
                    'value' => 'Cost',
                  ),
                  23 => 
                  array (
                    'key' => 'List Price',
                    'value' => 'ListPrice',
                  ),
                  24 => 
                  array (
                    'key' => 'Profit Margin',
                    'value' => 'profitMargin',
                  ),
                  25 => 
                  array (
                    'key' => 'Discount for EU ',
                    'value' => 'DiscountforEU',
                  ),
                  26 => 
                  array (
                    'key' => 'Additional  Discount For Eu',
                    'value' => 'addDiscountforEU',
                  ),
                  27 => 
                  array (
                    'key' => 'End User Price',
                    'value' => 'EndUserPrice',
                  ),
                  28 => 
                  array (
                    'key' => 'Discount for Dealer',
                    'value' => 'DiscountforDealer',
                  ),
                  29 => 
                  array (
                    'key' => 'Additional Discount For Dealer',
                    'value' => 'addDiscountforDealer',
                  ),
                  30 => 
                  array (
                    'key' => 'Dealer Price',
                    'value' => 'DealerPrice',
                  ),
                  31 => 
                  array (
                    'key' => 'Discount for Loose ',
                    'value' => 'DiscountforLoose',
                  ),
                  32 => 
                  array (
                    'key' => 'Additional Discount For Loose Price',
                    'value' => 'addDiscountforLoose',
                  ),
                  33 => 
                  array (
                    'key' => 'Loose Price',
                    'value' => 'LoosePrice',
                  ),
                  34 => 
                  array (
                    'key' => 'Discount for Bulk ',
                    'value' => 'DiscountforBulk',
                  ),
                  35 => 
                  array (
                    'key' => 'Additional Discount For Bulk Price',
                    'value' => 'addDiscountforBulk',
                  ),
                  36 => 
                  array (
                    'key' => 'Bulk Price',
                    'value' => 'BulkPrice',
                  ),
                  37 => 
                  array (
                    'key' => 'Technical Specification',
                    'value' => 'TechnicalSpecification',
                  ),
                ),
                 'width' => 500,
                 'height' => 300,
                 'maxItems' => '',
                 'renderType' => 'list',
                 'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\ProExportConfigOptionProvider',
                 'optionsProviderData' => '\\Pimcore\\Model\\DataObject\\Product',
                 'queryColumnType' => 'text',
                 'columnType' => 'text',
                 'phpdocType' => 'array',
                 'dynamicOptions' => false,
                 'name' => 'salesAttribute',
                 'title' => 'Sales Attribute',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
          )),
          4 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
             'fieldtype' => 'fieldcontainer',
             'labelWidth' => 100,
             'layout' => 'hbox',
             'fieldLabel' => '',
             'name' => 'Field Container',
             'type' => NULL,
             'region' => NULL,
             'title' => NULL,
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                 'fieldtype' => 'multiselect',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'SKU',
                    'value' => 'SKU',
                  ),
                  1 => 
                  array (
                    'key' => 'Product  Name',
                    'value' => 'ProductName',
                  ),
                  2 => 
                  array (
                    'key' => 'Model',
                    'value' => 'Model',
                  ),
                  3 => 
                  array (
                    'key' => 'Category',
                    'value' => 'Category',
                  ),
                  4 => 
                  array (
                    'key' => 'Brand Name',
                    'value' => 'BrandName',
                  ),
                  5 => 
                  array (
                    'key' => 'Machine Type',
                    'value' => 'MachineType',
                  ),
                  6 => 
                  array (
                    'key' => 'Country',
                    'value' => 'Country',
                  ),
                  7 => 
                  array (
                    'key' => 'Short Description',
                    'value' => 'ShortDescription',
                  ),
                  8 => 
                  array (
                    'key' => 'Color',
                    'value' => 'Color',
                  ),
                  9 => 
                  array (
                    'key' => 'Height (cm)',
                    'value' => 'Height',
                  ),
                  10 => 
                  array (
                    'key' => 'Gross Weight (kg)',
                    'value' => 'GrossWeight',
                  ),
                  11 => 
                  array (
                    'key' => 'Net Weight (kg)',
                    'value' => 'NetWeight',
                  ),
                  12 => 
                  array (
                    'key' => 'Packing Dimension Height (mm) ',
                    'value' => 'PackingDimensionHeight',
                  ),
                  13 => 
                  array (
                    'key' => 'Packing Dimension Length (mm) ',
                    'value' => 'PackingDimensionLength',
                  ),
                  14 => 
                  array (
                    'key' => 'Packing Dimension Width (mm) ',
                    'value' => 'PackingDimensionWidth',
                  ),
                  15 => 
                  array (
                    'key' => 'Description',
                    'value' => 'Description',
                  ),
                  16 => 
                  array (
                    'key' => 'Main Image',
                    'value' => 'Image',
                  ),
                  17 => 
                  array (
                    'key' => 'Image Gallery',
                    'value' => 'Gallery',
                  ),
                  18 => 
                  array (
                    'key' => 'Video Gallery',
                    'value' => 'Video',
                  ),
                  19 => 
                  array (
                    'key' => 'Currency',
                    'value' => 'currency',
                  ),
                  20 => 
                  array (
                    'key' => 'SST',
                    'value' => 'SST',
                  ),
                  21 => 
                  array (
                    'key' => 'Purchase Price',
                    'value' => 'PurchasePrice',
                  ),
                  22 => 
                  array (
                    'key' => 'Cost',
                    'value' => 'Cost',
                  ),
                  23 => 
                  array (
                    'key' => 'List Price',
                    'value' => 'ListPrice',
                  ),
                  24 => 
                  array (
                    'key' => 'Profit Margin',
                    'value' => 'profitMargin',
                  ),
                  25 => 
                  array (
                    'key' => 'Discount for EU ',
                    'value' => 'DiscountforEU',
                  ),
                  26 => 
                  array (
                    'key' => 'Additional  Discount For Eu',
                    'value' => 'addDiscountforEU',
                  ),
                  27 => 
                  array (
                    'key' => 'End User Price',
                    'value' => 'EndUserPrice',
                  ),
                  28 => 
                  array (
                    'key' => 'Discount for Dealer',
                    'value' => 'DiscountforDealer',
                  ),
                  29 => 
                  array (
                    'key' => 'Additional Discount For Dealer',
                    'value' => 'addDiscountforDealer',
                  ),
                  30 => 
                  array (
                    'key' => 'Dealer Price',
                    'value' => 'DealerPrice',
                  ),
                  31 => 
                  array (
                    'key' => 'Discount for Loose ',
                    'value' => 'DiscountforLoose',
                  ),
                  32 => 
                  array (
                    'key' => 'Additional Discount For Loose Price',
                    'value' => 'addDiscountforLoose',
                  ),
                  33 => 
                  array (
                    'key' => 'Loose Price',
                    'value' => 'LoosePrice',
                  ),
                  34 => 
                  array (
                    'key' => 'Discount for Bulk ',
                    'value' => 'DiscountforBulk',
                  ),
                  35 => 
                  array (
                    'key' => 'Additional Discount For Bulk Price',
                    'value' => 'addDiscountforBulk',
                  ),
                  36 => 
                  array (
                    'key' => 'Bulk Price',
                    'value' => 'BulkPrice',
                  ),
                  37 => 
                  array (
                    'key' => 'Technical Specification',
                    'value' => 'TechnicalSpecification',
                  ),
                ),
                 'width' => 500,
                 'height' => 300,
                 'maxItems' => '',
                 'renderType' => 'list',
                 'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\ProExportConfigOptionProvider',
                 'optionsProviderData' => '\\Pimcore\\Model\\DataObject\\Product',
                 'queryColumnType' => 'text',
                 'columnType' => 'text',
                 'phpdocType' => 'array',
                 'dynamicOptions' => false,
                 'name' => 'purchaseAttribute',
                 'title' => 'Purchase Attribute',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
          )),
        ),
         'locked' => false,
         'icon' => '',
      )),
    ),
     'locked' => false,
     'icon' => NULL,
  )),
   'icon' => '',
   'previewUrl' => '',
   'group' => 'ProductExportConfig',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'generateTypeDeclarations' => false,
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'dao' => NULL,
));
