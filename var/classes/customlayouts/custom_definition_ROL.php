<?php 

/** 
*/ 


return Pimcore\Model\DataObject\ClassDefinition\CustomLayout::__set_state(array(
   'id' => 'ROL',
   'name' => 'ReadOnlyLayout',
   'description' => '',
   'creationDate' => 1625538872,
   'modificationDate' => 1630034455,
   'userOwner' => 2,
   'userModification' => 0,
   'classId' => 'tProduct',
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Tabpanel::__set_state(array(
         'fieldtype' => 'tabpanel',
         'border' => false,
         'tabPosition' => NULL,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'name' => 'Layout',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Base Attributes',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Region::__set_state(array(
                 'fieldtype' => 'region',
                 'name' => 'Base Attributes',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => 'Base Attributes',
                 'width' => 1200,
                 'height' => 630,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                     'fieldtype' => 'panel',
                     'labelWidth' => 100,
                     'layout' => NULL,
                     'border' => false,
                     'name' => 'Layout',
                     'type' => NULL,
                     'region' => 'west',
                     'title' => '',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 400,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => true,
                         'showCharCount' => false,
                         'name' => 'SKU',
                         'title' => 'SKU',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 400,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'ProductName',
                         'title' => 'Product  Name',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 400,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'Model',
                         'title' => 'Model',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      3 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                         'fieldtype' => 'select',
                         'options' => 
                        array (
                          0 => 
                          array (
                            'key' => 'Agriculture',
                            'value' => 11,
                          ),
                          1 => 
                          array (
                            'key' => 'Agriculture - Trimmer Line',
                            'value' => 25,
                          ),
                          2 => 
                          array (
                            'key' => 'Agriculture - Auger',
                            'value' => 40,
                          ),
                          3 => 
                          array (
                            'key' => 'Agriculture - Blower',
                            'value' => 41,
                          ),
                          4 => 
                          array (
                            'key' => 'Agriculture - Brush Cutter',
                            'value' => 42,
                          ),
                          5 => 
                          array (
                            'key' => 'Agriculture - Chainsaw',
                            'value' => 43,
                          ),
                          6 => 
                          array (
                            'key' => 'Agriculture - Fogger',
                            'value' => 44,
                          ),
                          7 => 
                          array (
                            'key' => 'Agriculture - Hedge Trimmer',
                            'value' => 45,
                          ),
                          8 => 
                          array (
                            'key' => 'Agriculture - Knapsack Sprayer',
                            'value' => 46,
                          ),
                          9 => 
                          array (
                            'key' => 'Agriculture - Mist Blower',
                            'value' => 47,
                          ),
                          10 => 
                          array (
                            'key' => 'Agriculture - Mist Duster',
                            'value' => 48,
                          ),
                          11 => 
                          array (
                            'key' => 'Agriculture - Mower',
                            'value' => 49,
                          ),
                          12 => 
                          array (
                            'key' => 'Agriculture - Outboard',
                            'value' => 50,
                          ),
                          13 => 
                          array (
                            'key' => 'Agriculture - Polesaw',
                            'value' => 51,
                          ),
                          14 => 
                          array (
                            'key' => 'Agriculture - Power Sprayer',
                            'value' => 52,
                          ),
                          15 => 
                          array (
                            'key' => 'Agriculture - Power Tiller',
                            'value' => 53,
                          ),
                          16 => 
                          array (
                            'key' => 'Construction',
                            'value' => 136,
                          ),
                          17 => 
                          array (
                            'key' => 'Construction - Plate Compactor',
                            'value' => 138,
                          ),
                          18 => 
                          array (
                            'key' => 'Construction - Bar Bender',
                            'value' => 139,
                          ),
                          19 => 
                          array (
                            'key' => 'Construction - Bar Cutter',
                            'value' => 140,
                          ),
                          20 => 
                          array (
                            'key' => 'Construction - Breaker',
                            'value' => 141,
                          ),
                          21 => 
                          array (
                            'key' => 'Construction - Concrete Grinder',
                            'value' => 142,
                          ),
                          22 => 
                          array (
                            'key' => 'Construction - Concrete Mixer',
                            'value' => 143,
                          ),
                          23 => 
                          array (
                            'key' => 'Construction - Concrete Screed',
                            'value' => 144,
                          ),
                          24 => 
                          array (
                            'key' => 'Construction - Lighting Tower',
                            'value' => 145,
                          ),
                          25 => 
                          array (
                            'key' => 'Construction - Mini Hoist',
                            'value' => 146,
                          ),
                          26 => 
                          array (
                            'key' => 'Construction - Power Trowel',
                            'value' => 147,
                          ),
                          27 => 
                          array (
                            'key' => 'Construction - Road Cutter',
                            'value' => 148,
                          ),
                          28 => 
                          array (
                            'key' => 'Construction - Road Roller',
                            'value' => 149,
                          ),
                          29 => 
                          array (
                            'key' => 'Construction - Scarifying Machine',
                            'value' => 150,
                          ),
                          30 => 
                          array (
                            'key' => 'Construction - Tamping Rammer',
                            'value' => 151,
                          ),
                          31 => 
                          array (
                            'key' => 'Industrial',
                            'value' => 137,
                          ),
                          32 => 
                          array (
                            'key' => 'Industrial - Air Compressor',
                            'value' => 152,
                          ),
                          33 => 
                          array (
                            'key' => 'Industrial - Air Cooler',
                            'value' => 153,
                          ),
                          34 => 
                          array (
                            'key' => 'Industrial - Engine',
                            'value' => 154,
                          ),
                          35 => 
                          array (
                            'key' => 'Industrial - Generator',
                            'value' => 155,
                          ),
                          36 => 
                          array (
                            'key' => 'Industrial - High Pressure Washer',
                            'value' => 156,
                          ),
                          37 => 
                          array (
                            'key' => 'Industrial - Hose',
                            'value' => 157,
                          ),
                          38 => 
                          array (
                            'key' => 'Industrial - Power Tool',
                            'value' => 158,
                          ),
                          39 => 
                          array (
                            'key' => 'Industrial - Vibrating Poker',
                            'value' => 159,
                          ),
                          40 => 
                          array (
                            'key' => 'Industrial - Water Pump',
                            'value' => 160,
                          ),
                          41 => 
                          array (
                            'key' => 'Industrial - Welder',
                            'value' => 161,
                          ),
                          42 => 
                          array (
                            'key' => 'Consumer Products',
                            'value' => 174,
                          ),
                        ),
                         'width' => 280,
                         'defaultValue' => '',
                         'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\CategoryOptionProvider',
                         'optionsProviderData' => '',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'dynamicOptions' => false,
                         'name' => 'Category',
                         'title' => 'Category',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      4 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                         'fieldtype' => 'select',
                         'options' => 
                        array (
                          0 => 
                          array (
                            'key' => 'KABA',
                            'value' => 28,
                          ),
                          1 => 
                          array (
                            'key' => 'VICTA',
                            'value' => 29,
                          ),
                          2 => 
                          array (
                            'key' => 'DEHRAY',
                            'value' => 30,
                          ),
                          3 => 
                          array (
                            'key' => 'TOKUDEN',
                            'value' => 31,
                          ),
                          4 => 
                          array (
                            'key' => 'SHINERAY',
                            'value' => 32,
                          ),
                          5 => 
                          array (
                            'key' => 'TOKUDEN POWERED BY SHINERAY',
                            'value' => 33,
                          ),
                          6 => 
                          array (
                            'key' => 'KOOP',
                            'value' => 34,
                          ),
                          7 => 
                          array (
                            'key' => 'KANAZAWA',
                            'value' => 35,
                          ),
                          8 => 
                          array (
                            'key' => 'OLA',
                            'value' => 36,
                          ),
                          9 => 
                          array (
                            'key' => 'GOOD GARDENER',
                            'value' => 37,
                          ),
                          10 => 
                          array (
                            'key' => 'FARMATE',
                            'value' => 38,
                          ),
                          11 => 
                          array (
                            'key' => 'YAMAMOTO',
                            'value' => 39,
                          ),
                          12 => 
                          array (
                            'key' => 'TKM',
                            'value' => 115,
                          ),
                          13 => 
                          array (
                            'key' => 'ACCEL',
                            'value' => 163,
                          ),
                          14 => 
                          array (
                            'key' => 'PROMAC',
                            'value' => 169,
                          ),
                        ),
                         'width' => 280,
                         'defaultValue' => '',
                         'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\BrandOptionProvider',
                         'optionsProviderData' => '',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'dynamicOptions' => false,
                         'name' => 'BrandName',
                         'title' => 'Brand Name',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      5 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                         'fieldtype' => 'select',
                         'options' => 
                        array (
                          0 => 
                          array (
                            'key' => 'Petrol-Gasoline',
                            'value' => 117,
                          ),
                          1 => 
                          array (
                            'key' => 'Diesel',
                            'value' => 118,
                          ),
                          2 => 
                          array (
                            'key' => 'Centrifugal',
                            'value' => 121,
                          ),
                          3 => 
                          array (
                            'key' => 'Self Priming',
                            'value' => 122,
                          ),
                          4 => 
                          array (
                            'key' => 'Electric',
                            'value' => 123,
                          ),
                          5 => 
                          array (
                            'key' => 'Oiless Silent',
                            'value' => 124,
                          ),
                          6 => 
                          array (
                            'key' => 'Inverter',
                            'value' => 125,
                          ),
                          7 => 
                          array (
                            'key' => 'Single-Drum',
                            'value' => 126,
                          ),
                          8 => 
                          array (
                            'key' => 'Double-Drum',
                            'value' => 127,
                          ),
                          9 => 
                          array (
                            'key' => 'Spiral',
                            'value' => 128,
                          ),
                          10 => 
                          array (
                            'key' => 'Link',
                            'value' => 129,
                          ),
                          11 => 
                          array (
                            'key' => 'Bar',
                            'value' => 130,
                          ),
                          12 => 
                          array (
                            'key' => 'Plunger Pump',
                            'value' => 131,
                          ),
                          13 => 
                          array (
                            'key' => 'Diaphragm Pump',
                            'value' => 132,
                          ),
                          14 => 
                          array (
                            'key' => 'ULV',
                            'value' => 133,
                          ),
                          15 => 
                          array (
                            'key' => 'HandPush',
                            'value' => 134,
                          ),
                          16 => 
                          array (
                            'key' => 'Bicycle',
                            'value' => 135,
                          ),
                        ),
                         'width' => 280,
                         'defaultValue' => '',
                         'optionsProviderClass' => '\\AppBundle\\OptionsProviders\\MachineTypeOptionProvider',
                         'optionsProviderData' => '',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'dynamicOptions' => false,
                         'name' => 'MachineType',
                         'title' => 'Machine Type',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      6 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Country::__set_state(array(
                         'fieldtype' => 'country',
                         'restrictTo' => '',
                         'options' => 
                        array (
                          0 => 
                          array (
                            'key' => 'Afghanistan',
                            'value' => 'AF',
                          ),
                          1 => 
                          array (
                            'key' => 'Albania',
                            'value' => 'AL',
                          ),
                          2 => 
                          array (
                            'key' => 'Algeria',
                            'value' => 'DZ',
                          ),
                          3 => 
                          array (
                            'key' => 'American Samoa',
                            'value' => 'AS',
                          ),
                          4 => 
                          array (
                            'key' => 'Andorra',
                            'value' => 'AD',
                          ),
                          5 => 
                          array (
                            'key' => 'Angola',
                            'value' => 'AO',
                          ),
                          6 => 
                          array (
                            'key' => 'Anguilla',
                            'value' => 'AI',
                          ),
                          7 => 
                          array (
                            'key' => 'Antarctica',
                            'value' => 'AQ',
                          ),
                          8 => 
                          array (
                            'key' => 'Antigua & Barbuda',
                            'value' => 'AG',
                          ),
                          9 => 
                          array (
                            'key' => 'Argentina',
                            'value' => 'AR',
                          ),
                          10 => 
                          array (
                            'key' => 'Armenia',
                            'value' => 'AM',
                          ),
                          11 => 
                          array (
                            'key' => 'Aruba',
                            'value' => 'AW',
                          ),
                          12 => 
                          array (
                            'key' => 'Ascension Island',
                            'value' => 'AC',
                          ),
                          13 => 
                          array (
                            'key' => 'Australia',
                            'value' => 'AU',
                          ),
                          14 => 
                          array (
                            'key' => 'Austria',
                            'value' => 'AT',
                          ),
                          15 => 
                          array (
                            'key' => 'Azerbaijan',
                            'value' => 'AZ',
                          ),
                          16 => 
                          array (
                            'key' => 'Bahamas',
                            'value' => 'BS',
                          ),
                          17 => 
                          array (
                            'key' => 'Bahrain',
                            'value' => 'BH',
                          ),
                          18 => 
                          array (
                            'key' => 'Bangladesh',
                            'value' => 'BD',
                          ),
                          19 => 
                          array (
                            'key' => 'Barbados',
                            'value' => 'BB',
                          ),
                          20 => 
                          array (
                            'key' => 'Belarus',
                            'value' => 'BY',
                          ),
                          21 => 
                          array (
                            'key' => 'Belgium',
                            'value' => 'BE',
                          ),
                          22 => 
                          array (
                            'key' => 'Belize',
                            'value' => 'BZ',
                          ),
                          23 => 
                          array (
                            'key' => 'Benin',
                            'value' => 'BJ',
                          ),
                          24 => 
                          array (
                            'key' => 'Bermuda',
                            'value' => 'BM',
                          ),
                          25 => 
                          array (
                            'key' => 'Bhutan',
                            'value' => 'BT',
                          ),
                          26 => 
                          array (
                            'key' => 'Bolivia',
                            'value' => 'BO',
                          ),
                          27 => 
                          array (
                            'key' => 'Bosnia & Herzegovina',
                            'value' => 'BA',
                          ),
                          28 => 
                          array (
                            'key' => 'Botswana',
                            'value' => 'BW',
                          ),
                          29 => 
                          array (
                            'key' => 'Brazil',
                            'value' => 'BR',
                          ),
                          30 => 
                          array (
                            'key' => 'British Indian Ocean Territory',
                            'value' => 'IO',
                          ),
                          31 => 
                          array (
                            'key' => 'British Virgin Islands',
                            'value' => 'VG',
                          ),
                          32 => 
                          array (
                            'key' => 'Brunei',
                            'value' => 'BN',
                          ),
                          33 => 
                          array (
                            'key' => 'Bulgaria',
                            'value' => 'BG',
                          ),
                          34 => 
                          array (
                            'key' => 'Burkina Faso',
                            'value' => 'BF',
                          ),
                          35 => 
                          array (
                            'key' => 'Burundi',
                            'value' => 'BI',
                          ),
                          36 => 
                          array (
                            'key' => 'Cambodia',
                            'value' => 'KH',
                          ),
                          37 => 
                          array (
                            'key' => 'Cameroon',
                            'value' => 'CM',
                          ),
                          38 => 
                          array (
                            'key' => 'Canada',
                            'value' => 'CA',
                          ),
                          39 => 
                          array (
                            'key' => 'Canary Islands',
                            'value' => 'IC',
                          ),
                          40 => 
                          array (
                            'key' => 'Cape Verde',
                            'value' => 'CV',
                          ),
                          41 => 
                          array (
                            'key' => 'Caribbean Netherlands',
                            'value' => 'BQ',
                          ),
                          42 => 
                          array (
                            'key' => 'Cayman Islands',
                            'value' => 'KY',
                          ),
                          43 => 
                          array (
                            'key' => 'Central African Republic',
                            'value' => 'CF',
                          ),
                          44 => 
                          array (
                            'key' => 'Ceuta & Melilla',
                            'value' => 'EA',
                          ),
                          45 => 
                          array (
                            'key' => 'Chad',
                            'value' => 'TD',
                          ),
                          46 => 
                          array (
                            'key' => 'Chile',
                            'value' => 'CL',
                          ),
                          47 => 
                          array (
                            'key' => 'China',
                            'value' => 'CN',
                          ),
                          48 => 
                          array (
                            'key' => 'Christmas Island',
                            'value' => 'CX',
                          ),
                          49 => 
                          array (
                            'key' => 'Cocos (Keeling) Islands',
                            'value' => 'CC',
                          ),
                          50 => 
                          array (
                            'key' => 'Colombia',
                            'value' => 'CO',
                          ),
                          51 => 
                          array (
                            'key' => 'Comoros',
                            'value' => 'KM',
                          ),
                          52 => 
                          array (
                            'key' => 'Congo - Brazzaville',
                            'value' => 'CG',
                          ),
                          53 => 
                          array (
                            'key' => 'Congo - Kinshasa',
                            'value' => 'CD',
                          ),
                          54 => 
                          array (
                            'key' => 'Cook Islands',
                            'value' => 'CK',
                          ),
                          55 => 
                          array (
                            'key' => 'Costa Rica',
                            'value' => 'CR',
                          ),
                          56 => 
                          array (
                            'key' => 'Croatia',
                            'value' => 'HR',
                          ),
                          57 => 
                          array (
                            'key' => 'Cuba',
                            'value' => 'CU',
                          ),
                          58 => 
                          array (
                            'key' => 'Curaçao',
                            'value' => 'CW',
                          ),
                          59 => 
                          array (
                            'key' => 'Cyprus',
                            'value' => 'CY',
                          ),
                          60 => 
                          array (
                            'key' => 'Czechia',
                            'value' => 'CZ',
                          ),
                          61 => 
                          array (
                            'key' => 'Côte d’Ivoire',
                            'value' => 'CI',
                          ),
                          62 => 
                          array (
                            'key' => 'Denmark',
                            'value' => 'DK',
                          ),
                          63 => 
                          array (
                            'key' => 'Diego Garcia',
                            'value' => 'DG',
                          ),
                          64 => 
                          array (
                            'key' => 'Djibouti',
                            'value' => 'DJ',
                          ),
                          65 => 
                          array (
                            'key' => 'Dominica',
                            'value' => 'DM',
                          ),
                          66 => 
                          array (
                            'key' => 'Dominican Republic',
                            'value' => 'DO',
                          ),
                          67 => 
                          array (
                            'key' => 'Ecuador',
                            'value' => 'EC',
                          ),
                          68 => 
                          array (
                            'key' => 'Egypt',
                            'value' => 'EG',
                          ),
                          69 => 
                          array (
                            'key' => 'El Salvador',
                            'value' => 'SV',
                          ),
                          70 => 
                          array (
                            'key' => 'Equatorial Guinea',
                            'value' => 'GQ',
                          ),
                          71 => 
                          array (
                            'key' => 'Eritrea',
                            'value' => 'ER',
                          ),
                          72 => 
                          array (
                            'key' => 'Estonia',
                            'value' => 'EE',
                          ),
                          73 => 
                          array (
                            'key' => 'Eswatini',
                            'value' => 'SZ',
                          ),
                          74 => 
                          array (
                            'key' => 'Ethiopia',
                            'value' => 'ET',
                          ),
                          75 => 
                          array (
                            'key' => 'Falkland Islands',
                            'value' => 'FK',
                          ),
                          76 => 
                          array (
                            'key' => 'Faroe Islands',
                            'value' => 'FO',
                          ),
                          77 => 
                          array (
                            'key' => 'Fiji',
                            'value' => 'FJ',
                          ),
                          78 => 
                          array (
                            'key' => 'Finland',
                            'value' => 'FI',
                          ),
                          79 => 
                          array (
                            'key' => 'France',
                            'value' => 'FR',
                          ),
                          80 => 
                          array (
                            'key' => 'French Guiana',
                            'value' => 'GF',
                          ),
                          81 => 
                          array (
                            'key' => 'French Polynesia',
                            'value' => 'PF',
                          ),
                          82 => 
                          array (
                            'key' => 'French Southern Territories',
                            'value' => 'TF',
                          ),
                          83 => 
                          array (
                            'key' => 'Gabon',
                            'value' => 'GA',
                          ),
                          84 => 
                          array (
                            'key' => 'Gambia',
                            'value' => 'GM',
                          ),
                          85 => 
                          array (
                            'key' => 'Georgia',
                            'value' => 'GE',
                          ),
                          86 => 
                          array (
                            'key' => 'Germany',
                            'value' => 'DE',
                          ),
                          87 => 
                          array (
                            'key' => 'Ghana',
                            'value' => 'GH',
                          ),
                          88 => 
                          array (
                            'key' => 'Gibraltar',
                            'value' => 'GI',
                          ),
                          89 => 
                          array (
                            'key' => 'Greece',
                            'value' => 'GR',
                          ),
                          90 => 
                          array (
                            'key' => 'Greenland',
                            'value' => 'GL',
                          ),
                          91 => 
                          array (
                            'key' => 'Grenada',
                            'value' => 'GD',
                          ),
                          92 => 
                          array (
                            'key' => 'Guadeloupe',
                            'value' => 'GP',
                          ),
                          93 => 
                          array (
                            'key' => 'Guam',
                            'value' => 'GU',
                          ),
                          94 => 
                          array (
                            'key' => 'Guatemala',
                            'value' => 'GT',
                          ),
                          95 => 
                          array (
                            'key' => 'Guernsey',
                            'value' => 'GG',
                          ),
                          96 => 
                          array (
                            'key' => 'Guinea',
                            'value' => 'GN',
                          ),
                          97 => 
                          array (
                            'key' => 'Guinea-Bissau',
                            'value' => 'GW',
                          ),
                          98 => 
                          array (
                            'key' => 'Guyana',
                            'value' => 'GY',
                          ),
                          99 => 
                          array (
                            'key' => 'Haiti',
                            'value' => 'HT',
                          ),
                          100 => 
                          array (
                            'key' => 'Honduras',
                            'value' => 'HN',
                          ),
                          101 => 
                          array (
                            'key' => 'Hong Kong SAR China',
                            'value' => 'HK',
                          ),
                          102 => 
                          array (
                            'key' => 'Hungary',
                            'value' => 'HU',
                          ),
                          103 => 
                          array (
                            'key' => 'Iceland',
                            'value' => 'IS',
                          ),
                          104 => 
                          array (
                            'key' => 'India',
                            'value' => 'IN',
                          ),
                          105 => 
                          array (
                            'key' => 'Indonesia',
                            'value' => 'ID',
                          ),
                          106 => 
                          array (
                            'key' => 'Iran',
                            'value' => 'IR',
                          ),
                          107 => 
                          array (
                            'key' => 'Iraq',
                            'value' => 'IQ',
                          ),
                          108 => 
                          array (
                            'key' => 'Ireland',
                            'value' => 'IE',
                          ),
                          109 => 
                          array (
                            'key' => 'Isle of Man',
                            'value' => 'IM',
                          ),
                          110 => 
                          array (
                            'key' => 'Israel',
                            'value' => 'IL',
                          ),
                          111 => 
                          array (
                            'key' => 'Italy',
                            'value' => 'IT',
                          ),
                          112 => 
                          array (
                            'key' => 'Jamaica',
                            'value' => 'JM',
                          ),
                          113 => 
                          array (
                            'key' => 'Japan',
                            'value' => 'JP',
                          ),
                          114 => 
                          array (
                            'key' => 'Jersey',
                            'value' => 'JE',
                          ),
                          115 => 
                          array (
                            'key' => 'Jordan',
                            'value' => 'JO',
                          ),
                          116 => 
                          array (
                            'key' => 'Kazakhstan',
                            'value' => 'KZ',
                          ),
                          117 => 
                          array (
                            'key' => 'Kenya',
                            'value' => 'KE',
                          ),
                          118 => 
                          array (
                            'key' => 'Kiribati',
                            'value' => 'KI',
                          ),
                          119 => 
                          array (
                            'key' => 'Kosovo',
                            'value' => 'XK',
                          ),
                          120 => 
                          array (
                            'key' => 'Kuwait',
                            'value' => 'KW',
                          ),
                          121 => 
                          array (
                            'key' => 'Kyrgyzstan',
                            'value' => 'KG',
                          ),
                          122 => 
                          array (
                            'key' => 'Laos',
                            'value' => 'LA',
                          ),
                          123 => 
                          array (
                            'key' => 'Latvia',
                            'value' => 'LV',
                          ),
                          124 => 
                          array (
                            'key' => 'Lebanon',
                            'value' => 'LB',
                          ),
                          125 => 
                          array (
                            'key' => 'Lesotho',
                            'value' => 'LS',
                          ),
                          126 => 
                          array (
                            'key' => 'Liberia',
                            'value' => 'LR',
                          ),
                          127 => 
                          array (
                            'key' => 'Libya',
                            'value' => 'LY',
                          ),
                          128 => 
                          array (
                            'key' => 'Liechtenstein',
                            'value' => 'LI',
                          ),
                          129 => 
                          array (
                            'key' => 'Lithuania',
                            'value' => 'LT',
                          ),
                          130 => 
                          array (
                            'key' => 'Luxembourg',
                            'value' => 'LU',
                          ),
                          131 => 
                          array (
                            'key' => 'Macao SAR China',
                            'value' => 'MO',
                          ),
                          132 => 
                          array (
                            'key' => 'Madagascar',
                            'value' => 'MG',
                          ),
                          133 => 
                          array (
                            'key' => 'Malawi',
                            'value' => 'MW',
                          ),
                          134 => 
                          array (
                            'key' => 'Malaysia',
                            'value' => 'MY',
                          ),
                          135 => 
                          array (
                            'key' => 'Maldives',
                            'value' => 'MV',
                          ),
                          136 => 
                          array (
                            'key' => 'Mali',
                            'value' => 'ML',
                          ),
                          137 => 
                          array (
                            'key' => 'Malta',
                            'value' => 'MT',
                          ),
                          138 => 
                          array (
                            'key' => 'Marshall Islands',
                            'value' => 'MH',
                          ),
                          139 => 
                          array (
                            'key' => 'Martinique',
                            'value' => 'MQ',
                          ),
                          140 => 
                          array (
                            'key' => 'Mauritania',
                            'value' => 'MR',
                          ),
                          141 => 
                          array (
                            'key' => 'Mauritius',
                            'value' => 'MU',
                          ),
                          142 => 
                          array (
                            'key' => 'Mayotte',
                            'value' => 'YT',
                          ),
                          143 => 
                          array (
                            'key' => 'Mexico',
                            'value' => 'MX',
                          ),
                          144 => 
                          array (
                            'key' => 'Micronesia',
                            'value' => 'FM',
                          ),
                          145 => 
                          array (
                            'key' => 'Moldova',
                            'value' => 'MD',
                          ),
                          146 => 
                          array (
                            'key' => 'Monaco',
                            'value' => 'MC',
                          ),
                          147 => 
                          array (
                            'key' => 'Mongolia',
                            'value' => 'MN',
                          ),
                          148 => 
                          array (
                            'key' => 'Montenegro',
                            'value' => 'ME',
                          ),
                          149 => 
                          array (
                            'key' => 'Montserrat',
                            'value' => 'MS',
                          ),
                          150 => 
                          array (
                            'key' => 'Morocco',
                            'value' => 'MA',
                          ),
                          151 => 
                          array (
                            'key' => 'Mozambique',
                            'value' => 'MZ',
                          ),
                          152 => 
                          array (
                            'key' => 'Myanmar (Burma)',
                            'value' => 'MM',
                          ),
                          153 => 
                          array (
                            'key' => 'Namibia',
                            'value' => 'NA',
                          ),
                          154 => 
                          array (
                            'key' => 'Nauru',
                            'value' => 'NR',
                          ),
                          155 => 
                          array (
                            'key' => 'Nepal',
                            'value' => 'NP',
                          ),
                          156 => 
                          array (
                            'key' => 'Netherlands',
                            'value' => 'NL',
                          ),
                          157 => 
                          array (
                            'key' => 'New Caledonia',
                            'value' => 'NC',
                          ),
                          158 => 
                          array (
                            'key' => 'New Zealand',
                            'value' => 'NZ',
                          ),
                          159 => 
                          array (
                            'key' => 'Nicaragua',
                            'value' => 'NI',
                          ),
                          160 => 
                          array (
                            'key' => 'Niger',
                            'value' => 'NE',
                          ),
                          161 => 
                          array (
                            'key' => 'Nigeria',
                            'value' => 'NG',
                          ),
                          162 => 
                          array (
                            'key' => 'Niue',
                            'value' => 'NU',
                          ),
                          163 => 
                          array (
                            'key' => 'Norfolk Island',
                            'value' => 'NF',
                          ),
                          164 => 
                          array (
                            'key' => 'North Korea',
                            'value' => 'KP',
                          ),
                          165 => 
                          array (
                            'key' => 'North Macedonia',
                            'value' => 'MK',
                          ),
                          166 => 
                          array (
                            'key' => 'Northern Mariana Islands',
                            'value' => 'MP',
                          ),
                          167 => 
                          array (
                            'key' => 'Norway',
                            'value' => 'NO',
                          ),
                          168 => 
                          array (
                            'key' => 'Oman',
                            'value' => 'OM',
                          ),
                          169 => 
                          array (
                            'key' => 'Pakistan',
                            'value' => 'PK',
                          ),
                          170 => 
                          array (
                            'key' => 'Palau',
                            'value' => 'PW',
                          ),
                          171 => 
                          array (
                            'key' => 'Palestinian Territories',
                            'value' => 'PS',
                          ),
                          172 => 
                          array (
                            'key' => 'Panama',
                            'value' => 'PA',
                          ),
                          173 => 
                          array (
                            'key' => 'Papua New Guinea',
                            'value' => 'PG',
                          ),
                          174 => 
                          array (
                            'key' => 'Paraguay',
                            'value' => 'PY',
                          ),
                          175 => 
                          array (
                            'key' => 'Peru',
                            'value' => 'PE',
                          ),
                          176 => 
                          array (
                            'key' => 'Philippines',
                            'value' => 'PH',
                          ),
                          177 => 
                          array (
                            'key' => 'Pitcairn Islands',
                            'value' => 'PN',
                          ),
                          178 => 
                          array (
                            'key' => 'Poland',
                            'value' => 'PL',
                          ),
                          179 => 
                          array (
                            'key' => 'Portugal',
                            'value' => 'PT',
                          ),
                          180 => 
                          array (
                            'key' => 'Pseudo-Accents',
                            'value' => 'XA',
                          ),
                          181 => 
                          array (
                            'key' => 'Pseudo-Bidi',
                            'value' => 'XB',
                          ),
                          182 => 
                          array (
                            'key' => 'Puerto Rico',
                            'value' => 'PR',
                          ),
                          183 => 
                          array (
                            'key' => 'Qatar',
                            'value' => 'QA',
                          ),
                          184 => 
                          array (
                            'key' => 'Romania',
                            'value' => 'RO',
                          ),
                          185 => 
                          array (
                            'key' => 'Russia',
                            'value' => 'RU',
                          ),
                          186 => 
                          array (
                            'key' => 'Rwanda',
                            'value' => 'RW',
                          ),
                          187 => 
                          array (
                            'key' => 'Réunion',
                            'value' => 'RE',
                          ),
                          188 => 
                          array (
                            'key' => 'Samoa',
                            'value' => 'WS',
                          ),
                          189 => 
                          array (
                            'key' => 'San Marino',
                            'value' => 'SM',
                          ),
                          190 => 
                          array (
                            'key' => 'Saudi Arabia',
                            'value' => 'SA',
                          ),
                          191 => 
                          array (
                            'key' => 'Senegal',
                            'value' => 'SN',
                          ),
                          192 => 
                          array (
                            'key' => 'Serbia',
                            'value' => 'RS',
                          ),
                          193 => 
                          array (
                            'key' => 'Seychelles',
                            'value' => 'SC',
                          ),
                          194 => 
                          array (
                            'key' => 'Sierra Leone',
                            'value' => 'SL',
                          ),
                          195 => 
                          array (
                            'key' => 'Singapore',
                            'value' => 'SG',
                          ),
                          196 => 
                          array (
                            'key' => 'Sint Maarten',
                            'value' => 'SX',
                          ),
                          197 => 
                          array (
                            'key' => 'Slovakia',
                            'value' => 'SK',
                          ),
                          198 => 
                          array (
                            'key' => 'Slovenia',
                            'value' => 'SI',
                          ),
                          199 => 
                          array (
                            'key' => 'Solomon Islands',
                            'value' => 'SB',
                          ),
                          200 => 
                          array (
                            'key' => 'Somalia',
                            'value' => 'SO',
                          ),
                          201 => 
                          array (
                            'key' => 'South Africa',
                            'value' => 'ZA',
                          ),
                          202 => 
                          array (
                            'key' => 'South Georgia & South Sandwich Islands',
                            'value' => 'GS',
                          ),
                          203 => 
                          array (
                            'key' => 'South Korea',
                            'value' => 'KR',
                          ),
                          204 => 
                          array (
                            'key' => 'South Sudan',
                            'value' => 'SS',
                          ),
                          205 => 
                          array (
                            'key' => 'Spain',
                            'value' => 'ES',
                          ),
                          206 => 
                          array (
                            'key' => 'Sri Lanka',
                            'value' => 'LK',
                          ),
                          207 => 
                          array (
                            'key' => 'St. Barthélemy',
                            'value' => 'BL',
                          ),
                          208 => 
                          array (
                            'key' => 'St. Helena',
                            'value' => 'SH',
                          ),
                          209 => 
                          array (
                            'key' => 'St. Kitts & Nevis',
                            'value' => 'KN',
                          ),
                          210 => 
                          array (
                            'key' => 'St. Lucia',
                            'value' => 'LC',
                          ),
                          211 => 
                          array (
                            'key' => 'St. Martin',
                            'value' => 'MF',
                          ),
                          212 => 
                          array (
                            'key' => 'St. Pierre & Miquelon',
                            'value' => 'PM',
                          ),
                          213 => 
                          array (
                            'key' => 'St. Vincent & Grenadines',
                            'value' => 'VC',
                          ),
                          214 => 
                          array (
                            'key' => 'Sudan',
                            'value' => 'SD',
                          ),
                          215 => 
                          array (
                            'key' => 'Suriname',
                            'value' => 'SR',
                          ),
                          216 => 
                          array (
                            'key' => 'Svalbard & Jan Mayen',
                            'value' => 'SJ',
                          ),
                          217 => 
                          array (
                            'key' => 'Sweden',
                            'value' => 'SE',
                          ),
                          218 => 
                          array (
                            'key' => 'Switzerland',
                            'value' => 'CH',
                          ),
                          219 => 
                          array (
                            'key' => 'Syria',
                            'value' => 'SY',
                          ),
                          220 => 
                          array (
                            'key' => 'São Tomé & Príncipe',
                            'value' => 'ST',
                          ),
                          221 => 
                          array (
                            'key' => 'Taiwan',
                            'value' => 'TW',
                          ),
                          222 => 
                          array (
                            'key' => 'Tajikistan',
                            'value' => 'TJ',
                          ),
                          223 => 
                          array (
                            'key' => 'Tanzania',
                            'value' => 'TZ',
                          ),
                          224 => 
                          array (
                            'key' => 'Thailand',
                            'value' => 'TH',
                          ),
                          225 => 
                          array (
                            'key' => 'Timor-Leste',
                            'value' => 'TL',
                          ),
                          226 => 
                          array (
                            'key' => 'Togo',
                            'value' => 'TG',
                          ),
                          227 => 
                          array (
                            'key' => 'Tokelau',
                            'value' => 'TK',
                          ),
                          228 => 
                          array (
                            'key' => 'Tonga',
                            'value' => 'TO',
                          ),
                          229 => 
                          array (
                            'key' => 'Trinidad & Tobago',
                            'value' => 'TT',
                          ),
                          230 => 
                          array (
                            'key' => 'Tristan da Cunha',
                            'value' => 'TA',
                          ),
                          231 => 
                          array (
                            'key' => 'Tunisia',
                            'value' => 'TN',
                          ),
                          232 => 
                          array (
                            'key' => 'Turkey',
                            'value' => 'TR',
                          ),
                          233 => 
                          array (
                            'key' => 'Turkmenistan',
                            'value' => 'TM',
                          ),
                          234 => 
                          array (
                            'key' => 'Turks & Caicos Islands',
                            'value' => 'TC',
                          ),
                          235 => 
                          array (
                            'key' => 'Tuvalu',
                            'value' => 'TV',
                          ),
                          236 => 
                          array (
                            'key' => 'U.S. Outlying Islands',
                            'value' => 'UM',
                          ),
                          237 => 
                          array (
                            'key' => 'U.S. Virgin Islands',
                            'value' => 'VI',
                          ),
                          238 => 
                          array (
                            'key' => 'Uganda',
                            'value' => 'UG',
                          ),
                          239 => 
                          array (
                            'key' => 'Ukraine',
                            'value' => 'UA',
                          ),
                          240 => 
                          array (
                            'key' => 'United Arab Emirates',
                            'value' => 'AE',
                          ),
                          241 => 
                          array (
                            'key' => 'United Kingdom',
                            'value' => 'GB',
                          ),
                          242 => 
                          array (
                            'key' => 'United States',
                            'value' => 'US',
                          ),
                          243 => 
                          array (
                            'key' => 'Uruguay',
                            'value' => 'UY',
                          ),
                          244 => 
                          array (
                            'key' => 'Uzbekistan',
                            'value' => 'UZ',
                          ),
                          245 => 
                          array (
                            'key' => 'Vanuatu',
                            'value' => 'VU',
                          ),
                          246 => 
                          array (
                            'key' => 'Vatican City',
                            'value' => 'VA',
                          ),
                          247 => 
                          array (
                            'key' => 'Venezuela',
                            'value' => 'VE',
                          ),
                          248 => 
                          array (
                            'key' => 'Vietnam',
                            'value' => 'VN',
                          ),
                          249 => 
                          array (
                            'key' => 'Wallis & Futuna',
                            'value' => 'WF',
                          ),
                          250 => 
                          array (
                            'key' => 'Western Sahara',
                            'value' => 'EH',
                          ),
                          251 => 
                          array (
                            'key' => 'Yemen',
                            'value' => 'YE',
                          ),
                          252 => 
                          array (
                            'key' => 'Zambia',
                            'value' => 'ZM',
                          ),
                          253 => 
                          array (
                            'key' => 'Zimbabwe',
                            'value' => 'ZW',
                          ),
                          254 => 
                          array (
                            'key' => 'Åland Islands',
                            'value' => 'AX',
                          ),
                        ),
                         'width' => '',
                         'defaultValue' => NULL,
                         'optionsProviderClass' => NULL,
                         'optionsProviderData' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'dynamicOptions' => false,
                         'name' => 'Country',
                         'title' => 'Country',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => 'width:500px',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      7 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                         'fieldtype' => 'textarea',
                         'width' => 400,
                         'height' => 75,
                         'maxLength' => 120,
                         'showCharCount' => true,
                         'excludeFromSearchIndex' => false,
                         'queryColumnType' => 'longtext',
                         'columnType' => 'longtext',
                         'phpdocType' => 'string',
                         'name' => 'ShortDescription',
                         'title' => 'Short Description',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                    ),
                     'locked' => false,
                     'icon' => '',
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                     'fieldtype' => 'panel',
                     'labelWidth' => 100,
                     'layout' => NULL,
                     'border' => true,
                     'name' => 'Layout',
                     'type' => NULL,
                     'region' => 'east',
                     'title' => '',
                     'width' => 600,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor::__set_state(array(
                         'fieldtype' => 'rgbaColor',
                         'width' => 330,
                         'queryColumnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'columnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\RgbaColor',
                         'name' => 'Color',
                         'title' => 'Color 1',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor::__set_state(array(
                         'fieldtype' => 'rgbaColor',
                         'width' => NULL,
                         'queryColumnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'columnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\RgbaColor',
                         'name' => 'Color2',
                         'title' => 'Color 2',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor::__set_state(array(
                         'fieldtype' => 'rgbaColor',
                         'width' => NULL,
                         'queryColumnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'columnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\RgbaColor',
                         'name' => 'Color3',
                         'title' => 'Color 3',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      3 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor::__set_state(array(
                         'fieldtype' => 'rgbaColor',
                         'width' => NULL,
                         'queryColumnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'columnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\RgbaColor',
                         'name' => 'Color4',
                         'title' => 'Color 4',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      4 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\RgbaColor::__set_state(array(
                         'fieldtype' => 'rgbaColor',
                         'width' => NULL,
                         'queryColumnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'columnType' => 
                        array (
                          'rgb' => 'VARCHAR(6) NULL DEFAULT NULL',
                          'a' => 'VARCHAR(2) NULL DEFAULT NULL',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\RgbaColor',
                         'name' => 'Color5',
                         'title' => 'Color 5',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      5 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'Height',
                         'title' => 'Height (cm)',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      6 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'GrossWeight',
                         'title' => 'Gross Weight (kg)',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      7 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'NetWeight',
                         'title' => 'Net Weight (kg)',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      8 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'PackingDimensionHeight',
                         'title' => 'Packing Dimension Height (mm) ',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      9 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'PackingDimensionLength',
                         'title' => 'Packing Dimension Length (mm) ',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      10 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                         'fieldtype' => 'numeric',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'double',
                         'columnType' => 'double',
                         'phpdocType' => 'float',
                         'integer' => false,
                         'unsigned' => true,
                         'minValue' => 0,
                         'maxValue' => NULL,
                         'unique' => false,
                         'decimalSize' => NULL,
                         'decimalPrecision' => NULL,
                         'name' => 'PackingDimensionWidth',
                         'title' => 'Packing Dimension Width (mm) ',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                    ),
                     'locked' => false,
                     'icon' => '',
                  )),
                ),
                 'locked' => false,
                 'icon' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                 'fieldtype' => 'panel',
                 'labelWidth' => 100,
                 'layout' => NULL,
                 'border' => false,
                 'name' => 'Layout',
                 'type' => NULL,
                 'region' => 'south',
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Wysiwyg::__set_state(array(
                     'fieldtype' => 'wysiwyg',
                     'width' => '',
                     'height' => '',
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'toolbarConfig' => '',
                     'excludeFromSearchIndex' => false,
                     'name' => 'Description',
                     'title' => 'Description',
                     'tooltip' => '',
                     'mandatory' => true,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
                 'icon' => '',
              )),
            ),
             'locked' => false,
             'icon' => '',
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'name' => 'Media',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Media',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Image::__set_state(array(
                 'fieldtype' => 'image',
                 'width' => '',
                 'height' => '',
                 'uploadPath' => '/_default_upload_bucket',
                 'queryColumnType' => 'int(11)',
                 'columnType' => 'int(11)',
                 'phpdocType' => '\\Pimcore\\Model\\Asset\\Image',
                 'name' => 'Image',
                 'title' => 'Main Image',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => true,
                 'visibleSearch' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ImageGallery::__set_state(array(
                 'fieldtype' => 'imageGallery',
                 'queryColumnType' => 
                array (
                  'images' => 'text',
                  'hotspots' => 'text',
                ),
                 'columnType' => 
                array (
                  'images' => 'text',
                  'hotspots' => 'text',
                ),
                 'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\ImageGallery',
                 'width' => 300,
                 'height' => 300,
                 'uploadPath' => '/_default_upload_bucket',
                 'ratioX' => NULL,
                 'ratioY' => NULL,
                 'predefinedDataTemplates' => '',
                 'name' => 'Gallery',
                 'title' => 'Image Gallery',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Block::__set_state(array(
                 'fieldtype' => 'video',
                 'columnType' => 'text',
                 'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\Video',
                 'name' => 'Video',
                 'title' => 'Video',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'width' => 300,
                 'height' => 300,
                 'queryColumnType' => 'text',
              )),
            ),
             'locked' => false,
             'icon' => '',
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'name' => 'Sales Attributes',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Sales Attributes',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                 'fieldtype' => 'select',
                 'options' => 
                array (
                  0 => 
                  array (
                    'key' => 'AED',
                    'value' => 'AED-&#1583;.&#1573;',
                  ),
                  1 => 
                  array (
                    'key' => 'AFN',
                    'value' => 'AFN-&#65;&#102;',
                  ),
                  2 => 
                  array (
                    'key' => 'ALL',
                    'value' => 'ALL-&#76;&#101;&#107;',
                  ),
                  3 => 
                  array (
                    'key' => 'ANG',
                    'value' => 'ANG-&#402;',
                  ),
                  4 => 
                  array (
                    'key' => 'AOA',
                    'value' => 'AOA-&#75;&#122;',
                  ),
                  5 => 
                  array (
                    'key' => 'ARS',
                    'value' => 'ARS-&#36;',
                  ),
                  6 => 
                  array (
                    'key' => 'AUD',
                    'value' => 'AUD-&#36;',
                  ),
                  7 => 
                  array (
                    'key' => 'AWG',
                    'value' => 'AWG-&#402;',
                  ),
                  8 => 
                  array (
                    'key' => 'AZN',
                    'value' => 'AZN-&#1084;&#1072;&#1085;',
                  ),
                  9 => 
                  array (
                    'key' => 'BAM',
                    'value' => 'BAM-&#75;&#77;',
                  ),
                  10 => 
                  array (
                    'key' => 'BBD',
                    'value' => 'BBD-&#36;',
                  ),
                  11 => 
                  array (
                    'key' => 'BDT',
                    'value' => 'BDT-&#2547;',
                  ),
                  12 => 
                  array (
                    'key' => 'BGN',
                    'value' => 'BGN-&#1083;&#1074;',
                  ),
                  13 => 
                  array (
                    'key' => 'BHD',
                    'value' => 'BHD-.&#1583;.&#1576;',
                  ),
                  14 => 
                  array (
                    'key' => 'BIF',
                    'value' => 'BIF-&#70;&#66;&#117;',
                  ),
                  15 => 
                  array (
                    'key' => 'BMD',
                    'value' => 'BMD-&#36;',
                  ),
                  16 => 
                  array (
                    'key' => 'BND',
                    'value' => 'BND-&#36;',
                  ),
                  17 => 
                  array (
                    'key' => 'BOB',
                    'value' => 'BOB-&#36;&#98;',
                  ),
                  18 => 
                  array (
                    'key' => 'BRL',
                    'value' => 'BRL-&#82;&#36;',
                  ),
                  19 => 
                  array (
                    'key' => 'BSD',
                    'value' => 'BSD-&#36;',
                  ),
                  20 => 
                  array (
                    'key' => 'BTN',
                    'value' => 'BTN-&#78;&#117;&#46;',
                  ),
                  21 => 
                  array (
                    'key' => 'BWP',
                    'value' => 'BWP-&#80;',
                  ),
                  22 => 
                  array (
                    'key' => 'BYR',
                    'value' => 'BYR-&#112;&#46;',
                  ),
                  23 => 
                  array (
                    'key' => 'BZD',
                    'value' => 'BZD-&#66;&#90;&#36;',
                  ),
                  24 => 
                  array (
                    'key' => 'CAD',
                    'value' => 'CAD-&#36;',
                  ),
                  25 => 
                  array (
                    'key' => 'CDF',
                    'value' => 'CDF-&#70;&#67;',
                  ),
                  26 => 
                  array (
                    'key' => 'CHF',
                    'value' => 'CHF-&#67;&#72;&#70;',
                  ),
                  27 => 
                  array (
                    'key' => 'CLP',
                    'value' => 'CLP-&#36;',
                  ),
                  28 => 
                  array (
                    'key' => 'CNY',
                    'value' => 'CNY-&#165;',
                  ),
                  29 => 
                  array (
                    'key' => 'COP',
                    'value' => 'COP-&#36;',
                  ),
                  30 => 
                  array (
                    'key' => 'CRC',
                    'value' => 'CRC-&#8353;',
                  ),
                  31 => 
                  array (
                    'key' => 'CUP',
                    'value' => 'CUP-&#8396;',
                  ),
                  32 => 
                  array (
                    'key' => 'CVE',
                    'value' => 'CVE-&#36;',
                  ),
                  33 => 
                  array (
                    'key' => 'CZK',
                    'value' => 'CZK-&#75;&#269;',
                  ),
                  34 => 
                  array (
                    'key' => 'DJF',
                    'value' => 'DJF-&#70;&#100;&#106;',
                  ),
                  35 => 
                  array (
                    'key' => 'DKK',
                    'value' => 'DKK-&#107;&#114;',
                  ),
                  36 => 
                  array (
                    'key' => 'DOP',
                    'value' => 'DOP-&#82;&#68;&#36;',
                  ),
                  37 => 
                  array (
                    'key' => 'DZD',
                    'value' => 'DZD-&#1583;&#1580;',
                  ),
                  38 => 
                  array (
                    'key' => 'EGP',
                    'value' => 'EGP-&#163;',
                  ),
                  39 => 
                  array (
                    'key' => 'ETB',
                    'value' => 'ETB-&#66;&#114;',
                  ),
                  40 => 
                  array (
                    'key' => 'EUR',
                    'value' => 'EUR-&#8364;',
                  ),
                  41 => 
                  array (
                    'key' => 'FJD',
                    'value' => 'FJD-&#36;',
                  ),
                  42 => 
                  array (
                    'key' => 'FKP',
                    'value' => 'FKP-&#163;',
                  ),
                  43 => 
                  array (
                    'key' => 'GBP',
                    'value' => 'GBP-&#163;',
                  ),
                  44 => 
                  array (
                    'key' => 'GEL',
                    'value' => 'GEL-&#4314;',
                  ),
                  45 => 
                  array (
                    'key' => 'GHS',
                    'value' => 'GHS-&#162;',
                  ),
                  46 => 
                  array (
                    'key' => 'GIP',
                    'value' => 'GIP-&#163;',
                  ),
                  47 => 
                  array (
                    'key' => 'GMD',
                    'value' => 'GMD-&#68;',
                  ),
                  48 => 
                  array (
                    'key' => 'GNF',
                    'value' => 'GNF-&#70;&#71;',
                  ),
                  49 => 
                  array (
                    'key' => 'GTQ',
                    'value' => 'GTQ-&#81;',
                  ),
                  50 => 
                  array (
                    'key' => 'GYD',
                    'value' => 'GYD-&#36;',
                  ),
                  51 => 
                  array (
                    'key' => 'HKD',
                    'value' => 'HKD-&#36;',
                  ),
                  52 => 
                  array (
                    'key' => 'HNL',
                    'value' => 'HNL-&#76;',
                  ),
                  53 => 
                  array (
                    'key' => 'HRK',
                    'value' => 'HRK-&#107;&#110;',
                  ),
                  54 => 
                  array (
                    'key' => 'HTG',
                    'value' => 'HTG-&#71;',
                  ),
                  55 => 
                  array (
                    'key' => 'HUF',
                    'value' => 'HUF-&#70;&#116;',
                  ),
                  56 => 
                  array (
                    'key' => 'IDR',
                    'value' => 'IDR-&#82;&#112;',
                  ),
                  57 => 
                  array (
                    'key' => 'ILS',
                    'value' => 'ILS-&#8362;',
                  ),
                  58 => 
                  array (
                    'key' => 'INR',
                    'value' => 'INR-&#8377;',
                  ),
                  59 => 
                  array (
                    'key' => 'IQD',
                    'value' => 'IQD-&#1593;.&#1583;',
                  ),
                  60 => 
                  array (
                    'key' => 'IRR',
                    'value' => 'IRR-&#65020;',
                  ),
                  61 => 
                  array (
                    'key' => 'ISK',
                    'value' => 'ISK-&#107;&#114;',
                  ),
                  62 => 
                  array (
                    'key' => 'JEP',
                    'value' => 'JEP-&#163;',
                  ),
                  63 => 
                  array (
                    'key' => 'JMD',
                    'value' => 'JMD-&#74;&#36;',
                  ),
                  64 => 
                  array (
                    'key' => 'JOD',
                    'value' => 'JOD-&#74;&#68;',
                  ),
                  65 => 
                  array (
                    'key' => 'JPY',
                    'value' => 'JPY-&#165;',
                  ),
                  66 => 
                  array (
                    'key' => 'KES',
                    'value' => 'KES-&#75;&#83;&#104;',
                  ),
                  67 => 
                  array (
                    'key' => 'KGS',
                    'value' => 'KGS-&#1083;&#1074;',
                  ),
                  68 => 
                  array (
                    'key' => 'KHR',
                    'value' => 'KHR-&#6107;',
                  ),
                  69 => 
                  array (
                    'key' => 'KMF',
                    'value' => 'KMF-&#67;&#70;',
                  ),
                  70 => 
                  array (
                    'key' => 'KPW',
                    'value' => 'KPW-&#8361;',
                  ),
                  71 => 
                  array (
                    'key' => 'KRW',
                    'value' => 'KRW-&#8361;',
                  ),
                  72 => 
                  array (
                    'key' => 'KWD',
                    'value' => 'KWD-&#1583;.&#1603;',
                  ),
                  73 => 
                  array (
                    'key' => 'KYD',
                    'value' => 'KYD-&#36;',
                  ),
                  74 => 
                  array (
                    'key' => 'KZT',
                    'value' => 'KZT-&#1083;&#1074;',
                  ),
                  75 => 
                  array (
                    'key' => 'LAK',
                    'value' => 'LAK-&#8365;',
                  ),
                  76 => 
                  array (
                    'key' => 'LBP',
                    'value' => 'LBP-&#163;',
                  ),
                  77 => 
                  array (
                    'key' => 'LKR',
                    'value' => 'LKR-&#8360;',
                  ),
                  78 => 
                  array (
                    'key' => 'LRD',
                    'value' => 'LRD-&#36;',
                  ),
                  79 => 
                  array (
                    'key' => 'LSL',
                    'value' => 'LSL-&#76;',
                  ),
                  80 => 
                  array (
                    'key' => 'LTL',
                    'value' => 'LTL-&#76;&#116;',
                  ),
                  81 => 
                  array (
                    'key' => 'LVL',
                    'value' => 'LVL-&#76;&#115;',
                  ),
                  82 => 
                  array (
                    'key' => 'LYD',
                    'value' => 'LYD-&#1604;.&#1583;',
                  ),
                  83 => 
                  array (
                    'key' => 'MAD',
                    'value' => 'MAD-&#1583;.&#1605;.',
                  ),
                  84 => 
                  array (
                    'key' => 'MDL',
                    'value' => 'MDL-&#76;',
                  ),
                  85 => 
                  array (
                    'key' => 'MGA',
                    'value' => 'MGA-&#65;&#114;',
                  ),
                  86 => 
                  array (
                    'key' => 'MKD',
                    'value' => 'MKD-&#1076;&#1077;&#1085;',
                  ),
                  87 => 
                  array (
                    'key' => 'MMK',
                    'value' => 'MMK-&#75;',
                  ),
                  88 => 
                  array (
                    'key' => 'MNT',
                    'value' => 'MNT-&#8366;',
                  ),
                  89 => 
                  array (
                    'key' => 'MOP',
                    'value' => 'MOP-&#77;&#79;&#80;&#36;',
                  ),
                  90 => 
                  array (
                    'key' => 'MRO',
                    'value' => 'MRO-&#85;&#77;',
                  ),
                  91 => 
                  array (
                    'key' => 'MUR',
                    'value' => 'MUR-&#8360;',
                  ),
                  92 => 
                  array (
                    'key' => 'MVR',
                    'value' => 'MVR-.&#1923;',
                  ),
                  93 => 
                  array (
                    'key' => 'MWK',
                    'value' => 'MWK-&#77;&#75;',
                  ),
                  94 => 
                  array (
                    'key' => 'MXN',
                    'value' => 'MXN-&#36;',
                  ),
                  95 => 
                  array (
                    'key' => 'MYR',
                    'value' => 'MYR-&#82;&#77;',
                  ),
                  96 => 
                  array (
                    'key' => 'MZN',
                    'value' => 'MZN-&#77;&#84;',
                  ),
                  97 => 
                  array (
                    'key' => 'NAD',
                    'value' => 'NAD-&#36;',
                  ),
                  98 => 
                  array (
                    'key' => 'NGN',
                    'value' => 'NGN-&#8358;',
                  ),
                  99 => 
                  array (
                    'key' => 'NIO',
                    'value' => 'NIO-&#67;&#36;',
                  ),
                  100 => 
                  array (
                    'key' => 'NOK',
                    'value' => 'NOK-&#107;&#114;',
                  ),
                  101 => 
                  array (
                    'key' => 'NPR',
                    'value' => 'NPR-&#8360;',
                  ),
                  102 => 
                  array (
                    'key' => 'NZD',
                    'value' => 'NZD-&#36;',
                  ),
                  103 => 
                  array (
                    'key' => 'OMR',
                    'value' => 'OMR-&#65020;',
                  ),
                  104 => 
                  array (
                    'key' => 'PAB',
                    'value' => 'PAB-&#66;&#47;&#46;',
                  ),
                  105 => 
                  array (
                    'key' => 'PEN',
                    'value' => 'PEN-&#83;&#47;&#46;',
                  ),
                  106 => 
                  array (
                    'key' => 'PGK',
                    'value' => 'PGK-&#75;',
                  ),
                  107 => 
                  array (
                    'key' => 'PHP',
                    'value' => 'PHP-&#8369;',
                  ),
                  108 => 
                  array (
                    'key' => 'PKR',
                    'value' => 'PKR-&#8360;',
                  ),
                  109 => 
                  array (
                    'key' => 'PLN',
                    'value' => 'PLN-&#122;&#322;',
                  ),
                  110 => 
                  array (
                    'key' => 'PYG',
                    'value' => 'PYG-&#71;&#115;',
                  ),
                  111 => 
                  array (
                    'key' => 'QAR',
                    'value' => 'QAR-&#65020;',
                  ),
                  112 => 
                  array (
                    'key' => 'RON',
                    'value' => 'RON-&#108;&#101;&#105;',
                  ),
                  113 => 
                  array (
                    'key' => 'RSD',
                    'value' => 'RSD-&#1044;&#1080;&#1085;&#46;',
                  ),
                  114 => 
                  array (
                    'key' => 'RUB',
                    'value' => 'RUB-&#1088;&#1091;&#1073;',
                  ),
                  115 => 
                  array (
                    'key' => 'RWF',
                    'value' => 'RWF-&#1585;.&#1587;',
                  ),
                  116 => 
                  array (
                    'key' => 'SAR',
                    'value' => 'SAR-&#65020;',
                  ),
                  117 => 
                  array (
                    'key' => 'SBD',
                    'value' => 'SBD-&#36;',
                  ),
                  118 => 
                  array (
                    'key' => 'SCR',
                    'value' => 'SCR-&#8360;',
                  ),
                  119 => 
                  array (
                    'key' => 'SDG',
                    'value' => 'SDG-&#163;',
                  ),
                  120 => 
                  array (
                    'key' => 'SEK',
                    'value' => 'SEK-&#107;&#114;',
                  ),
                  121 => 
                  array (
                    'key' => 'SGD',
                    'value' => 'SGD-&#36;',
                  ),
                  122 => 
                  array (
                    'key' => 'SHP',
                    'value' => 'SHP-&#163;',
                  ),
                  123 => 
                  array (
                    'key' => 'SLL',
                    'value' => 'SLL-&#76;&#101;',
                  ),
                  124 => 
                  array (
                    'key' => 'SOS',
                    'value' => 'SOS-&#83;',
                  ),
                  125 => 
                  array (
                    'key' => 'SRD',
                    'value' => 'SRD-&#36;',
                  ),
                  126 => 
                  array (
                    'key' => 'STD',
                    'value' => 'STD-&#68;&#98;',
                  ),
                  127 => 
                  array (
                    'key' => 'SVC',
                    'value' => 'SVC-&#36;',
                  ),
                  128 => 
                  array (
                    'key' => 'SYP',
                    'value' => 'SYP-&#163;',
                  ),
                  129 => 
                  array (
                    'key' => 'SZL',
                    'value' => 'SZL-&#76;',
                  ),
                  130 => 
                  array (
                    'key' => 'THB',
                    'value' => 'THB-&#3647;',
                  ),
                  131 => 
                  array (
                    'key' => 'TJS',
                    'value' => 'TJS-&#84;&#74;&#83;',
                  ),
                  132 => 
                  array (
                    'key' => 'TMT',
                    'value' => 'TMT-&#109;',
                  ),
                  133 => 
                  array (
                    'key' => 'TND',
                    'value' => 'TND-&#1583;.&#1578;',
                  ),
                  134 => 
                  array (
                    'key' => 'TOP',
                    'value' => 'TOP-&#84;&#36;',
                  ),
                  135 => 
                  array (
                    'key' => 'TRY',
                    'value' => 'TRY-&#8356;',
                  ),
                  136 => 
                  array (
                    'key' => 'TTD',
                    'value' => 'TTD-&#36;',
                  ),
                  137 => 
                  array (
                    'key' => 'TWD',
                    'value' => 'TWD-&#78;&#84;&#36;',
                  ),
                  138 => 
                  array (
                    'key' => 'UAH',
                    'value' => 'UAH-&#8372;',
                  ),
                  139 => 
                  array (
                    'key' => 'UGX',
                    'value' => 'UGX-&#85;&#83;&#104;',
                  ),
                  140 => 
                  array (
                    'key' => 'USD',
                    'value' => 'USD-&#36;',
                  ),
                  141 => 
                  array (
                    'key' => 'UYU',
                    'value' => 'UYU-&#36;&#85;',
                  ),
                  142 => 
                  array (
                    'key' => 'UZS',
                    'value' => 'UZS-&#1083;&#1074;',
                  ),
                  143 => 
                  array (
                    'key' => 'VEF',
                    'value' => 'VEF-&#66;&#115;',
                  ),
                  144 => 
                  array (
                    'key' => 'VND',
                    'value' => 'VND-&#8363;',
                  ),
                  145 => 
                  array (
                    'key' => 'VUV',
                    'value' => 'VUV-&#86;&#84;',
                  ),
                  146 => 
                  array (
                    'key' => 'WST',
                    'value' => 'WST-&#87;&#83;&#36;',
                  ),
                  147 => 
                  array (
                    'key' => 'XAF',
                    'value' => 'XAF-&#70;&#67;&#70;&#65;',
                  ),
                  148 => 
                  array (
                    'key' => 'XCD',
                    'value' => 'XCD-&#36;',
                  ),
                  149 => 
                  array (
                    'key' => 'XPF',
                    'value' => 'XPF-&#70;',
                  ),
                  150 => 
                  array (
                    'key' => 'YER',
                    'value' => 'YER-&#65020;',
                  ),
                  151 => 
                  array (
                    'key' => 'ZAR',
                    'value' => 'ZAR-&#82;',
                  ),
                  152 => 
                  array (
                    'key' => 'ZMK',
                    'value' => 'ZMK-&#90;&#75;',
                  ),
                  153 => 
                  array (
                    'key' => 'ZWL',
                    'value' => 'ZWL-&#90;&#36;',
                  ),
                ),
                 'width' => '',
                 'defaultValue' => 'MYR-&#82;&#77;',
                 'optionsProviderClass' => 'AppBundle\\OptionsProviders\\CurrencyOptionProvider',
                 'optionsProviderData' => '',
                 'queryColumnType' => 'varchar',
                 'columnType' => 'varchar',
                 'columnLength' => 190,
                 'phpdocType' => 'string',
                 'dynamicOptions' => false,
                 'name' => 'currency',
                 'title' => 'Currency',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'defaultValueGenerator' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Checkbox::__set_state(array(
                 'fieldtype' => 'checkbox',
                 'defaultValue' => NULL,
                 'queryColumnType' => 'tinyint(1)',
                 'columnType' => 'tinyint(1)',
                 'phpdocType' => 'bool',
                 'name' => 'SST',
                 'title' => 'SST',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'defaultValueGenerator' => '',
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                 'fieldtype' => 'numeric',
                 'width' => 330,
                 'defaultValue' => NULL,
                 'queryColumnType' => 'double',
                 'columnType' => 'double',
                 'phpdocType' => 'float',
                 'integer' => false,
                 'unsigned' => false,
                 'minValue' => NULL,
                 'maxValue' => NULL,
                 'unique' => false,
                 'decimalSize' => NULL,
                 'decimalPrecision' => NULL,
                 'name' => 'PurchasePrice',
                 'title' => 'Purchase Price',
                 'tooltip' => '',
                 'mandatory' => true,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'defaultValueGenerator' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                 'fieldtype' => 'numeric',
                 'width' => 330,
                 'defaultValue' => NULL,
                 'queryColumnType' => 'double',
                 'columnType' => 'double',
                 'phpdocType' => 'float',
                 'integer' => false,
                 'unsigned' => false,
                 'minValue' => NULL,
                 'maxValue' => NULL,
                 'unique' => false,
                 'decimalSize' => NULL,
                 'decimalPrecision' => NULL,
                 'name' => 'Cost',
                 'title' => 'Cost',
                 'tooltip' => 'Cost is calculated based Purchase cost of the product, US Exchange Rate.',
                 'mandatory' => true,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'defaultValueGenerator' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 100,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => NULL,
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 330,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'ListPrice',
                     'title' => 'List Price',
                     'tooltip' => '',
                     'mandatory' => true,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                     'defaultValueGenerator' => '',
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 330,
                     'defaultValue' => 0.45,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => 0.45,
                     'maxValue' => 0.85,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'profitMargin',
                     'title' => 'Profit Margin',
                     'tooltip' => 'Minimum Value should be 0.75',
                     'mandatory' => true,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                     'defaultValueGenerator' => '',
                  )),
                ),
                 'locked' => false,
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                 'fieldtype' => 'panel',
                 'labelWidth' => 100,
                 'layout' => NULL,
                 'border' => false,
                 'name' => 'Discount',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => 'Discount',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => NULL,
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => '10',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'DiscountforEU',
                         'title' => 'Discount for EU ',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => NULL,
                         'defaultValue' => '0',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'addDiscountforEU',
                         'title' => 'Additional  Discount For Eu',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'EndUserPrice',
                         'title' => 'End User Price',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                    ),
                     'locked' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => NULL,
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => '10',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'DiscountforDealer',
                         'title' => 'Discount for Dealer',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => NULL,
                         'defaultValue' => '0',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'addDiscountforDealer',
                         'title' => 'Additional Discount For Dealer',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'DealerPrice',
                         'title' => 'Dealer Price',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                    ),
                     'locked' => false,
                  )),
                  2 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => NULL,
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => '10',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'DiscountforLoose',
                         'title' => 'Discount for Loose ',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => NULL,
                         'defaultValue' => '0',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'addDiscountforLoose',
                         'title' => 'Additional Discount For Loose Price',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'LoosePrice',
                         'title' => 'Loose Price',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                    ),
                     'locked' => false,
                  )),
                  3 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => NULL,
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => '10',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'DiscountforBulk',
                         'title' => 'Discount for Bulk ',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => NULL,
                         'defaultValue' => '0',
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'addDiscountforBulk',
                         'title' => 'Additional Discount For Bulk Price',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => 330,
                         'defaultValue' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'BulkPrice',
                         'title' => 'Bulk Price',
                         'tooltip' => '',
                         'mandatory' => true,
                         'noteditable' => true,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                         'defaultValueGenerator' => '',
                      )),
                    ),
                     'locked' => false,
                  )),
                ),
                 'locked' => false,
                 'icon' => '',
              )),
            ),
             'locked' => false,
             'icon' => '',
          )),
          3 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'name' => 'RelatedParts',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Related Parts',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation::__set_state(array(
                 'fieldtype' => 'manyToManyObjectRelation',
                 'width' => '',
                 'height' => '',
                 'maxItems' => '',
                 'queryColumnType' => 'text',
                 'phpdocType' => 'array',
                 'relationType' => true,
                 'visibleFields' => 'id,ProductName,SKU,Model,Category,BrandName,MachineType',
                 'allowToCreateNewObject' => false,
                 'optimizedAdminLoading' => false,
                 'visibleFieldDefinitions' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'Parts',
                  ),
                ),
                 'pathFormatterClass' => '',
                 'name' => 'relatedParts',
                 'title' => 'relatedParts',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
             'icon' => '',
          )),
          4 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'name' => 'Technical Attributes',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Technical Attributes',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Classificationstore::__set_state(array(
                 'fieldtype' => 'classificationstore',
                 'phpdocType' => '\\Pimcore\\Model\\DataObject\\Classificationstore',
                 'childs' => 
                array (
                ),
                 'name' => 'TechnicalSpecification',
                 'region' => NULL,
                 'layout' => NULL,
                 'title' => 'Technical Specification',
                 'width' => '',
                 'height' => '',
                 'maxTabs' => NULL,
                 'labelWidth' => 0,
                 'localized' => false,
                 'storeId' => '1',
                 'hideEmptyData' => false,
                 'disallowAddRemove' => false,
                 'referencedFields' => 
                array (
                ),
                 'fieldDefinitionsCache' => NULL,
                 'allowedGroupIds' => 
                array (
                ),
                 'activeGroupDefinitions' => 
                array (
                ),
                 'maxItems' => 2,
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => true,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
             'icon' => '',
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
     'icon' => NULL,
  )),
   'default' => 0,
   'dao' => NULL,
));
