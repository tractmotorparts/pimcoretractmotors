<?php 

namespace Pimcore\Model\DataObject\Brand;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Brand current()
 * @method DataObject\Brand[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "tbrand";
protected $className = "Brand";


/**
* Filter by Brand (Brand )
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBrand ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Brand")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Description (Description)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescription ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Description")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by BrandLogo (Brand  Logo)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBrandLogo ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("BrandLogo")->addListingFilter($this, $data, $operator);
	return $this;
}



}
