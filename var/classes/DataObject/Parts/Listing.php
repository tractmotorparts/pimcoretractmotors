<?php 

namespace Pimcore\Model\DataObject\Parts;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Parts current()
 * @method DataObject\Parts[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "Parts";
protected $className = "Parts";


/**
* Filter by SKU (SKU)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySKU ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("SKU")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by ProductName (Product Name)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByProductName ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("ProductName")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Model (Model)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByModel ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Model")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Category (Category)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCategory ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Category")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by BrandName (Brand Name)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBrandName ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("BrandName")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by MachineType (Machine Type)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMachineType ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("MachineType")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Country (Country)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCountry ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Country")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by ShortDescription (Short Description)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByShortDescription ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("ShortDescription")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Height (Height (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByHeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Height")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by GrossWeight (Gross Weight (kg))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByGrossWeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("GrossWeight")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by NetWeight (Net Weight (kg))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByNetWeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("NetWeight")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by PackingDimensionHeight (Packing Dimension Height (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPackingDimensionHeight ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("PackingDimensionHeight")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by PackingDimensionLength (Packing Dimension Length (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPackingDimensionLength ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("PackingDimensionLength")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by PackingDimensionWidth (Packing Dimension Width (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPackingDimensionWidth ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("PackingDimensionWidth")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Thickness (Thickness (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByThickness ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Thickness")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Width (Width (mm))
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByWidth ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Width")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Description (Description)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescription ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Description")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Image (Main Image)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByImage ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Image")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by currency (Currency)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCurrency ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("currency")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by SST (SST)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySST ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("SST")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by PurchasePrice (Purchase Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPurchasePrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("PurchasePrice")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Cost (Cost)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCost ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Cost")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by ListPrice (List Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByListPrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("ListPrice")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by profitMargin (Profit Margin)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByProfitMargin ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("profitMargin")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by DiscountforEU (Discount for EU)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDiscountforEU ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("DiscountforEU")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by addDiscountforEU (Additional  Discount For Eu)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByAddDiscountforEU ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("addDiscountforEU")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by EndUserPrice (End User Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByEndUserPrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("EndUserPrice")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by DiscountforDealer (Discount for Dealer)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDiscountforDealer ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("DiscountforDealer")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by addDiscountforDealer (Additional Discount For Dealer)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByAddDiscountforDealer ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("addDiscountforDealer")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by DealerPrice (Dealer Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDealerPrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("DealerPrice")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by DiscountforLoose (Discount for Loose )
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDiscountforLoose ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("DiscountforLoose")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by addDiscountforLoose (Additional Discount For Loose Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByAddDiscountforLoose ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("addDiscountforLoose")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by LoosePrice (Loose Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByLoosePrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("LoosePrice")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by DiscountforBulk (Discount for Bulk )
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDiscountforBulk ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("DiscountforBulk")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by addDiscountforBulk (Additional Discount For Bulk Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByAddDiscountforBulk ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("addDiscountforBulk")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by BulkPrice (Bulk Price)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBulkPrice ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("BulkPrice")->addListingFilter($this, $data, $operator);
	return $this;
}



}
