<?php 

namespace Pimcore\Model\DataObject\ProductExportConfig;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ProductExportConfig current()
 * @method DataObject\ProductExportConfig[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "prc";
protected $className = "ProductExportConfig";


/**
* Filter by exportFor (Export For he Class)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByExportFor ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("exportFor")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by baseAttribute (Base Attribute)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByBaseAttribute ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("baseAttribute")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by mediaAttribute (Media Attribute)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMediaAttribute ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("mediaAttribute")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by salesAttribute (Sales Attribute)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterBySalesAttribute ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("salesAttribute")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by purchaseAttribute (Purchase Attribute)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByPurchaseAttribute ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("purchaseAttribute")->addListingFilter($this, $data, $operator);
	return $this;
}



}
