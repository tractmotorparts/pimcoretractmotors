<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- SKU [input]
- ProductName [input]
- Model [input]
- Category [select]
- BrandName [select]
- MachineType [select]
- Country [country]
- ShortDescription [textarea]
- Color [rgbaColor]
- Height [numeric]
- GrossWeight [numeric]
- NetWeight [numeric]
- PackingDimensionHeight [numeric]
- PackingDimensionLength [numeric]
- PackingDimensionWidth [numeric]
- Thickness [numeric]
- Width [numeric]
- Description [wysiwyg]
- Image [image]
- Gallery [imageGallery]
- Video [block]
-- Video [video]
- currency [select]
- SST [checkbox]
- PurchasePrice [numeric]
- Cost [numeric]
- ListPrice [numeric]
- profitMargin [numeric]
- DiscountforEU [numeric]
- addDiscountforEU [numeric]
- EndUserPrice [numeric]
- DiscountforDealer [numeric]
- addDiscountforDealer [numeric]
- DealerPrice [numeric]
- DiscountforLoose [numeric]
- addDiscountforLoose [numeric]
- LoosePrice [numeric]
- DiscountforBulk [numeric]
- addDiscountforBulk [numeric]
- BulkPrice [numeric]
- TechnicalSpecification [classificationstore]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getBySKU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByProductName ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByModel ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByCategory ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByBrandName ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByMachineType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByCountry ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByShortDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByHeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByGrossWeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByNetWeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByPackingDimensionHeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByPackingDimensionLength ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByPackingDimensionWidth ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByThickness ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByWidth ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByImage ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByCurrency ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getBySST ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByPurchasePrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByCost ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByListPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByProfitMargin ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDiscountforEU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByAddDiscountforEU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByEndUserPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDiscountforDealer ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByAddDiscountforDealer ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDealerPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDiscountforLoose ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByAddDiscountforLoose ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByLoosePrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByDiscountforBulk ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByAddDiscountforBulk ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Parts\Listing|\Pimcore\Model\DataObject\Parts getByBulkPrice ($value, $limit = 0, $offset = 0) 
*/

class Parts extends Concrete {

protected $o_classId = "Parts";
protected $o_className = "Parts";
protected $SKU;
protected $ProductName;
protected $Model;
protected $Category;
protected $BrandName;
protected $MachineType;
protected $Country;
protected $ShortDescription;
protected $Color;
protected $Height;
protected $GrossWeight;
protected $NetWeight;
protected $PackingDimensionHeight;
protected $PackingDimensionLength;
protected $PackingDimensionWidth;
protected $Thickness;
protected $Width;
protected $Description;
protected $Image;
protected $Gallery;
protected $Video;
protected $currency;
protected $SST;
protected $PurchasePrice;
protected $Cost;
protected $ListPrice;
protected $profitMargin;
protected $DiscountforEU;
protected $addDiscountforEU;
protected $EndUserPrice;
protected $DiscountforDealer;
protected $addDiscountforDealer;
protected $DealerPrice;
protected $DiscountforLoose;
protected $addDiscountforLoose;
protected $LoosePrice;
protected $DiscountforBulk;
protected $addDiscountforBulk;
protected $BulkPrice;
protected $TechnicalSpecification;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Parts
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get SKU - SKU
* @return string|null
*/
public function getSKU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("SKU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->SKU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set SKU - SKU
* @param string|null $SKU
* @return \Pimcore\Model\DataObject\Parts
*/
public function setSKU ($SKU) {
	$fd = $this->getClass()->getFieldDefinition("SKU");
	$this->SKU = $SKU;
	return $this;
}

/**
* Get ProductName - Product Name
* @return string|null
*/
public function getProductName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ProductName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ProductName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ProductName - Product Name
* @param string|null $ProductName
* @return \Pimcore\Model\DataObject\Parts
*/
public function setProductName ($ProductName) {
	$fd = $this->getClass()->getFieldDefinition("ProductName");
	$this->ProductName = $ProductName;
	return $this;
}

/**
* Get Model - Model
* @return string|null
*/
public function getModel () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Model"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Model;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Model - Model
* @param string|null $Model
* @return \Pimcore\Model\DataObject\Parts
*/
public function setModel ($Model) {
	$fd = $this->getClass()->getFieldDefinition("Model");
	$this->Model = $Model;
	return $this;
}

/**
* Get Category - Category
* @return string|null
*/
public function getCategory () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Category"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Category;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Category - Category
* @param string|null $Category
* @return \Pimcore\Model\DataObject\Parts
*/
public function setCategory ($Category) {
	$fd = $this->getClass()->getFieldDefinition("Category");
	$this->Category = $Category;
	return $this;
}

/**
* Get BrandName - Brand Name
* @return string|null
*/
public function getBrandName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("BrandName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->BrandName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set BrandName - Brand Name
* @param string|null $BrandName
* @return \Pimcore\Model\DataObject\Parts
*/
public function setBrandName ($BrandName) {
	$fd = $this->getClass()->getFieldDefinition("BrandName");
	$this->BrandName = $BrandName;
	return $this;
}

/**
* Get MachineType - Machine Type
* @return string|null
*/
public function getMachineType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("MachineType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->MachineType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set MachineType - Machine Type
* @param string|null $MachineType
* @return \Pimcore\Model\DataObject\Parts
*/
public function setMachineType ($MachineType) {
	$fd = $this->getClass()->getFieldDefinition("MachineType");
	$this->MachineType = $MachineType;
	return $this;
}

/**
* Get Country - Country
* @return string|null
*/
public function getCountry () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Country"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Country;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Country - Country
* @param string|null $Country
* @return \Pimcore\Model\DataObject\Parts
*/
public function setCountry ($Country) {
	$fd = $this->getClass()->getFieldDefinition("Country");
	$this->Country = $Country;
	return $this;
}

/**
* Get ShortDescription - Short Description
* @return string|null
*/
public function getShortDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ShortDescription"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ShortDescription;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ShortDescription - Short Description
* @param string|null $ShortDescription
* @return \Pimcore\Model\DataObject\Parts
*/
public function setShortDescription ($ShortDescription) {
	$fd = $this->getClass()->getFieldDefinition("ShortDescription");
	$this->ShortDescription = $ShortDescription;
	return $this;
}

/**
* Get Color - Color
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color - Color
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color
* @return \Pimcore\Model\DataObject\Parts
*/
public function setColor ($Color) {
	$fd = $this->getClass()->getFieldDefinition("Color");
	$this->Color = $Color;
	return $this;
}

/**
* Get Height - Height (mm)
* @return float|null
*/
public function getHeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Height"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Height;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Height - Height (mm)
* @param float|null $Height
* @return \Pimcore\Model\DataObject\Parts
*/
public function setHeight ($Height) {
	$fd = $this->getClass()->getFieldDefinition("Height");
	$this->Height = $fd->preSetData($this, $Height);
	return $this;
}

/**
* Get GrossWeight - Gross Weight (kg)
* @return float|null
*/
public function getGrossWeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("GrossWeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->GrossWeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set GrossWeight - Gross Weight (kg)
* @param float|null $GrossWeight
* @return \Pimcore\Model\DataObject\Parts
*/
public function setGrossWeight ($GrossWeight) {
	$fd = $this->getClass()->getFieldDefinition("GrossWeight");
	$this->GrossWeight = $fd->preSetData($this, $GrossWeight);
	return $this;
}

/**
* Get NetWeight - Net Weight (kg)
* @return float|null
*/
public function getNetWeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("NetWeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->NetWeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set NetWeight - Net Weight (kg)
* @param float|null $NetWeight
* @return \Pimcore\Model\DataObject\Parts
*/
public function setNetWeight ($NetWeight) {
	$fd = $this->getClass()->getFieldDefinition("NetWeight");
	$this->NetWeight = $fd->preSetData($this, $NetWeight);
	return $this;
}

/**
* Get PackingDimensionHeight - Packing Dimension Height (mm)
* @return float|null
*/
public function getPackingDimensionHeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionHeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionHeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionHeight - Packing Dimension Height (mm)
* @param float|null $PackingDimensionHeight
* @return \Pimcore\Model\DataObject\Parts
*/
public function setPackingDimensionHeight ($PackingDimensionHeight) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionHeight");
	$this->PackingDimensionHeight = $fd->preSetData($this, $PackingDimensionHeight);
	return $this;
}

/**
* Get PackingDimensionLength - Packing Dimension Length (mm)
* @return float|null
*/
public function getPackingDimensionLength () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionLength"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionLength;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionLength - Packing Dimension Length (mm)
* @param float|null $PackingDimensionLength
* @return \Pimcore\Model\DataObject\Parts
*/
public function setPackingDimensionLength ($PackingDimensionLength) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionLength");
	$this->PackingDimensionLength = $fd->preSetData($this, $PackingDimensionLength);
	return $this;
}

/**
* Get PackingDimensionWidth - Packing Dimension Width (mm)
* @return float|null
*/
public function getPackingDimensionWidth () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionWidth"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionWidth;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionWidth - Packing Dimension Width (mm)
* @param float|null $PackingDimensionWidth
* @return \Pimcore\Model\DataObject\Parts
*/
public function setPackingDimensionWidth ($PackingDimensionWidth) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionWidth");
	$this->PackingDimensionWidth = $fd->preSetData($this, $PackingDimensionWidth);
	return $this;
}

/**
* Get Thickness - Thickness (mm)
* @return float|null
*/
public function getThickness () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Thickness"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Thickness;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Thickness - Thickness (mm)
* @param float|null $Thickness
* @return \Pimcore\Model\DataObject\Parts
*/
public function setThickness ($Thickness) {
	$fd = $this->getClass()->getFieldDefinition("Thickness");
	$this->Thickness = $fd->preSetData($this, $Thickness);
	return $this;
}

/**
* Get Width - Width (mm)
* @return float|null
*/
public function getWidth () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Width"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Width;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Width - Width (mm)
* @param float|null $Width
* @return \Pimcore\Model\DataObject\Parts
*/
public function setWidth ($Width) {
	$fd = $this->getClass()->getFieldDefinition("Width");
	$this->Width = $fd->preSetData($this, $Width);
	return $this;
}

/**
* Get Description - Description
* @return string|null
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string|null $Description
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Image - Main Image
* @return \Pimcore\Model\Asset\Image|null
*/
public function getImage () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image - Main Image
* @param \Pimcore\Model\Asset\Image|null $Image
* @return \Pimcore\Model\DataObject\Parts
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

/**
* Get Gallery - Image Gallery
* @return \Pimcore\Model\DataObject\Data\ImageGallery
*/
public function getGallery () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Gallery"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Gallery;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Gallery - Image Gallery
* @param \Pimcore\Model\DataObject\Data\ImageGallery $Gallery
* @return \Pimcore\Model\DataObject\Parts
*/
public function setGallery ($Gallery) {
	$fd = $this->getClass()->getFieldDefinition("Gallery");
	$this->Gallery = $Gallery;
	return $this;
}

/**
* Get Video - Video Gallery
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getVideo () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Video"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Video")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Video - Video Gallery
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $Video
* @return \Pimcore\Model\DataObject\Parts
*/
public function setVideo ($Video) {
	$fd = $this->getClass()->getFieldDefinition("Video");
	$this->Video = $fd->preSetData($this, $Video);
	return $this;
}

/**
* Get currency - Currency
* @return string|null
*/
public function getCurrency () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("currency"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->currency;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set currency - Currency
* @param string|null $currency
* @return \Pimcore\Model\DataObject\Parts
*/
public function setCurrency ($currency) {
	$fd = $this->getClass()->getFieldDefinition("currency");
	$this->currency = $currency;
	return $this;
}

/**
* Get SST - SST
* @return bool|null
*/
public function getSST () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("SST"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->SST;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set SST - SST
* @param bool|null $SST
* @return \Pimcore\Model\DataObject\Parts
*/
public function setSST ($SST) {
	$fd = $this->getClass()->getFieldDefinition("SST");
	$this->SST = $SST;
	return $this;
}

/**
* Get PurchasePrice - Purchase Price
* @return float|null
*/
public function getPurchasePrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PurchasePrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PurchasePrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PurchasePrice - Purchase Price
* @param float|null $PurchasePrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setPurchasePrice ($PurchasePrice) {
	$fd = $this->getClass()->getFieldDefinition("PurchasePrice");
	$this->PurchasePrice = $fd->preSetData($this, $PurchasePrice);
	return $this;
}

/**
* Get Cost - Cost
* @return float|null
*/
public function getCost () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Cost"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Cost;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Cost - Cost
* @param float|null $Cost
* @return \Pimcore\Model\DataObject\Parts
*/
public function setCost ($Cost) {
	$fd = $this->getClass()->getFieldDefinition("Cost");
	$this->Cost = $fd->preSetData($this, $Cost);
	return $this;
}

/**
* Get ListPrice - List Price
* @return float|null
*/
public function getListPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ListPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ListPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ListPrice - List Price
* @param float|null $ListPrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setListPrice ($ListPrice) {
	$fd = $this->getClass()->getFieldDefinition("ListPrice");
	$this->ListPrice = $fd->preSetData($this, $ListPrice);
	return $this;
}

/**
* Get profitMargin - Profit Margin
* @return float|null
*/
public function getProfitMargin () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("profitMargin"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->profitMargin;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set profitMargin - Profit Margin
* @param float|null $profitMargin
* @return \Pimcore\Model\DataObject\Parts
*/
public function setProfitMargin ($profitMargin) {
	$fd = $this->getClass()->getFieldDefinition("profitMargin");
	$this->profitMargin = $fd->preSetData($this, $profitMargin);
	return $this;
}

/**
* Get DiscountforEU - Discount for EU
* @return float|null
*/
public function getDiscountforEU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforEU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforEU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforEU - Discount for EU
* @param float|null $DiscountforEU
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDiscountforEU ($DiscountforEU) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforEU");
	$this->DiscountforEU = $fd->preSetData($this, $DiscountforEU);
	return $this;
}

/**
* Get addDiscountforEU - Additional  Discount For Eu
* @return float|null
*/
public function getAddDiscountforEU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforEU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforEU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforEU - Additional  Discount For Eu
* @param float|null $addDiscountforEU
* @return \Pimcore\Model\DataObject\Parts
*/
public function setAddDiscountforEU ($addDiscountforEU) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforEU");
	$this->addDiscountforEU = $fd->preSetData($this, $addDiscountforEU);
	return $this;
}

/**
* Get EndUserPrice - End User Price
* @return float|null
*/
public function getEndUserPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("EndUserPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->EndUserPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set EndUserPrice - End User Price
* @param float|null $EndUserPrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setEndUserPrice ($EndUserPrice) {
	$fd = $this->getClass()->getFieldDefinition("EndUserPrice");
	$this->EndUserPrice = $fd->preSetData($this, $EndUserPrice);
	return $this;
}

/**
* Get DiscountforDealer - Discount for Dealer
* @return float|null
*/
public function getDiscountforDealer () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforDealer"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforDealer;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforDealer - Discount for Dealer
* @param float|null $DiscountforDealer
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDiscountforDealer ($DiscountforDealer) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforDealer");
	$this->DiscountforDealer = $fd->preSetData($this, $DiscountforDealer);
	return $this;
}

/**
* Get addDiscountforDealer - Additional Discount For Dealer
* @return float|null
*/
public function getAddDiscountforDealer () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforDealer"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforDealer;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforDealer - Additional Discount For Dealer
* @param float|null $addDiscountforDealer
* @return \Pimcore\Model\DataObject\Parts
*/
public function setAddDiscountforDealer ($addDiscountforDealer) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforDealer");
	$this->addDiscountforDealer = $fd->preSetData($this, $addDiscountforDealer);
	return $this;
}

/**
* Get DealerPrice - Dealer Price
* @return float|null
*/
public function getDealerPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DealerPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DealerPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DealerPrice - Dealer Price
* @param float|null $DealerPrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDealerPrice ($DealerPrice) {
	$fd = $this->getClass()->getFieldDefinition("DealerPrice");
	$this->DealerPrice = $fd->preSetData($this, $DealerPrice);
	return $this;
}

/**
* Get DiscountforLoose - Discount for Loose 
* @return float|null
*/
public function getDiscountforLoose () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforLoose"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforLoose;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforLoose - Discount for Loose 
* @param float|null $DiscountforLoose
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDiscountforLoose ($DiscountforLoose) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforLoose");
	$this->DiscountforLoose = $fd->preSetData($this, $DiscountforLoose);
	return $this;
}

/**
* Get addDiscountforLoose - Additional Discount For Loose Price
* @return float|null
*/
public function getAddDiscountforLoose () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforLoose"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforLoose;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforLoose - Additional Discount For Loose Price
* @param float|null $addDiscountforLoose
* @return \Pimcore\Model\DataObject\Parts
*/
public function setAddDiscountforLoose ($addDiscountforLoose) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforLoose");
	$this->addDiscountforLoose = $fd->preSetData($this, $addDiscountforLoose);
	return $this;
}

/**
* Get LoosePrice - Loose Price
* @return float|null
*/
public function getLoosePrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("LoosePrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->LoosePrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set LoosePrice - Loose Price
* @param float|null $LoosePrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setLoosePrice ($LoosePrice) {
	$fd = $this->getClass()->getFieldDefinition("LoosePrice");
	$this->LoosePrice = $fd->preSetData($this, $LoosePrice);
	return $this;
}

/**
* Get DiscountforBulk - Discount for Bulk 
* @return float|null
*/
public function getDiscountforBulk () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforBulk"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforBulk;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforBulk - Discount for Bulk 
* @param float|null $DiscountforBulk
* @return \Pimcore\Model\DataObject\Parts
*/
public function setDiscountforBulk ($DiscountforBulk) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforBulk");
	$this->DiscountforBulk = $fd->preSetData($this, $DiscountforBulk);
	return $this;
}

/**
* Get addDiscountforBulk - Additional Discount For Bulk Price
* @return float|null
*/
public function getAddDiscountforBulk () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforBulk"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforBulk;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforBulk - Additional Discount For Bulk Price
* @param float|null $addDiscountforBulk
* @return \Pimcore\Model\DataObject\Parts
*/
public function setAddDiscountforBulk ($addDiscountforBulk) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforBulk");
	$this->addDiscountforBulk = $fd->preSetData($this, $addDiscountforBulk);
	return $this;
}

/**
* Get BulkPrice - Bulk Price
* @return float|null
*/
public function getBulkPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("BulkPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->BulkPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set BulkPrice - Bulk Price
* @param float|null $BulkPrice
* @return \Pimcore\Model\DataObject\Parts
*/
public function setBulkPrice ($BulkPrice) {
	$fd = $this->getClass()->getFieldDefinition("BulkPrice");
	$this->BulkPrice = $fd->preSetData($this, $BulkPrice);
	return $this;
}

/**
* Get TechnicalSpecification - TechnicalSpecification
* @return \Pimcore\Model\DataObject\Classificationstore
*/
public function getTechnicalSpecification () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("TechnicalSpecification"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("TechnicalSpecification")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set TechnicalSpecification - TechnicalSpecification
* @param \Pimcore\Model\DataObject\Classificationstore $TechnicalSpecification
* @return \Pimcore\Model\DataObject\Parts
*/
public function setTechnicalSpecification ($TechnicalSpecification) {
	$fd = $this->getClass()->getFieldDefinition("TechnicalSpecification");
	$this->TechnicalSpecification = $TechnicalSpecification;
	return $this;
}

}

