<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- Brand [input]
- Description [wysiwyg]
- BrandLogo [image]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Brand\Listing|\Pimcore\Model\DataObject\Brand getByBrand ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Brand\Listing|\Pimcore\Model\DataObject\Brand getByDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Brand\Listing|\Pimcore\Model\DataObject\Brand getByBrandLogo ($value, $limit = 0, $offset = 0) 
*/

class Brand extends Concrete {

protected $o_classId = "tbrand";
protected $o_className = "Brand";
protected $Brand;
protected $Description;
protected $BrandLogo;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Brand
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Brand - Brand 
* @return string|null
*/
public function getBrand () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Brand"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Brand;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Brand - Brand 
* @param string|null $Brand
* @return \Pimcore\Model\DataObject\Brand
*/
public function setBrand ($Brand) {
	$fd = $this->getClass()->getFieldDefinition("Brand");
	$this->Brand = $Brand;
	return $this;
}

/**
* Get Description - Description
* @return string|null
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string|null $Description
* @return \Pimcore\Model\DataObject\Brand
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get BrandLogo - Brand  Logo
* @return \Pimcore\Model\Asset\Image|null
*/
public function getBrandLogo () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("BrandLogo"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->BrandLogo;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set BrandLogo - Brand  Logo
* @param \Pimcore\Model\Asset\Image|null $BrandLogo
* @return \Pimcore\Model\DataObject\Brand
*/
public function setBrandLogo ($BrandLogo) {
	$fd = $this->getClass()->getFieldDefinition("BrandLogo");
	$this->BrandLogo = $BrandLogo;
	return $this;
}

}

