<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- SKU [input]
- ProductName [input]
- Model [input]
- Category [select]
- BrandName [select]
- MachineType [select]
- Country [country]
- ShortDescription [textarea]
- Color [rgbaColor]
- Color2 [rgbaColor]
- Color3 [rgbaColor]
- Color4 [rgbaColor]
- Color5 [rgbaColor]
- Height [numeric]
- GrossWeight [numeric]
- NetWeight [numeric]
- PackingDimensionHeight [numeric]
- PackingDimensionLength [numeric]
- PackingDimensionWidth [numeric]
- Description [wysiwyg]
- Image [image]
- Gallery [imageGallery]
- Video [block]
-- Video [video]
- currency [select]
- SST [checkbox]
- PurchasePrice [numeric]
- Cost [numeric]
- ListPrice [numeric]
- profitMargin [numeric]
- DiscountforEU [input]
- addDiscountforEU [input]
- EndUserPrice [input]
- DiscountforDealer [input]
- addDiscountforDealer [input]
- DealerPrice [input]
- DiscountforLoose [input]
- addDiscountforLoose [input]
- LoosePrice [input]
- DiscountforBulk [input]
- addDiscountforBulk [input]
- BulkPrice [input]
- relatedParts [manyToManyObjectRelation]
- TechnicalSpecification [classificationstore]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySKU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByProductName ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByModel ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByCategory ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByBrandName ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByMachineType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByCountry ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByShortDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByHeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByGrossWeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByNetWeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPackingDimensionHeight ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPackingDimensionLength ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPackingDimensionWidth ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByImage ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByCurrency ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getBySST ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByPurchasePrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByCost ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByListPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByProfitMargin ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDiscountforEU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByAddDiscountforEU ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByEndUserPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDiscountforDealer ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByAddDiscountforDealer ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDealerPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDiscountforLoose ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByAddDiscountforLoose ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByLoosePrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByDiscountforBulk ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByAddDiscountforBulk ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByBulkPrice ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing|\Pimcore\Model\DataObject\Product getByRelatedParts ($value, $limit = 0, $offset = 0) 
*/

class Product extends Concrete {

protected $o_classId = "tProduct";
protected $o_className = "Product";
protected $SKU;
protected $ProductName;
protected $Model;
protected $Category;
protected $BrandName;
protected $MachineType;
protected $Country;
protected $ShortDescription;
protected $Color;
protected $Color2;
protected $Color3;
protected $Color4;
protected $Color5;
protected $Height;
protected $GrossWeight;
protected $NetWeight;
protected $PackingDimensionHeight;
protected $PackingDimensionLength;
protected $PackingDimensionWidth;
protected $Description;
protected $Image;
protected $Gallery;
protected $Video;
protected $currency;
protected $SST;
protected $PurchasePrice;
protected $Cost;
protected $ListPrice;
protected $profitMargin;
protected $DiscountforEU;
protected $addDiscountforEU;
protected $EndUserPrice;
protected $DiscountforDealer;
protected $addDiscountforDealer;
protected $DealerPrice;
protected $DiscountforLoose;
protected $addDiscountforLoose;
protected $LoosePrice;
protected $DiscountforBulk;
protected $addDiscountforBulk;
protected $BulkPrice;
protected $relatedParts;
protected $TechnicalSpecification;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Product
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get SKU - SKU
* @return string|null
*/
public function getSKU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("SKU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->SKU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set SKU - SKU
* @param string|null $SKU
* @return \Pimcore\Model\DataObject\Product
*/
public function setSKU ($SKU) {
	$fd = $this->getClass()->getFieldDefinition("SKU");
	$this->SKU = $SKU;
	return $this;
}

/**
* Get ProductName - Product  Name
* @return string|null
*/
public function getProductName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ProductName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ProductName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ProductName - Product  Name
* @param string|null $ProductName
* @return \Pimcore\Model\DataObject\Product
*/
public function setProductName ($ProductName) {
	$fd = $this->getClass()->getFieldDefinition("ProductName");
	$this->ProductName = $ProductName;
	return $this;
}

/**
* Get Model - Model
* @return string|null
*/
public function getModel () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Model"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Model;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Model - Model
* @param string|null $Model
* @return \Pimcore\Model\DataObject\Product
*/
public function setModel ($Model) {
	$fd = $this->getClass()->getFieldDefinition("Model");
	$this->Model = $Model;
	return $this;
}

/**
* Get Category - Category
* @return string|null
*/
public function getCategory () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Category"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Category;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Category - Category
* @param string|null $Category
* @return \Pimcore\Model\DataObject\Product
*/
public function setCategory ($Category) {
	$fd = $this->getClass()->getFieldDefinition("Category");
	$this->Category = $Category;
	return $this;
}

/**
* Get BrandName - Brand Name
* @return string|null
*/
public function getBrandName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("BrandName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->BrandName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set BrandName - Brand Name
* @param string|null $BrandName
* @return \Pimcore\Model\DataObject\Product
*/
public function setBrandName ($BrandName) {
	$fd = $this->getClass()->getFieldDefinition("BrandName");
	$this->BrandName = $BrandName;
	return $this;
}

/**
* Get MachineType - Machine Type
* @return string|null
*/
public function getMachineType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("MachineType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->MachineType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set MachineType - Machine Type
* @param string|null $MachineType
* @return \Pimcore\Model\DataObject\Product
*/
public function setMachineType ($MachineType) {
	$fd = $this->getClass()->getFieldDefinition("MachineType");
	$this->MachineType = $MachineType;
	return $this;
}

/**
* Get Country - Country
* @return string|null
*/
public function getCountry () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Country"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Country;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Country - Country
* @param string|null $Country
* @return \Pimcore\Model\DataObject\Product
*/
public function setCountry ($Country) {
	$fd = $this->getClass()->getFieldDefinition("Country");
	$this->Country = $Country;
	return $this;
}

/**
* Get ShortDescription - Short Description
* @return string|null
*/
public function getShortDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ShortDescription"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ShortDescription;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ShortDescription - Short Description
* @param string|null $ShortDescription
* @return \Pimcore\Model\DataObject\Product
*/
public function setShortDescription ($ShortDescription) {
	$fd = $this->getClass()->getFieldDefinition("ShortDescription");
	$this->ShortDescription = $ShortDescription;
	return $this;
}

/**
* Get Color - Color 1
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color - Color 1
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color
* @return \Pimcore\Model\DataObject\Product
*/
public function setColor ($Color) {
	$fd = $this->getClass()->getFieldDefinition("Color");
	$this->Color = $Color;
	return $this;
}

/**
* Get Color2 - Color 2
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor2 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color2"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color2;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color2 - Color 2
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color2
* @return \Pimcore\Model\DataObject\Product
*/
public function setColor2 ($Color2) {
	$fd = $this->getClass()->getFieldDefinition("Color2");
	$this->Color2 = $Color2;
	return $this;
}

/**
* Get Color3 - Color 3
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor3 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color3"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color3;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color3 - Color 3
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color3
* @return \Pimcore\Model\DataObject\Product
*/
public function setColor3 ($Color3) {
	$fd = $this->getClass()->getFieldDefinition("Color3");
	$this->Color3 = $Color3;
	return $this;
}

/**
* Get Color4 - Color 4
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor4 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color4"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color4;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color4 - Color 4
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color4
* @return \Pimcore\Model\DataObject\Product
*/
public function setColor4 ($Color4) {
	$fd = $this->getClass()->getFieldDefinition("Color4");
	$this->Color4 = $Color4;
	return $this;
}

/**
* Get Color5 - Color 5
* @return \Pimcore\Model\DataObject\Data\RgbaColor|null
*/
public function getColor5 () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Color5"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Color5;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Color5 - Color 5
* @param \Pimcore\Model\DataObject\Data\RgbaColor|null $Color5
* @return \Pimcore\Model\DataObject\Product
*/
public function setColor5 ($Color5) {
	$fd = $this->getClass()->getFieldDefinition("Color5");
	$this->Color5 = $Color5;
	return $this;
}

/**
* Get Height - Height (cm)
* @return float|null
*/
public function getHeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Height"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Height;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Height - Height (cm)
* @param float|null $Height
* @return \Pimcore\Model\DataObject\Product
*/
public function setHeight ($Height) {
	$fd = $this->getClass()->getFieldDefinition("Height");
	$this->Height = $fd->preSetData($this, $Height);
	return $this;
}

/**
* Get GrossWeight - Gross Weight (kg)
* @return float|null
*/
public function getGrossWeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("GrossWeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->GrossWeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set GrossWeight - Gross Weight (kg)
* @param float|null $GrossWeight
* @return \Pimcore\Model\DataObject\Product
*/
public function setGrossWeight ($GrossWeight) {
	$fd = $this->getClass()->getFieldDefinition("GrossWeight");
	$this->GrossWeight = $fd->preSetData($this, $GrossWeight);
	return $this;
}

/**
* Get NetWeight - Net Weight (kg)
* @return float|null
*/
public function getNetWeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("NetWeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->NetWeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set NetWeight - Net Weight (kg)
* @param float|null $NetWeight
* @return \Pimcore\Model\DataObject\Product
*/
public function setNetWeight ($NetWeight) {
	$fd = $this->getClass()->getFieldDefinition("NetWeight");
	$this->NetWeight = $fd->preSetData($this, $NetWeight);
	return $this;
}

/**
* Get PackingDimensionHeight - Packing Dimension Height (mm) 
* @return float|null
*/
public function getPackingDimensionHeight () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionHeight"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionHeight;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionHeight - Packing Dimension Height (mm) 
* @param float|null $PackingDimensionHeight
* @return \Pimcore\Model\DataObject\Product
*/
public function setPackingDimensionHeight ($PackingDimensionHeight) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionHeight");
	$this->PackingDimensionHeight = $fd->preSetData($this, $PackingDimensionHeight);
	return $this;
}

/**
* Get PackingDimensionLength - Packing Dimension Length (mm) 
* @return float|null
*/
public function getPackingDimensionLength () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionLength"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionLength;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionLength - Packing Dimension Length (mm) 
* @param float|null $PackingDimensionLength
* @return \Pimcore\Model\DataObject\Product
*/
public function setPackingDimensionLength ($PackingDimensionLength) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionLength");
	$this->PackingDimensionLength = $fd->preSetData($this, $PackingDimensionLength);
	return $this;
}

/**
* Get PackingDimensionWidth - Packing Dimension Width (mm) 
* @return float|null
*/
public function getPackingDimensionWidth () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PackingDimensionWidth"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PackingDimensionWidth;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PackingDimensionWidth - Packing Dimension Width (mm) 
* @param float|null $PackingDimensionWidth
* @return \Pimcore\Model\DataObject\Product
*/
public function setPackingDimensionWidth ($PackingDimensionWidth) {
	$fd = $this->getClass()->getFieldDefinition("PackingDimensionWidth");
	$this->PackingDimensionWidth = $fd->preSetData($this, $PackingDimensionWidth);
	return $this;
}

/**
* Get Description - Description
* @return string|null
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string|null $Description
* @return \Pimcore\Model\DataObject\Product
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Image - Main Image
* @return \Pimcore\Model\Asset\Image|null
*/
public function getImage () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image - Main Image
* @param \Pimcore\Model\Asset\Image|null $Image
* @return \Pimcore\Model\DataObject\Product
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

/**
* Get Gallery - Image Gallery
* @return \Pimcore\Model\DataObject\Data\ImageGallery
*/
public function getGallery () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Gallery"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Gallery;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Gallery - Image Gallery
* @param \Pimcore\Model\DataObject\Data\ImageGallery $Gallery
* @return \Pimcore\Model\DataObject\Product
*/
public function setGallery ($Gallery) {
	$fd = $this->getClass()->getFieldDefinition("Gallery");
	$this->Gallery = $Gallery;
	return $this;
}

/**
* Get Video - Video Gallery
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getVideo () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Video"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Video")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Video - Video Gallery
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $Video
* @return \Pimcore\Model\DataObject\Product
*/
public function setVideo ($Video) {
	$fd = $this->getClass()->getFieldDefinition("Video");
	$this->Video = $fd->preSetData($this, $Video);
	return $this;
}

/**
* Get currency - Currency
* @return string|null
*/
public function getCurrency () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("currency"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->currency;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set currency - Currency
* @param string|null $currency
* @return \Pimcore\Model\DataObject\Product
*/
public function setCurrency ($currency) {
	$fd = $this->getClass()->getFieldDefinition("currency");
	$this->currency = $currency;
	return $this;
}

/**
* Get SST - SST
* @return bool|null
*/
public function getSST () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("SST"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->SST;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set SST - SST
* @param bool|null $SST
* @return \Pimcore\Model\DataObject\Product
*/
public function setSST ($SST) {
	$fd = $this->getClass()->getFieldDefinition("SST");
	$this->SST = $SST;
	return $this;
}

/**
* Get PurchasePrice - Purchase Price
* @return float|null
*/
public function getPurchasePrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("PurchasePrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->PurchasePrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set PurchasePrice - Purchase Price
* @param float|null $PurchasePrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setPurchasePrice ($PurchasePrice) {
	$fd = $this->getClass()->getFieldDefinition("PurchasePrice");
	$this->PurchasePrice = $fd->preSetData($this, $PurchasePrice);
	return $this;
}

/**
* Get Cost - Cost
* @return float|null
*/
public function getCost () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Cost"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Cost;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Cost - Cost
* @param float|null $Cost
* @return \Pimcore\Model\DataObject\Product
*/
public function setCost ($Cost) {
	$fd = $this->getClass()->getFieldDefinition("Cost");
	$this->Cost = $fd->preSetData($this, $Cost);
	return $this;
}

/**
* Get ListPrice - List Price
* @return float|null
*/
public function getListPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("ListPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->ListPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set ListPrice - List Price
* @param float|null $ListPrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setListPrice ($ListPrice) {
	$fd = $this->getClass()->getFieldDefinition("ListPrice");
	$this->ListPrice = $fd->preSetData($this, $ListPrice);
	return $this;
}

/**
* Get profitMargin - Profit Margin
* @return float|null
*/
public function getProfitMargin () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("profitMargin"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->profitMargin;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set profitMargin - Profit Margin
* @param float|null $profitMargin
* @return \Pimcore\Model\DataObject\Product
*/
public function setProfitMargin ($profitMargin) {
	$fd = $this->getClass()->getFieldDefinition("profitMargin");
	$this->profitMargin = $fd->preSetData($this, $profitMargin);
	return $this;
}

/**
* Get DiscountforEU - Discount for EU 
* @return string|null
*/
public function getDiscountforEU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforEU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforEU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforEU - Discount for EU 
* @param string|null $DiscountforEU
* @return \Pimcore\Model\DataObject\Product
*/
public function setDiscountforEU ($DiscountforEU) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforEU");
	$this->DiscountforEU = $DiscountforEU;
	return $this;
}

/**
* Get addDiscountforEU - Additional  Discount For Eu
* @return string|null
*/
public function getAddDiscountforEU () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforEU"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforEU;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforEU - Additional  Discount For Eu
* @param string|null $addDiscountforEU
* @return \Pimcore\Model\DataObject\Product
*/
public function setAddDiscountforEU ($addDiscountforEU) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforEU");
	$this->addDiscountforEU = $addDiscountforEU;
	return $this;
}

/**
* Get EndUserPrice - End User Price
* @return string|null
*/
public function getEndUserPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("EndUserPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->EndUserPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set EndUserPrice - End User Price
* @param string|null $EndUserPrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setEndUserPrice ($EndUserPrice) {
	$fd = $this->getClass()->getFieldDefinition("EndUserPrice");
	$this->EndUserPrice = $EndUserPrice;
	return $this;
}

/**
* Get DiscountforDealer - Discount for Dealer
* @return string|null
*/
public function getDiscountforDealer () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforDealer"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforDealer;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforDealer - Discount for Dealer
* @param string|null $DiscountforDealer
* @return \Pimcore\Model\DataObject\Product
*/
public function setDiscountforDealer ($DiscountforDealer) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforDealer");
	$this->DiscountforDealer = $DiscountforDealer;
	return $this;
}

/**
* Get addDiscountforDealer - Additional Discount For Dealer
* @return string|null
*/
public function getAddDiscountforDealer () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforDealer"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforDealer;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforDealer - Additional Discount For Dealer
* @param string|null $addDiscountforDealer
* @return \Pimcore\Model\DataObject\Product
*/
public function setAddDiscountforDealer ($addDiscountforDealer) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforDealer");
	$this->addDiscountforDealer = $addDiscountforDealer;
	return $this;
}

/**
* Get DealerPrice - Dealer Price
* @return string|null
*/
public function getDealerPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DealerPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DealerPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DealerPrice - Dealer Price
* @param string|null $DealerPrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setDealerPrice ($DealerPrice) {
	$fd = $this->getClass()->getFieldDefinition("DealerPrice");
	$this->DealerPrice = $DealerPrice;
	return $this;
}

/**
* Get DiscountforLoose - Discount for Loose 
* @return string|null
*/
public function getDiscountforLoose () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforLoose"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforLoose;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforLoose - Discount for Loose 
* @param string|null $DiscountforLoose
* @return \Pimcore\Model\DataObject\Product
*/
public function setDiscountforLoose ($DiscountforLoose) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforLoose");
	$this->DiscountforLoose = $DiscountforLoose;
	return $this;
}

/**
* Get addDiscountforLoose - Additional Discount For Loose Price
* @return string|null
*/
public function getAddDiscountforLoose () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforLoose"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforLoose;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforLoose - Additional Discount For Loose Price
* @param string|null $addDiscountforLoose
* @return \Pimcore\Model\DataObject\Product
*/
public function setAddDiscountforLoose ($addDiscountforLoose) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforLoose");
	$this->addDiscountforLoose = $addDiscountforLoose;
	return $this;
}

/**
* Get LoosePrice - Loose Price
* @return string|null
*/
public function getLoosePrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("LoosePrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->LoosePrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set LoosePrice - Loose Price
* @param string|null $LoosePrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setLoosePrice ($LoosePrice) {
	$fd = $this->getClass()->getFieldDefinition("LoosePrice");
	$this->LoosePrice = $LoosePrice;
	return $this;
}

/**
* Get DiscountforBulk - Discount for Bulk 
* @return string|null
*/
public function getDiscountforBulk () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("DiscountforBulk"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->DiscountforBulk;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set DiscountforBulk - Discount for Bulk 
* @param string|null $DiscountforBulk
* @return \Pimcore\Model\DataObject\Product
*/
public function setDiscountforBulk ($DiscountforBulk) {
	$fd = $this->getClass()->getFieldDefinition("DiscountforBulk");
	$this->DiscountforBulk = $DiscountforBulk;
	return $this;
}

/**
* Get addDiscountforBulk - Additional Discount For Bulk Price
* @return string|null
*/
public function getAddDiscountforBulk () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("addDiscountforBulk"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->addDiscountforBulk;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set addDiscountforBulk - Additional Discount For Bulk Price
* @param string|null $addDiscountforBulk
* @return \Pimcore\Model\DataObject\Product
*/
public function setAddDiscountforBulk ($addDiscountforBulk) {
	$fd = $this->getClass()->getFieldDefinition("addDiscountforBulk");
	$this->addDiscountforBulk = $addDiscountforBulk;
	return $this;
}

/**
* Get BulkPrice - Bulk Price
* @return string|null
*/
public function getBulkPrice () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("BulkPrice"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->BulkPrice;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set BulkPrice - Bulk Price
* @param string|null $BulkPrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setBulkPrice ($BulkPrice) {
	$fd = $this->getClass()->getFieldDefinition("BulkPrice");
	$this->BulkPrice = $BulkPrice;
	return $this;
}

/**
* Get relatedParts - relatedParts
* @return \Pimcore\Model\DataObject\Parts[]
*/
public function getRelatedParts () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("relatedParts"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("relatedParts")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set relatedParts - relatedParts
* @param \Pimcore\Model\DataObject\Parts[] $relatedParts
* @return \Pimcore\Model\DataObject\Product
*/
public function setRelatedParts ($relatedParts) {
	$fd = $this->getClass()->getFieldDefinition("relatedParts");
	$hideUnpublished = \Pimcore\Model\DataObject\Concrete::getHideUnpublished();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished(false);
	$currentData = $this->getRelatedParts();
	\Pimcore\Model\DataObject\Concrete::setHideUnpublished($hideUnpublished);
	$isEqual = $fd->isEqual($currentData, $relatedParts);
	if (!$isEqual) {
		$this->markFieldDirty("relatedParts", true);
	}
	$this->relatedParts = $fd->preSetData($this, $relatedParts);
	return $this;
}

/**
* Get TechnicalSpecification - Technical Specification
* @return \Pimcore\Model\DataObject\Classificationstore
*/
public function getTechnicalSpecification () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("TechnicalSpecification"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("TechnicalSpecification")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set TechnicalSpecification - Technical Specification
* @param \Pimcore\Model\DataObject\Classificationstore $TechnicalSpecification
* @return \Pimcore\Model\DataObject\Product
*/
public function setTechnicalSpecification ($TechnicalSpecification) {
	$fd = $this->getClass()->getFieldDefinition("TechnicalSpecification");
	$this->TechnicalSpecification = $TechnicalSpecification;
	return $this;
}

}

