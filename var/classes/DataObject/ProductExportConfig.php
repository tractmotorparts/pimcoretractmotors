<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- exportFor [select]
- baseAttribute [multiselect]
- mediaAttribute [multiselect]
- salesAttribute [multiselect]
- purchaseAttribute [multiselect]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\ProductExportConfig\Listing|\Pimcore\Model\DataObject\ProductExportConfig getByExportFor ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ProductExportConfig\Listing|\Pimcore\Model\DataObject\ProductExportConfig getByBaseAttribute ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ProductExportConfig\Listing|\Pimcore\Model\DataObject\ProductExportConfig getByMediaAttribute ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ProductExportConfig\Listing|\Pimcore\Model\DataObject\ProductExportConfig getBySalesAttribute ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\ProductExportConfig\Listing|\Pimcore\Model\DataObject\ProductExportConfig getByPurchaseAttribute ($value, $limit = 0, $offset = 0) 
*/

class ProductExportConfig extends Concrete {

protected $o_classId = "prc";
protected $o_className = "ProductExportConfig";
protected $exportFor;
protected $baseAttribute;
protected $mediaAttribute;
protected $salesAttribute;
protected $purchaseAttribute;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get exportFor - Export For he Class
* @return string|null
*/
public function getExportFor () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("exportFor"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->exportFor;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set exportFor - Export For he Class
* @param string|null $exportFor
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public function setExportFor ($exportFor) {
	$fd = $this->getClass()->getFieldDefinition("exportFor");
	$this->exportFor = $exportFor;
	return $this;
}

/**
* Get baseAttribute - Base Attribute
* @return array|null
*/
public function getBaseAttribute () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("baseAttribute"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->baseAttribute;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set baseAttribute - Base Attribute
* @param array|null $baseAttribute
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public function setBaseAttribute ($baseAttribute) {
	$fd = $this->getClass()->getFieldDefinition("baseAttribute");
	$this->baseAttribute = $baseAttribute;
	return $this;
}

/**
* Get mediaAttribute - Media Attribute
* @return array|null
*/
public function getMediaAttribute () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("mediaAttribute"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->mediaAttribute;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set mediaAttribute - Media Attribute
* @param array|null $mediaAttribute
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public function setMediaAttribute ($mediaAttribute) {
	$fd = $this->getClass()->getFieldDefinition("mediaAttribute");
	$this->mediaAttribute = $mediaAttribute;
	return $this;
}

/**
* Get salesAttribute - Sales Attribute
* @return array|null
*/
public function getSalesAttribute () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("salesAttribute"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->salesAttribute;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set salesAttribute - Sales Attribute
* @param array|null $salesAttribute
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public function setSalesAttribute ($salesAttribute) {
	$fd = $this->getClass()->getFieldDefinition("salesAttribute");
	$this->salesAttribute = $salesAttribute;
	return $this;
}

/**
* Get purchaseAttribute - Purchase Attribute
* @return array|null
*/
public function getPurchaseAttribute () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("purchaseAttribute"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->purchaseAttribute;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set purchaseAttribute - Purchase Attribute
* @param array|null $purchaseAttribute
* @return \Pimcore\Model\DataObject\ProductExportConfig
*/
public function setPurchaseAttribute ($purchaseAttribute) {
	$fd = $this->getClass()->getFieldDefinition("purchaseAttribute");
	$this->purchaseAttribute = $purchaseAttribute;
	return $this;
}

}

