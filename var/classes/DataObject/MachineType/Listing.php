<?php 

namespace Pimcore\Model\DataObject\MachineType;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\MachineType current()
 * @method DataObject\MachineType[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "machineType";
protected $className = "MachineType";


/**
* Filter by MachineType (Machine Type)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByMachineType ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("MachineType")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Description (Description)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescription ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Description")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Image (Image)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByImage ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Image")->addListingFilter($this, $data, $operator);
	return $this;
}



}
