<?php 

namespace Pimcore\Model\DataObject\Category;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Category current()
 * @method DataObject\Category[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "tcategory";
protected $className = "Category";


/**
* Filter by CategoryName (Category Name)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByCategoryName ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("CategoryName")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Description (Description)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByDescription ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Description")->addListingFilter($this, $data, $operator);
	return $this;
}

/**
* Filter by Image (Image)
* @param string|int|float|float|array $data  comparison data, can be scalar or array (if operator is e.g. "IN (?)")
* @param string $operator  SQL comparison operator, e.g. =, <, >= etc. You can use "?" as placeholder, e.g. "IN (?)"
* @return static
*/
public function filterByImage ($data, $operator = '=') {
	$this->getClass()->getFieldDefinition("Image")->addListingFilter($this, $data, $operator);
	return $this;
}



}
