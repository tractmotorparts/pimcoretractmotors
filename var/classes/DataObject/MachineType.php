<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- MachineType [input]
- Description [wysiwyg]
- Image [image]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\MachineType\Listing|\Pimcore\Model\DataObject\MachineType getByMachineType ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\MachineType\Listing|\Pimcore\Model\DataObject\MachineType getByDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\MachineType\Listing|\Pimcore\Model\DataObject\MachineType getByImage ($value, $limit = 0, $offset = 0) 
*/

class MachineType extends Concrete {

protected $o_classId = "machineType";
protected $o_className = "MachineType";
protected $MachineType;
protected $Description;
protected $Image;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\MachineType
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get MachineType - Machine Type
* @return string|null
*/
public function getMachineType () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("MachineType"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->MachineType;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set MachineType - Machine Type
* @param string|null $MachineType
* @return \Pimcore\Model\DataObject\MachineType
*/
public function setMachineType ($MachineType) {
	$fd = $this->getClass()->getFieldDefinition("MachineType");
	$this->MachineType = $MachineType;
	return $this;
}

/**
* Get Description - Description
* @return string|null
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string|null $Description
* @return \Pimcore\Model\DataObject\MachineType
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Image - Image
* @return \Pimcore\Model\Asset\Image|null
*/
public function getImage () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image - Image
* @param \Pimcore\Model\Asset\Image|null $Image
* @return \Pimcore\Model\DataObject\MachineType
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

}

