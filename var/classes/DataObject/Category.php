<?php 

/** 
* Inheritance: no
* Variants: no


Fields Summary: 
- CategoryName [input]
- Description [wysiwyg]
- Image [image]
*/ 

namespace Pimcore\Model\DataObject;

use Pimcore\Model\DataObject\Exception\InheritanceParentNotFoundException;
use Pimcore\Model\DataObject\PreGetValueHookInterface;

/**
* @method static \Pimcore\Model\DataObject\Category\Listing|\Pimcore\Model\DataObject\Category getByCategoryName ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Category\Listing|\Pimcore\Model\DataObject\Category getByDescription ($value, $limit = 0, $offset = 0) 
* @method static \Pimcore\Model\DataObject\Category\Listing|\Pimcore\Model\DataObject\Category getByImage ($value, $limit = 0, $offset = 0) 
*/

class Category extends Concrete {

protected $o_classId = "tcategory";
protected $o_className = "Category";
protected $CategoryName;
protected $Description;
protected $Image;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Category
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get CategoryName - Category Name
* @return string|null
*/
public function getCategoryName () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("CategoryName"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->CategoryName;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set CategoryName - Category Name
* @param string|null $CategoryName
* @return \Pimcore\Model\DataObject\Category
*/
public function setCategoryName ($CategoryName) {
	$fd = $this->getClass()->getFieldDefinition("CategoryName");
	$this->CategoryName = $CategoryName;
	return $this;
}

/**
* Get Description - Description
* @return string|null
*/
public function getDescription () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Description"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Description - Description
* @param string|null $Description
* @return \Pimcore\Model\DataObject\Category
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Image - Image
* @return \Pimcore\Model\Asset\Image|null
*/
public function getImage () {
	if($this instanceof PreGetValueHookInterface && !\Pimcore::inAdmin()) { 
		$preValue = $this->preGetValue("Image"); 
		if($preValue !== null) { 
			return $preValue;
		}
	} 

	$data = $this->Image;

	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}

	return $data;
}

/**
* Set Image - Image
* @param \Pimcore\Model\Asset\Image|null $Image
* @return \Pimcore\Model\DataObject\Category
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

}

