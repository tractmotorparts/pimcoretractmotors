<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Pimcore\Bundle\AdminBundle\Session\Handler\AdminSessionHandler' shared autowired service.

include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/bundles/AdminBundle/Session/Handler/AdminSessionHandlerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/bundles/AdminBundle/Session/Handler/AdminSessionHandler.php';

$this->services['Pimcore\\Bundle\\AdminBundle\\Session\\Handler\\AdminSessionHandler'] = $instance = new \Pimcore\Bundle\AdminBundle\Session\Handler\AdminSessionHandler(($this->services['session'] ?? $this->load('getSessionService.php')));

$a = ($this->services['monolog.logger.pimcore_admin.session'] ?? $this->load('getMonolog_Logger_PimcoreAdmin_SessionService.php'));

$instance->setLogger($a);
$instance->setLogger($a);

return $instance;
