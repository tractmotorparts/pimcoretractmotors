<?php
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Targeting/Debug/OverrideHandler.php';

class OverrideHandler_fb58919 extends \Pimcore\Targeting\Debug\OverrideHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Pimcore\Targeting\Debug\OverrideHandler|null wrapped object, if the proxy is initialized
     */
    private $valueHolderd73c0 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer7b19b = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties0cf19 = [
        
    ];

    public function getForm(\Symfony\Component\HttpFoundation\Request $request) : \Symfony\Component\Form\FormInterface
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getForm', array('request' => $request), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        return $this->valueHolderd73c0->getForm($request);
    }

    public function handleRequest(\Symfony\Component\HttpFoundation\Request $request)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'handleRequest', array('request' => $request), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        return $this->valueHolderd73c0->handleRequest($request);
    }

    public function handleForm(\Symfony\Component\Form\FormInterface $form, \Symfony\Component\HttpFoundation\Request $request)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'handleForm', array('form' => $form, 'request' => $request), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        return $this->valueHolderd73c0->handleForm($form, $request);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Pimcore\Targeting\Debug\OverrideHandler $instance) {
            unset($instance->formFactory, $instance->overrideHandlers);
        }, $instance, 'Pimcore\\Targeting\\Debug\\OverrideHandler')->__invoke($instance);

        $instance->initializer7b19b = $initializer;

        return $instance;
    }

    public function __construct(\Symfony\Component\Form\FormFactoryInterface $formFactory, $overrideHandlers)
    {
        static $reflection;

        if (! $this->valueHolderd73c0) {
            $reflection = $reflection ?? new \ReflectionClass('Pimcore\\Targeting\\Debug\\OverrideHandler');
            $this->valueHolderd73c0 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Pimcore\Targeting\Debug\OverrideHandler $instance) {
            unset($instance->formFactory, $instance->overrideHandlers);
        }, $this, 'Pimcore\\Targeting\\Debug\\OverrideHandler')->__invoke($this);

        }

        $this->valueHolderd73c0->__construct($formFactory, $overrideHandlers);
    }

    public function & __get($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__get', ['name' => $name], $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        if (isset(self::$publicProperties0cf19[$name])) {
            return $this->valueHolderd73c0->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Pimcore\\Targeting\\Debug\\OverrideHandler');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        $realInstanceReflection = new \ReflectionClass('Pimcore\\Targeting\\Debug\\OverrideHandler');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__isset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        $realInstanceReflection = new \ReflectionClass('Pimcore\\Targeting\\Debug\\OverrideHandler');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__unset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        $realInstanceReflection = new \ReflectionClass('Pimcore\\Targeting\\Debug\\OverrideHandler');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__clone', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        $this->valueHolderd73c0 = clone $this->valueHolderd73c0;
    }

    public function __sleep()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__sleep', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;

        return array('valueHolderd73c0');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Pimcore\Targeting\Debug\OverrideHandler $instance) {
            unset($instance->formFactory, $instance->overrideHandlers);
        }, $this, 'Pimcore\\Targeting\\Debug\\OverrideHandler')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer7b19b = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer7b19b;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'initializeProxy', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderd73c0;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd73c0;
    }
}
