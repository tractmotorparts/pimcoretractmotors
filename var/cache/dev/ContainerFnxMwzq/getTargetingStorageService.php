<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Pimcore\Targeting\DataProvider\TargetingStorage' shared autowired service.

include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Targeting/DataProvider/DataProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Targeting/DataProvider/TargetingStorage.php';

return $this->privates['Pimcore\\Targeting\\DataProvider\\TargetingStorage'] = new \Pimcore\Targeting\DataProvider\TargetingStorage(($this->privates['Pimcore\\Targeting\\Storage\\CookieStorage'] ?? $this->getCookieStorageService()));
