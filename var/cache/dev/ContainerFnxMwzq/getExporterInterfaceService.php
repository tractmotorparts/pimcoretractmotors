<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Pimcore\Translation\ExportService\Exporter\ExporterInterface' shared autowired service.

include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Translation/ExportService/Exporter/ExporterInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Translation/ExportService/Exporter/Xliff12Exporter.php';
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Translation/Escaper/Xliff12Escaper.php';

return $this->privates['Pimcore\\Translation\\ExportService\\Exporter\\ExporterInterface'] = new \Pimcore\Translation\ExportService\Exporter\Xliff12Exporter(($this->privates['Pimcore\\Translation\\Escaper\\Xliff12Escaper'] ?? ($this->privates['Pimcore\\Translation\\Escaper\\Xliff12Escaper'] = new \Pimcore\Translation\Escaper\Xliff12Escaper())));
