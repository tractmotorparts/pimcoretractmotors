<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Misc:admin-css.html.php */
class __TwigTemplate_22a053245a4cad50b62757c900198a5971e9d889153c8057922a298fc4a0c649 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Misc:admin-css.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Misc:admin-css.html.php"));

        // line 1
        echo "/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

 /* THIS FILE IS GENERATED DYNAMICALLY BECAUSE OF DYNAMIC CSS CLASSES IN THE ADMIN */


<?php // custom views ?>

<?php if (is_array(\$this->customviews)) { ?>
    <?php foreach (\$this->customviews as \$cv) { ?>

    <?php if (\$cv[\"icon\"]) {
            \$treetype = \$cv[\"treetype\"] ? \$cv[\"treetype\"] : \"object\";
            ?>
    .pimcore_<?= \$treetype ?>_customview_icon_<?= \$cv[\"id\"]; ?> {
        background: center / contain url(<?= \$cv[\"icon\"]; ?>) no-repeat !important;
    }
    <?php } ?>

    <?php } ?>
<?php } ?>


<?php // language icons ?>

<?php
    \$languages = \\Pimcore\\Tool::getValidLanguages();
    \$adminLanguages = \\Pimcore\\Tool\\Admin::getLanguages();
    \$languages = array_unique(array_merge(\$languages, \$adminLanguages));
?>

<?php foreach (\$languages as \$language) {
        \$iconFile = \\Pimcore\\Tool::getLanguageFlagFile(\$language, false);
    ?>

    /* tab icon for localized fields [ <?= \$language ?> ] */
    .pimcore_icon_language_<?= strtolower(\$language) ?> {
        background: url(<?= \$iconFile ?>) center center/contain no-repeat;
    }

    /* grid column header icon in translations [ <?= \$language ?> ] */
    .x-column-header_<?= strtolower(\$language) ?> {
        background: url(<?= \$iconFile ?>) no-repeat left center/contain !important;
        padding-left:22px !important;
    }

<?php } ?>

<?php // CUSTOM BRANDING ?>
<?php if (\$this->config instanceof Pimcore\\Config && !empty(\$interfaceColor = \$this->config['branding']['color_admin_interface'])) { ?>
        #pimcore_signet {
            background-color: <?= \$interfaceColor ?> !important;
        }

        #pimcore_avatar {
            background-color: <?= \$interfaceColor ?> !important;
        }

        #pimcore_navigation li:hover:after {
            background-color: <?= \$interfaceColor ?> !important;
        }
<?php } ?>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Misc:admin-css.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

 /* THIS FILE IS GENERATED DYNAMICALLY BECAUSE OF DYNAMIC CSS CLASSES IN THE ADMIN */


<?php // custom views ?>

<?php if (is_array(\$this->customviews)) { ?>
    <?php foreach (\$this->customviews as \$cv) { ?>

    <?php if (\$cv[\"icon\"]) {
            \$treetype = \$cv[\"treetype\"] ? \$cv[\"treetype\"] : \"object\";
            ?>
    .pimcore_<?= \$treetype ?>_customview_icon_<?= \$cv[\"id\"]; ?> {
        background: center / contain url(<?= \$cv[\"icon\"]; ?>) no-repeat !important;
    }
    <?php } ?>

    <?php } ?>
<?php } ?>


<?php // language icons ?>

<?php
    \$languages = \\Pimcore\\Tool::getValidLanguages();
    \$adminLanguages = \\Pimcore\\Tool\\Admin::getLanguages();
    \$languages = array_unique(array_merge(\$languages, \$adminLanguages));
?>

<?php foreach (\$languages as \$language) {
        \$iconFile = \\Pimcore\\Tool::getLanguageFlagFile(\$language, false);
    ?>

    /* tab icon for localized fields [ <?= \$language ?> ] */
    .pimcore_icon_language_<?= strtolower(\$language) ?> {
        background: url(<?= \$iconFile ?>) center center/contain no-repeat;
    }

    /* grid column header icon in translations [ <?= \$language ?> ] */
    .x-column-header_<?= strtolower(\$language) ?> {
        background: url(<?= \$iconFile ?>) no-repeat left center/contain !important;
        padding-left:22px !important;
    }

<?php } ?>

<?php // CUSTOM BRANDING ?>
<?php if (\$this->config instanceof Pimcore\\Config && !empty(\$interfaceColor = \$this->config['branding']['color_admin_interface'])) { ?>
        #pimcore_signet {
            background-color: <?= \$interfaceColor ?> !important;
        }

        #pimcore_avatar {
            background-color: <?= \$interfaceColor ?> !important;
        }

        #pimcore_navigation li:hover:after {
            background-color: <?= \$interfaceColor ?> !important;
        }
<?php } ?>
", "PimcoreAdminBundle:Admin/Misc:admin-css.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Misc/admin-css.html.php");
    }
}
