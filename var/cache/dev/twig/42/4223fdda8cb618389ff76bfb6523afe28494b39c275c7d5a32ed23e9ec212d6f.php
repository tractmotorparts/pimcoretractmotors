<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php */
class __TwigTemplate_8506107c633825fd3d0d2a6ca9d3d2387ad352f65ce56b0792cb055c3e155628 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php"));

        // line 1
        echo "<?php
/**
 * @var \\Pimcore\\Templating\\PhpEngine \$this
 * @var \\Pimcore\\Templating\\PhpEngine \$view
 * @var \\Pimcore\\Templating\\GlobalVariables \$app
 */

\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');
\$this->get(\"translate\")->setDomain(\"admin\");
?>



<?php if (\$this->error) { ?>
    <div class=\"text error\">
        <?= \$this->translate(\$this->error) ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your verification code\"); ?>
    </div>
<?php } ?>

<form method=\"post\" action=\"<?=\$this->url('pimcore_admin_2fa-verify')?>\">
    <input name=\"_auth_code\" id=\"_auth_code\" autocomplete=\"one-time-code\" type=\"password\" placeholder=\"<?= \$this->translate(\"2fa_code\"); ?>\" required autofocus>
    <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

    <button type=\"submit\"><?= \$this->translate(\"Login\"); ?></button>
</form>

<a href=\"<?= \$view->router()->path('pimcore_admin_logout') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/**
 * @var \\Pimcore\\Templating\\PhpEngine \$this
 * @var \\Pimcore\\Templating\\PhpEngine \$view
 * @var \\Pimcore\\Templating\\GlobalVariables \$app
 */

\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');
\$this->get(\"translate\")->setDomain(\"admin\");
?>



<?php if (\$this->error) { ?>
    <div class=\"text error\">
        <?= \$this->translate(\$this->error) ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your verification code\"); ?>
    </div>
<?php } ?>

<form method=\"post\" action=\"<?=\$this->url('pimcore_admin_2fa-verify')?>\">
    <input name=\"_auth_code\" id=\"_auth_code\" autocomplete=\"one-time-code\" type=\"password\" placeholder=\"<?= \$this->translate(\"2fa_code\"); ?>\" required autofocus>
    <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

    <button type=\"submit\"><?= \$this->translate(\"Login\"); ?></button>
</form>

<a href=\"<?= \$view->router()->path('pimcore_admin_logout') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


", "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/twoFactorAuthentication.html.php");
    }
}
