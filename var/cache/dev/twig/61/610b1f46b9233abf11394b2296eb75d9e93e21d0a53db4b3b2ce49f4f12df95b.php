<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :layouts:print_catalog.html.twig */
class __TwigTemplate_14045faa6675112ab8acec6ee54d17cfdbf189dabd38b11977eb301dc8f3f15b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head_stylesheets' => [$this, 'block_head_stylesheets'],
            'headscripts' => [$this, 'block_headscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":layouts:print_catalog.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":layouts:print_catalog.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"\">
    <head>
        ";
        // line 4
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 7
        echo "        <link href=\"https://fonts.googleapis.com/css?family=Hind+Guntur:300,400,500,600,700&display=swap\" rel=\"stylesheet\">
        
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"print\">
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"screen\">
        
        ";
        // line 12
        if ((array_key_exists("printermarks", $context) && ((isset($context["printermarks"]) || array_key_exists("printermarks", $context) ? $context["printermarks"] : (function () { throw new RuntimeError('Variable "printermarks" does not exist.', 12, $this->source); })()) == true))) {
            // line 13
            echo "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/pimcoreadmin/css/print/print-printermarks.css\" media=\"print\" />
        ";
        }
        // line 15
        echo "        
        ";
        // line 16
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 16, $this->source); })())) {
            // line 17
            echo "            <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/print/css/print-edit.css\"  media=\"screen\" />
        ";
        }
        // line 19
        echo "        
        <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>
        
        ";
        // line 22
        $this->displayBlock('headscripts', $context, $blocks);
        // line 25
        echo "    </head>
    <body>
        <div class=\"site\">
        ";
        // line 28
        $this->displayBlock("_content", $context, $blocks);
        echo "
        </div>
    </body>
</html>

";
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_head_stylesheets($context, array $blocks = [])
    {
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->defer($this, 'head_stylesheets');
    }

    // line 4
    public function block_head_stylesheets_deferred($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 5
        echo "            ";
        echo call_user_func_array($this->env->getFunction('pimcore_head_link')->getCallable(), []);
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
    }

    public function block_headscripts($context, array $blocks = [])
    {
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->defer($this, 'headscripts');
    }

    // line 22
    public function block_headscripts_deferred($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "headscripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "headscripts"));

        // line 23
        echo "            ";
        echo call_user_func_array($this->env->getFunction('pimcore_head_script')->getCallable(), []);
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
    }

    public function getTemplateName()
    {
        return ":layouts:print_catalog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 23,  136 => 22,  117 => 5,  108 => 4,  86 => 28,  81 => 25,  79 => 22,  74 => 19,  70 => 17,  68 => 16,  65 => 15,  61 => 13,  59 => 12,  52 => 7,  50 => 4,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"\">
    <head>
        {% block head_stylesheets deferred %}
            {{ pimcore_head_link() }}
        {% endblock %}
        <link href=\"https://fonts.googleapis.com/css?family=Hind+Guntur:300,400,500,600,700&display=swap\" rel=\"stylesheet\">
        
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"print\">
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"screen\">
        
        {% if printermarks is defined and printermarks == true %}
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/pimcoreadmin/css/print/print-printermarks.css\" media=\"print\" />
        {% endif %}
        
        {% if editmode %}
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/print/css/print-edit.css\"  media=\"screen\" />
        {% endif %}
        
        <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>
        
        {% block headscripts deferred %}
            {{ pimcore_head_script() }}
        {% endblock %}
    </head>
    <body>
        <div class=\"site\">
        {{ block('_content') }}
        </div>
    </body>
</html>

", ":layouts:print_catalog.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/layouts/print_catalog.html.twig");
    }
}
