<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-toc:view.html.twig */
class __TwigTemplate_3584ceaeaa5567635028e5f8650e2899f79a53b7b2d78012f9f6adb12c5ccbc9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-toc:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-toc:view.html.twig"));

        // line 1
        echo "<style>
    @page{
        size: auto;
        margin: 0;
        padding: 0;
        background-origin: -ro-page-box;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-1 {
        padding-top: 2mm;
        font-size: 12pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 600;
        padding-left: 0;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-2 {
        padding-left: 5mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-3 {
        padding-left: 10mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
</style>
";
        // line 32
        twig_get_attribute($this->env, $this->source, call_user_func_array($this->env->getFunction('pimcore_head_script')->getCallable(), []), "appendFile", [0 => $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/bundles/web2printtools/vendor/js/awesomizr.js")], "method", false, false, false, 32);
        // line 34
        if ( !(isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 34, $this->source); })())) {
            // line 35
            echo "    <script type=\"text/javascript\">
        \$(document).ready(function() {
            Awesomizr.createTableOfContents({
                /* toc container */
                insertiontarget: '#toc-wrapper',
                insertiontype: 'beforeend',
                /* levels to look for and link to in toc*/
                elements: ['h2.heading','h2.subheading','h3.childheading'],
                /* container element for the toc */
                container: {tag: 'ul', addClass: 'toc'},
                /* container element for one line in the toc */
                line: {tag: 'li'},
                disabledocumenttitle: false,
                toctitle: ' ',

                /* method of getting the text for the toc lines */
                text: function (elem) {
                    
                    return elem.textContent;
                }
            });
        });
    </script>
";
        }
        // line 83
        echo "<main class=\"table-of-contents\">    
    <table>
        <thead>
            <tr>
                <th>
                    <div class=\"table-page-header\">
                        <h1>";
        // line 89
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "toc_heading");
        echo "</h1>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id=\"toc-wrapper\">
                    
                    ";
        // line 98
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 98, $this->source); })())) {
            // line 99
            echo "                        TABLE OF CONTENTS IS GENERATED ON PDF-EXPORT
                    ";
        }
        // line 101
        echo "                </td>
            </tr>
        </tbody>
    </table>     
</main>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-toc:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 101,  128 => 99,  126 => 98,  114 => 89,  106 => 83,  80 => 35,  78 => 34,  76 => 32,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
    @page{
        size: auto;
        margin: 0;
        padding: 0;
        background-origin: -ro-page-box;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-1 {
        padding-top: 2mm;
        font-size: 12pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 600;
        padding-left: 0;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-2 {
        padding-left: 5mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-3 {
        padding-left: 10mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
</style>
{% do pimcore_head_script().appendFile(asset('/bundles/web2printtools/vendor/js/awesomizr.js')) %}
{#<script type=\"text/javascript\" src=\"/bundles/web2printtools/vendor/js/awesomizr.js\"></script>#}
{% if not editmode %}
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            Awesomizr.createTableOfContents({
                /* toc container */
                insertiontarget: '#toc-wrapper',
                insertiontype: 'beforeend',
                /* levels to look for and link to in toc*/
                elements: ['h2.heading','h2.subheading','h3.childheading'],
                /* container element for the toc */
                container: {tag: 'ul', addClass: 'toc'},
                /* container element for one line in the toc */
                line: {tag: 'li'},
                disabledocumenttitle: false,
                toctitle: ' ',

                /* method of getting the text for the toc lines */
                text: function (elem) {
                    
                    return elem.textContent;
                }
            });
        });
    </script>
{% endif %}
{#<main class=\"table-of-contents\" style=\"height:100vh\">
    <section class=\"pdf-top-container\">
        <div class=\"pdf-inner-container\">
            <div class=\"pdf-red-band\">
                &nbsp;
            </div>
        </div>
    </section>
    <section class=\"pdf-content\">
        <div class=\"pdf-inner-container\" id=\"toc-wrapper\">
            <h2>{{ pimcore_input('toc_heading') }}</h2>
            {% if editmode %}
                TABLE OF CONTENTS IS GENERATED ON PDF-EXPORT
            {% endif %}
        </div>
    </section>
    <section class=\"pdf-bottom-container\">
        <div class=\"pdf-inner-container\">
            <div class=\"pdf-red-band\">
                &nbsp;
            </div>
        </div>
    </section> 
</main>#}
<main class=\"table-of-contents\">    
    <table>
        <thead>
            <tr>
                <th>
                    <div class=\"table-page-header\">
                        <h1>{{ pimcore_input('toc_heading') }}</h1>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id=\"toc-wrapper\">
                    
                    {% if editmode %}
                        TABLE OF CONTENTS IS GENERATED ON PDF-EXPORT
                    {% endif %}
                </td>
            </tr>
        </tbody>
    </table>     
</main>
", ":Areas/print-toc:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-toc/view.html.twig");
    }
}
