<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:lostpassword.html.php */
class __TwigTemplate_66a8841d19e4c218cf021db915c431e93c7f517dd8f4857c0699080103202ea0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:lostpassword.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:lostpassword.html.php"));

        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');

\$this->get(\"translate\")->setDomain(\"admin\");

?>



<?php if (\$this->getRequest()->getMethod() === 'POST') { ?>
    <div class=\"text error\">
        <?= \$this->translate(\"A temporary login link has been sent to your email address.\"); ?>
        <br/>
        <?= \$this->translate(\"Please check your mailbox.\"); ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your username and pimcore will send a login link to your email address\"); ?>
    </div>

    <form method=\"post\" action=\"<?= \$view->router()->path('pimcore_admin_login_lostpassword') ?>\">
        <input type=\"text\" name=\"username\" autocomplete=\"username\" placeholder=\"<?= \$this->translate(\"Username\"); ?>\" required autofocus>
        <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

        <button type=\"submit\" name=\"submit\"><?= \$this->translate(\"Submit\"); ?></button>
    </form>
<?php } ?>

<a href=\"<?= \$view->router()->path('pimcore_admin_login') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:lostpassword.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');

\$this->get(\"translate\")->setDomain(\"admin\");

?>



<?php if (\$this->getRequest()->getMethod() === 'POST') { ?>
    <div class=\"text error\">
        <?= \$this->translate(\"A temporary login link has been sent to your email address.\"); ?>
        <br/>
        <?= \$this->translate(\"Please check your mailbox.\"); ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your username and pimcore will send a login link to your email address\"); ?>
    </div>

    <form method=\"post\" action=\"<?= \$view->router()->path('pimcore_admin_login_lostpassword') ?>\">
        <input type=\"text\" name=\"username\" autocomplete=\"username\" placeholder=\"<?= \$this->translate(\"Username\"); ?>\" required autofocus>
        <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

        <button type=\"submit\" name=\"submit\"><?= \$this->translate(\"Submit\"); ?></button>
    </form>
<?php } ?>

<a href=\"<?= \$view->router()->path('pimcore_admin_login') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


", "PimcoreAdminBundle:Admin/Login:lostpassword.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/lostpassword.html.php");
    }
}
