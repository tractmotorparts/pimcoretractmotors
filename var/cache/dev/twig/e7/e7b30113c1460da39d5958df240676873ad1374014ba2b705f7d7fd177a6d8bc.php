<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php */
class __TwigTemplate_14f77bac9caf5a26eded340620d7fcc7034deff5406d10afb4220ca234f3b2ce extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php"));

        // line 1
        echo "<?php
    /**
     * @var \\Pimcore\\Model\\Element\\AbstractElement \$element
     */
    \$element = \$this->element;
    \$this->get(\"translate\")->setDomain(\"admin\");
?>
<div class=\"data-table <?= \$this->cls ?>\">
    <table>
        <?php if(\$element instanceof \\Pimcore\\Model\\DataObject\\Concrete) { ?>
            <tr>
                <th><?= \$this->translate('class') ?></th>
                <td><?= \$element->getClassName() ?> [<?= \$element->getClassId() ?>]</td>
            </tr>
        <?php } ?>

        <?php if(\$element instanceof \\Pimcore\\Model\\Asset) { ?>
            <tr>
                <th><?= \$this->translate('mimetype') ?></th>
                <td><?= \$element->getMimetype() ?></td>
            </tr>
        <?php } ?>

        <?php if(\$element->getProperty('language')) { ?>
            <tr>
                <th><?= \$this->translate('language') ?></th>
                <td style=\"padding-left: 40px; background: url(<?= \\Pimcore\\Tool::getLanguageFlagFile(\$element->getProperty('language'), false); ?>) left top no-repeat; background-size: 31px 21px;\">
                    <?php
                    \$locales = \\Pimcore\\Tool::getSupportedLocales();
                    ?>
                    <?= \$locales[\$element->getProperty('language')] ?>
                </td>
            </tr>
        <?php } ?>

        <?php if(\$element instanceof \\Pimcore\\Model\\Document\\Page) { ?>
            <?php if(\$element->getTitle()) { ?>
                <tr>
                    <th><?= \$this->translate('title') ?></th>
                    <td><?= \$element->getTitle() ?></td>
                </tr>
            <?php } ?>

            <?php if(\$element->getDescription()) { ?>
                <tr>
                    <th><?= \$this->translate('description') ?></th>
                    <td><?= \$element->getDescription() ?></td>
                </tr>
            <?php } ?>

            <?php if(\$element->getProperty('navigation_name')) { ?>
                <tr>
                    <th><?= \$this->translate('name') ?></th>
                    <td><?= \$element->getProperty('navigation_name') ?></td>
                </tr>
            <?php } ?>
        <?php } ?>


        <?php
        \$owner = \\Pimcore\\Model\\User::getById(\$element->getUserOwner());
        ?>
        <?php if(\$owner) { ?>
            <tr>
                <th><?= \$this->translate('userowner') ?></th>
                <td><?= \$owner->getName() ?></td>
            </tr>
        <?php } ?>
        <?php
        \$editor = \\Pimcore\\Model\\User::getById(\$element->getUserModification());
        ?>
        <?php if(\$editor) { ?>
            <tr>
                <th><?= \$this->translate('usermodification') ?></th>
                <td><?= \$editor->getName() ?></td>
            </tr>
        <?php } ?>

        <tr>
            <th><?= \$this->translate('creationdate') ?></th>
            <td><?= date('Y-m-d H:i', \$element->getCreationDate()) ?></td>
        </tr>
        <tr>
            <th><?= \$this->translate('modificationdate') ?></th>
            <td><?= date('Y-m-d H:i', \$element->getModificationDate()) ?></td>
        </tr>
    </table>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
    /**
     * @var \\Pimcore\\Model\\Element\\AbstractElement \$element
     */
    \$element = \$this->element;
    \$this->get(\"translate\")->setDomain(\"admin\");
?>
<div class=\"data-table <?= \$this->cls ?>\">
    <table>
        <?php if(\$element instanceof \\Pimcore\\Model\\DataObject\\Concrete) { ?>
            <tr>
                <th><?= \$this->translate('class') ?></th>
                <td><?= \$element->getClassName() ?> [<?= \$element->getClassId() ?>]</td>
            </tr>
        <?php } ?>

        <?php if(\$element instanceof \\Pimcore\\Model\\Asset) { ?>
            <tr>
                <th><?= \$this->translate('mimetype') ?></th>
                <td><?= \$element->getMimetype() ?></td>
            </tr>
        <?php } ?>

        <?php if(\$element->getProperty('language')) { ?>
            <tr>
                <th><?= \$this->translate('language') ?></th>
                <td style=\"padding-left: 40px; background: url(<?= \\Pimcore\\Tool::getLanguageFlagFile(\$element->getProperty('language'), false); ?>) left top no-repeat; background-size: 31px 21px;\">
                    <?php
                    \$locales = \\Pimcore\\Tool::getSupportedLocales();
                    ?>
                    <?= \$locales[\$element->getProperty('language')] ?>
                </td>
            </tr>
        <?php } ?>

        <?php if(\$element instanceof \\Pimcore\\Model\\Document\\Page) { ?>
            <?php if(\$element->getTitle()) { ?>
                <tr>
                    <th><?= \$this->translate('title') ?></th>
                    <td><?= \$element->getTitle() ?></td>
                </tr>
            <?php } ?>

            <?php if(\$element->getDescription()) { ?>
                <tr>
                    <th><?= \$this->translate('description') ?></th>
                    <td><?= \$element->getDescription() ?></td>
                </tr>
            <?php } ?>

            <?php if(\$element->getProperty('navigation_name')) { ?>
                <tr>
                    <th><?= \$this->translate('name') ?></th>
                    <td><?= \$element->getProperty('navigation_name') ?></td>
                </tr>
            <?php } ?>
        <?php } ?>


        <?php
        \$owner = \\Pimcore\\Model\\User::getById(\$element->getUserOwner());
        ?>
        <?php if(\$owner) { ?>
            <tr>
                <th><?= \$this->translate('userowner') ?></th>
                <td><?= \$owner->getName() ?></td>
            </tr>
        <?php } ?>
        <?php
        \$editor = \\Pimcore\\Model\\User::getById(\$element->getUserModification());
        ?>
        <?php if(\$editor) { ?>
            <tr>
                <th><?= \$this->translate('usermodification') ?></th>
                <td><?= \$editor->getName() ?></td>
            </tr>
        <?php } ?>

        <tr>
            <th><?= \$this->translate('creationdate') ?></th>
            <td><?= date('Y-m-d H:i', \$element->getCreationDate()) ?></td>
        </tr>
        <tr>
            <th><?= \$this->translate('modificationdate') ?></th>
            <td><?= date('Y-m-d H:i', \$element->getModificationDate()) ?></td>
        </tr>
    </table>
</div>
", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/SearchAdmin/Search/Quicksearch/info-table.html.php");
    }
}
