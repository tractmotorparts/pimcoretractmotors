<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php */
class __TwigTemplate_560918f7fc22fd5dc55d61686e94a7357d4526a133bd2c7e4158884fe5464162 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php"));

        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">
        html, body {
            padding: 0;
            margin: 0;
        }

        body {
            text-align: center;
            position: relative;
        }

        img {
            max-width: 100%;
        }

        #left, #right {
            position: absolute;
            top:0;
            width:50%;
        }

        #left {
            left: 0;
            z-index: 1;
        }

        #right {
            right: 0;
            z-index: 2;
            border-left: 1px dashed darkred;
        }
    </style>
</head>
<body>

    <?php if(\$this->image) { ?>
        <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
            'id' => \$this->image
        ])?>\">
    <?php } else { ?>
        <div id=\"left\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image1
            ])?>\">
        </div>
        <div id=\"right\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image2
            ])?>\">
        </div>
    <?php } ?>

</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">
        html, body {
            padding: 0;
            margin: 0;
        }

        body {
            text-align: center;
            position: relative;
        }

        img {
            max-width: 100%;
        }

        #left, #right {
            position: absolute;
            top:0;
            width:50%;
        }

        #left {
            left: 0;
            z-index: 1;
        }

        #right {
            right: 0;
            z-index: 2;
            border-left: 1px dashed darkred;
        }
    </style>
</head>
<body>

    <?php if(\$this->image) { ?>
        <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
            'id' => \$this->image
        ])?>\">
    <?php } else { ?>
        <div id=\"left\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image1
            ])?>\">
        </div>
        <div id=\"right\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image2
            ])?>\">
        </div>
    <?php } ?>

</body>
</html>
", "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Document/Document/diff-versions.html.php");
    }
}
