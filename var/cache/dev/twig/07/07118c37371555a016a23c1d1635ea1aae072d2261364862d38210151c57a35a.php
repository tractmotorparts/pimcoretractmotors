<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-multiple-product:view.html.twig */
class __TwigTemplate_7ed1a3d44f879028b494c9e7f94358274165b38e6f61eedebd2b83e99a01275b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-multiple-product:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-multiple-product:view.html.twig"));

        // line 1
        echo "<div class=\"pdf-multiple-product\">
    <table style=\"width: 100%;\">
        <thead>
            <tr>
                <td>
                    <div class=\"page-head-row\">
                        <div class=\"page-head-left\">

                        </div>
                        <div class=\"page-head-right\">
                            <h2>";
        // line 11
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_sub_head_1", ["placeholder" => "Enter Category/Subcategory Name"]);
        echo "</h2>
                        </div>
                    </div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class=\"sub-heading\">";
        // line 20
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_brand_name", ["placeholder" => "Enter Brand Name"]);
        echo "</div>
                    <div class=\"pro-row\">
                        ";
        // line 22
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 22, $this->source); })())) {
            // line 23
            echo "                            ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_1", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "type" => "Object", "selectedClass" => "Product", "reload" => true]);
            // line 31
            echo "
                        ";
        } else {
            // line 33
            echo "                            ";
            $context["productGridData_1"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_1");
            // line 34
            echo "                            ";
            if ((twig_length_filter($this->env, (isset($context["productGridData_1"]) || array_key_exists("productGridData_1", $context) ? $context["productGridData_1"] : (function () { throw new RuntimeError('Variable "productGridData_1" does not exist.', 34, $this->source); })())) > 0)) {
                // line 35
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["productGridData_1"]) || array_key_exists("productGridData_1", $context) ? $context["productGridData_1"] : (function () { throw new RuntimeError('Variable "productGridData_1" does not exist.', 35, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 36
                    echo "                                <div class=\"pro-col\">
                                    <img src=\"";
                    // line 37
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 37)), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 37), "html", null, true);
                    echo "\" >
                                    <h3>";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 38), "html", null, true);
                    echo "</h3>
                                </div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "                            ";
            }
            // line 42
            echo "                        ";
        }
        // line 43
        echo "                    </div>
                    ";
        // line 44
        if ( !(isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 44, $this->source); })())) {
            // line 45
            echo "                        ";
            if ((twig_length_filter($this->env, (isset($context["productGridData_1"]) || array_key_exists("productGridData_1", $context) ? $context["productGridData_1"] : (function () { throw new RuntimeError('Variable "productGridData_1" does not exist.', 45, $this->source); })())) > 0)) {
                // line 46
                echo "                        <div class=\"multi-prod-compare-table\">
                            <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <thead>
                                    <tr>
                                        <th style=\"width:20%; text-align: left;\">Model</th>
                                        ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["productGridData_1"]) || array_key_exists("productGridData_1", $context) ? $context["productGridData_1"] : (function () { throw new RuntimeError('Variable "productGridData_1" does not exist.', 51, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 52
                    echo "                                         <th style=\"width:12%\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 52), "html", null, true);
                    echo "</th>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 54
                echo "                                     </tr>
                                </thead>
                                <tbody>
                                    ";
                // line 57
                $context["productAttrList"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getTechnichleAttrMultipleProducts((isset($context["productGridData_1"]) || array_key_exists("productGridData_1", $context) ? $context["productGridData_1"] : (function () { throw new RuntimeError('Variable "productGridData_1" does not exist.', 57, $this->source); })()));
                // line 58
                echo "
                                    ";
                // line 59
                if ((isset($context["productAttrList"]) || array_key_exists("productAttrList", $context) ? $context["productAttrList"] : (function () { throw new RuntimeError('Variable "productAttrList" does not exist.', 59, $this->source); })())) {
                    // line 60
                    echo "                                        ";
                    $context["titles"] = twig_get_attribute($this->env, $this->source, (isset($context["productAttrList"]) || array_key_exists("productAttrList", $context) ? $context["productAttrList"] : (function () { throw new RuntimeError('Variable "productAttrList" does not exist.', 60, $this->source); })()), "title", [], "any", false, false, false, 60);
                    // line 61
                    echo "                                        ";
                    $context["techAttributes"] = twig_get_attribute($this->env, $this->source, (isset($context["productAttrList"]) || array_key_exists("productAttrList", $context) ? $context["productAttrList"] : (function () { throw new RuntimeError('Variable "productAttrList" does not exist.', 61, $this->source); })()), "technichleItems", [], "any", false, false, false, 61);
                    // line 62
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["titles"]) || array_key_exists("titles", $context) ? $context["titles"] : (function () { throw new RuntimeError('Variable "titles" does not exist.', 62, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                        // line 63
                        echo "                                        <tr>
                                            <td style=\"text-align: left;\">";
                        // line 64
                        echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                        echo "</td>
                                            ";
                        // line 65
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["techAttributes"]) || array_key_exists("techAttributes", $context) ? $context["techAttributes"] : (function () { throw new RuntimeError('Variable "techAttributes" does not exist.', 65, $this->source); })()));
                        foreach ($context['_seq'] as $context["_key"] => $context["tattr"]) {
                            // line 66
                            echo "                                                <td>";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tattr"], $context["title"], [], "array", false, false, false, 66), "html", null, true);
                            echo "</td>
                                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tattr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 68
                        echo "                                        </tr>
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "                                    ";
                } else {
                    // line 71
                    echo "                                        <tr>
                                            <td style=\"text-align: left;\">No Technicle Attributes Found</td>
                                        </tr>
                                    ";
                }
                // line 75
                echo "                                </tbody>
                            </table>
                        </div>
                        ";
            }
            // line 79
            echo "                    ";
        }
        // line 80
        echo "                    <p style=\"page-break-after: always;\">&nbsp;</p>
                    <div class=\"product-detail-text\">
                        <div class=\"prote-box\">
                            <h4>";
        // line 83
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_1", ["placeholder" => "Enter Title 1"]);
        echo "</h4>
                            ";
        // line 87
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_1", ["height" => 200]);
        // line 90
        echo "
                            <h4>";
        // line 91
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_2", ["placeholder" => "Enter Title 2"]);
        echo "</h4>
                            ";
        // line 95
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_2", ["height" => 200]);
        // line 98
        echo "
                            <h4>";
        // line 99
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_3", ["placeholder" => "Enter Title 3"]);
        echo "</h4>
                            ";
        // line 103
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_3", ["height" => 200]);
        // line 106
        echo "
                        </div>
                    </div>
                    <div class=\"sub-heading\">";
        // line 109
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_brand_name_2", ["placeholder" => "Enter Brand Name"]);
        echo "</div>
                    ";
        // line 110
        if ( !(isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 110, $this->source); })())) {
            echo "   
                    <div class=\"multi-prod-compare-table\">
                        ";
            // line 112
            $context["productGridData_2"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_2");
            // line 113
            echo "                        ";
            if ((twig_length_filter($this->env, (isset($context["productGridData_2"]) || array_key_exists("productGridData_2", $context) ? $context["productGridData_2"] : (function () { throw new RuntimeError('Variable "productGridData_2" does not exist.', 113, $this->source); })())) > 0)) {
                // line 114
                echo "                        <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                            <thead>
                                <tr>
                                    <th style=\"width:20%; text-align: left;\">Model</th>
                                    ";
                // line 118
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["productGridData_2"]) || array_key_exists("productGridData_2", $context) ? $context["productGridData_2"] : (function () { throw new RuntimeError('Variable "productGridData_2" does not exist.', 118, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 119
                    echo "                                    <th style=\"width:12%\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 119), "html", null, true);
                    echo "</th>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 121
                echo "                                </tr>
                            </thead>
                            <tbody>
                                ";
                // line 124
                $context["productAttrList2"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getTechnichleAttrMultipleProducts((isset($context["productGridData_2"]) || array_key_exists("productGridData_2", $context) ? $context["productGridData_2"] : (function () { throw new RuntimeError('Variable "productGridData_2" does not exist.', 124, $this->source); })()));
                // line 125
                echo "                                ";
                $context["titles2"] = twig_get_attribute($this->env, $this->source, (isset($context["productAttrList2"]) || array_key_exists("productAttrList2", $context) ? $context["productAttrList2"] : (function () { throw new RuntimeError('Variable "productAttrList2" does not exist.', 125, $this->source); })()), "title", [], "any", false, false, false, 125);
                // line 126
                echo "                                ";
                $context["techAttributes2"] = twig_get_attribute($this->env, $this->source, (isset($context["productAttrList2"]) || array_key_exists("productAttrList2", $context) ? $context["productAttrList2"] : (function () { throw new RuntimeError('Variable "productAttrList2" does not exist.', 126, $this->source); })()), "technichleItems", [], "any", false, false, false, 126);
                // line 127
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["titles2"]) || array_key_exists("titles2", $context) ? $context["titles2"] : (function () { throw new RuntimeError('Variable "titles2" does not exist.', 127, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                    // line 128
                    echo "                                <tr>
                                    <td style=\"text-align: left;\">";
                    // line 129
                    echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                    echo "</td>
                                    ";
                    // line 130
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["techAttributes2"]) || array_key_exists("techAttributes2", $context) ? $context["techAttributes2"] : (function () { throw new RuntimeError('Variable "techAttributes2" does not exist.', 130, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["tattr"]) {
                        // line 131
                        echo "                                        <td>";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tattr"], $context["title"], [], "array", false, false, false, 131), "html", null, true);
                        echo "</td>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tattr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 133
                    echo "                                </tr>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 135
                echo "                            </tbody>
                        </table>
                        ";
            }
            // line 138
            echo "                    </div>
                    ";
        }
        // line 140
        echo "                    <div class=\"pro-row two-col-prod\">
                        ";
        // line 141
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 141, $this->source); })())) {
            // line 142
            echo "                            ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_2", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "type" => "Object", "selectedClass" => "Product", "reload" => true]);
            // line 150
            echo "
                        ";
        } else {
            // line 152
            echo "                            ";
            if ((twig_length_filter($this->env, (isset($context["productGridData_2"]) || array_key_exists("productGridData_2", $context) ? $context["productGridData_2"] : (function () { throw new RuntimeError('Variable "productGridData_2" does not exist.', 152, $this->source); })())) > 0)) {
                // line 153
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["productGridData_2"]) || array_key_exists("productGridData_2", $context) ? $context["productGridData_2"] : (function () { throw new RuntimeError('Variable "productGridData_2" does not exist.', 153, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 154
                    echo "                                <div class=\"pro-col\">
                                    <img src=\"";
                    // line 155
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 155)), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 155), "html", null, true);
                    echo "\">
                                    <h3>";
                    // line 156
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 156), "html", null, true);
                    echo "</h3>
                                </div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 159
                echo "                            ";
            }
            // line 160
            echo "                        ";
        }
        // line 161
        echo "                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>

                </td>
            </tr>
        </tfoot>
    </table>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-multiple-product:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  380 => 161,  377 => 160,  374 => 159,  365 => 156,  359 => 155,  356 => 154,  351 => 153,  348 => 152,  344 => 150,  341 => 142,  339 => 141,  336 => 140,  332 => 138,  327 => 135,  320 => 133,  311 => 131,  307 => 130,  303 => 129,  300 => 128,  295 => 127,  292 => 126,  289 => 125,  287 => 124,  282 => 121,  273 => 119,  269 => 118,  263 => 114,  260 => 113,  258 => 112,  253 => 110,  249 => 109,  244 => 106,  241 => 103,  237 => 99,  234 => 98,  231 => 95,  227 => 91,  224 => 90,  221 => 87,  217 => 83,  212 => 80,  209 => 79,  203 => 75,  197 => 71,  194 => 70,  187 => 68,  178 => 66,  174 => 65,  170 => 64,  167 => 63,  162 => 62,  159 => 61,  156 => 60,  154 => 59,  151 => 58,  149 => 57,  144 => 54,  135 => 52,  131 => 51,  124 => 46,  121 => 45,  119 => 44,  116 => 43,  113 => 42,  110 => 41,  101 => 38,  95 => 37,  92 => 36,  87 => 35,  84 => 34,  81 => 33,  77 => 31,  74 => 23,  72 => 22,  67 => 20,  55 => 11,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"pdf-multiple-product\">
    <table style=\"width: 100%;\">
        <thead>
            <tr>
                <td>
                    <div class=\"page-head-row\">
                        <div class=\"page-head-left\">

                        </div>
                        <div class=\"page-head-right\">
                            <h2>{{pimcore_input(\"category_sub_head_1\", {'placeholder':'Enter Category/Subcategory Name'}) }}</h2>
                        </div>
                    </div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class=\"sub-heading\">{{pimcore_input(\"category_brand_name\", {'placeholder':'Enter Brand Name'}) }}</div>
                    <div class=\"pro-row\">
                        {% if editmode %}
                            {{
                                pimcore_outputchanneltable('multiple_product_grid_1', {
                                    'disableClassSelection': true,
                                    'disableFavoriteOutputChannel': true,
                                    'type': 'Object',
                                    'selectedClass':'Product',
                                    'reload':true
                                })
                            }}
                        {% else %}
                            {% set productGridData_1 = pimcore_outputchanneltable('multiple_product_grid_1') %}
                            {% if productGridData_1|length >0 %}
                                {% for element in productGridData_1 %}
                                <div class=\"pro-col\">
                                    <img src=\"{{pimcore_generate_image_url(element.getImage())}}\" alt=\"{{element.getProductName() }}\" >
                                    <h3>{{element.getModel() }}</h3>
                                </div>
                                {% endfor %}
                            {% endif %}
                        {% endif %}
                    </div>
                    {% if not editmode %}
                        {% if productGridData_1|length > 0 %}
                        <div class=\"multi-prod-compare-table\">
                            <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <thead>
                                    <tr>
                                        <th style=\"width:20%; text-align: left;\">Model</th>
                                        {% for element in productGridData_1 %}
                                         <th style=\"width:12%\">{{element.getModel() }}</th>
                                        {% endfor %}
                                     </tr>
                                </thead>
                                <tbody>
                                    {% set productAttrList=pimcore_get_technicleAttribute(productGridData_1) %}

                                    {% if productAttrList %}
                                        {% set titles=productAttrList.title %}
                                        {% set techAttributes=productAttrList.technichleItems %}
                                        {% for title in titles %}
                                        <tr>
                                            <td style=\"text-align: left;\">{{title}}</td>
                                            {% for tattr in techAttributes %}
                                                <td>{{ tattr[title] }}</td>
                                            {% endfor %}
                                        </tr>
                                        {% endfor %}
                                    {% else %}
                                        <tr>
                                            <td style=\"text-align: left;\">No Technicle Attributes Found</td>
                                        </tr>
                                    {% endif %}
                                </tbody>
                            </table>
                        </div>
                        {% endif %}
                    {% endif %}
                    <p style=\"page-break-after: always;\">&nbsp;</p>
                    <div class=\"product-detail-text\">
                        <div class=\"prote-box\">
                            <h4>{{pimcore_input(\"category_desc_head_1\", {'placeholder':'Enter Title 1'}) }}</h4>
                            {#<ul>
                                <li>Motor over-load protector is available</li>
                            </ul>#}
                            {{  pimcore_wysiwyg(\"category_desc_1\", {
                                    \"height\": 200
                                }) 
                            }}
                            <h4>{{pimcore_input(\"category_desc_head_2\", {'placeholder':'Enter Title 2'}) }}</h4>
                            {#<ul>
                                <li>To protect against dust and water splash</li>
                            </ul>#}
                            {{  pimcore_wysiwyg(\"category_desc_2\", {
                                    \"height\": 200
                                }) 
                            }}
                            <h4>{{pimcore_input(\"category_desc_head_3\", {'placeholder':'Enter Title 3'}) }}</h4>
                            {#<ul>
                                <li>Four poles electric motor fits overload protection </li>
                            </ul>#}
                            {{  pimcore_wysiwyg(\"category_desc_3\", {
                                    \"height\": 200
                                }) 
                            }}
                        </div>
                    </div>
                    <div class=\"sub-heading\">{{pimcore_input(\"category_brand_name_2\", {'placeholder':'Enter Brand Name'}) }}</div>
                    {% if not editmode %}   
                    <div class=\"multi-prod-compare-table\">
                        {% set productGridData_2 = pimcore_outputchanneltable('multiple_product_grid_2') %}
                        {% if productGridData_2|length > 0 %}
                        <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                            <thead>
                                <tr>
                                    <th style=\"width:20%; text-align: left;\">Model</th>
                                    {% for element in productGridData_2 %}
                                    <th style=\"width:12%\">{{element.getModel() }}</th>
                                    {% endfor %}
                                </tr>
                            </thead>
                            <tbody>
                                {% set productAttrList2=pimcore_get_technicleAttribute(productGridData_2) %}
                                {% set titles2=productAttrList2.title %}
                                {% set techAttributes2=productAttrList2.technichleItems %}
                                {% for title in titles2 %}
                                <tr>
                                    <td style=\"text-align: left;\">{{title}}</td>
                                    {% for tattr in techAttributes2 %}
                                        <td>{{ tattr[title] }}</td>
                                    {% endfor %}
                                </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                        {% endif %}
                    </div>
                    {% endif %}
                    <div class=\"pro-row two-col-prod\">
                        {% if editmode %}
                            {{
                                pimcore_outputchanneltable('multiple_product_grid_2', {
                                    'disableClassSelection': true,
                                    'disableFavoriteOutputChannel': true,
                                    'type': 'Object',
                                    'selectedClass':'Product',
                                    'reload':true
                                })
                            }}
                        {% else %}
                            {% if productGridData_2|length > 0 %}
                                {% for element in productGridData_2 %}
                                <div class=\"pro-col\">
                                    <img src=\"{{pimcore_generate_image_url(element.getImage())}}\" alt=\"{{element.getProductName() }}\">
                                    <h3>{{element.getModel() }}</h3>
                                </div>
                                {% endfor %}
                            {% endif %}
                        {% endif %}
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>

                </td>
            </tr>
        </tfoot>
    </table>
</div>
", ":Areas/print-multiple-product:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-multiple-product/view.html.twig");
    }
}
