<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php */
class __TwigTemplate_b53feafa309df3664ee6c2012f7784ec7c4ce14c91dcfeaa34239bdb475dce8a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php"));

        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">

        /* hide from ie on mac \\*/
        html {
            height: 100%;
            overflow: hidden;
        }
        /* end hide */

        body {
            height: 100%;
            margin: 0;
            padding: 0;
            background: #000;
        }

        #videoContainer {
            text-align: center;
            position: absolute;
            top:50%;
            margin-top: -200px;
            width: 100%;
        }

        video {

        }

    </style>

</head>

<body>


<?php
    \$previewImage = \"\";
    if(\\Pimcore\\Video::isAvailable()) {
        \$previewImage = \$view->router()->path('pimcore_admin_asset_getvideothumbnail', [
            'id' => \$this->asset->getId(),
            'treepreview' => 'true'
        ]);
    }

    \$serveVideoPreview = \$view->router()->path('pimcore_admin_asset_servevideopreview', [
        'id' => \$this->asset->getId()
    ]);
?>

<div id=\"videoContainer\">
    <video id=\"video\" controls=\"controls\" height=\"400\" poster=\"<?= \$previewImage ?>\">
        <source src=\"<?=\$serveVideoPreview ?>\" type=\"video/mp4\" />
    </video>
</div>


</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">

        /* hide from ie on mac \\*/
        html {
            height: 100%;
            overflow: hidden;
        }
        /* end hide */

        body {
            height: 100%;
            margin: 0;
            padding: 0;
            background: #000;
        }

        #videoContainer {
            text-align: center;
            position: absolute;
            top:50%;
            margin-top: -200px;
            width: 100%;
        }

        video {

        }

    </style>

</head>

<body>


<?php
    \$previewImage = \"\";
    if(\\Pimcore\\Video::isAvailable()) {
        \$previewImage = \$view->router()->path('pimcore_admin_asset_getvideothumbnail', [
            'id' => \$this->asset->getId(),
            'treepreview' => 'true'
        ]);
    }

    \$serveVideoPreview = \$view->router()->path('pimcore_admin_asset_servevideopreview', [
        'id' => \$this->asset->getId()
    ]);
?>

<div id=\"videoContainer\">
    <video id=\"video\" controls=\"controls\" height=\"400\" poster=\"<?= \$previewImage ?>\">
        <source src=\"<?=\$serveVideoPreview ?>\" type=\"video/mp4\" />
    </video>
</div>


</body>
</html>
", "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Asset/getPreviewVideoDisplay.html.php");
    }
}
