<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-by-category:view.html.twig */
class __TwigTemplate_94ed9a4476dcb9f15326eca38ce4d3d03236e39253068eafb7a9fc1d443322a6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-by-category:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-by-category:view.html.twig"));

        // line 1
        echo "<style>
    @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }


        .product-catalogue .page-header h2 {
            margin-top: 0;
            background-color: #cf0101;
            height: 40px;
            text-transform: uppercase;
            line-height: 40px;
            margin: 0;
            font-size: 24px;
        }

        .pdf-right-container {
            padding-right: 0;
            width: 100%;
            padding-left: 0;
            padding-top: 0;
        }

        .pdf-sub-cat-content .product-row .product-box {
            text-align: left;
            padding: 12px;
            width: 30.6%;
            list-style: none;
            display: inline-block;
            margin: 15px 4% 25px 0px;
            box-shadow: 0px 1px 9px 4px #d2d2d2;
            position: relative;
            height: 100%;
        }

        .pdf-cat-content {
            padding: 15px;
        }
        .pdf-sub-cat-content h3 { color: #4974c3; margin: 10px 0 0; font-size: 20px;  text-transform: uppercase;}
        .pdf-cat-content h2 { font-size: 22px !important; text-transform: uppercase;}
        .page-header { padding: 30px 15px 0;}

        @media print {
           .product-catalogue thead {
                display: table-header-group;
            }

            .product-catalogue  tfoot {
                display: table-footer-group;
                position: relative;
                bottom: 0;
            }            
        }
</style>
";
        // line 58
        $context["counter"] = 0;
        // line 59
        echo "<main class=\"product-catalogue\">
    <table style=\"width:100%\">
        <thead>
            <tr>
                <th>
                    <div class=\"page-header\">
                        <h2 class=\"heading\">";
        // line 65
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_head", ["placeholder" => "Category Name"]);
        echo "</h2>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table style=\"width:100%\">
                        <tbody>
                            <tr>
                                <td>   
                                   <section class=\"pdf-cat-content\">
                                       ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->getBlockIterator($this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "block", "ChildBlock", ["limit" => 50])));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            // line 79
            echo "                                        <h2 class=\"subheading\">";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", ("category_sub_head_" . $context["k"]), ["placeholder" => "Subcategory Name"]);
            echo "</h2>
                                        <div class=\"pdf-sub-cat-content\">
                                           ";
            // line 82
            echo "                                            <div class=\"title-with-logo\">
                                                <div class=\"tle\">
                                                    <h3 class=\"childheading\">";
            // line 84
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", ("product_brand_" . $context["k"]), ["placeholder" => "Enter Brand Name"]);
            echo "</h3>
                                                </div>
                                                <div class=\"rt-lgo\">
                                                    ";
            // line 88
            echo "                                                    ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "brand-logo");
            echo "
                                                </div>
                                            </div>
                                           ";
            // line 91
            if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 91, $this->source); })())) {
                // line 92
                echo "                                           ";
                echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" .                 // line 93
$context["k"]), ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "reload" => false, "selectedClass" => "Product"]);
                // line 99
                echo "
                                           ";
            } else {
                // line 101
                echo "                                           ";
                $context["configArray"] = $this->extensions['Web2PrintToolsBundle\Twig\OutputChannelExtension']->buildOutputDataConfig(twig_get_attribute($this->env, $this->source, $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" . $context["k"])), "getOutputChannel", [], "method", false, false, false, 101));
                // line 102
                echo "                                           ";
                $context["elements"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" . $context["k"]));
                // line 103
                echo "                                           <div class=\"product-row\">
                                                ";
                // line 104
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new RuntimeError('Variable "elements" does not exist.', 104, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 105
                    echo "                                                ";
                    $context["counter"] = ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 105, $this->source); })()) + 1);
                    // line 106
                    echo "                                              
                                                <div class=\"product-box\" id=\"product_";
                    // line 107
                    echo twig_escape_filter($this->env, (isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 107, $this->source); })()), "html", null, true);
                    echo "\">
                                                    <div class=\"img-placeholder\">
                                                        <img style=\"width: 100px;\" src=\"";
                    // line 109
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 109)), "html", null, true);
                    echo "\" alt=\"product_image\">
                                                    </div>
                                                    ";
                    // line 112
                    echo "                                                    <h4>
                                                        ";
                    // line 113
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 113), "html", null, true);
                    echo " 
                                                        <span class=\"modal-no\">";
                    // line 114
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 114), "html", null, true);
                    echo "</span>
                                                    </h4>
                                                    <p>";
                    // line 116
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getShortDescription", [], "method", false, false, false, 116), "html", null, true);
                    echo "
                                                    </p>
                                                    ";
                    // line 118
                    $context["currency"] = twig_get_attribute($this->env, $this->source, $context["element"], "getCurrency", [], "method", false, false, false, 118);
                    // line 119
                    echo "                                                    ";
                    $context["defaultCurrency"] = "\$";
                    // line 120
                    echo "                                                    ";
                    if ((isset($context["currency"]) || array_key_exists("currency", $context) ? $context["currency"] : (function () { throw new RuntimeError('Variable "currency" does not exist.', 120, $this->source); })())) {
                        // line 121
                        echo "                                                    ";
                        $context["expldCurrency"] = twig_split_filter($this->env, (isset($context["currency"]) || array_key_exists("currency", $context) ? $context["currency"] : (function () { throw new RuntimeError('Variable "currency" does not exist.', 121, $this->source); })()), "-");
                        // line 122
                        echo "                                                    ";
                        // line 123
                        echo "                                                    ";
                        $context["defaultCurrency"] = (((twig_get_attribute($this->env, $this->source, (isset($context["expldCurrency"]) || array_key_exists("expldCurrency", $context) ? $context["expldCurrency"] : (function () { throw new RuntimeError('Variable "expldCurrency" does not exist.', 123, $this->source); })()), 1, [], "array", false, false, false, 123) != "")) ? (twig_get_attribute($this->env, $this->source, (isset($context["expldCurrency"]) || array_key_exists("expldCurrency", $context) ? $context["expldCurrency"] : (function () { throw new RuntimeError('Variable "expldCurrency" does not exist.', 123, $this->source); })()), 1, [], "array", false, false, false, 123)) : ((isset($context["defaultCurrency"]) || array_key_exists("defaultCurrency", $context) ? $context["defaultCurrency"] : (function () { throw new RuntimeError('Variable "defaultCurrency" does not exist.', 123, $this->source); })())));
                        // line 124
                        echo "                                                    ";
                    }
                    // line 125
                    echo "                                                    <span class=\"price\">";
                    echo (isset($context["defaultCurrency"]) || array_key_exists("defaultCurrency", $context) ? $context["defaultCurrency"] : (function () { throw new RuntimeError('Variable "defaultCurrency" does not exist.', 125, $this->source); })());
                    echo "  ";
                    echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 125)), "html", null, true);
                    echo "</span>
                                                </div>
                                               ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 128
                echo "                                            </div>
                                            ";
            }
            // line 130
            echo "                                        </div>
                                       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "      
                                   </section>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</main>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-product-by-category:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 131,  246 => 130,  242 => 128,  230 => 125,  227 => 124,  224 => 123,  222 => 122,  219 => 121,  216 => 120,  213 => 119,  211 => 118,  206 => 116,  201 => 114,  197 => 113,  194 => 112,  189 => 109,  184 => 107,  181 => 106,  178 => 105,  174 => 104,  171 => 103,  168 => 102,  165 => 101,  161 => 99,  159 => 93,  157 => 92,  155 => 91,  148 => 88,  142 => 84,  138 => 82,  132 => 79,  128 => 78,  112 => 65,  104 => 59,  102 => 58,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
    @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }


        .product-catalogue .page-header h2 {
            margin-top: 0;
            background-color: #cf0101;
            height: 40px;
            text-transform: uppercase;
            line-height: 40px;
            margin: 0;
            font-size: 24px;
        }

        .pdf-right-container {
            padding-right: 0;
            width: 100%;
            padding-left: 0;
            padding-top: 0;
        }

        .pdf-sub-cat-content .product-row .product-box {
            text-align: left;
            padding: 12px;
            width: 30.6%;
            list-style: none;
            display: inline-block;
            margin: 15px 4% 25px 0px;
            box-shadow: 0px 1px 9px 4px #d2d2d2;
            position: relative;
            height: 100%;
        }

        .pdf-cat-content {
            padding: 15px;
        }
        .pdf-sub-cat-content h3 { color: #4974c3; margin: 10px 0 0; font-size: 20px;  text-transform: uppercase;}
        .pdf-cat-content h2 { font-size: 22px !important; text-transform: uppercase;}
        .page-header { padding: 30px 15px 0;}

        @media print {
           .product-catalogue thead {
                display: table-header-group;
            }

            .product-catalogue  tfoot {
                display: table-footer-group;
                position: relative;
                bottom: 0;
            }            
        }
</style>
{% set counter =0 %}
<main class=\"product-catalogue\">
    <table style=\"width:100%\">
        <thead>
            <tr>
                <th>
                    <div class=\"page-header\">
                        <h2 class=\"heading\">{{ pimcore_input(\"category_head\", {'placeholder':'Category Name'}) }}</h2>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table style=\"width:100%\">
                        <tbody>
                            <tr>
                                <td>   
                                   <section class=\"pdf-cat-content\">
                                       {% for k in pimcore_iterate_block(pimcore_block('ChildBlock',{'limit' : 50})) %}
                                        <h2 class=\"subheading\">{{pimcore_input(\"category_sub_head_\"~k, {'placeholder':'Subcategory Name'}) }}</h2>
                                        <div class=\"pdf-sub-cat-content\">
                                           {#<h3 class=\"childheading\">{{ pimcore_input(\"product_brand_\"~k, {'placeholder':'Enter Brand Name'}) }}</h3>#}
                                            <div class=\"title-with-logo\">
                                                <div class=\"tle\">
                                                    <h3 class=\"childheading\">{{ pimcore_input(\"product_brand_\"~k, {'placeholder':'Enter Brand Name'}) }}</h3>
                                                </div>
                                                <div class=\"rt-lgo\">
                                                    {#<img src=\"./images/Bitmap.jpg\">#}
                                                    {{ pimcore_image(\"brand-logo\") }}
                                                </div>
                                            </div>
                                           {% if editmode %}
                                           {{
                                               pimcore_outputchanneltable('tableconfig_'~k, {
                                                   'disableClassSelection': true,
                                                   'disableFavoriteOutputChannel': true,
                                                   'reload':false,
                                                   'selectedClass':'Product'
                                               })
                                           }}
                                           {% else %}
                                           {% set configArray = output_channel_build_output_data_config(pimcore_outputchanneltable('tableconfig_'~k).getOutputChannel()) %}
                                           {% set elements = pimcore_outputchanneltable('tableconfig_'~k) %}
                                           <div class=\"product-row\">
                                                {% for element in elements %}
                                                {% set counter =counter+1 %}
                                              
                                                <div class=\"product-box\" id=\"product_{{ counter }}\">
                                                    <div class=\"img-placeholder\">
                                                        <img style=\"width: 100px;\" src=\"{{pimcore_generate_image_url(element.getImage())}}\" alt=\"product_image\">
                                                    </div>
                                                    {#<h4>{{ element.getProductName() }} | {{element.getModel() }}</h4>#}
                                                    <h4>
                                                        {{ element.getProductName() }} 
                                                        <span class=\"modal-no\">{{element.getModel() }}</span>
                                                    </h4>
                                                    <p>{{ element.getShortDescription() }}
                                                    </p>
                                                    {% set currency=element.getCurrency() %}
                                                    {% set defaultCurrency='\$' %}
                                                    {% if currency %}
                                                    {% set expldCurrency = currency|split('-') %}
                                                    {#{{dump(expldCurrency)}}#}
                                                    {% set defaultCurrency= expldCurrency[1]!=''?expldCurrency[1]:defaultCurrency %}
                                                    {% endif %}
                                                    <span class=\"price\">{{ defaultCurrency|raw }}  {{ element.getListPrice()|round }}</span>
                                                </div>
                                               {% endfor %}
                                            </div>
                                            {% endif %}
                                        </div>
                                       {% endfor %}      
                                   </section>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</main>


", ":Areas/print-product-by-category:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-by-category/view.html.twig");
    }
}
