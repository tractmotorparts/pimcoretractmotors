<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-comparision:view.html.twig */
class __TwigTemplate_676dfc3a8d1ebd7408947a8efe69de6fc15ba8e0cc359eee56b073c479ff575c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-comparision:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-comparision:view.html.twig"));

        // line 1
        echo "<style>
       .table-pro-compare .img-placeholder img {
            width: 150px;
        }

        .table-pro-compare .img-placeholder {
            border: 1px solid;
            padding: 2px;
        }
</style>
<div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                ";
        // line 17
        echo "            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        
                        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
                            <tr>
                                <td><h2>";
        // line 31
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "product-compare-title");
        echo "</h2></td>
                            </tr>
                            ";
        // line 33
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 33, $this->source); })())) {
            // line 34
            echo "                             <tr>
                                 <td>
                                    <div class=\"outPutconfig\">
                                        ";
            // line 37
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "reload" => true, "selectedClass" => "Product"]);
            // line 44
            echo "
                                        <strong style=\"color:red\"><span>NOTE:</span><span>Please select minimum 2 and maximum 3 product </span></strong>
                                    </div>
                                  </td>
                             </tr>
                            ";
        } else {
            // line 50
            echo "                            <tr>
                                <td>
                                    ";
            // line 52
            $context["configArray"] = $this->extensions['Web2PrintToolsBundle\Twig\OutputChannelExtension']->buildOutputDataConfig(twig_get_attribute($this->env, $this->source, $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable"), "getOutputChannel", [], "method", false, false, false, 52));
            // line 53
            echo "                                    ";
            $context["elements"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable");
            // line 54
            echo "                                    ";
            if ((isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new RuntimeError('Variable "elements" does not exist.', 54, $this->source); })())) {
                // line 55
                echo "                                        ";
                $context["productCount"] = twig_length_filter($this->env, (isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new RuntimeError('Variable "elements" does not exist.', 55, $this->source); })()));
                // line 56
                echo "                                        ";
                if (((isset($context["productCount"]) || array_key_exists("productCount", $context) ? $context["productCount"] : (function () { throw new RuntimeError('Variable "productCount" does not exist.', 56, $this->source); })()) > 0)) {
                    // line 57
                    echo "                                        <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                           <tr>
                                                <td style=\"border: 0;\"></td>
                                                ";
                    // line 60
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new RuntimeError('Variable "elements" does not exist.', 60, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                        // line 61
                        echo "                                                <td class=\"column_1\">
                                                   <div class=\"img-placeholder\">
                                                        <img src=\"";
                        // line 63
                        echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 63)), "html", null, true);
                        echo "\" />   
                                                   </div>
                                                   <div class=\"product-content\">
                                                        <h2>";
                        // line 66
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 66), "html", null, true);
                        echo "</h2>
                                                        <p class=\"description\">";
                        // line 67
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 67), "html", null, true);
                        echo "</p>
                                                        ";
                        // line 68
                        $context["currency"] = twig_get_attribute($this->env, $this->source, $context["element"], "getCurrency", [], "method", false, false, false, 68);
                        // line 69
                        echo "                                                        ";
                        $context["defaultCurrency"] = "\$";
                        // line 70
                        echo "                                                        ";
                        if ((isset($context["currency"]) || array_key_exists("currency", $context) ? $context["currency"] : (function () { throw new RuntimeError('Variable "currency" does not exist.', 70, $this->source); })())) {
                            // line 71
                            echo "                                                        ";
                            $context["expldCurrency"] = twig_split_filter($this->env, (isset($context["currency"]) || array_key_exists("currency", $context) ? $context["currency"] : (function () { throw new RuntimeError('Variable "currency" does not exist.', 71, $this->source); })()), "-");
                            // line 72
                            echo "                                                        ";
                            // line 73
                            echo "                                                        ";
                            $context["defaultCurrency"] = (((twig_get_attribute($this->env, $this->source, (isset($context["expldCurrency"]) || array_key_exists("expldCurrency", $context) ? $context["expldCurrency"] : (function () { throw new RuntimeError('Variable "expldCurrency" does not exist.', 73, $this->source); })()), 1, [], "array", false, false, false, 73) != "")) ? (twig_get_attribute($this->env, $this->source, (isset($context["expldCurrency"]) || array_key_exists("expldCurrency", $context) ? $context["expldCurrency"] : (function () { throw new RuntimeError('Variable "expldCurrency" does not exist.', 73, $this->source); })()), 1, [], "array", false, false, false, 73)) : ((isset($context["defaultCurrency"]) || array_key_exists("defaultCurrency", $context) ? $context["defaultCurrency"] : (function () { throw new RuntimeError('Variable "defaultCurrency" does not exist.', 73, $this->source); })())));
                            // line 74
                            echo "                                                        ";
                        }
                        // line 75
                        echo "                                                        <p class=\"price\">";
                        echo (isset($context["defaultCurrency"]) || array_key_exists("defaultCurrency", $context) ? $context["defaultCurrency"] : (function () { throw new RuntimeError('Variable "defaultCurrency" does not exist.', 75, $this->source); })());
                        echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 75)), "html", null, true);
                        echo "</p>
                                                   </div>
                                                </td>
                                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 79
                    echo "                                            </tr>
                                            ";
                    // line 80
                    $context["productAttrList"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getProductCompareResult((isset($context["elements"]) || array_key_exists("elements", $context) ? $context["elements"] : (function () { throw new RuntimeError('Variable "elements" does not exist.', 80, $this->source); })()));
                    // line 81
                    echo "                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["productAttrList"]) || array_key_exists("productAttrList", $context) ? $context["productAttrList"] : (function () { throw new RuntimeError('Variable "productAttrList" does not exist.', 81, $this->source); })()));
                    foreach ($context['_seq'] as $context["title"] => $context["products"]) {
                        // line 82
                        echo "                                            <tr>
                                                <td style=\"text-align: left\">";
                        // line 83
                        echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                        echo "</td>
                                                ";
                        // line 84
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["products"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                            // line 85
                            echo "                                                <td style=\"text-align: left\">";
                            echo $context["value"];
                            echo "</td>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 87
                        echo "                                            </tr>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['title'], $context['products'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 89
                    echo "                                        </table>
                                        ";
                }
                // line 91
                echo "                                    ";
            }
            // line 92
            echo "                                </td>
                            </tr>
                            ";
        }
        // line 95
        echo "                        </table>
                        
                    </div>
                </td>
            </tr>
        </tbody>
</table>
 </div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-product-comparision:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 95,  218 => 92,  215 => 91,  211 => 89,  204 => 87,  195 => 85,  191 => 84,  187 => 83,  184 => 82,  179 => 81,  177 => 80,  174 => 79,  162 => 75,  159 => 74,  156 => 73,  154 => 72,  151 => 71,  148 => 70,  145 => 69,  143 => 68,  139 => 67,  135 => 66,  129 => 63,  125 => 61,  121 => 60,  116 => 57,  113 => 56,  110 => 55,  107 => 54,  104 => 53,  102 => 52,  98 => 50,  90 => 44,  88 => 37,  83 => 34,  81 => 33,  76 => 31,  60 => 17,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
       .table-pro-compare .img-placeholder img {
            width: 150px;
        }

        .table-pro-compare .img-placeholder {
            border: 1px solid;
            padding: 2px;
        }
</style>
<div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                {#<img   style=\"width: 100%;  position: absolute; left:0; bottom:0;\" src=\"/static/print/images/vertical-logo.png\">#}
            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        
                        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
                            <tr>
                                <td><h2>{{ pimcore_input(\"product-compare-title\") }}</h2></td>
                            </tr>
                            {%  if editmode %}
                             <tr>
                                 <td>
                                    <div class=\"outPutconfig\">
                                        {{
                                            pimcore_outputchanneltable('productCompareTable', {
                                                'disableClassSelection': true,
                                                'disableFavoriteOutputChannel': true,
                                                'reload':true,
                                                'selectedClass':'Product'
                                            })
                                        }}
                                        <strong style=\"color:red\"><span>NOTE:</span><span>Please select minimum 2 and maximum 3 product </span></strong>
                                    </div>
                                  </td>
                             </tr>
                            {% else %}
                            <tr>
                                <td>
                                    {% set configArray = output_channel_build_output_data_config(pimcore_outputchanneltable('productCompareTable').getOutputChannel()) %}
                                    {% set elements = pimcore_outputchanneltable('productCompareTable') %}
                                    {% if elements %}
                                        {% set productCount = (elements|length) %}
                                        {% if productCount>0 %}
                                        <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                           <tr>
                                                <td style=\"border: 0;\"></td>
                                                {% for element in elements %}
                                                <td class=\"column_1\">
                                                   <div class=\"img-placeholder\">
                                                        <img src=\"{{pimcore_generate_image_url(element.getImage())}}\" />   
                                                   </div>
                                                   <div class=\"product-content\">
                                                        <h2>{{element.getProductName() }}</h2>
                                                        <p class=\"description\">{{element.getModel() }}</p>
                                                        {% set currency=element.getCurrency() %}
                                                        {% set defaultCurrency='\$' %}
                                                        {% if currency %}
                                                        {% set expldCurrency = currency|split('-') %}
                                                        {#{{dump(expldCurrency)}}#}
                                                        {% set defaultCurrency= expldCurrency[1]!=''?expldCurrency[1]:defaultCurrency %}
                                                        {% endif %}
                                                        <p class=\"price\">{{ defaultCurrency|raw }}{{element.getListPrice()|round }}</p>
                                                   </div>
                                                </td>
                                                {% endfor %}
                                            </tr>
                                            {% set productAttrList=pimcore_get_compare_result(elements) %}
                                            {% for title,products in productAttrList %}
                                            <tr>
                                                <td style=\"text-align: left\">{{ title }}</td>
                                                {% for value in products %}
                                                <td style=\"text-align: left\">{{value|raw}}</td>
                                                {% endfor %}
                                            </tr>
                                            {% endfor %}
                                        </table>
                                        {% endif %}
                                    {% endif %}
                                </td>
                            </tr>
                            {% endif %}
                        </table>
                        
                    </div>
                </td>
            </tr>
        </tbody>
</table>
 </div>
", ":Areas/print-product-comparision:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-comparision/view.html.twig");
    }
}
