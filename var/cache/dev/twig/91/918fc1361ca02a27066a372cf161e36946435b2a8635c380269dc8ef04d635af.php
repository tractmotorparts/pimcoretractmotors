<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:layout.html.php */
class __TwigTemplate_f68fb5117d81d11a34b029ff1175589eda0f86e255c79b621d9e467c31e339ac extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:layout.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:layout.html.php"));

        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
/** @var \\Pimcore\\Config \$config */

\$config = \$this->config;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Pimcore!</title>

    <meta charset=\"UTF-8\">
    <meta name=\"robots\" content=\"noindex, follow\"/>

    <link rel=\"icon\" type=\"image/png\" href=\"/bundles/pimcoreadmin/img/favicon/favicon-32x32.png\"/>

    <link rel=\"stylesheet\" href=\"/bundles/pimcoreadmin/css/login.css\" type=\"text/css\"/>

    <?php foreach (\$this->pluginCssPaths as \$pluginCssPath): ?>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"<?= \$pluginCssPath ?>?_dc=<?= time(); ?>\"/>
    <?php endforeach; ?>
</head>
<body class=\"pimcore_version_6 <?= \$config['branding']['login_screen_invert_colors'] ? 'inverted' : '' ?>\">

<?php
    if (empty(\$backgroundImageUrl = \$config['branding']['login_screen_custom_image'])) {
        \$defaultImages = ['pimconaut-ecommerce.svg', 'pimconaut-world.svg', 'pimconaut-engineer.svg', 'pimconaut-moon.svg', 'pimconaut-rocket.svg'];
        \$backgroundImageUrl = '/bundles/pimcoreadmin/img/login/' . \$defaultImages[array_rand(\$defaultImages)];
    }
?>

    <style type=\"text/css\">
        #background {
            background-image: url(\"<?= \$backgroundImageUrl ?>\");
        }
    </style>

<?php if (!empty(\$customColor = \$config['branding']['color_login_screen'])) { ?>
    <style type=\"text/css\">
        #content button {
            background: <?= \$customColor ?>;
        }

        #content a {
            color: <?= \$customColor ?>;
        }
    </style>
<?php } ?>

<div id=\"logo\">
    <img src=\"<?=\$view->router()->path('pimcore_settings_display_custom_logo')?><?= \$config['branding']['login_screen_invert_colors'] ? '' : '?white=true' ?>\">
</div>

<div id=\"content\">
    <?php \$view->slots()->output('_content') ?>
</div>

<?php /*
<div id=\"news\">
    <h2>News</h2>
    <hr>
    <p>
        <a href=\"#\">Where is Master Data Management Heading in the Future?</a>
    </p>
    <hr>
    <p>
        <a href=\"#\">Priint and Pimcore announce technology partnership to ease publishing workflows</a>
    </p>
</div>
 */ ?>

<div id=\"contentBackground\"></div>
<div id=\"background\"></div>
<div id=\"footer\">
    &copy; 2009-<?= date(\"Y\") ?> <a href=\"http://www.pimcore.org/\">Pimcore GmbH</a><br>
    BE RESPECTFUL AND HONOR OUR WORK FOR FREE & OPEN SOURCE SOFTWARE BY NOT REMOVING OUR COPYRIGHT NOTICE!
    KEEP IN MIND THAT REMOVING THE COPYRIGHT NOTICE IS VIOLATING OUR LICENSING TERMS!
</div>

<script type=\"text/javascript\" src=\"https://liveupdate.pimcore.org/imageservice\"></script>

<?php \$view->slots()->output('below_footer') ?>

</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:layout.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
/** @var \\Pimcore\\Config \$config */

\$config = \$this->config;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Pimcore!</title>

    <meta charset=\"UTF-8\">
    <meta name=\"robots\" content=\"noindex, follow\"/>

    <link rel=\"icon\" type=\"image/png\" href=\"/bundles/pimcoreadmin/img/favicon/favicon-32x32.png\"/>

    <link rel=\"stylesheet\" href=\"/bundles/pimcoreadmin/css/login.css\" type=\"text/css\"/>

    <?php foreach (\$this->pluginCssPaths as \$pluginCssPath): ?>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"<?= \$pluginCssPath ?>?_dc=<?= time(); ?>\"/>
    <?php endforeach; ?>
</head>
<body class=\"pimcore_version_6 <?= \$config['branding']['login_screen_invert_colors'] ? 'inverted' : '' ?>\">

<?php
    if (empty(\$backgroundImageUrl = \$config['branding']['login_screen_custom_image'])) {
        \$defaultImages = ['pimconaut-ecommerce.svg', 'pimconaut-world.svg', 'pimconaut-engineer.svg', 'pimconaut-moon.svg', 'pimconaut-rocket.svg'];
        \$backgroundImageUrl = '/bundles/pimcoreadmin/img/login/' . \$defaultImages[array_rand(\$defaultImages)];
    }
?>

    <style type=\"text/css\">
        #background {
            background-image: url(\"<?= \$backgroundImageUrl ?>\");
        }
    </style>

<?php if (!empty(\$customColor = \$config['branding']['color_login_screen'])) { ?>
    <style type=\"text/css\">
        #content button {
            background: <?= \$customColor ?>;
        }

        #content a {
            color: <?= \$customColor ?>;
        }
    </style>
<?php } ?>

<div id=\"logo\">
    <img src=\"<?=\$view->router()->path('pimcore_settings_display_custom_logo')?><?= \$config['branding']['login_screen_invert_colors'] ? '' : '?white=true' ?>\">
</div>

<div id=\"content\">
    <?php \$view->slots()->output('_content') ?>
</div>

<?php /*
<div id=\"news\">
    <h2>News</h2>
    <hr>
    <p>
        <a href=\"#\">Where is Master Data Management Heading in the Future?</a>
    </p>
    <hr>
    <p>
        <a href=\"#\">Priint and Pimcore announce technology partnership to ease publishing workflows</a>
    </p>
</div>
 */ ?>

<div id=\"contentBackground\"></div>
<div id=\"background\"></div>
<div id=\"footer\">
    &copy; 2009-<?= date(\"Y\") ?> <a href=\"http://www.pimcore.org/\">Pimcore GmbH</a><br>
    BE RESPECTFUL AND HONOR OUR WORK FOR FREE & OPEN SOURCE SOFTWARE BY NOT REMOVING OUR COPYRIGHT NOTICE!
    KEEP IN MIND THAT REMOVING THE COPYRIGHT NOTICE IS VIOLATING OUR LICENSING TERMS!
</div>

<script type=\"text/javascript\" src=\"https://liveupdate.pimcore.org/imageservice\"></script>

<?php \$view->slots()->output('below_footer') ?>

</body>
</html>
", "PimcoreAdminBundle:Admin/Login:layout.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/layout.html.php");
    }
}
