<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:deeplink.html.php */
class __TwigTemplate_4a4aaadb156f065153a37e5c7237c231aa27c8485c88ba5be9d135cd5f6e9eb7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:deeplink.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Login:deeplink.html.php"));

        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>

<!DOCTYPE html>
<html>
<head>
    <?php
    \$redirect = \$view->router()->path('pimcore_admin_login', [
        'deeplink' => 'true',
        'perspective' => \$this->perspective
    ]);
    ?>

    <script src=\"/bundles/pimcoreadmin/js/pimcore/common.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/functions.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/helpers.js\"></script>
    <script>
        <?php if (\$this->tab) { ?>
            pimcore.helpers.clearOpenTab();
            pimcore.helpers.rememberOpenTab(\"<?= \$this->tab ?>\", true);
        <?php } ?>
        window.location.href = \"<?= \$redirect ?>\";
    </script>
</head>
<body>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:deeplink.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>

<!DOCTYPE html>
<html>
<head>
    <?php
    \$redirect = \$view->router()->path('pimcore_admin_login', [
        'deeplink' => 'true',
        'perspective' => \$this->perspective
    ]);
    ?>

    <script src=\"/bundles/pimcoreadmin/js/pimcore/common.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/functions.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/helpers.js\"></script>
    <script>
        <?php if (\$this->tab) { ?>
            pimcore.helpers.clearOpenTab();
            pimcore.helpers.rememberOpenTab(\"<?= \$this->tab ?>\", true);
        <?php } ?>
        window.location.href = \"<?= \$redirect ?>\";
    </script>
</head>
<body>
</body>
</html>
", "PimcoreAdminBundle:Admin/Login:deeplink.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/deeplink.html.php");
    }
}
