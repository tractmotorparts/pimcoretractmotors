<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Areabrick:wrapper.html.twig */
class __TwigTemplate_d39996e578adb9fd1b946a87bd3f9e189709e4106c3946e593bde51d2f609edc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'areabrickWrapper' => [$this, 'block_areabrickWrapper'],
            'areabrickOpenTag' => [$this, 'block_areabrickOpenTag'],
            'areabrickFrontend' => [$this, 'block_areabrickFrontend'],
            'areabrickCloseTag' => [$this, 'block_areabrickCloseTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Areabrick:wrapper.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Areabrick:wrapper.html.twig"));

        // line 4
        echo "
";
        // line 6
        echo "
";
        // line 9
        echo "
";
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('areabrickWrapper', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_areabrickWrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickWrapper"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickWrapper"));

        // line 14
        echo "    ";
        $this->displayBlock('areabrickOpenTag', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        if ((twig_get_attribute($this->env, $this->source, (isset($context["brick"]) || array_key_exists("brick", $context) ? $context["brick"] : (function () { throw new RuntimeError('Variable "brick" does not exist.', 18, $this->source); })()), "hasEditTemplate", [], "method", false, false, false, 18) && (isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 18, $this->source); })()))) {
            // line 19
            echo "
            <div class=\"pimcore_area_edit_button\" data-name=\"";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 20, $this->source); })()), "tag", [], "any", false, false, false, 20), "name", [], "any", false, false, false, 20), "html", null, true);
            echo "\" data-real-name=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 20, $this->source); })()), "tag", [], "any", false, false, false, 20), "realName", [], "any", false, false, false, 20), "html", null, true);
            echo "\"></div>

        ";
        }
        // line 23
        echo "
        ";
        // line 24
        $this->displayBlock('areabrickFrontend', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        if (((twig_get_attribute($this->env, $this->source, (isset($context["brick"]) || array_key_exists("brick", $context) ? $context["brick"] : (function () { throw new RuntimeError('Variable "brick" does not exist.', 28, $this->source); })()), "hasEditTemplate", [], "method", false, false, false, 28) && (isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 28, $this->source); })())) && (isset($context["editTemplate"]) || array_key_exists("editTemplate", $context) ? $context["editTemplate"] : (function () { throw new RuntimeError('Variable "editTemplate" does not exist.', 28, $this->source); })()))) {
            // line 29
            echo "
            <div class=\"pimcore_area_editmode pimcore_area_editmode_hidden\" data-name=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 30, $this->source); })()), "tag", [], "any", false, false, false, 30), "name", [], "any", false, false, false, 30), "html", null, true);
            echo "\" data-real-name=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 30, $this->source); })()), "tag", [], "any", false, false, false, 30), "realName", [], "any", false, false, false, 30), "html", null, true);
            echo "\">
                ";
            // line 31
            echo twig_get_attribute($this->env, $this->source, (isset($context["templating"]) || array_key_exists("templating", $context) ? $context["templating"] : (function () { throw new RuntimeError('Variable "templating" does not exist.', 31, $this->source); })()), "render", [0 => (isset($context["editTemplate"]) || array_key_exists("editTemplate", $context) ? $context["editTemplate"] : (function () { throw new RuntimeError('Variable "editTemplate" does not exist.', 31, $this->source); })()), 1 => (isset($context["editParameters"]) || array_key_exists("editParameters", $context) ? $context["editParameters"] : (function () { throw new RuntimeError('Variable "editParameters" does not exist.', 31, $this->source); })())], "method", false, false, false, 31);
            echo "
            </div>

        ";
        }
        // line 35
        echo "
    ";
        // line 36
        $this->displayBlock('areabrickCloseTag', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_areabrickOpenTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickOpenTag"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickOpenTag"));

        // line 15
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, (isset($context["brick"]) || array_key_exists("brick", $context) ? $context["brick"] : (function () { throw new RuntimeError('Variable "brick" does not exist.', 15, $this->source); })()), "htmlTagOpen", [0 => (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 15, $this->source); })())], "method", false, false, false, 15);
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_areabrickFrontend($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickFrontend"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickFrontend"));

        // line 25
        echo "            ";
        echo twig_get_attribute($this->env, $this->source, (isset($context["templating"]) || array_key_exists("templating", $context) ? $context["templating"] : (function () { throw new RuntimeError('Variable "templating" does not exist.', 25, $this->source); })()), "render", [0 => (isset($context["viewTemplate"]) || array_key_exists("viewTemplate", $context) ? $context["viewTemplate"] : (function () { throw new RuntimeError('Variable "viewTemplate" does not exist.', 25, $this->source); })()), 1 => (isset($context["viewParameters"]) || array_key_exists("viewParameters", $context) ? $context["viewParameters"] : (function () { throw new RuntimeError('Variable "viewParameters" does not exist.', 25, $this->source); })())], "method", false, false, false, 25);
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 36
    public function block_areabrickCloseTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickCloseTag"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "areabrickCloseTag"));

        // line 37
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, (isset($context["brick"]) || array_key_exists("brick", $context) ? $context["brick"] : (function () { throw new RuntimeError('Variable "brick" does not exist.', 37, $this->source); })()), "htmlTagClose", [0 => (isset($context["info"]) || array_key_exists("info", $context) ? $context["info"] : (function () { throw new RuntimeError('Variable "info" does not exist.', 37, $this->source); })())], "method", false, false, false, 37);
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Areabrick:wrapper.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  192 => 37,  182 => 36,  169 => 25,  159 => 24,  146 => 15,  136 => 14,  126 => 36,  123 => 35,  116 => 31,  110 => 30,  107 => 29,  105 => 28,  102 => 27,  100 => 24,  97 => 23,  89 => 20,  86 => 19,  84 => 18,  81 => 17,  78 => 14,  59 => 13,  56 => 12,  53 => 9,  50 => 6,  47 => 4,);
    }

    public function getSourceContext()
    {
        return new Source("{# @var brick \\Pimcore\\Extension\\Document\\Areabrick\\AreabrickInterface #}
{# @var info \\Pimcore\\Model\\Document\\Tag\\Area\\Info #}
{# @var templating \\Symfony\\Component\\Templating\\EngineInterface #}

{# @var editmode bool #}

{# @var viewTemplate string #}
{# @var viewParameters array #}

{# @var editTemplate string #}
{# @var editParameters array #}

{% block areabrickWrapper %}
    {% block areabrickOpenTag %}
        {{ brick.htmlTagOpen(info) | raw }}
    {% endblock %}

        {% if brick.hasEditTemplate() and editmode %}

            <div class=\"pimcore_area_edit_button\" data-name=\"{{ info.tag.name }}\" data-real-name=\"{{ info.tag.realName }}\"></div>

        {% endif %}

        {% block areabrickFrontend %}
            {{ templating.render(viewTemplate, viewParameters) | raw }}
        {% endblock %}

        {% if brick.hasEditTemplate() and editmode and editTemplate %}

            <div class=\"pimcore_area_editmode pimcore_area_editmode_hidden\" data-name=\"{{ info.tag.name }}\" data-real-name=\"{{ info.tag.realName }}\">
                {{ templating.render(editTemplate, editParameters) | raw }}
            </div>

        {% endif %}

    {% block areabrickCloseTag %}
        {{ brick.htmlTagClose(info) | raw }}
    {% endblock %}
{% endblock %}", "PimcoreCoreBundle:Areabrick:wrapper.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Areabrick/wrapper.html.twig");
    }
}
