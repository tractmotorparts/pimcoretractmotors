<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:content_list.html.twig */
class __TwigTemplate_d94c74beda6988ed572e37c1e2cfef9d6a3259d91be6cf74ee7580c57e6b1b3e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":web2print:content_list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":web2print:content_list.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Tract Motors Content Listing Page</title>
</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap');
    *{box-sizing:border-box;}
    body{margin:0;padding:0;background:white;font-family:'Open Sans', sans-serif, Arial, Helvetica, sans-serif;font-size:15pt;}
    .inner-container{max-width:80%;margin:auto}
    .red-band{height:95px;background:#cf0101;width:60%}
    .content{margin-top:95px;}
     h1{font-size:45px;}
    .bottom-container{position:absolute;bottom:0;left:0;width:100%}
    .bottom-container .red-band{height:60px;}
    .content ul{padding:0;}
    .content ul li{list-style:none;margin-bottom:10px;font-size:25px;}
    .content ul li span{margin-right:28px}
</style>

<body>

<section class=\"top-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

<section class=\"content\">
    <div class=\"inner-container\">
        <h1>Contents</h1>
        <ul>
            ";
        // line 38
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "areablock", "content", ["allowed" => [0 => "print-catalog-content-list-title"]]);
        // line 45
        echo "
        </ul>
    </div>
</section>

<section class=\"bottom-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

</body>

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":web2print:content_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 45,  82 => 38,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Tract Motors Content Listing Page</title>
</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap');
    *{box-sizing:border-box;}
    body{margin:0;padding:0;background:white;font-family:'Open Sans', sans-serif, Arial, Helvetica, sans-serif;font-size:15pt;}
    .inner-container{max-width:80%;margin:auto}
    .red-band{height:95px;background:#cf0101;width:60%}
    .content{margin-top:95px;}
     h1{font-size:45px;}
    .bottom-container{position:absolute;bottom:0;left:0;width:100%}
    .bottom-container .red-band{height:60px;}
    .content ul{padding:0;}
    .content ul li{list-style:none;margin-bottom:10px;font-size:25px;}
    .content ul li span{margin-right:28px}
</style>

<body>

<section class=\"top-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

<section class=\"content\">
    <div class=\"inner-container\">
        <h1>Contents</h1>
        <ul>
            {{
                pimcore_areablock('content', {
                    'allowed': [
                        'print-catalog-content-list-title',
                        
                    ]
                })
            }}
        </ul>
    </div>
</section>

<section class=\"bottom-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

</body>

</html>
", ":web2print:content_list.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/content_list.html.twig");
    }
}
