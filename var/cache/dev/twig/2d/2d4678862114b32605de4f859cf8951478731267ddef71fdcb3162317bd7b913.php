<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Asset:showVersionImage.html.php */
class __TwigTemplate_dfa2b76fc26da4ab9342a56b88ded195d13b9b07967bb5a40142b5e5d63929a1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Asset:showVersionImage.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:Admin/Asset:showVersionImage.html.php"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">

        html, body, #wrapper {
            height: 100%;
            margin: 0;
            padding: 0;
            border: none;
            text-align: center;
        }

        #wrapper {
            margin: 0 auto;
            text-align: left;
            vertical-align: middle;
            width: 400px;
        }


    </style>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/pimcoreadmin/css/object_versions.css\"/>

</head>

<body>

<?php
    \$thumbnail = PIMCORE_SYSTEM_TEMP_DIRECTORY . \"/image-version-preview-\" . uniqid() . \".png\";
    \$convert = \\Pimcore\\Image::getInstance();
    \$tempFile = \$this->asset->getTemporaryFile();
    \$convert->load(\$tempFile);
    \$convert->contain(500,500);
    \$convert->save(\$thumbnail, \"png\");

    \$dataUri = \"data:image/png;base64,\" . base64_encode(file_get_contents(\$thumbnail));
    unlink(\$thumbnail);
    unlink(\$tempFile);

use Pimcore\\Model\\Asset\\MetaData\\ClassDefinition\\Data\\Data; ?>

<table id=\"wrapper\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
    <tr>
        <td align=\"center\">
            <img src=\"<?= \$dataUri ?>\"/>
              <table class=\"preview\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tbody>
                            <tr class=\"odd\">
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?php echo \$this->asset->getFileName(); ?></td>
                            </tr>
                            <tr>
                                <td>Creation Date</td>
                                <td><?php echo date('m/d/Y H:i:s', \$this->asset->getCreationDate()); ?></td>
                            </tr>
                            <tr>
                                <td>Modification Date</td>
                                <td><?php echo date('m/d/Y H:i:s', \$this->asset->getModificationDate()); ?></td>
                            </tr>
                            <tr>
                                <td>File Size</td>
                                <td><?php echo \$this->asset->getFileSize(true); ?> </td>
                            </tr>
                            <tr>
                                <td>Mime Type</td>
                                <td><?php echo \$this->asset->getMimetype(); ?></td>
                            </tr>
                            <tr>
                                <td>Dimensions</td>
                                <td><?php
                                    if (is_array(\$this->asset->getDimensions())) {
                                        echo \$this->asset->getDimensions()[\"width\"] . \" X \" . \$this->asset->getDimensions()[\"height\"];
                                    }
                                    ?></td>
                            </tr>
                            <?php
                            if (\$this->asset->getHasMetadata()) {
                                ?>
                                <?php
                                \$metaData = \$this->asset->getMetadata();

                                \$loader = \\Pimcore::getContainer()->get('pimcore.implementation_loader.asset.metadata.data');


                                if (is_array(\$metaData) && count(\$metaData) > 0) {
                                    foreach (\$metaData as \$data) {
                                        \$preview = \$data[\"data\"];
                                        try {
                                            /** @var Data \$instance */
                                            \$instance = \$loader->build(\$data['type']);
                                            \$preview = \$instance->getVersionPreview(\$preview, \$data);
                                        } catch (\\Pimcore\\Loader\\ImplementationLoader\\Exception\\UnsupportedException \$e) {

                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo \$data['name']; ?>
                                                (<?php echo \$data['type']; ?>)
                                            </td>
                                            <td><?php echo \$preview; ?>
                                            </td>
                                            <?php ?>
                                        </tr>

                                        <?php
                                    }
                                }
                                ?>
                            <?php }
                            ?>
                        </tbody>
                    </table>

        </td>
    </tr>
</table>


</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Asset:showVersionImage.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">

        html, body, #wrapper {
            height: 100%;
            margin: 0;
            padding: 0;
            border: none;
            text-align: center;
        }

        #wrapper {
            margin: 0 auto;
            text-align: left;
            vertical-align: middle;
            width: 400px;
        }


    </style>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/pimcoreadmin/css/object_versions.css\"/>

</head>

<body>

<?php
    \$thumbnail = PIMCORE_SYSTEM_TEMP_DIRECTORY . \"/image-version-preview-\" . uniqid() . \".png\";
    \$convert = \\Pimcore\\Image::getInstance();
    \$tempFile = \$this->asset->getTemporaryFile();
    \$convert->load(\$tempFile);
    \$convert->contain(500,500);
    \$convert->save(\$thumbnail, \"png\");

    \$dataUri = \"data:image/png;base64,\" . base64_encode(file_get_contents(\$thumbnail));
    unlink(\$thumbnail);
    unlink(\$tempFile);

use Pimcore\\Model\\Asset\\MetaData\\ClassDefinition\\Data\\Data; ?>

<table id=\"wrapper\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
    <tr>
        <td align=\"center\">
            <img src=\"<?= \$dataUri ?>\"/>
              <table class=\"preview\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tbody>
                            <tr class=\"odd\">
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?php echo \$this->asset->getFileName(); ?></td>
                            </tr>
                            <tr>
                                <td>Creation Date</td>
                                <td><?php echo date('m/d/Y H:i:s', \$this->asset->getCreationDate()); ?></td>
                            </tr>
                            <tr>
                                <td>Modification Date</td>
                                <td><?php echo date('m/d/Y H:i:s', \$this->asset->getModificationDate()); ?></td>
                            </tr>
                            <tr>
                                <td>File Size</td>
                                <td><?php echo \$this->asset->getFileSize(true); ?> </td>
                            </tr>
                            <tr>
                                <td>Mime Type</td>
                                <td><?php echo \$this->asset->getMimetype(); ?></td>
                            </tr>
                            <tr>
                                <td>Dimensions</td>
                                <td><?php
                                    if (is_array(\$this->asset->getDimensions())) {
                                        echo \$this->asset->getDimensions()[\"width\"] . \" X \" . \$this->asset->getDimensions()[\"height\"];
                                    }
                                    ?></td>
                            </tr>
                            <?php
                            if (\$this->asset->getHasMetadata()) {
                                ?>
                                <?php
                                \$metaData = \$this->asset->getMetadata();

                                \$loader = \\Pimcore::getContainer()->get('pimcore.implementation_loader.asset.metadata.data');


                                if (is_array(\$metaData) && count(\$metaData) > 0) {
                                    foreach (\$metaData as \$data) {
                                        \$preview = \$data[\"data\"];
                                        try {
                                            /** @var Data \$instance */
                                            \$instance = \$loader->build(\$data['type']);
                                            \$preview = \$instance->getVersionPreview(\$preview, \$data);
                                        } catch (\\Pimcore\\Loader\\ImplementationLoader\\Exception\\UnsupportedException \$e) {

                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo \$data['name']; ?>
                                                (<?php echo \$data['type']; ?>)
                                            </td>
                                            <td><?php echo \$preview; ?>
                                            </td>
                                            <?php ?>
                                        </tr>

                                        <?php
                                    }
                                }
                                ?>
                            <?php }
                            ?>
                        </tbody>
                    </table>

        </td>
    </tr>
</table>


</body>
</html>
", "PimcoreAdminBundle:Admin/Asset:showVersionImage.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Asset/showVersionImage.html.php");
    }
}
