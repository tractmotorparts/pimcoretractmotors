<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php */
class __TwigTemplate_427bc6a32e1a4bc52f68002b5d055114609a2067774dccf7b97125d958b30c08 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php"));

        // line 1
        echo "<?php
/**
 * @var \\Pimcore\\Model\\Document\\Page \$element
 */
\$element = \$this->element;
\$previewImage = null;
if (\$element instanceof \\Pimcore\\Model\\Document\\Page && \$this->config['documents']['generate_preview']) {
    \$thumbnailFileHdpi = \$element->getPreviewImageFilesystemPath(true);
    if (file_exists(\$thumbnailFileHdpi)) {
        \$previewImage = \$this->path('pimcore_admin_page_display_preview_image', ['id' => \$element->getId(), 'hdpi' => true]);
    }

}

?>

<?php if(\$previewImage) {?>
    <div class=\"full-preview\">
        <img src=\"<?= \$previewImage ?>\" onload=\"this.parentNode.className += ' complete';\">
        <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
    </div>
<?php } else { ?>
    <div class=\"mega-icon <?= \$this->iconCls ?>\"></div>
    <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
<?php } ?>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php
/**
 * @var \\Pimcore\\Model\\Document\\Page \$element
 */
\$element = \$this->element;
\$previewImage = null;
if (\$element instanceof \\Pimcore\\Model\\Document\\Page && \$this->config['documents']['generate_preview']) {
    \$thumbnailFileHdpi = \$element->getPreviewImageFilesystemPath(true);
    if (file_exists(\$thumbnailFileHdpi)) {
        \$previewImage = \$this->path('pimcore_admin_page_display_preview_image', ['id' => \$element->getId(), 'hdpi' => true]);
    }

}

?>

<?php if(\$previewImage) {?>
    <div class=\"full-preview\">
        <img src=\"<?= \$previewImage ?>\" onload=\"this.parentNode.className += ' complete';\">
        <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
    </div>
<?php } else { ?>
    <div class=\"mega-icon <?= \$this->iconCls ?>\"></div>
    <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
<?php } ?>

", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/SearchAdmin/Search/Quicksearch/document.html.php");
    }
}
