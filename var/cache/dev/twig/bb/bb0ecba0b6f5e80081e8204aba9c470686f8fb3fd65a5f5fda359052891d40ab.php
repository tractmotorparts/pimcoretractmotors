<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-single-product:view.html.twig */
class __TwigTemplate_23b0add45e53a762d0805c02efc69167498a76675dd1bccc3f34e3620fb479ea extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-single-product:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-single-product:view.html.twig"));

        // line 1
        echo "<style>
.product-list-price p {
    color: #333;
    font-size: 1.5em;
    padding-left: 15px;
    line-height: 23px;
    margin-top: 18px;
    font-weight: 600;
}
.product-list-price {
    display: flex;
    align-items: center;
}
</style>
        <div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                ";
        // line 21
        echo "            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        ";
        // line 32
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "renderlet", "product1", ["controller" => "web2print", "action" => "product", "className" => [0 => "Product"], "editmode" => true, "type" => "object", "title" => "Drag a product here", "reload" => true, "height" => 1000]);
        // line 43
        echo "
                     </div>
                </td>
            </tr>
        </tbody>
    </table>
        </div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-single-product:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 43,  77 => 32,  64 => 21,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
.product-list-price p {
    color: #333;
    font-size: 1.5em;
    padding-left: 15px;
    line-height: 23px;
    margin-top: 18px;
    font-weight: 600;
}
.product-list-price {
    display: flex;
    align-items: center;
}
</style>
        <div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                {#<img   style=\"width: 100%;  position: absolute; left:0; bottom:0;\" src=\"/static/print/images/vertical-logo.png\">#}
            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        {{
                            pimcore_renderlet('product1', {
                                \"controller\" : \"web2print\",
                                \"action\" : \"product\",
                                'className': ['Product'],
                                'editmode': true,
                                'type':'object',
                                \"title\" : \"Drag a product here\",
                                \"reload\":true,
                                \"height\" :1000
                            })
                        }}
                     </div>
                </td>
            </tr>
        </tbody>
    </table>
        </div>
", ":Areas/print-single-product:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-single-product/view.html.twig");
    }
}
