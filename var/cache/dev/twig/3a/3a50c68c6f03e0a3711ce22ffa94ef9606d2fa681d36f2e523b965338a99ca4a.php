<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-catalog-cover:view.html.twig */
class __TwigTemplate_564fdc368e4153a6d310cc83a5e9e3d7c0fc9b387c9b425bf998bd6f36b994ec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-catalog-cover:view.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":Areas/print-product-catalog-cover:view.html.twig"));

        // line 1
        echo "<style>
   @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
        }
        @media print {
            .bg-img img {
                position: fixed !important;
                left: 0;
                top: 0;
                width: 100%;
            }
        }
</style>
";
        // line 24
        if ((isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 24, $this->source); })())) {
            // line 25
            echo "    ";
        }
        // line 30
        echo "<div class=\"cover-page-wrap\">
    <table border=\"0\" style=\"width: 100%; border: 0; border-spacing: 0;\">
        <tr>
            <td style=\"height: 100vh;
                background-repeat: no-repeat;
                background-size: cover;
                position: relative;
                background-position: right;\">
                <div class=\"bg-img\">
                    ";
        // line 40
        echo "                    ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "backgroud-cover-image");
        echo "
                </div>
               <table class=\"title-table\">
                   <tr><td><h1 class=\"heading\"> ";
        // line 43
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "cover-page-title", ["placeholder" => "Enter Title 1"]);
        echo "<br><span> ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "cover-page-subtitle", ["placeholder" => "Enter Title 2"]);
        echo "</span></h1></td></tr>
                   <tr><td>
                           ";
        // line 45
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "backgroud-cover-logo", ["class" => "company-logo", "style" => "width:400px"]);
        echo "
                           ";
        // line 47
        echo "                       </td>
                   </tr>
               </table>               
            </td>
        </tr>
    </table>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":Areas/print-product-catalog-cover:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 47,  98 => 45,  91 => 43,  84 => 40,  73 => 30,  70 => 25,  68 => 24,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
   @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
        }
        @media print {
            .bg-img img {
                position: fixed !important;
                left: 0;
                top: 0;
                width: 100%;
            }
        }
</style>
{% if editmode %}
    {#<style>
        .heading{top: 170px}
        #pimcore_editable_content_1_backgroud-cover-logo{top:280px;}
    </style>#}
{% endif %}
<div class=\"cover-page-wrap\">
    <table border=\"0\" style=\"width: 100%; border: 0; border-spacing: 0;\">
        <tr>
            <td style=\"height: 100vh;
                background-repeat: no-repeat;
                background-size: cover;
                position: relative;
                background-position: right;\">
                <div class=\"bg-img\">
                    {#<img src=\"https://i.ibb.co/hWkrQ4j/cover-img.jpg\">#}
                    {{ pimcore_image(\"backgroud-cover-image\") }}
                </div>
               <table class=\"title-table\">
                   <tr><td><h1 class=\"heading\"> {{ pimcore_input(\"cover-page-title\",{'placeholder':'Enter Title 1'}) }}<br><span> {{ pimcore_input(\"cover-page-subtitle\",{'placeholder':'Enter Title 2'}) }}</span></h1></td></tr>
                   <tr><td>
                           {{ pimcore_image(\"backgroud-cover-logo\",{\"class\":\"company-logo\",\"style\":\"width:400px\"}) }}
                           {#<img class=\"company-logo\" style=\"width: 400px;\" src=\"https://tractmotor.com.my/wp-content/uploads/2017/07/logo.png\">#}
                       </td>
                   </tr>
               </table>               
            </td>
        </tr>
    </table>
</div>
", ":Areas/print-product-catalog-cover:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-catalog-cover/view.html.twig");
    }
}
