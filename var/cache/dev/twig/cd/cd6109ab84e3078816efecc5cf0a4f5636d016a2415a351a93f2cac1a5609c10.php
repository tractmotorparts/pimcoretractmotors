<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:product.html.twig_31-08-2021 */
class __TwigTemplate_9fbae983965b1c1ef554fb8fa92bf13bef402c2da7d9cbf8956e0e55a824108e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":web2print:product.html.twig_31-08-2021"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", ":web2print:product.html.twig_31-08-2021"));

        // line 1
        if ((isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 1, $this->source); })())) {
            // line 2
            echo "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>";
            // line 4
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 4, $this->source); })()), "getProductName", [], "method", false, false, false, 4), "html", null, true);
            echo "</h2></td>
    </tr>
    <tr>
        <td>
            <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

                <tr>
                    ";
            // line 11
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 11, $this->source); })()), "getGallery", [], "method", false, false, false, 11), "getItems", [], "method", false, false, false, 11)) > 0)) {
                // line 12
                echo "                        ";
                $context["break"] = false;
                // line 13
                echo "                        ";
                $context["counter"] = 0;
                // line 14
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 14, $this->source); })()), "getGallery", [], "method", false, false, false, 14), "getItems", [], "method", false, false, false, 14));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new RuntimeError('Variable "break" does not exist.', 14, $this->source); })())) {
                        echo "   
                        
                        <td class=\"column_1\">
                           ";
                        // line 17
                        if ($context["item"]) {
                            // line 18
                            echo "                            <div class=\"img-placeholder\">
                               <img src=\"";
                            // line 19
                            echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["item"], "getImage", [], "method", false, false, false, 19)), "html", null, true);
                            echo "\" />  
                           </div>
                           ";
                        }
                        // line 22
                        echo "                        </td>
                        ";
                        // line 23
                        $context["counter"] = ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 23, $this->source); })()) + 1);
                        // line 24
                        echo "                        ";
                        if (((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 24, $this->source); })()) == 3)) {
                            // line 25
                            echo "                             ";
                            $context["break"] = true;
                            // line 26
                            echo "                        ";
                        }
                        // line 27
                        echo "                        ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 28
                echo "                    ";
            } else {
                // line 29
                echo "                    <td class=\"column_1\">
                        <div class=\"img-placeholder\">
                             <img src=\"";
                // line 31
                echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 31, $this->source); })()), "getImage", [], "method", false, false, false, 31)), "html", null, true);
                echo "\" />     
                        </div>
                     </td>
                    ";
            }
            // line 35
            echo "                </tr>

            </table>
        </td>
    </tr>
</table>
<div class=\"product-list-price\">
<h2>List Price  :</h2> 
<p>RM ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 43, $this->source); })()), "getListPrice", [], "method", false, false, false, 43), "html", null, true);
            echo "</p>

</div>
<div class=\"product-discription\">
<h2>Product Description</h2>
<p>";
            // line 48
            echo twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 48, $this->source); })()), "getDescription", [], "method", false, false, false, 48);
            echo "</p>
</div>
<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>Product Specification</h2></td>
    </tr>
    <tr>
        <td>
            <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0\">
                <tbody>
                    <tr>
                        <td>
                            <table class=\"pdf-data-table product-specification-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <tbody>
                                    ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) || array_key_exists("productList", $context) ? $context["productList"] : (function () { throw new RuntimeError('Variable "productList" does not exist.', 62, $this->source); })()));
            foreach ($context['_seq'] as $context["head"] => $context["product"]) {
                // line 63
                echo "                                        ";
                if (($context["head"] == "TechnicalSpecification")) {
                    // line 64
                    echo "                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["product"]);
                    foreach ($context['_seq'] as $context["h"] => $context["v"]) {
                        echo " 
                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">";
                        // line 66
                        echo twig_escape_filter($this->env, $context["h"], "html", null, true);
                        echo " </td>
                                                    <td style=\"width:60;text-align: left\">";
                        // line 67
                        echo $context["v"];
                        echo " </td>
                                                </tr>
                                             ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['h'], $context['v'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "                                        ";
                } else {
                    // line 71
                    echo "                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">";
                    // line 72
                    echo twig_escape_filter($this->env, $context["head"], "html", null, true);
                    echo " </td>
                                                    <td style=\"width:60%;text-align: left\">";
                    // line 73
                    echo $context["product"];
                    echo "</td>
                                                </tr>
                                        ";
                }
                // line 76
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['head'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ";
            // line 83
            $context["relatedParts"] = twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 83, $this->source); })()), "getRelatedParts", [], "method", false, false, false, 83);
            // line 84
            echo "             ";
            if ((isset($context["relatedParts"]) || array_key_exists("relatedParts", $context) ? $context["relatedParts"] : (function () { throw new RuntimeError('Variable "relatedParts" does not exist.', 84, $this->source); })())) {
                // line 85
                echo "             <section class=\"related-products\">
                <h2>Related Parts</h2>
                ";
                // line 87
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["relatedParts"]) || array_key_exists("relatedParts", $context) ? $context["relatedParts"] : (function () { throw new RuntimeError('Variable "relatedParts" does not exist.', 87, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 88
                    echo "                <div class=\"parts\">
                    <div class=\"parts-box\">
                        <img src=\"";
                    // line 90
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 90)), "html", null, true);
                    echo "\">
                        <span>
                            <p class=\"part-name\">";
                    // line 92
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 92), "html", null, true);
                    echo "</p>
                            <p class=\"part-price\">RM ";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 93), "html", null, true);
                    echo "</p>
                        </span>
                     </div>
                </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 98
                echo "            </section>
            ";
            }
            // line 100
            echo "                                
        </td>
    </tr>
</table>
";
        } elseif (        // line 104
(isset($context["editmode"]) || array_key_exists("editmode", $context) ? $context["editmode"] : (function () { throw new RuntimeError('Variable "editmode" does not exist.', 104, $this->source); })())) {
            // line 105
            echo "    <p>Drag a product here</p>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":web2print:product.html.twig_31-08-2021";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 105,  263 => 104,  257 => 100,  253 => 98,  242 => 93,  238 => 92,  233 => 90,  229 => 88,  225 => 87,  221 => 85,  218 => 84,  216 => 83,  208 => 77,  202 => 76,  196 => 73,  192 => 72,  189 => 71,  186 => 70,  177 => 67,  173 => 66,  165 => 64,  162 => 63,  158 => 62,  141 => 48,  133 => 43,  123 => 35,  116 => 31,  112 => 29,  109 => 28,  102 => 27,  99 => 26,  96 => 25,  93 => 24,  91 => 23,  88 => 22,  82 => 19,  79 => 18,  77 => 17,  67 => 14,  64 => 13,  61 => 12,  59 => 11,  49 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if product %}
<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>{{ product.getProductName() }}</h2></td>
    </tr>
    <tr>
        <td>
            <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

                <tr>
                    {% if (product.getGallery().getItems()|length) > 0 %}
                        {% set break = false %}
                        {% set counter= 0 %}
                        {% for item in product.getGallery().getItems() if not break %}   
                        
                        <td class=\"column_1\">
                           {% if item %}
                            <div class=\"img-placeholder\">
                               <img src=\"{{ pimcore_generate_image_url(item.getImage()) }}\" />  
                           </div>
                           {% endif %}
                        </td>
                        {% set counter=counter+1 %}
                        {% if counter == 3 %}
                             {% set break = true %}
                        {% endif %}
                        {% endfor %}
                    {% else %}
                    <td class=\"column_1\">
                        <div class=\"img-placeholder\">
                             <img src=\"{{pimcore_generate_image_url(product.getImage()) }}\" />     
                        </div>
                     </td>
                    {% endif %}
                </tr>

            </table>
        </td>
    </tr>
</table>
<div class=\"product-list-price\">
<h2>List Price  :</h2> 
<p>RM {{product.getListPrice() }}</p>

</div>
<div class=\"product-discription\">
<h2>Product Description</h2>
<p>{{ product.getDescription()|raw }}</p>
</div>
<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>Product Specification</h2></td>
    </tr>
    <tr>
        <td>
            <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0\">
                <tbody>
                    <tr>
                        <td>
                            <table class=\"pdf-data-table product-specification-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <tbody>
                                    {% for head,product in productList %}
                                        {% if head=='TechnicalSpecification' %}
                                            {% for h,v  in  product %} 
                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">{{ h }} </td>
                                                    <td style=\"width:60;text-align: left\">{{ v|raw }} </td>
                                                </tr>
                                             {% endfor %}
                                        {% else %}
                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">{{ head }} </td>
                                                    <td style=\"width:60%;text-align: left\">{{ product|raw }}</td>
                                                </tr>
                                        {% endif %}
                                    {% endfor %}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             {% set relatedParts=product.getRelatedParts() %}
             {% if relatedParts %}
             <section class=\"related-products\">
                <h2>Related Parts</h2>
                {% for element in relatedParts %}
                <div class=\"parts\">
                    <div class=\"parts-box\">
                        <img src=\"{{pimcore_generate_image_url(element.getImage())}}\">
                        <span>
                            <p class=\"part-name\">{{element.getProductName() }}</p>
                            <p class=\"part-price\">RM {{element.getListPrice() }}</p>
                        </span>
                     </div>
                </div>
                {% endfor %}
            </section>
            {% endif %}
                                
        </td>
    </tr>
</table>
{%  elseif editmode %}
    <p>Drag a product here</p>
{% endif %}
", ":web2print:product.html.twig_31-08-2021", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/product.html.twig_31-08-2021");
    }
}
