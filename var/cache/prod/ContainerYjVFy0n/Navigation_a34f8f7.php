<?php

include_once \dirname(__DIR__, 4).'/vendor/symfony/symfony/src/Symfony/Component/Templating/Helper/HelperInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/symfony/src/Symfony/Component/Templating/Helper/Helper.php';
include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Templating/Helper/Navigation.php';
class Navigation_a34f8f7 extends \Pimcore\Templating\Helper\Navigation implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderd73c0 = null;
    private $initializer7b19b = null;
    private static $publicProperties0cf19 = [
        
    ];
    public function getName()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getName', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getName();
    }
    public function buildNavigation(\Pimcore\Model\Document $activeDocument, ?\Pimcore\Model\Document $navigationRootDocument = null, ?string $htmlMenuPrefix = null, ?callable $pageCallback = null, $cache = true, $maxDepth = null, $cacheLifetime = null) : \Pimcore\Navigation\Container
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'buildNavigation', array('activeDocument' => $activeDocument, 'navigationRootDocument' => $navigationRootDocument, 'htmlMenuPrefix' => $htmlMenuPrefix, 'pageCallback' => $pageCallback, 'cache' => $cache, 'maxDepth' => $maxDepth, 'cacheLifetime' => $cacheLifetime), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->buildNavigation($activeDocument, $navigationRootDocument, $htmlMenuPrefix, $pageCallback, $cache, $maxDepth, $cacheLifetime);
    }
    public function build(array $params) : \Pimcore\Navigation\Container
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'build', array('params' => $params), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->build($params);
    }
    public function getRenderer(string $alias) : \Pimcore\Navigation\Renderer\RendererInterface
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getRenderer', array('alias' => $alias), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getRenderer($alias);
    }
    public function render(\Pimcore\Navigation\Container $container, string $rendererName = 'menu', string $renderMethod = 'render', ... $rendererArguments)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'render', array('container' => $container, 'rendererName' => $rendererName, 'renderMethod' => $renderMethod, 'rendererArguments' => $rendererArguments), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->render($container, $rendererName, $renderMethod, ...$rendererArguments);
    }
    public function __call($method, array $arguments = []) : \Pimcore\Navigation\Renderer\RendererInterface
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__call', array('method' => $method, 'arguments' => $arguments), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->__call($method, $arguments);
    }
    public function setCharset($charset)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'setCharset', array('charset' => $charset), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->setCharset($charset);
    }
    public function getCharset()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getCharset', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getCharset();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->charset);
        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $instance, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($instance);
        $instance->initializer7b19b = $initializer;
        return $instance;
    }
    public function __construct(\Pimcore\Navigation\Builder $builder, \Psr\Container\ContainerInterface $rendererLocator)
    {
        static $reflection;
        if (! $this->valueHolderd73c0) {
            $reflection = $reflection ?? new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
            $this->valueHolderd73c0 = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);
        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $this, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($this);
        }
        $this->valueHolderd73c0->__construct($builder, $rendererLocator);
    }
    public function & __get($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__get', ['name' => $name], $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        if (isset(self::$publicProperties0cf19[$name])) {
            return $this->valueHolderd73c0->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__isset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__unset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__clone', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $this->valueHolderd73c0 = clone $this->valueHolderd73c0;
    }
    public function __sleep()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__sleep', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return array('valueHolderd73c0');
    }
    public function __wakeup()
    {
        unset($this->charset);
        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $this, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer7b19b = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer7b19b;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'initializeProxy', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderd73c0;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd73c0;
    }
}
