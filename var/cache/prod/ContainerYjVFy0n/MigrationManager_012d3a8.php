<?php

include_once \dirname(__DIR__, 4).'/vendor/pimcore/pimcore/lib/Migrations/MigrationManager.php';
class MigrationManager_012d3a8 extends \Pimcore\Migrations\MigrationManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderd73c0 = null;
    private $initializer7b19b = null;
    private static $publicProperties0cf19 = [
        
    ];
    public function getConfiguration(string $migrationSet) : \Pimcore\Migrations\Configuration\Configuration
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getConfiguration', array('migrationSet' => $migrationSet), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getConfiguration($migrationSet);
    }
    public function getVersion(string $migrationSet, string $versionId) : \Doctrine\DBAL\Migrations\Version
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getVersion', array('migrationSet' => $migrationSet, 'versionId' => $versionId), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getVersion($migrationSet, $versionId);
    }
    public function getBundleConfiguration(\Symfony\Component\HttpKernel\Bundle\BundleInterface $bundle) : \Pimcore\Migrations\Configuration\Configuration
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getBundleConfiguration', array('bundle' => $bundle), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getBundleConfiguration($bundle);
    }
    public function getBundleVersion(\Symfony\Component\HttpKernel\Bundle\BundleInterface $bundle, string $versionId) : \Doctrine\DBAL\Migrations\Version
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getBundleVersion', array('bundle' => $bundle, 'versionId' => $versionId), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getBundleVersion($bundle, $versionId);
    }
    public function getInstallConfiguration(\Pimcore\Migrations\Configuration\Configuration $configuration, \Pimcore\Extension\Bundle\Installer\MigrationInstallerInterface $installer) : \Pimcore\Migrations\Configuration\InstallConfiguration
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'getInstallConfiguration', array('configuration' => $configuration, 'installer' => $installer), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->getInstallConfiguration($configuration, $installer);
    }
    public function executeVersion(\Doctrine\DBAL\Migrations\Version $version, bool $up = true, bool $dryRun = false) : array
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'executeVersion', array('version' => $version, 'up' => $up, 'dryRun' => $dryRun), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->executeVersion($version, $up, $dryRun);
    }
    public function markVersionAsMigrated(\Doctrine\DBAL\Migrations\Version $version, bool $includePrevious = true)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'markVersionAsMigrated', array('version' => $version, 'includePrevious' => $includePrevious), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->markVersionAsMigrated($version, $includePrevious);
    }
    public function markVersionAsNotMigrated(\Doctrine\DBAL\Migrations\Version $version)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'markVersionAsNotMigrated', array('version' => $version), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->markVersionAsNotMigrated($version);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $instance, 'Pimcore\\Migrations\\MigrationManager')->__invoke($instance);
        $instance->initializer7b19b = $initializer;
        return $instance;
    }
    public function __construct(\Pimcore\Db\ConnectionInterface $connection, \Pimcore\Migrations\Configuration\ConfigurationFactory $configurationFactory)
    {
        static $reflection;
        if (! $this->valueHolderd73c0) {
            $reflection = $reflection ?? new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
            $this->valueHolderd73c0 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $this, 'Pimcore\\Migrations\\MigrationManager')->__invoke($this);
        }
        $this->valueHolderd73c0->__construct($connection, $configurationFactory);
    }
    public function & __get($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__get', ['name' => $name], $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        if (isset(self::$publicProperties0cf19[$name])) {
            return $this->valueHolderd73c0->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__isset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__unset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__clone', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $this->valueHolderd73c0 = clone $this->valueHolderd73c0;
    }
    public function __sleep()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__sleep', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return array('valueHolderd73c0');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $this, 'Pimcore\\Migrations\\MigrationManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer7b19b = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer7b19b;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'initializeProxy', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderd73c0;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd73c0;
    }
}
