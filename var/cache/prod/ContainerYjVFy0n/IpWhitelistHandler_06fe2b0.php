<?php

include_once \dirname(__DIR__, 4).'/vendor/scheb/two-factor-bundle/Security/TwoFactor/Handler/AuthenticationHandlerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/scheb/two-factor-bundle/Security/TwoFactor/Handler/IpWhitelistHandler.php';
class IpWhitelistHandler_06fe2b0 extends \Scheb\TwoFactorBundle\Security\TwoFactor\Handler\IpWhitelistHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderd73c0 = null;
    private $initializer7b19b = null;
    private static $publicProperties0cf19 = [
        
    ];
    public function beginTwoFactorAuthentication(\Scheb\TwoFactorBundle\Security\TwoFactor\AuthenticationContextInterface $context) : \Symfony\Component\Security\Core\Authentication\Token\TokenInterface
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'beginTwoFactorAuthentication', array('context' => $context), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return $this->valueHolderd73c0->beginTwoFactorAuthentication($context);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Handler\IpWhitelistHandler $instance) {
            unset($instance->authenticationHandler, $instance->ipWhitelistProvider);
        }, $instance, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler')->__invoke($instance);
        $instance->initializer7b19b = $initializer;
        return $instance;
    }
    public function __construct(\Scheb\TwoFactorBundle\Security\TwoFactor\Handler\AuthenticationHandlerInterface $authenticationHandler, \Scheb\TwoFactorBundle\Security\TwoFactor\IpWhitelist\IpWhitelistProviderInterface $ipWhitelistProvider)
    {
        static $reflection;
        if (! $this->valueHolderd73c0) {
            $reflection = $reflection ?? new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler');
            $this->valueHolderd73c0 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Handler\IpWhitelistHandler $instance) {
            unset($instance->authenticationHandler, $instance->ipWhitelistProvider);
        }, $this, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler')->__invoke($this);
        }
        $this->valueHolderd73c0->__construct($authenticationHandler, $ipWhitelistProvider);
    }
    public function & __get($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__get', ['name' => $name], $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        if (isset(self::$publicProperties0cf19[$name])) {
            return $this->valueHolderd73c0->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__isset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__unset', array('name' => $name), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $realInstanceReflection = new \ReflectionClass('Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd73c0;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolderd73c0;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__clone', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        $this->valueHolderd73c0 = clone $this->valueHolderd73c0;
    }
    public function __sleep()
    {
        $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, '__sleep', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
        return array('valueHolderd73c0');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Scheb\TwoFactorBundle\Security\TwoFactor\Handler\IpWhitelistHandler $instance) {
            unset($instance->authenticationHandler, $instance->ipWhitelistProvider);
        }, $this, 'Scheb\\TwoFactorBundle\\Security\\TwoFactor\\Handler\\IpWhitelistHandler')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer7b19b = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer7b19b;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer7b19b && ($this->initializer7b19b->__invoke($valueHolderd73c0, $this, 'initializeProxy', array(), $this->initializer7b19b) || 1) && $this->valueHolderd73c0 = $valueHolderd73c0;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderd73c0;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd73c0;
    }
}
