<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Targeting/toolbar:macros.html.twig */
class __TwigTemplate_5095d21f01bbf2d7e1f7f8d8a4859fc19a003227ea2f0e9c01dc39958eff4f97 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
";
        // line 16
        echo "
";
    }

    // line 1
    public function macro_label($__value__ = null, $__classes__ = null, $__attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "classes" => $__classes__,
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    <span class=\"_ptgtb__label ";
            (( !twig_test_empty(($context["classes"] ?? null))) ? (print (twig_escape_filter($this->env, twig_join_filter(($context["classes"] ?? null), " "), "html", null, true))) : (print ("")));
            echo "\"";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attributes"] ?? null));
            foreach ($context['_seq'] as $context["attr"] => $context["val"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attr"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attr'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "</span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 5
    public function macro_trigger_label($__value__ = null, $__target__ = null, $__classes__ = null, $__attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "target" => $__target__,
            "classes" => $__classes__,
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 6
            echo "    ";
            $macros["toolbar"] = $this;
            // line 7
            echo "
    ";
            // line 8
            $context["attributes"] = twig_array_merge(["data-ptgtb-toggle" => "collapse", "data-ptgtb-target" =>             // line 10
($context["target"] ?? null), "data-ptgtb-collapse-default" => "collapse"], ((            // line 12
array_key_exists("attributes", $context)) ? (_twig_default_filter(($context["attributes"] ?? null), [])) : ([])));
            // line 13
            echo "
    ";
            // line 14
            echo twig_call_macro($macros["toolbar"], "macro_label", [($context["value"] ?? null), ($context["classes"] ?? null), ($context["attributes"] ?? null)], 14, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 17
    public function macro_metric($__label__ = null, $__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 18
            echo "    <span class=\"_ptgtb__metric\">
        <span class=\"_ptgtb__metric__label\">";
            // line 19
            echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            echo "</span>
        <span class=\"_ptgtb__metric__value\">";
            // line 20
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "</span>
    </span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 24
    public function macro_identifier($__token__ = null, $__name__ = null, $__suffix__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "token" => $__token__,
            "name" => $__name__,
            "suffix" => $__suffix__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 25
            echo "_ptgb-";
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
            if ((array_key_exists("suffix", $context) &&  !twig_test_empty(($context["suffix"] ?? null)))) {
                echo "-";
                echo twig_escape_filter($this->env, ($context["suffix"] ?? null), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Targeting/toolbar:macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 25,  160 => 24,  148 => 20,  144 => 19,  141 => 18,  127 => 17,  116 => 14,  113 => 13,  111 => 12,  110 => 10,  109 => 8,  106 => 7,  103 => 6,  87 => 5,  60 => 2,  45 => 1,  40 => 16,  37 => 4,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Targeting/toolbar:macros.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Targeting/toolbar/macros.html.twig");
    }
}
