<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__ff4598f73ae2218b0e6638773e9672795b587f06e86cf827720c9432012746f8 */
class __TwigTemplate_8f4d6e27ff95e7155283bd95441a844a5070f0bea008c23cf519c919e76e60ff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>KB20E</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>KABA KB20E</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting For Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__ff4598f73ae2218b0e6638773e9672795b587f06e86cf827720c9432012746f8";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__ff4598f73ae2218b0e6638773e9672795b587f06e86cf827720c9432012746f8", "");
    }
}
