<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__23fd62562195f2648e9c8a501bbecdff3d7acfcc3b57be0358c465fb477c3870 */
class __TwigTemplate_30c23c50c61ba8b6b0192afbe6927a3ba8f9e4ce58f13d0b6917c64b3b8ab9a9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA KB16E is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__23fd62562195f2648e9c8a501bbecdff3d7acfcc3b57be0358c465fb477c3870";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__23fd62562195f2648e9c8a501bbecdff3d7acfcc3b57be0358c465fb477c3870", "");
    }
}
