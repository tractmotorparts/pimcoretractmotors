<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:content_list.html.twig */
class __TwigTemplate_0bb95ba05151b6c26daf1db0f4896f6a30315399a176173aabeec2dc29d4b187 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Tract Motors Content Listing Page</title>
</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap');
    *{box-sizing:border-box;}
    body{margin:0;padding:0;background:white;font-family:'Open Sans', sans-serif, Arial, Helvetica, sans-serif;font-size:15pt;}
    .inner-container{max-width:80%;margin:auto}
    .red-band{height:95px;background:#cf0101;width:60%}
    .content{margin-top:95px;}
     h1{font-size:45px;}
    .bottom-container{position:absolute;bottom:0;left:0;width:100%}
    .bottom-container .red-band{height:60px;}
    .content ul{padding:0;}
    .content ul li{list-style:none;margin-bottom:10px;font-size:25px;}
    .content ul li span{margin-right:28px}
</style>

<body>

<section class=\"top-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

<section class=\"content\">
    <div class=\"inner-container\">
        <h1>Contents</h1>
        <ul>
            ";
        // line 38
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "areablock", "content", ["allowed" => [0 => "print-catalog-content-list-title"]]);
        // line 45
        echo "
        </ul>
    </div>
</section>

<section class=\"bottom-container\">
    <div class=\"inner-container\">
        <div class=\"red-band\">
            &nbsp;
        </div>
    </div>
</section>

</body>

</html>
";
    }

    public function getTemplateName()
    {
        return ":web2print:content_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 45,  76 => 38,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":web2print:content_list.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/content_list.html.twig");
    }
}
