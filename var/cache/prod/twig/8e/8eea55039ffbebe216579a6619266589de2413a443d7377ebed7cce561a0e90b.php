<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__b44a184449cbb01e140cf68dcb264f6d06f19a0fe6be9ac83441bd521a9cb167 */
class __TwigTemplate_47540ed478ec43417d7ca25972714bb969d75a55202f24245ba6e2fd543d82d2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>KDF8500Q3</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>KOOP KDF8500Q(-3) DIESEL GENERATOR SET</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting For Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__b44a184449cbb01e140cf68dcb264f6d06f19a0fe6be9ac83441bd521a9cb167";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__b44a184449cbb01e140cf68dcb264f6d06f19a0fe6be9ac83441bd521a9cb167", "");
    }
}
