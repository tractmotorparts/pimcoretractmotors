<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__08121ea7fef7bbc9b7280b70c28c5ba72bfa0837084be1558fc2438deb0706b3 */
class __TwigTemplate_471893b5ed6eb9b73437b4b67422b4a19da2a9396554f8e756f5bcd2366e723a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "TOKUDEN TK450RC ROAD CUTTER BODY WITH14\" BLADE WITH SHINERAY SR460 ENGINE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__08121ea7fef7bbc9b7280b70c28c5ba72bfa0837084be1558fc2438deb0706b3";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__08121ea7fef7bbc9b7280b70c28c5ba72bfa0837084be1558fc2438deb0706b3", "");
    }
}
