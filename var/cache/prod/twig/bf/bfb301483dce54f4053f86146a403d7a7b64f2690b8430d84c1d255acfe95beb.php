<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__573bd92f80ba5fcbba31000cf266abb352112b79c1ab8c39e683b861eded3c5c */
class __TwigTemplate_07b3108e5a35b30b7f651e3323533354b12ca65ecb8107721243cb43fc515e7b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V3001 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__573bd92f80ba5fcbba31000cf266abb352112b79c1ab8c39e683b861eded3c5c";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__573bd92f80ba5fcbba31000cf266abb352112b79c1ab8c39e683b861eded3c5c", "");
    }
}
