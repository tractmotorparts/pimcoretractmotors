<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-toc:view.html.twig */
class __TwigTemplate_535de04bcbe6e8d920e80352a3cee709cf4f894038248a838068054222c14813 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    @page{
        size: auto;
        margin: 0;
        padding: 0;
        background-origin: -ro-page-box;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-1 {
        padding-top: 2mm;
        font-size: 12pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 600;
        padding-left: 0;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-2 {
        padding-left: 5mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
    #toc-wrapper .toc.ro-toc .ro-toc-level-3 {
        padding-left: 10mm;
        padding-right: 0.5mm;
        padding-top: 1mm;
        font-size: 10pt;
        font-family: 'Hind Guntur', sans-serif;
        font-weight: 400;
    }
</style>
";
        // line 32
        twig_get_attribute($this->env, $this->source, call_user_func_array($this->env->getFunction('pimcore_head_script')->getCallable(), []), "appendFile", [0 => $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/bundles/web2printtools/vendor/js/awesomizr.js")], "method", false, false, false, 32);
        // line 34
        if ( !($context["editmode"] ?? null)) {
            // line 35
            echo "    <script type=\"text/javascript\">
        \$(document).ready(function() {
            Awesomizr.createTableOfContents({
                /* toc container */
                insertiontarget: '#toc-wrapper',
                insertiontype: 'beforeend',
                /* levels to look for and link to in toc*/
                elements: ['h2.heading','h2.subheading','h3.childheading'],
                /* container element for the toc */
                container: {tag: 'ul', addClass: 'toc'},
                /* container element for one line in the toc */
                line: {tag: 'li'},
                disabledocumenttitle: false,
                toctitle: ' ',

                /* method of getting the text for the toc lines */
                text: function (elem) {
                    
                    return elem.textContent;
                }
            });
        });
    </script>
";
        }
        // line 83
        echo "<main class=\"table-of-contents\">    
    <table>
        <thead>
            <tr>
                <th>
                    <div class=\"table-page-header\">
                        <h1>";
        // line 89
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "toc_heading");
        echo "</h1>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id=\"toc-wrapper\">
                    
                    ";
        // line 98
        if (($context["editmode"] ?? null)) {
            // line 99
            echo "                        TABLE OF CONTENTS IS GENERATED ON PDF-EXPORT
                    ";
        }
        // line 101
        echo "                </td>
            </tr>
        </tbody>
    </table>     
</main>
";
    }

    public function getTemplateName()
    {
        return ":Areas/print-toc:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 101,  122 => 99,  120 => 98,  108 => 89,  100 => 83,  74 => 35,  72 => 34,  70 => 32,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-toc:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-toc/view.html.twig");
    }
}
