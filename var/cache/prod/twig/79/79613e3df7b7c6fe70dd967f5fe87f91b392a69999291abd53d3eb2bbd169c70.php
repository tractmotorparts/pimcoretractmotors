<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__37d4bbeb86274785356426bb0d85a240ff15305624429723eba8dc18af249fdb */
class __TwigTemplate_40120a8f9ad7b6c3249bfccc3aaea046faf74f831dd672c149d67d29ccf9f293 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KANAZAWA TB43 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__37d4bbeb86274785356426bb0d85a240ff15305624429723eba8dc18af249fdb";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__37d4bbeb86274785356426bb0d85a240ff15305624429723eba8dc18af249fdb", "");
    }
}
