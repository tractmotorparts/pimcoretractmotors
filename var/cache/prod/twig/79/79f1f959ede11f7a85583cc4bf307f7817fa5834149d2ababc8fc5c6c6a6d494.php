<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__aaa20a3bc87ad8a69eb74463194c3e2eb313fb0b9844becf62503a087ed96132 */
class __TwigTemplate_0e46fce6b312346241c26133ac153b8d707e8a3fc5bfbc1dc6c902bb47813a43 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA T430 MIST BLOWER TURBO is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__aaa20a3bc87ad8a69eb74463194c3e2eb313fb0b9844becf62503a087ed96132";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__aaa20a3bc87ad8a69eb74463194c3e2eb313fb0b9844becf62503a087ed96132", "");
    }
}
