<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2ae096d760a59822e1ef095d8ff6997616316e5b76649c1252f2abb347f95b3d */
class __TwigTemplate_b92178d9d3f2c037eab8a339fffaa69ea2e6bfb7ee77d6c261d7b50a1d06f4f5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "TOKUDEN TK450RC ROAD CUTTER BODY WITH 14\" BLADE WITH HONDA GX390 ENGINE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2ae096d760a59822e1ef095d8ff6997616316e5b76649c1252f2abb347f95b3d";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2ae096d760a59822e1ef095d8ff6997616316e5b76649c1252f2abb347f95b3d", "");
    }
}
