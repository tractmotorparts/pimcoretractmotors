<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__035a3419646d6f3773d8a27672c0d8b27e87546f0d287334e03a5e337d60b38b */
class __TwigTemplate_f9a122035bc02bbb7e9e293b12cb16f09c5e9c56ab757ab517ae080ae8ea4169 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KANAZAWA TU33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__035a3419646d6f3773d8a27672c0d8b27e87546f0d287334e03a5e337d60b38b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__035a3419646d6f3773d8a27672c0d8b27e87546f0d287334e03a5e337d60b38b", "");
    }
}
