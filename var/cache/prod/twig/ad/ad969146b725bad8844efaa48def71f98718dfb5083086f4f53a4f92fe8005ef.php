<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__52fdea4ce9ca4b7f153a645240f027d8460808df358a0a05ba8c23b69360d824 */
class __TwigTemplate_0576e51dc875f646459fb2d50a5eec67794d5372b895fa91a41b4dc326128705 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA B20 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__52fdea4ce9ca4b7f153a645240f027d8460808df358a0a05ba8c23b69360d824";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__52fdea4ce9ca4b7f153a645240f027d8460808df358a0a05ba8c23b69360d824", "");
    }
}
