<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:asynchronousTrackingCode.html.twig */
class __TwigTemplate_bb09df353f3a0d4b92411c511a0ffaab3c46d0dcc6e18397f029f3310a0a4ef1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'beforeScript' => [$this, 'block_beforeScript'],
            'beforeInit' => [$this, 'block_beforeInit'],
            'trackInit' => [$this, 'block_trackInit'],
            'beforeTrack' => [$this, 'block_beforeTrack'],
            'track' => [$this, 'block_track'],
            'afterTrack' => [$this, 'block_afterTrack'],
            'async' => [$this, 'block_async'],
            'afterScript' => [$this, 'block_afterScript'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
<script>
    ";
        // line 4
        $this->displayBlock('beforeScript', $context, $blocks);
        // line 5
        echo "    ";
        $this->displayBlock('beforeInit', $context, $blocks);
        // line 6
        echo "
    window._gaq = window._gaq || [];

    ";
        // line 9
        $this->displayBlock('trackInit', $context, $blocks);
        // line 13
        echo "
    ";
        // line 14
        $this->displayBlock('beforeTrack', $context, $blocks);
        // line 15
        echo "
    ";
        // line 16
        $this->displayBlock('track', $context, $blocks);
        // line 27
        echo "
    ";
        // line 28
        $this->displayBlock('afterTrack', $context, $blocks);
        // line 29
        echo "
    (function() {
        ";
        // line 31
        $this->displayBlock('async', $context, $blocks);
        // line 42
        echo "    })();

    ";
        // line 44
        $this->displayBlock('afterScript', $context, $blocks);
        // line 45
        echo "</script>

";
        // line 47
        $this->displayBlock('afterScriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 4
    public function block_beforeScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScript", [], "any", false, false, false, 4);
    }

    // line 5
    public function block_beforeInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeInit", [], "any", false, false, false, 5);
    }

    // line 9
    public function block_trackInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    _gaq.push(['_setAccount', '";
        echo twig_escape_filter($this->env, ($context["trackId"] ?? null), "html", null, true);
        echo "']);
    _gaq.push (['_gat._anonymizeIp']);
    ";
    }

    // line 14
    public function block_beforeTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeTrack", [], "any", false, false, false, 14);
    }

    // line 16
    public function block_track($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    if (typeof _gaqPageView != \"undefined\"){
        _gaq.push(['_trackPageview', _gaqPageView]);
    } else {
        ";
        // line 20
        if (($context["defaultPath"] ?? null)) {
            // line 21
            echo "        _gaq.push(['_trackPageview', '";
            echo twig_escape_filter($this->env, ($context["defaultPath"] ?? null), "html", null, true);
            echo "']);
        ";
        } else {
            // line 23
            echo "        _gaq.push(['_trackPageview']);
        ";
        }
        // line 25
        echo "    }
    ";
    }

    // line 28
    public function block_afterTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterTrack", [], "any", false, false, false, 28);
    }

    // line 31
    public function block_async($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ";
        // line 34
        if (($context["retargeting"] ?? null)) {
            // line 35
            echo "        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        ";
        } else {
            // line 37
            echo "        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ";
        }
        // line 39
        echo "
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        ";
    }

    // line 44
    public function block_afterScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScript", [], "any", false, false, false, 44);
    }

    // line 47
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 47);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:asynchronousTrackingCode.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  204 => 47,  197 => 44,  191 => 39,  187 => 37,  183 => 35,  181 => 34,  177 => 32,  173 => 31,  166 => 28,  161 => 25,  157 => 23,  151 => 21,  149 => 20,  144 => 17,  140 => 16,  133 => 14,  125 => 10,  121 => 9,  114 => 5,  107 => 4,  100 => 1,  96 => 47,  92 => 45,  90 => 44,  86 => 42,  84 => 31,  80 => 29,  78 => 28,  75 => 27,  73 => 16,  70 => 15,  68 => 14,  65 => 13,  63 => 9,  58 => 6,  55 => 5,  53 => 4,  49 => 2,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:asynchronousTrackingCode.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Analytics/Tracking/Google/Analytics/asynchronousTrackingCode.html.twig");
    }
}
