<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__168f9b58899bea9937155f898928f5513b4c791dd11b2ce36810bfdc2c40049a */
class __TwigTemplate_5515e4ad8d7dd7bafd657347cafcc29ef1bdf414d24c1c070955b6df06c1eded extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC 5LBS 2.6MM NYLON ROPE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__168f9b58899bea9937155f898928f5513b4c791dd11b2ce36810bfdc2c40049a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__168f9b58899bea9937155f898928f5513b4c791dd11b2ce36810bfdc2c40049a", "");
    }
}
