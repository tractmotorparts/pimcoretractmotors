<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__1c17399a07c4f51fcf8c857e153efaf99864ad3ef491965d9faae0092347722a */
class __TwigTemplate_8d0b6c3d0c866e71ce50c7143d8f47316d4eda14cd08876d485afe25c97eacee extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA M430 MIST BLOWER is approved";
    }

    public function getTemplateName()
    {
        return "__string_template__1c17399a07c4f51fcf8c857e153efaf99864ad3ef491965d9faae0092347722a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__1c17399a07c4f51fcf8c857e153efaf99864ad3ef491965d9faae0092347722a", "");
    }
}
