<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Misc:http-error-log-detail.html.php */
class __TwigTemplate_10d72fc9e08c313435ce0e0382953a8309653c5fa53a4042caf8d0906d3c6838 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

    <?php
        \$this->get(\"translate\")->setDomain(\"admin\");
    ?>

<style type=\"text/css\">
    body {
        margin: 0;
        padding: 10px;
        font-family: Arial;
        font-size: 12px;
    }

    h2 {
        border-bottom: 1px solid #000;
    }

    .sub {
        font-style: italic;
        border:0;
    }

    table {
        border-left: 1px solid #000;
        border-top: 1px solid #000;
        border-collapse: collapse;
    }

    td, th {
        border-right: 1px solid #000;
        border-bottom: 1px solid #000;
        padding: 2px;
    }

    th {
        text-align: left;
    }
</style>


</head>

<body>

<h2><?= \$this->data[\"code\"]; ?> | <?= \$this->data[\"uri\"]; ?></h2>

<?php foreach (\$this->data as \$key => \$value) { ?>
    <?php if(in_array(\$key, array(\"parametersGet\", \"parametersPost\", \"serverVars\", \"cookies\"))) { ?>

        <?php if (!empty(\$value)) { ?>
        <h2 class=\"sub\"><?= \$this->translate(\$key); ?></h2>

        <table>
            <?php foreach (\$value as \$k => \$v) { ?>
                <tr>
                    <th valign=\"top\"><?= \$k; ?></th>
                    <td valign=\"top\"><?= \$v; ?></td>
                </tr>
            <?php } ?>
        </table>
        <?php } ?>
    <?php } ?>
<?php } ?>


</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Misc:http-error-log-detail.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Misc:http-error-log-detail.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Misc/http-error-log-detail.html.php");
    }
}
