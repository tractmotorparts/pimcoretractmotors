<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__bda5801783dc644876e81a4ac66233bddde31b065f1224a59aa0d8cf2ed2f89a */
class __TwigTemplate_2548fb03eacad0d6c28a68191aef5332bbbd707390e6b6730cdd105983d6b21e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA TB33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__bda5801783dc644876e81a4ac66233bddde31b065f1224a59aa0d8cf2ed2f89a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__bda5801783dc644876e81a4ac66233bddde31b065f1224a59aa0d8cf2ed2f89a", "");
    }
}
