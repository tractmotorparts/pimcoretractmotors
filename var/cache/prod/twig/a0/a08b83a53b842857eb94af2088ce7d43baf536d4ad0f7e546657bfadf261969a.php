<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__8e07ca5a13fb68c3bd515f7c23b3a920ab01d45c73d4a7ba02848c4f709b46e4 */
class __TwigTemplate_9aa82d468d99255dc678ce10c70ff36ce68c248a4d1ce5a0c09266d40ea76479 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>SRGC600</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>TOKUDEN POWERED BY SHINERAY SRGC600 GRASS CUTTER  WITH TK270 ,9HP PETROL ENGINE</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Aprroved</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__8e07ca5a13fb68c3bd515f7c23b3a920ab01d45c73d4a7ba02848c4f709b46e4";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__8e07ca5a13fb68c3bd515f7c23b3a920ab01d45c73d4a7ba02848c4f709b46e4", "");
    }
}
