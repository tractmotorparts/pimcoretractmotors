<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__534a6781c9ce54bf7c2af0129ba709304f95c2b6110f0b061ca483cefb03ff9d */
class __TwigTemplate_ec97c8e1459f408c144e261ac2e682fbe3f731e7069bd9c471ee399eb4d8025f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA KB20E is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__534a6781c9ce54bf7c2af0129ba709304f95c2b6110f0b061ca483cefb03ff9d";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__534a6781c9ce54bf7c2af0129ba709304f95c2b6110f0b061ca483cefb03ff9d", "");
    }
}
