<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Profiler:data_collector.html.twig */
class __TwigTemplate_8ea92d12c1ba6d0fd21c45c202b02e1bfca252a0bf0d2fed67977d24d4ad4146 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu' => [$this, 'block_menu'],
            'panel' => [$this, 'block_panel'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "PimcoreCoreBundle:Profiler:data_collector.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["macros"] = $this;
        // line 5
        echo "
    ";
        // line 6
        ob_start(function () { return ''; });
        // line 7
        echo "        ";
        // line 8
        echo "        <div style=\"padding-top: 3px\">
            ";
        // line 9
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </div>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 12
        echo "
    ";
        // line 13
        ob_start(function () { return ''; });
        // line 14
        echo "        ";
        // line 16
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Version</b>
            <span>";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "version", [], "any", false, false, false, 18), "html", null, true);
        echo " build ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "revision", [], "any", false, false, false, 18), "html", null, true);
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Context</b>
            <span>";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "context", [], "any", false, false, false, 23), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 26
        echo "
    ";
        // line 29
        echo "    ";
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", ["link" => true]);
        echo "
";
    }

    // line 32
    public function block_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "    <span class=\"label\">
        <span class=\"icon\">
            ";
        // line 35
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </span>
        <strong>Pimcore</strong>
    </span>
";
    }

    // line 41
    public function block_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        $macros["macros"] = $this;
        // line 43
        echo "
    <h2>Pimcore</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "version", [], "any", false, false, false, 48), "html", null, true);
        echo "</span>
            <span class=\"label\">Version</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "revision", [], "any", false, false, false, 53), "html", null, true);
        echo "</span>
            <span class=\"label\">Build</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["collector"] ?? null), "context", [], "any", false, false, false, 58), "html", null, true);
        echo "</span>
            <span class=\"label\">Request Context</span>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:data_collector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 58,  147 => 53,  139 => 48,  132 => 43,  129 => 42,  125 => 41,  116 => 35,  112 => 33,  108 => 32,  101 => 29,  98 => 26,  92 => 23,  82 => 18,  78 => 16,  76 => 14,  74 => 13,  71 => 12,  65 => 9,  62 => 8,  60 => 7,  58 => 6,  55 => 5,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Profiler:data_collector.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Profiler/data_collector.html.twig");
    }
}
