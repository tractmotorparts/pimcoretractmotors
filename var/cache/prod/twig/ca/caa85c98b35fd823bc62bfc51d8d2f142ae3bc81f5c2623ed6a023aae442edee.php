<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__4c6ba386a4ee0883e3576c2ffbd4f0a2ee1843781b6a7a5badeaa55a5e66745f */
class __TwigTemplate_93ecf3e8958e53e24fe1126387da87cec6dfa9bb3f1a05589d9e90b650d873c2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "TKM GX35 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__4c6ba386a4ee0883e3576c2ffbd4f0a2ee1843781b6a7a5badeaa55a5e66745f";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__4c6ba386a4ee0883e3576c2ffbd4f0a2ee1843781b6a7a5badeaa55a5e66745f", "");
    }
}
