<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__664852c196c15769a66e553ecce36b6bee9a385526f091b0596f2507a1144c46 */
class __TwigTemplate_fe4848eeecd37cde8a13ffe2ea30d6a1f1ba926e889f5d1d7812a6584e5d841f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA TB43 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__664852c196c15769a66e553ecce36b6bee9a385526f091b0596f2507a1144c46";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__664852c196c15769a66e553ecce36b6bee9a385526f091b0596f2507a1144c46", "");
    }
}
