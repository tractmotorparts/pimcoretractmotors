<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2037c977fc983e06fc0c5cddd93aaba930224bfa0eda818f68c803530bcb7e1e */
class __TwigTemplate_c2bc4a367b8ac5c2124978ab3ea00cf8aefeb91627619878e39e172ca88f4eab extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>HYT2W</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>FARMATE HYT2W</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting For Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__2037c977fc983e06fc0c5cddd93aaba930224bfa0eda818f68c803530bcb7e1e";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2037c977fc983e06fc0c5cddd93aaba930224bfa0eda818f68c803530bcb7e1e", "");
    }
}
