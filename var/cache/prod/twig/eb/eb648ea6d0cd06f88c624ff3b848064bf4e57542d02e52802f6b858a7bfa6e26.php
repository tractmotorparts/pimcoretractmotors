<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a96c200de543368552236782f21013935444dd66b8b29b5c93402dced6fb77db */
class __TwigTemplate_680d8a660dbfafd1b2e4c19b4b79dae8ab9d9eaa86b6c7f9e3f0a8d123303424 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>5NYLON1</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>PROMAC 5KG 2.6MM TRIMMER LINE</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__a96c200de543368552236782f21013935444dd66b8b29b5c93402dced6fb77db";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a96c200de543368552236782f21013935444dd66b8b29b5c93402dced6fb77db", "");
    }
}
