<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__110b0d0dca619e94150fc19254ab0dc01b115c596989e9a31e04939c2d5bb8d6 */
class __TwigTemplate_0beddd7694a7e51a6c7e53bb9c1163caf07254e0255335168916a0416f73ecc1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA BM16 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__110b0d0dca619e94150fc19254ab0dc01b115c596989e9a31e04939c2d5bb8d6";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__110b0d0dca619e94150fc19254ab0dc01b115c596989e9a31e04939c2d5bb8d6", "");
    }
}
