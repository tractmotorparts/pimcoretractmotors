<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__6648073b072d3670181a3a49a689b3c7a66c9f3eb52ed97b0e09a5613a8dba64 */
class __TwigTemplate_fc98ef62a175e83e9dadfb842f2e1c15dfeef04989a86dc54bd736574a3a7eff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "TOKUDEN POWERED BY SHINERAY SRGC600 GRASS CUTTER  WITH TK270 ,9HP PETROL ENGINE is approved";
    }

    public function getTemplateName()
    {
        return "__string_template__6648073b072d3670181a3a49a689b3c7a66c9f3eb52ed97b0e09a5613a8dba64";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__6648073b072d3670181a3a49a689b3c7a66c9f3eb52ed97b0e09a5613a8dba64", "");
    }
}
