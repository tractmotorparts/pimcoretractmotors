<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:deeplink.html.php */
class __TwigTemplate_d2c47c6c4be837354c603f94739bfe599d4ff44d90cfa206756af483140d52af extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>

<!DOCTYPE html>
<html>
<head>
    <?php
    \$redirect = \$view->router()->path('pimcore_admin_login', [
        'deeplink' => 'true',
        'perspective' => \$this->perspective
    ]);
    ?>

    <script src=\"/bundles/pimcoreadmin/js/pimcore/common.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/functions.js\"></script>
    <script src=\"/bundles/pimcoreadmin/js/pimcore/helpers.js\"></script>
    <script>
        <?php if (\$this->tab) { ?>
            pimcore.helpers.clearOpenTab();
            pimcore.helpers.rememberOpenTab(\"<?= \$this->tab ?>\", true);
        <?php } ?>
        window.location.href = \"<?= \$redirect ?>\";
    </script>
</head>
<body>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:deeplink.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Login:deeplink.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/deeplink.html.php");
    }
}
