<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2ff399ff405014b091b0bc8f28538529ac98bdbea83c91672e8de607ace6a53e */
class __TwigTemplate_3273f482a0e22b0461224199e4e630024e30a7e15aefb8d43d1c88ab9667eb4a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V328 SE II is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2ff399ff405014b091b0bc8f28538529ac98bdbea83c91672e8de607ace6a53e";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2ff399ff405014b091b0bc8f28538529ac98bdbea83c91672e8de607ace6a53e", "");
    }
}
