<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__7edfe70f1831e1ba7192437cab1d1a6639014b5e45bd8d5f35f2e32f0b0d8115 */
class __TwigTemplate_48d3ed5a2af1d5ef07d4d949601e4dd61d2789cbde46b2a5622d1a0eb5cee2b2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA TL33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__7edfe70f1831e1ba7192437cab1d1a6639014b5e45bd8d5f35f2e32f0b0d8115";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__7edfe70f1831e1ba7192437cab1d1a6639014b5e45bd8d5f35f2e32f0b0d8115", "");
    }
}
