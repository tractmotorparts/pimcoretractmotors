<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__975e2622f0a472b76a2c1b8a9e3ea3cd901280f0660f54a915d7a353228f4d82 */
class __TwigTemplate_1d845d5cdea1b75101a2653361919c98b2729fd68830af90891d44caf4efa063 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "YAMAMOTO BBC182 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__975e2622f0a472b76a2c1b8a9e3ea3cd901280f0660f54a915d7a353228f4d82";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__975e2622f0a472b76a2c1b8a9e3ea3cd901280f0660f54a915d7a353228f4d82", "");
    }
}
