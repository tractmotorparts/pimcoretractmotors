<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__548768786e2c7891a8f55f7dfd3c2b15dff1d8b332e0b9565adabd8b290f41bf */
class __TwigTemplate_7d8cb22407df97a45f94e2128fbeeaf5d5086bb9afa3af8569d303f2630a61d4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA TU33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__548768786e2c7891a8f55f7dfd3c2b15dff1d8b332e0b9565adabd8b290f41bf";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__548768786e2c7891a8f55f7dfd3c2b15dff1d8b332e0b9565adabd8b290f41bf", "");
    }
}
