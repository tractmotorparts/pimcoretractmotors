<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Asset:showVersionDocument.html.php */
class __TwigTemplate_d9b829697cf618dda6c22055594a8d309ebc81f8e4ca1db34f4a0eb3a6a6362a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
include(\"showVersionUnknown.html.php\");";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Asset:showVersionDocument.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Asset:showVersionDocument.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Asset/showVersionDocument.html.php");
    }
}
