<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f9d1f1214a98254f9fba8be9d86cf8f3ce9c4dd2df5963d10609b8f9f14be81f */
class __TwigTemplate_bd0c7ee75a81cfcafe7f35ec92e33346610a1f135e489c6641d3949615d892a3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA ECO348 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__f9d1f1214a98254f9fba8be9d86cf8f3ce9c4dd2df5963d10609b8f9f14be81f";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__f9d1f1214a98254f9fba8be9d86cf8f3ce9c4dd2df5963d10609b8f9f14be81f", "");
    }
}
