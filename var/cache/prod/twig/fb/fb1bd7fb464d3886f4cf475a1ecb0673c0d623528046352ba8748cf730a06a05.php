<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__0d58ab20c592c855b91f658ba1978496b55d133f934129815f49caf9bdfbc5ee */
class __TwigTemplate_0fa36bd2fec80accbb7fcfc53bdeba5dc9e1f5b4b8202aeca2dae26381fd5d7f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC Multi Purpose Use Apron is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__0d58ab20c592c855b91f658ba1978496b55d133f934129815f49caf9bdfbc5ee";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__0d58ab20c592c855b91f658ba1978496b55d133f934129815f49caf9bdfbc5ee", "");
    }
}
