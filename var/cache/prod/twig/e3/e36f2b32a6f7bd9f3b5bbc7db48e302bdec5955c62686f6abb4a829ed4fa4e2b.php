<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f87d53b29604a8babe73541e9e8f1136dadf0535fc819129503e7cd59bbabf7b */
class __TwigTemplate_05c6a8e24ef3a099e049a346a2fe04cab5d5d7d0b9a702a6bbed19dd948cc826 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KOOP KDF8500Q(-3) DIESEL GENERATOR SET is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__f87d53b29604a8babe73541e9e8f1136dadf0535fc819129503e7cd59bbabf7b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__f87d53b29604a8babe73541e9e8f1136dadf0535fc819129503e7cd59bbabf7b", "");
    }
}
