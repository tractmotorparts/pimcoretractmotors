<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php */
class __TwigTemplate_de4fe1d9f29cc14af46cb13722ed2fea70d8ff0955e5c5f4fd920170894cf0c3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head lang=\"en\">
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">
        html, body {
            padding: 0;
            margin: 0;
        }

        body {
            text-align: center;
            position: relative;
        }

        img {
            max-width: 100%;
        }

        #left, #right {
            position: absolute;
            top:0;
            width:50%;
        }

        #left {
            left: 0;
            z-index: 1;
        }

        #right {
            right: 0;
            z-index: 2;
            border-left: 1px dashed darkred;
        }
    </style>
</head>
<body>

    <?php if(\$this->image) { ?>
        <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
            'id' => \$this->image
        ])?>\">
    <?php } else { ?>
        <div id=\"left\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image1
            ])?>\">
        </div>
        <div id=\"right\">
            <img src=\"<?=\$view->router()->path('pimcore_admin_document_document_diffversionsimage', [
                'id' => \$this->image2
            ])?>\">
        </div>
    <?php } ?>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Document/Document:diff-versions.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Document/Document/diff-versions.html.php");
    }
}
