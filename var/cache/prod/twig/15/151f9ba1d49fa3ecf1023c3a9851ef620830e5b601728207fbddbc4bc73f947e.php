<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Analytics/Tracking/GoogleTagManager:dataLayer.html.twig */
class __TwigTemplate_a0f54220b4cd4cdb41477aad835fe3917289a680825d40a996f8f29c2fa57cac extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script>
    window.dataLayer = window.dataLayer || [];

    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["trackedCodes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["trackedCode"]) {
            // line 5
            echo "        ";
            echo $context["trackedCode"];
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trackedCode'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</script>";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Analytics/Tracking/GoogleTagManager:dataLayer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 7,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Analytics/Tracking/GoogleTagManager:dataLayer.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Analytics/Tracking/GoogleTagManager/dataLayer.html.twig");
    }
}
