<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f54710d95cef7df066ab8de548800bf753587064137f050be2e6e77ab7a19ab7 */
class __TwigTemplate_bd6495c22a31dadf32bccfe4d1d9c867252f602fd8121803ab7553269cf5d6f8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html>
<head><style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style></head>
<body>
                       <table style=\"width: 100%; border: 1px solid black;\">
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">SKU</th>
                              <td style=\"border: 1px solid black; padding: 5px;\">KTB43</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Name</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">KANAZAWA TB43</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Status</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">Awaiting Approval</td>
                            </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__f54710d95cef7df066ab8de548800bf753587064137f050be2e6e77ab7a19ab7";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__f54710d95cef7df066ab8de548800bf753587064137f050be2e6e77ab7a19ab7", "");
    }
}
