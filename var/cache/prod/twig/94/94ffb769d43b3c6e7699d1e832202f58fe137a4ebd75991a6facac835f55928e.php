<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:product.html.twig_31-08-2021 */
class __TwigTemplate_ebed9b0a0c24c1eb7db8758e4bab635ab5fa8a210056331bbc67d0f3b5f34733 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["product"] ?? null)) {
            // line 2
            echo "<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>";
            // line 4
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getProductName", [], "method", false, false, false, 4), "html", null, true);
            echo "</h2></td>
    </tr>
    <tr>
        <td>
            <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">

                <tr>
                    ";
            // line 11
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getGallery", [], "method", false, false, false, 11), "getItems", [], "method", false, false, false, 11)) > 0)) {
                // line 12
                echo "                        ";
                $context["break"] = false;
                // line 13
                echo "                        ";
                $context["counter"] = 0;
                // line 14
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getGallery", [], "method", false, false, false, 14), "getItems", [], "method", false, false, false, 14));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    if ( !($context["break"] ?? null)) {
                        echo "   
                        
                        <td class=\"column_1\">
                           ";
                        // line 17
                        if ($context["item"]) {
                            // line 18
                            echo "                            <div class=\"img-placeholder\">
                               <img src=\"";
                            // line 19
                            echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["item"], "getImage", [], "method", false, false, false, 19)), "html", null, true);
                            echo "\" />  
                           </div>
                           ";
                        }
                        // line 22
                        echo "                        </td>
                        ";
                        // line 23
                        $context["counter"] = (($context["counter"] ?? null) + 1);
                        // line 24
                        echo "                        ";
                        if ((($context["counter"] ?? null) == 3)) {
                            // line 25
                            echo "                             ";
                            $context["break"] = true;
                            // line 26
                            echo "                        ";
                        }
                        // line 27
                        echo "                        ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 28
                echo "                    ";
            } else {
                // line 29
                echo "                    <td class=\"column_1\">
                        <div class=\"img-placeholder\">
                             <img src=\"";
                // line 31
                echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getImage", [], "method", false, false, false, 31)), "html", null, true);
                echo "\" />     
                        </div>
                     </td>
                    ";
            }
            // line 35
            echo "                </tr>

            </table>
        </td>
    </tr>
</table>
<div class=\"product-list-price\">
<h2>List Price  :</h2> 
<p>RM ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getListPrice", [], "method", false, false, false, 43), "html", null, true);
            echo "</p>

</div>
<div class=\"product-discription\">
<h2>Product Description</h2>
<p>";
            // line 48
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getDescription", [], "method", false, false, false, 48);
            echo "</p>
</div>
<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
    <tr>
        <td><h2>Product Specification</h2></td>
    </tr>
    <tr>
        <td>
            <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0\">
                <tbody>
                    <tr>
                        <td>
                            <table class=\"pdf-data-table product-specification-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <tbody>
                                    ";
            // line 62
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["productList"] ?? null));
            foreach ($context['_seq'] as $context["head"] => $context["product"]) {
                // line 63
                echo "                                        ";
                if (($context["head"] == "TechnicalSpecification")) {
                    // line 64
                    echo "                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["product"]);
                    foreach ($context['_seq'] as $context["h"] => $context["v"]) {
                        echo " 
                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">";
                        // line 66
                        echo twig_escape_filter($this->env, $context["h"], "html", null, true);
                        echo " </td>
                                                    <td style=\"width:60;text-align: left\">";
                        // line 67
                        echo $context["v"];
                        echo " </td>
                                                </tr>
                                             ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['h'], $context['v'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "                                        ";
                } else {
                    // line 71
                    echo "                                                <tr>
                                                    <td style=\"width:40%;text-align: left\">";
                    // line 72
                    echo twig_escape_filter($this->env, $context["head"], "html", null, true);
                    echo " </td>
                                                    <td style=\"width:60%;text-align: left\">";
                    // line 73
                    echo $context["product"];
                    echo "</td>
                                                </tr>
                                        ";
                }
                // line 76
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['head'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ";
            // line 83
            $context["relatedParts"] = twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "getRelatedParts", [], "method", false, false, false, 83);
            // line 84
            echo "             ";
            if (($context["relatedParts"] ?? null)) {
                // line 85
                echo "             <section class=\"related-products\">
                <h2>Related Parts</h2>
                ";
                // line 87
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["relatedParts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 88
                    echo "                <div class=\"parts\">
                    <div class=\"parts-box\">
                        <img src=\"";
                    // line 90
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 90)), "html", null, true);
                    echo "\">
                        <span>
                            <p class=\"part-name\">";
                    // line 92
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 92), "html", null, true);
                    echo "</p>
                            <p class=\"part-price\">RM ";
                    // line 93
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 93), "html", null, true);
                    echo "</p>
                        </span>
                     </div>
                </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 98
                echo "            </section>
            ";
            }
            // line 100
            echo "                                
        </td>
    </tr>
</table>
";
        } elseif (        // line 104
($context["editmode"] ?? null)) {
            // line 105
            echo "    <p>Drag a product here</p>
";
        }
    }

    public function getTemplateName()
    {
        return ":web2print:product.html.twig_31-08-2021";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 105,  257 => 104,  251 => 100,  247 => 98,  236 => 93,  232 => 92,  227 => 90,  223 => 88,  219 => 87,  215 => 85,  212 => 84,  210 => 83,  202 => 77,  196 => 76,  190 => 73,  186 => 72,  183 => 71,  180 => 70,  171 => 67,  167 => 66,  159 => 64,  156 => 63,  152 => 62,  135 => 48,  127 => 43,  117 => 35,  110 => 31,  106 => 29,  103 => 28,  96 => 27,  93 => 26,  90 => 25,  87 => 24,  85 => 23,  82 => 22,  76 => 19,  73 => 18,  71 => 17,  61 => 14,  58 => 13,  55 => 12,  53 => 11,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":web2print:product.html.twig_31-08-2021", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/product.html.twig_31-08-2021");
    }
}
