<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__475a0bfef8665799d597f808f2becd32a675d3bb57448ff1226fb6cd17691ed1 */
class __TwigTemplate_107280ce93937520d99727a55b6275d56a6f5d80c0a4d8ea00579c1b2661c722 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V1800 MIST BLOWER is approved";
    }

    public function getTemplateName()
    {
        return "__string_template__475a0bfef8665799d597f808f2becd32a675d3bb57448ff1226fb6cd17691ed1";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__475a0bfef8665799d597f808f2becd32a675d3bb57448ff1226fb6cd17691ed1", "");
    }
}
