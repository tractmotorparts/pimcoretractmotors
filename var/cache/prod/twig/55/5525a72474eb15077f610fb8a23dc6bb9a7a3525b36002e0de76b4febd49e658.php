<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__91ad08ee54e66ee127eb881e1083e9565d31c2ec65f7b635ff45d16eaf2dd0ff */
class __TwigTemplate_130ddfc016cd1cc653559ecc19afeb98123be6f88489b7c602ad1bc26e8b2860 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html>
<head><style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style></head>
<body>
                       <table style=\"width: 100%; border: 1px solid black;\">
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">SKU</th>
                              <td style=\"border: 1px solid black; padding: 5px;\">M430</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Name</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">VICTA M430 MIST BLOWER</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Status</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">Awaiting Approval</td>
                            </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__91ad08ee54e66ee127eb881e1083e9565d31c2ec65f7b635ff45d16eaf2dd0ff";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__91ad08ee54e66ee127eb881e1083e9565d31c2ec65f7b635ff45d16eaf2dd0ff", "");
    }
}
