<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:default.html.twig */
class __TwigTemplate_80ecfed0f5f7814fc78210ac2fd5661dcf50e7fede31a42bbd8fa37324c9675e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            '_content' => [$this, 'block__content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layouts/print_catalog.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layouts/print_catalog.html.twig", ":web2print:default.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block__content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        echo twig_include($this->env, $context, "web2print/default_no_layout.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return ":web2print:default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":web2print:default.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/default.html.twig");
    }
}
