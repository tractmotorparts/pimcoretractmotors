<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SchebTwoFactorBundle:Authentication:form.html.twig */
class __TwigTemplate_0bc520e3395d737a19cee31f7728caab2ab4ea91a0b2424175a251b2b2664c29 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "
";
        // line 7
        if (($context["authenticationError"] ?? null)) {
            // line 8
            echo "<p>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["authenticationError"] ?? null), ($context["authenticationErrorData"] ?? null)), "html", null, true);
            echo "</p>
";
        }
        // line 10
        echo "
";
        // line 12
        echo "<p>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scheb_two_factor.choose_provider"), "html", null, true);
        echo ":
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["availableTwoFactorProviders"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["provider"]) {
            // line 14
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("2fa_login", ["preferProvider" => $context["provider"]]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["provider"], "html", null, true);
            echo "</a>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['provider'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "</p>

";
        // line 19
        echo "<p class=\"label\"><label for=\"_auth_code\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scheb_two_factor.auth_code"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["twoFactorProvider"] ?? null), "html", null, true);
        echo ":</label></p>

<form class=\"form\" action=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("2fa_login_check");
        echo "\" method=\"post\">
    <p class=\"widget\">
        <input
            id=\"_auth_code\"
            type=\"text\"
            name=\"";
        // line 26
        echo twig_escape_filter($this->env, ($context["authCodeParameterName"] ?? null), "html", null, true);
        echo "\"
            autocomplete=\"one-time-code\"
            autofocus
            ";
        // line 35
        echo "        />
    </p>

    ";
        // line 38
        if (($context["displayTrustedOption"] ?? null)) {
            // line 39
            echo "        <p class=\"widget\"><label for=\"_trusted\"><input id=\"_trusted\" type=\"checkbox\" name=\"";
            echo twig_escape_filter($this->env, ($context["trustedParameterName"] ?? null), "html", null, true);
            echo "\" /> ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scheb_two_factor.trusted"), "html", null, true);
            echo "</label></p>
    ";
        }
        // line 41
        echo "    ";
        if (($context["isCsrfProtectionEnabled"] ?? null)) {
            // line 42
            echo "        <input type=\"hidden\" name=\"";
            echo twig_escape_filter($this->env, ($context["csrfParameterName"] ?? null), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(($context["csrfTokenId"] ?? null)), "html", null, true);
            echo "\">
    ";
        }
        // line 44
        echo "    <p class=\"submit\"><input type=\"submit\" value=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scheb_two_factor.login"), "html", null, true);
        echo "\" /></p>
</form>

";
        // line 48
        echo "<p class=\"cancel\"><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_security_logout");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scheb_two_factor.cancel"), "html", null, true);
        echo "</a></p>
";
    }

    public function getTemplateName()
    {
        return "SchebTwoFactorBundle:Authentication:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 48,  123 => 44,  115 => 42,  112 => 41,  104 => 39,  102 => 38,  97 => 35,  91 => 26,  83 => 21,  75 => 19,  71 => 16,  60 => 14,  56 => 13,  51 => 12,  48 => 10,  42 => 8,  40 => 7,  37 => 5,);
    }

    public function getSourceContext()
    {
        return new Source("", "SchebTwoFactorBundle:Authentication:form.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/scheb/two-factor-bundle/Resources/views/Authentication/form.html.twig");
    }
}
