<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e2c820cc37b5e06ff774ad7ba0205c59ab29fe97b01ff0dda0fddb709c60f032 */
class __TwigTemplate_f1a0f84e3a8efef2257195f9cea2f633d69078ec8db3b7d3be7dc263e86a2a74 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html>
<head><style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style></head>
<body>
                       <table style=\"width: 100%; border: 1px solid black;\">
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">SKU</th>
                              <td style=\"border: 1px solid black; padding: 5px;\">ECO348</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Name</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">VICTA ECO348</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Status</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">Awaiting For Approval</td>
                            </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__e2c820cc37b5e06ff774ad7ba0205c59ab29fe97b01ff0dda0fddb709c60f032";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__e2c820cc37b5e06ff774ad7ba0205c59ab29fe97b01ff0dda0fddb709c60f032", "");
    }
}
