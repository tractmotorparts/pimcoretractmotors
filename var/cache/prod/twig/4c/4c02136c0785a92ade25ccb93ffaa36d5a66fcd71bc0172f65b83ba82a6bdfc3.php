<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php */
class __TwigTemplate_520fa0cc5cadd5602b9c2bb050dd2931b3a3004914bb4def2557da328f84a837 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">

    <style type=\"text/css\">

        /* hide from ie on mac \\*/
        html {
            height: 100%;
            overflow: hidden;
        }
        /* end hide */

        body {
            height: 100%;
            margin: 0;
            padding: 0;
            background: #000;
        }

        #videoContainer {
            text-align: center;
            position: absolute;
            top:50%;
            margin-top: -200px;
            width: 100%;
        }

        video {

        }

    </style>

</head>

<body>


<?php
    \$previewImage = \"\";
    if(\\Pimcore\\Video::isAvailable()) {
        \$previewImage = \$view->router()->path('pimcore_admin_asset_getvideothumbnail', [
            'id' => \$this->asset->getId(),
            'treepreview' => 'true'
        ]);
    }

    \$serveVideoPreview = \$view->router()->path('pimcore_admin_asset_servevideopreview', [
        'id' => \$this->asset->getId()
    ]);
?>

<div id=\"videoContainer\">
    <video id=\"video\" controls=\"controls\" height=\"400\" poster=\"<?= \$previewImage ?>\">
        <source src=\"<?=\$serveVideoPreview ?>\" type=\"video/mp4\" />
    </video>
</div>


</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Asset:getPreviewVideoDisplay.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Asset/getPreviewVideoDisplay.html.php");
    }
}
