<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__c8046b7231d958f3cbfca2f684ca4c3238dcee2bc22a311f13a4f8b080f9f7f9 */
class __TwigTemplate_0ecd7bbeff6417f04e3a4e9eaf1083cca0a2140d1a9a608cea1e52981ef91224 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V328 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__c8046b7231d958f3cbfca2f684ca4c3238dcee2bc22a311f13a4f8b080f9f7f9";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__c8046b7231d958f3cbfca2f684ca4c3238dcee2bc22a311f13a4f8b080f9f7f9", "");
    }
}
