<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__88dc89a934af6a1c8d09016aa946eb791251957d45add980ea0450bf63371466 */
class __TwigTemplate_670777eb03c4eb3e42392c23d6e966f01ff55aaf7ba0e6e7e49aade35a2ef2d7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html>
<head><style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style></head>
<body>
                       <table style=\"width: 100%; border: 1px solid black;\">
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">SKU</th>
                              <td style=\"border: 1px solid black; padding: 5px;\">SRGC600</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Name</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">TOKUDEN POWERED BY SHINERAY SRGC600 GRASS CUTTER  WITH TK270 ,9HP PETROL ENGINE</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Status</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">Awaiting Approval</td>
                            </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__88dc89a934af6a1c8d09016aa946eb791251957d45add980ea0450bf63371466";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__88dc89a934af6a1c8d09016aa946eb791251957d45add980ea0450bf63371466", "");
    }
}
