<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-by-category:view.html.twig */
class __TwigTemplate_cc6d8b46c713890a8135d07c5a34c348a3a002ddcf7068f2adfa0dd1b858df41 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }


        .product-catalogue .page-header h2 {
            margin-top: 0;
            background-color: #cf0101;
            height: 40px;
            text-transform: uppercase;
            line-height: 40px;
            margin: 0;
            font-size: 24px;
        }

        .pdf-right-container {
            padding-right: 0;
            width: 100%;
            padding-left: 0;
            padding-top: 0;
        }

        .pdf-sub-cat-content .product-row .product-box {
            text-align: left;
            padding: 12px;
            width: 30.6%;
            list-style: none;
            display: inline-block;
            margin: 15px 4% 25px 0px;
            box-shadow: 0px 1px 9px 4px #d2d2d2;
            position: relative;
            height: 100%;
        }

        .pdf-cat-content {
            padding: 15px;
        }
        .pdf-sub-cat-content h3 { color: #4974c3; margin: 10px 0 0; font-size: 20px;  text-transform: uppercase;}
        .pdf-cat-content h2 { font-size: 22px !important; text-transform: uppercase;}
        .page-header { padding: 30px 15px 0;}

        @media print {
           .product-catalogue thead {
                display: table-header-group;
            }

            .product-catalogue  tfoot {
                display: table-footer-group;
                position: relative;
                bottom: 0;
            }            
        }
</style>
";
        // line 58
        $context["counter"] = 0;
        // line 59
        echo "<main class=\"product-catalogue\">
    <table style=\"width:100%\">
        <thead>
            <tr>
                <th>
                    <div class=\"page-header\">
                        <h2 class=\"heading\">";
        // line 65
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_head", ["placeholder" => "Category Name"]);
        echo "</h2>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table style=\"width:100%\">
                        <tbody>
                            <tr>
                                <td>   
                                   <section class=\"pdf-cat-content\">
                                       ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->getBlockIterator($this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "block", "ChildBlock", ["limit" => 50])));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            // line 79
            echo "                                        <h2 class=\"subheading\">";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", ("category_sub_head_" . $context["k"]), ["placeholder" => "Subcategory Name"]);
            echo "</h2>
                                        <div class=\"pdf-sub-cat-content\">
                                           ";
            // line 82
            echo "                                            <div class=\"title-with-logo\">
                                                <div class=\"tle\">
                                                    <h3 class=\"childheading\">";
            // line 84
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", ("product_brand_" . $context["k"]), ["placeholder" => "Enter Brand Name"]);
            echo "</h3>
                                                </div>
                                                <div class=\"rt-lgo\">
                                                    ";
            // line 88
            echo "                                                    ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "brand-logo");
            echo "
                                                </div>
                                            </div>
                                           ";
            // line 91
            if (($context["editmode"] ?? null)) {
                // line 92
                echo "                                           ";
                echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" .                 // line 93
$context["k"]), ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "reload" => false, "selectedClass" => "Product"]);
                // line 99
                echo "
                                           ";
            } else {
                // line 101
                echo "                                           ";
                $context["configArray"] = $this->extensions['Web2PrintToolsBundle\Twig\OutputChannelExtension']->buildOutputDataConfig(twig_get_attribute($this->env, $this->source, $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" . $context["k"])), "getOutputChannel", [], "method", false, false, false, 101));
                // line 102
                echo "                                           ";
                $context["elements"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", ("tableconfig_" . $context["k"]));
                // line 103
                echo "                                           <div class=\"product-row\">
                                                ";
                // line 104
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 105
                    echo "                                                ";
                    $context["counter"] = (($context["counter"] ?? null) + 1);
                    // line 106
                    echo "                                              
                                                <div class=\"product-box\" id=\"product_";
                    // line 107
                    echo twig_escape_filter($this->env, ($context["counter"] ?? null), "html", null, true);
                    echo "\">
                                                    <div class=\"img-placeholder\">
                                                        <img style=\"width: 100px;\" src=\"";
                    // line 109
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 109)), "html", null, true);
                    echo "\" alt=\"product_image\">
                                                    </div>
                                                    ";
                    // line 112
                    echo "                                                    <h4>
                                                        ";
                    // line 113
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 113), "html", null, true);
                    echo " 
                                                        <span class=\"modal-no\">";
                    // line 114
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 114), "html", null, true);
                    echo "</span>
                                                    </h4>
                                                    <p>";
                    // line 116
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getShortDescription", [], "method", false, false, false, 116), "html", null, true);
                    echo "
                                                    </p>
                                                    ";
                    // line 118
                    $context["currency"] = twig_get_attribute($this->env, $this->source, $context["element"], "getCurrency", [], "method", false, false, false, 118);
                    // line 119
                    echo "                                                    ";
                    $context["defaultCurrency"] = "\$";
                    // line 120
                    echo "                                                    ";
                    if (($context["currency"] ?? null)) {
                        // line 121
                        echo "                                                    ";
                        $context["expldCurrency"] = twig_split_filter($this->env, ($context["currency"] ?? null), "-");
                        // line 122
                        echo "                                                    ";
                        // line 123
                        echo "                                                    ";
                        $context["defaultCurrency"] = ((((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["expldCurrency"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null) != "")) ? ((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["expldCurrency"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null)) : (($context["defaultCurrency"] ?? null)));
                        // line 124
                        echo "                                                    ";
                    }
                    // line 125
                    echo "                                                    <span class=\"price\">";
                    echo ($context["defaultCurrency"] ?? null);
                    echo "  ";
                    echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 125)), "html", null, true);
                    echo "</span>
                                                </div>
                                               ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 128
                echo "                                            </div>
                                            ";
            }
            // line 130
            echo "                                        </div>
                                       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "      
                                   </section>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</main>


";
    }

    public function getTemplateName()
    {
        return ":Areas/print-product-by-category:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 131,  240 => 130,  236 => 128,  224 => 125,  221 => 124,  218 => 123,  216 => 122,  213 => 121,  210 => 120,  207 => 119,  205 => 118,  200 => 116,  195 => 114,  191 => 113,  188 => 112,  183 => 109,  178 => 107,  175 => 106,  172 => 105,  168 => 104,  165 => 103,  162 => 102,  159 => 101,  155 => 99,  153 => 93,  151 => 92,  149 => 91,  142 => 88,  136 => 84,  132 => 82,  126 => 79,  122 => 78,  106 => 65,  98 => 59,  96 => 58,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-product-by-category:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-by-category/view.html.twig");
    }
}
