<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php */
class __TwigTemplate_944cbda0f524b59402d06c874f3adf538516d2b63e99260a54112e528ccf3417 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/**
 * @var \\Pimcore\\Model\\Document\\Page \$element
 */
\$element = \$this->element;
\$previewImage = null;
if (\$element instanceof \\Pimcore\\Model\\Document\\Page && \$this->config['documents']['generate_preview']) {
    \$thumbnailFileHdpi = \$element->getPreviewImageFilesystemPath(true);
    if (file_exists(\$thumbnailFileHdpi)) {
        \$previewImage = \$this->path('pimcore_admin_page_display_preview_image', ['id' => \$element->getId(), 'hdpi' => true]);
    }

}

?>

<?php if(\$previewImage) {?>
    <div class=\"full-preview\">
        <img src=\"<?= \$previewImage ?>\" onload=\"this.parentNode.className += ' complete';\">
        <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
    </div>
<?php } else { ?>
    <div class=\"mega-icon <?= \$this->iconCls ?>\"></div>
    <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
<?php } ?>

";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:document.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/SearchAdmin/Search/Quicksearch/document.html.php");
    }
}
