<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__734d046c00ee78c797c1e7644d0f725af4ddb2726fa24a8ca6e2ba187b035a48 */
class __TwigTemplate_b7c667f77e48a4545b286ebf684d5b1981cbf0fb5a26b3089606b6158ca8afdc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA SUM328 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__734d046c00ee78c797c1e7644d0f725af4ddb2726fa24a8ca6e2ba187b035a48";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__734d046c00ee78c797c1e7644d0f725af4ddb2726fa24a8ca6e2ba187b035a48", "");
    }
}
