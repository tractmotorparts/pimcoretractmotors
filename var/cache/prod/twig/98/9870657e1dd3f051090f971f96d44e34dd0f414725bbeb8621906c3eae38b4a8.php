<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2e843735028fa6d7cd5fcb31c7c86c40a7b0faeb1042e94f3acfb6d892625f74 */
class __TwigTemplate_8465b175fe6ca5132a5216bb98865b22af69518d165a5aae1aa3504005bcecbf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA T430 MIST BLOWER TURBO is approved";
    }

    public function getTemplateName()
    {
        return "__string_template__2e843735028fa6d7cd5fcb31c7c86c40a7b0faeb1042e94f3acfb6d892625f74";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2e843735028fa6d7cd5fcb31c7c86c40a7b0faeb1042e94f3acfb6d892625f74", "");
    }
}
