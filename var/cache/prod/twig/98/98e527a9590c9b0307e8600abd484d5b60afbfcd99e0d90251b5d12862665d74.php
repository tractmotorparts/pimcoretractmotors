<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__61c91d05b3b0844e054e6b44f4c6d742cd69c31aa2b3e8d4db6bc1c82f4287ef */
class __TwigTemplate_0c272e857a67980f56533f52cc8cd3c6e31d7a5aa18a576e1cb859873b7310da extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>ECO348</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>VICTA ECO348</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Aprroved</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__61c91d05b3b0844e054e6b44f4c6d742cd69c31aa2b3e8d4db6bc1c82f4287ef";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__61c91d05b3b0844e054e6b44f4c6d742cd69c31aa2b3e8d4db6bc1c82f4287ef", "");
    }
}
