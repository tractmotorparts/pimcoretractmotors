<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Targeting/toolbar:toolbar.html.twig */
class __TwigTemplate_0aa25f8cf9190342ca9a74890c3649192b33ac065b0e4893e10c6fbaca1b89a3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'overview' => [$this, 'block_overview'],
            'overviewTable' => [$this, 'block_overviewTable'],
            'rules' => [$this, 'block_rules'],
            'rulesTable' => [$this, 'block_rulesTable'],
            'targetGroups' => [$this, 'block_targetGroups'],
            'targetGroupsTable' => [$this, 'block_targetGroupsTable'],
            'documentTargetGroups' => [$this, 'block_documentTargetGroups'],
            'documentTargetGroupsTable' => [$this, 'block_documentTargetGroupsTable'],
            'advanced' => [$this, 'block_advanced'],
            'visitorInfo' => [$this, 'block_visitorInfo'],
            'storage' => [$this, 'block_storage'],
            'visitorStorage' => [$this, 'block_visitorStorage'],
            'sessionStorage' => [$this, 'block_sessionStorage'],
            'overrides' => [$this, 'block_overrides'],
            'overrideForm' => [$this, 'block_overrideForm'],
            'css' => [$this, 'block_css'],
            'js' => [$this, 'block_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["toolbar"] = $this->macros["toolbar"] = $this->loadTemplate("@PimcoreCore/Targeting/toolbar/macros.html.twig", "PimcoreCoreBundle:Targeting/toolbar:toolbar.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('toolbar', $context, $blocks);
        // line 284
        echo "
";
        // line 285
        $this->displayBlock('css', $context, $blocks);
        // line 294
        echo "
";
        // line 295
        $this->displayBlock('js', $context, $blocks);
    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        ob_start(function () { return ''; });
        // line 5
        echo "    <div id=\"_ptgtb-";
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\" class=\"_ptgtb _ptgtb--collapsed\">
        <div class=\"_ptgtb__trigger\" title=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("targeting", [], "admin");
        echo "\">
            <span class=\"_ptgtb__toolbar-icon _ptgtb__toolbar-icon--target\">
                ";
        // line 8
        echo twig_include($this->env, $context, "@PimcoreCore/Profiler/target.svg.twig");
        echo "
            </span>

            <span class=\"_ptgtb__toolbar-icon _ptgtb__toolbar-icon--collapse\">
                ";
        // line 12
        echo twig_include($this->env, $context, "@PimcoreCore/Targeting/toolbar/icon/toolbar-collapse.svg.twig");
        echo "
            </span>
        </div>

        <a class=\"_ptgtb__toolbar-icon _ptgtb__toolbar-icon--close\" data-ptgtb-target=\"#_ptgtb-";
        // line 16
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\" title=\"Close Toolbar\">
            ";
        // line 17
        echo twig_include($this->env, $context, "@PimcoreCore/Targeting/toolbar/icon/close.svg.twig");
        echo "
        </a>

        <a
            class=\"_ptgtb__toolbar-icon _ptgtb__toolbar-icon--advanced-features\"
            title=\"Toggle Advanced Features\"
            data-ptgtb-toggle=\"collapse\"
            data-ptgtb-target=\"._ptgtb__advanced\"
            data-ptgtb-collapse-default=\"collapse\"
            data-ptgtb-collapse-store=\"advanced\"
            data-ptgtb-arrow=\"false\"
            >
            ";
        // line 29
        echo twig_include($this->env, $context, "@PimcoreCore/Targeting/toolbar/icon/advanced_features.svg.twig");
        echo "
        </a>

        <div class=\"_ptgtb__content\">
            <div class=\"_ptgtb__content-inner\">
                <h1>
                    Targeting
                </h1>

                ";
        // line 38
        $this->displayBlock('overview', $context, $blocks);
        // line 74
        echo "
                ";
        // line 75
        $this->displayBlock('rules', $context, $blocks);
        // line 115
        echo "
                ";
        // line 116
        $this->displayBlock('targetGroups', $context, $blocks);
        // line 153
        echo "
                ";
        // line 154
        $this->displayBlock('documentTargetGroups', $context, $blocks);
        // line 191
        echo "
                <div class=\"_ptgtb__advanced\">
                    ";
        // line 193
        $this->displayBlock('advanced', $context, $blocks);
        // line 253
        echo "                </div>

                ";
        // line 255
        $this->displayBlock('overrides', $context, $blocks);
        // line 279
        echo "            </div>
        </div>
    </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 38
    public function block_overview($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "                    <table class=\"_ptgtb__table\">
                        ";
        // line 40
        $this->displayBlock('overviewTable', $context, $blocks);
        // line 72
        echo "                    </table>
                ";
    }

    // line 40
    public function block_overviewTable($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "                            ";
        if ( !(null === ($context["documentTargetGroup"] ?? null))) {
            // line 42
            echo "                                <tr>
                                    <th>Document Target Group</th>
                                    <td class=\"_ptgtb__table__col-right\">
                                        ";
            // line 45
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_get_attribute($this->env, $this->source, ($context["documentTargetGroup"] ?? null), "name", [], "any", false, false, false, 45), [0 => "_ptgtb__label--target-group"]], 45, $context, $this->getSourceContext());
            echo "
                                    </td>
                                </tr>
                            ";
        }
        // line 49
        echo "
                            <tr>
                                <th>Visitor ID</th>
                                <td class=\"_ptgtb__table__col-right\">
                                    ";
        // line 53
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "visitorId", [], "any", false, false, false, 53))) {
            // line 54
            echo "                                        ";
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "visitorId", [], "any", false, false, false, 54)], 54, $context, $this->getSourceContext());
            echo "
                                    ";
        } else {
            // line 56
            echo "                                        -
                                    ";
        }
        // line 58
        echo "                                </td>
                            </tr>

                            <tr>
                                <th>Session ID</th>
                                <td class=\"_ptgtb__table__col-right\">
                                    ";
        // line 64
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "sessionId", [], "any", false, false, false, 64))) {
            // line 65
            echo "                                        ";
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "sessionId", [], "any", false, false, false, 65)], 65, $context, $this->getSourceContext());
            echo "
                                    ";
        } else {
            // line 67
            echo "                                        -
                                    ";
        }
        // line 69
        echo "                                </td>
                            </tr>
                        ";
    }

    // line 75
    public function block_rules($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 76
        echo "                    ";
        if ( !twig_test_empty(($context["rules"] ?? null))) {
            // line 77
            echo "                        <h2
                            class=\"_ptgtb__collapse__trigger--block\"
                            data-ptgtb-toggle=\"collapse\"
                            data-ptgtb-target=\"#";
            // line 80
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "rules"], 80, $context, $this->getSourceContext());
            echo "\"
                            data-ptgtb-collapse-store=\"rules\"
                        >
                            Matched Rules
                            ";
            // line 84
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_length_filter($this->env, ($context["rules"] ?? null))], 84, $context, $this->getSourceContext());
            echo "
                        </h2>

                        <table id=\"";
            // line 87
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "rules"], 87, $context, $this->getSourceContext());
            echo "\" class=\"_ptgtb__table\">
                            ";
            // line 88
            $this->displayBlock('rulesTable', $context, $blocks);
            // line 112
            echo "                        </table>
                    ";
        }
        // line 114
        echo "                ";
    }

    // line 88
    public function block_rulesTable($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rules"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["rule"]) {
            // line 90
            echo "                                    ";
            $context["rowIdentifier"] = twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "rules-details", twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 90)], 90, $context, $this->getSourceContext());
            // line 91
            echo "
                                    <tr class=\"_ptgtb__table__row-with-details\">
                                        <td>
                                            ";
            // line 94
            echo twig_call_macro($macros["toolbar"], "macro_trigger_label", [twig_get_attribute($this->env, $this->source, $context["rule"], "name", [], "any", false, false, false, 94), ("#" . ($context["rowIdentifier"] ?? null)), [0 => "_ptgtb__label--rule"]], 94, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>
                                    <tr id=\"";
            // line 97
            echo twig_escape_filter($this->env, ($context["rowIdentifier"] ?? null), "html", null, true);
            echo "\" class=\"_ptgtb__table__row-details\">
                                        <td>
                                            ";
            // line 99
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Rule ID", twig_get_attribute($this->env, $this->source, $context["rule"], "id", [], "any", false, false, false, 99)], 99, $context, $this->getSourceContext());
            echo "

                                            ";
            // line 101
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["rule"], "duration", [], "any", false, false, false, 101))) {
                // line 102
                echo "                                                ";
                echo twig_call_macro($macros["toolbar"], "macro_metric", ["Duration", (twig_round(twig_get_attribute($this->env, $this->source, $context["rule"], "duration", [], "any", false, false, false, 102), 2) . " ms")], 102, $context, $this->getSourceContext());
                echo "
                                            ";
            }
            // line 104
            echo "
                                            ";
            // line 105
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Conditions", twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rule"], "conditions", [], "any", false, false, false, 105))], 105, $context, $this->getSourceContext());
            echo "
                                            ";
            // line 106
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Actions", twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rule"], "actions", [], "any", false, false, false, 106))], 106, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>

                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rule'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "                            ";
    }

    // line 116
    public function block_targetGroups($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 117
        echo "                    ";
        if ( !twig_test_empty(($context["targetGroups"] ?? null))) {
            // line 118
            echo "                        <h2
                            class=\"_ptgtb__collapse__trigger--block\"
                            data-ptgtb-toggle=\"collapse\"
                            data-ptgtb-target=\"#";
            // line 121
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "target-groups"], 121, $context, $this->getSourceContext());
            echo "\"
                            data-ptgtb-collapse-store=\"target-groups\"
                        >
                            Assigned Target Groups
                            ";
            // line 125
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_length_filter($this->env, ($context["targetGroups"] ?? null))], 125, $context, $this->getSourceContext());
            echo "
                        </h2>

                        <table id=\"";
            // line 128
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "target-groups"], 128, $context, $this->getSourceContext());
            echo "\" class=\"_ptgtb__table\">
                            ";
            // line 129
            $this->displayBlock('targetGroupsTable', $context, $blocks);
            // line 150
            echo "                        </table>
                    ";
        }
        // line 152
        echo "                ";
    }

    // line 129
    public function block_targetGroupsTable($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["targetGroups"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["targetGroup"]) {
            // line 131
            echo "                                    ";
            $context["rowIdentifier"] = twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "target-groups-details", twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 131)], 131, $context, $this->getSourceContext());
            // line 132
            echo "
                                    <tr class=\"_ptgtb__table__row-with-details\">
                                        <td>
                                            ";
            // line 135
            echo twig_call_macro($macros["toolbar"], "macro_trigger_label", [twig_get_attribute($this->env, $this->source, $context["targetGroup"], "name", [], "any", false, false, false, 135), ("#" . ($context["rowIdentifier"] ?? null)), [0 => "_ptgtb__label--target-group"]], 135, $context, $this->getSourceContext());
            echo "
                                        </td>
                                        <td class=\"_ptgtb__table__col-number\">
                                            ";
            // line 138
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_get_attribute($this->env, $this->source, $context["targetGroup"], "count", [], "any", false, false, false, 138)], 138, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>
                                    <tr id=\"";
            // line 141
            echo twig_escape_filter($this->env, ($context["rowIdentifier"] ?? null), "html", null, true);
            echo "\" class=\"_ptgtb__table__row-details\">
                                        <td colspan=\"2\">
                                            ";
            // line 143
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Target Group ID", twig_get_attribute($this->env, $this->source, $context["targetGroup"], "id", [], "any", false, false, false, 143)], 143, $context, $this->getSourceContext());
            echo "
                                            ";
            // line 144
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Threshold", twig_get_attribute($this->env, $this->source, $context["targetGroup"], "threshold", [], "any", false, false, false, 144)], 144, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>

                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['targetGroup'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "                            ";
    }

    // line 154
    public function block_documentTargetGroups($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 155
        echo "                    ";
        if ( !twig_test_empty(($context["documentTargetGroups"] ?? null))) {
            // line 156
            echo "                        <h2
                            class=\"_ptgtb__collapse__trigger--block\"
                            data-ptgtb-toggle=\"collapse\"
                            data-ptgtb-target=\"#";
            // line 159
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "document-target-groups"], 159, $context, $this->getSourceContext());
            echo "\"
                            data-ptgtb-collapse-store=\"document-target-groups\"
                        >
                            Document Target Groups
                            ";
            // line 163
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_length_filter($this->env, ($context["documentTargetGroups"] ?? null))], 163, $context, $this->getSourceContext());
            echo "
                        </h2>

                        <table id=\"";
            // line 166
            echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "document-target-groups"], 166, $context, $this->getSourceContext());
            echo "\" class=\"_ptgtb__table\">
                            ";
            // line 167
            $this->displayBlock('documentTargetGroupsTable', $context, $blocks);
            // line 188
            echo "                        </table>
                    ";
        }
        // line 190
        echo "                ";
    }

    // line 167
    public function block_documentTargetGroupsTable($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 168
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["documentTargetGroups"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
            // line 169
            echo "                                    ";
            $context["rowIdentifier"] = twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "document-target-groups-details", twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 169)], 169, $context, $this->getSourceContext());
            // line 170
            echo "
                                    <tr class=\"_ptgtb__table__row-with-details\">
                                        <td>
                                            ";
            // line 173
            echo twig_call_macro($macros["toolbar"], "macro_trigger_label", [twig_truncate_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "document", [], "any", false, false, false, 173), "path", [], "any", false, false, false, 173), 32), ("#" . ($context["rowIdentifier"] ?? null)), [], ["title" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "document", [], "any", false, false, false, 173), "path", [], "any", false, false, false, 173)]], 173, $context, $this->getSourceContext());
            echo "
                                        </td>
                                        <td class=\"_ptgtb__table__col-right\">
                                            ";
            // line 176
            echo twig_call_macro($macros["toolbar"], "macro_label", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "targetGroup", [], "any", false, false, false, 176), "name", [], "any", false, false, false, 176), [0 => "_ptgtb__label--target-group"]], 176, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>
                                    <tr id=\"";
            // line 179
            echo twig_escape_filter($this->env, ($context["rowIdentifier"] ?? null), "html", null, true);
            echo "\" class=\"_ptgtb__table__row-details\">
                                        <td colspan=\"2\">
                                            ";
            // line 181
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Document ID", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "document", [], "any", false, false, false, 181), "id", [], "any", false, false, false, 181)], 181, $context, $this->getSourceContext());
            echo "
                                            ";
            // line 182
            echo twig_call_macro($macros["toolbar"], "macro_metric", ["Target Group ID", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "targetGroup", [], "any", false, false, false, 182), "id", [], "any", false, false, false, 182)], 182, $context, $this->getSourceContext());
            echo "
                                        </td>
                                    </tr>

                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 187
        echo "                            ";
    }

    // line 193
    public function block_advanced($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 194
        echo "                        ";
        $this->displayBlock('visitorInfo', $context, $blocks);
        // line 208
        echo "
                        ";
        // line 209
        $this->displayBlock('storage', $context, $blocks);
        // line 252
        echo "                    ";
    }

    // line 194
    public function block_visitorInfo($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 195
        echo "                            <h2
                                class=\"_ptgtb__collapse__trigger--block\"
                                data-ptgtb-toggle=\"collapse\"
                                data-ptgtb-target=\"#";
        // line 198
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "visitor-info"], 198, $context, $this->getSourceContext());
        echo "\"
                                data-ptgtb-collapse-default=\"collapse\"
                                data-ptgtb-collapse-store=\"visitor-info\"
                            >
                                Visitor Info
                            </h2>
                            <div id=\"";
        // line 204
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "visitor-info"], 204, $context, $this->getSourceContext());
        echo "\">
                                ";
        // line 205
        echo $this->extensions['Pimcore\Twig\Extension\DumpExtension']->dump(["data" => twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "data", [], "any", false, false, false, 205), "actions" => twig_get_attribute($this->env, $this->source, ($context["visitorInfo"] ?? null), "actions", [], "any", false, false, false, 205)]);
        echo "
                            </div>
                        ";
    }

    // line 209
    public function block_storage($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 210
        echo "                            <h2
                                class=\"_ptgtb__collapse__trigger--block\"
                                data-ptgtb-toggle=\"collapse\"
                                data-ptgtb-target=\"#";
        // line 213
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage"], 213, $context, $this->getSourceContext());
        echo "\"
                                data-ptgtb-collapse-default=\"collapse\"
                                data-ptgtb-collapse-store=\"storage\"
                            >
                                Storage
                            </h2>
                            <div id=\"";
        // line 219
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage"], 219, $context, $this->getSourceContext());
        echo "\" class=\"_ptgtb__storage\">

                                ";
        // line 221
        $this->displayBlock('visitorStorage', $context, $blocks);
        // line 235
        echo "
                                ";
        // line 236
        $this->displayBlock('sessionStorage', $context, $blocks);
        // line 250
        echo "                            </div>
                        ";
    }

    // line 221
    public function block_visitorStorage($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 222
        echo "                                    <h3
                                        class=\"_ptgtb__collapse__trigger--block\"
                                        data-ptgtb-toggle=\"collapse\"
                                        data-ptgtb-target=\"#";
        // line 225
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage-visitor"], 225, $context, $this->getSourceContext());
        echo "\"
                                        data-ptgtb-collapse-default=\"collapse\"
                                        data-ptgtb-collapse-store=\"storage-visitor\"
                                    >
                                        Visitor Storage
                                    </h3>
                                    <div id=\"";
        // line 231
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage-visitor"], 231, $context, $this->getSourceContext());
        echo "\">
                                        ";
        // line 232
        echo $this->extensions['Pimcore\Twig\Extension\DumpExtension']->dump(twig_get_attribute($this->env, $this->source, ($context["storage"] ?? null), "visitor", [], "any", false, false, false, 232));
        echo "
                                    </div>
                                ";
    }

    // line 236
    public function block_sessionStorage($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 237
        echo "                                    <h3
                                        class=\"_ptgtb__collapse__trigger--block\"
                                        data-ptgtb-toggle=\"collapse\"
                                        data-ptgtb-target=\"#";
        // line 240
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage-session"], 240, $context, $this->getSourceContext());
        echo "\"
                                        data-ptgtb-collapse-default=\"collapse\"
                                        data-ptgtb-collapse-store=\"storage-session\"
                                    >
                                        Session Storage
                                    </h3>
                                    <div id=\"";
        // line 246
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "storage-session"], 246, $context, $this->getSourceContext());
        echo "\">
                                        ";
        // line 247
        echo $this->extensions['Pimcore\Twig\Extension\DumpExtension']->dump(twig_get_attribute($this->env, $this->source, ($context["storage"] ?? null), "session", [], "any", false, false, false, 247));
        echo "
                                    </div>
                                ";
    }

    // line 255
    public function block_overrides($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 256
        echo "                    <h2
                        class=\"_ptgtb__collapse__trigger--block\"
                        data-ptgtb-toggle=\"collapse\"
                        data-ptgtb-target=\"#";
        // line 259
        echo twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "overrides"], 259, $context, $this->getSourceContext());
        echo "\"
                        data-ptgtb-collapse-default=\"collapse\"
                        data-ptgtb-collapse-store=\"overrides\"
                    >
                        Overrides
                    </h2>

                    ";
        // line 266
        $this->displayBlock('overrideForm', $context, $blocks);
        // line 278
        echo "                ";
    }

    // line 266
    public function block_overrideForm($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 267
        echo "                        ";
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["overrideForm"] ?? null), [0 => "form_div_layout.html.twig"], true);
        // line 268
        echo "                        ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["overrideForm"] ?? null), 'form_start', ["attr" => ["id" => twig_call_macro($macros["toolbar"], "macro_identifier", [($context["token"] ?? null), "overrides"], 268, $context, $this->getSourceContext()), "class" => "_ptgtb__override-form"]]);
        echo "
                        ";
        // line 269
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["overrideForm"] ?? null), 'widget');
        echo "

                        <div class=\"_ptgtb__override-form__button-row\">
                            <button type=\"reset\" value=\"\" class=\"_ptgtb--hidden\">Reset</button>
                            <button type=\"submit\" value=\"\">Apply</button>
                        </div>

                        ";
        // line 276
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["overrideForm"] ?? null), 'form_end');
        echo "
                    ";
    }

    // line 285
    public function block_css($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 286
        ob_start(function () { return ''; });
        // line 287
        echo "<style type=\"text/css\">";
        // line 288
        ob_start();
        // line 289
        echo twig_include($this->env, $context, "@PimcoreCore/Targeting/toolbar/toolbar.css");
        
; echo trim(str_replace("
", '', ob_get_clean())); 
        // line 291
        echo "</style>";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 295
    public function block_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 296
        echo "    ";
        echo twig_include($this->env, $context, "@PimcoreCore/Targeting/toolbar/toolbar_js.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Targeting/toolbar:toolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  814 => 296,  810 => 295,  805 => 291,  800 => 289,  798 => 288,  796 => 287,  794 => 286,  790 => 285,  784 => 276,  774 => 269,  769 => 268,  766 => 267,  762 => 266,  758 => 278,  756 => 266,  746 => 259,  741 => 256,  737 => 255,  730 => 247,  726 => 246,  717 => 240,  712 => 237,  708 => 236,  701 => 232,  697 => 231,  688 => 225,  683 => 222,  679 => 221,  674 => 250,  672 => 236,  669 => 235,  667 => 221,  662 => 219,  653 => 213,  648 => 210,  644 => 209,  637 => 205,  633 => 204,  624 => 198,  619 => 195,  615 => 194,  611 => 252,  609 => 209,  606 => 208,  603 => 194,  599 => 193,  595 => 187,  576 => 182,  572 => 181,  567 => 179,  561 => 176,  555 => 173,  550 => 170,  547 => 169,  529 => 168,  525 => 167,  521 => 190,  517 => 188,  515 => 167,  511 => 166,  505 => 163,  498 => 159,  493 => 156,  490 => 155,  486 => 154,  482 => 149,  463 => 144,  459 => 143,  454 => 141,  448 => 138,  442 => 135,  437 => 132,  434 => 131,  416 => 130,  412 => 129,  408 => 152,  404 => 150,  402 => 129,  398 => 128,  392 => 125,  385 => 121,  380 => 118,  377 => 117,  373 => 116,  369 => 111,  350 => 106,  346 => 105,  343 => 104,  337 => 102,  335 => 101,  330 => 99,  325 => 97,  319 => 94,  314 => 91,  311 => 90,  293 => 89,  289 => 88,  285 => 114,  281 => 112,  279 => 88,  275 => 87,  269 => 84,  262 => 80,  257 => 77,  254 => 76,  250 => 75,  244 => 69,  240 => 67,  234 => 65,  232 => 64,  224 => 58,  220 => 56,  214 => 54,  212 => 53,  206 => 49,  199 => 45,  194 => 42,  191 => 41,  187 => 40,  182 => 72,  180 => 40,  177 => 39,  173 => 38,  165 => 279,  163 => 255,  159 => 253,  157 => 193,  153 => 191,  151 => 154,  148 => 153,  146 => 116,  143 => 115,  141 => 75,  138 => 74,  136 => 38,  124 => 29,  109 => 17,  105 => 16,  98 => 12,  91 => 8,  86 => 6,  81 => 5,  78 => 4,  74 => 3,  70 => 295,  67 => 294,  65 => 285,  62 => 284,  60 => 3,  57 => 2,  55 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Targeting/toolbar:toolbar.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Targeting/toolbar/toolbar.html.twig");
    }
}
