<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:multiple_product_catalogue.html.twig */
class __TwigTemplate_880c8eef6927e33b7dbf77a4170dff60351a397dcccfc5abfb4276237203308a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            '_content' => [$this, 'block__content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layouts/print_catalog.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layouts/print_catalog.html.twig", ":web2print:multiple_product_catalogue.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block__content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        $this->extensions['AppBundle\Twig\Extension\PrintCatalogExtension']->createRegisterTitleStyling(twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_title"], "method", false, false, false, 3), twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_type"], "method", false, false, false, 3));
        // line 4
        echo "    <div class=\"page ";
        echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintCatalogExtension']->getRegisterName(twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_title"], "method", false, false, false, 4)), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_type"], "method", false, false, false, 4), "html", null, true);
        echo "\" id=\"documentpage-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "id", [], "any", false, false, false, 4), "html", null, true);
        echo "\">
        <div>
               ";
        // line 6
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "areablock", "content", ["allowed" => [0 => "print-product-catalog-cover", 1 => "print-multiple-product"]]);
        // line 13
        echo "
            

        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":web2print:multiple_product_catalogue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 13,  63 => 6,  53 => 4,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":web2print:multiple_product_catalogue.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/multiple_product_catalogue.html.twig");
    }
}
