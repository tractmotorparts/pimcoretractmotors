<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__4b7e8e50a485d35c722ca2b7196f20a6eb33fab6fa1bd3c63a52a38849f8024b */
class __TwigTemplate_9c3200a1c1773dff71e91a16bd0287d9f5b8d8a408f9fbc1ff4da122bda66242 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA TB33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__4b7e8e50a485d35c722ca2b7196f20a6eb33fab6fa1bd3c63a52a38849f8024b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__4b7e8e50a485d35c722ca2b7196f20a6eb33fab6fa1bd3c63a52a38849f8024b", "");
    }
}
