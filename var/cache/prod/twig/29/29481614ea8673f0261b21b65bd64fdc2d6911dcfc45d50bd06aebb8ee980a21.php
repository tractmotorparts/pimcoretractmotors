<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-catalog-cover:view.html.twig */
class __TwigTemplate_f1688839fcbe9b6360cf87b3b602be8db0bc4e3b5112ada00c859044878989bb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
   @page {
            size: auto;
            margin: 0;
            padding: 0;
            background-origin: -ro-page-box;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
        }
        @media print {
            .bg-img img {
                position: fixed !important;
                left: 0;
                top: 0;
                width: 100%;
            }
        }
</style>
";
        // line 24
        if (($context["editmode"] ?? null)) {
            // line 25
            echo "    ";
        }
        // line 30
        echo "<div class=\"cover-page-wrap\">
    <table border=\"0\" style=\"width: 100%; border: 0; border-spacing: 0;\">
        <tr>
            <td style=\"height: 100vh;
                background-repeat: no-repeat;
                background-size: cover;
                position: relative;
                background-position: right;\">
                <div class=\"bg-img\">
                    ";
        // line 40
        echo "                    ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "backgroud-cover-image");
        echo "
                </div>
               <table class=\"title-table\">
                   <tr><td><h1 class=\"heading\"> ";
        // line 43
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "cover-page-title", ["placeholder" => "Enter Title 1"]);
        echo "<br><span> ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "cover-page-subtitle", ["placeholder" => "Enter Title 2"]);
        echo "</span></h1></td></tr>
                   <tr><td>
                           ";
        // line 45
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "image", "backgroud-cover-logo", ["class" => "company-logo", "style" => "width:400px"]);
        echo "
                           ";
        // line 47
        echo "                       </td>
                   </tr>
               </table>               
            </td>
        </tr>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return ":Areas/print-product-catalog-cover:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 47,  92 => 45,  85 => 43,  78 => 40,  67 => 30,  64 => 25,  62 => 24,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-product-catalog-cover:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-catalog-cover/view.html.twig");
    }
}
