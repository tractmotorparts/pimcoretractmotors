<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Workflow/statusinfo:allPlacesStatusInfo.html.twig */
class __TwigTemplate_821dc85b165b7d7b9d0c7791b70691a58bc60fc42092ea954ba307ccd56ea9ba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "
<div>
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["places"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["place"]) {
            // line 6
            echo "
        <div class=\"pimcore-workflow-place-indicator\" style=\"background-color: ";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["place"], "backgroundColor", [], "any", false, false, false, 7), "html", null, true);
            echo "; color:";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["place"], "fontColor", [], "any", false, false, false, 7), "html", null, true);
            echo "; border: 1px solid ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["place"], "borderColor", [], "any", false, false, false, 7), "html", null, true);
            echo ";\" title=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["place"], "title", [], "any", false, false, false, 7), "html", null, true);
            echo "\">
            ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => twig_get_attribute($this->env, $this->source, $context["place"], "label", [], "any", false, false, false, 8), 1 => [], 2 => "admin"], "method", false, false, false, 8), "html", null, true);
            echo "
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['place'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Workflow/statusinfo:allPlacesStatusInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 11,  58 => 8,  48 => 7,  45 => 6,  41 => 5,  37 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Workflow/statusinfo:allPlacesStatusInfo.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Workflow/statusinfo/allPlacesStatusInfo.html.twig");
    }
}
