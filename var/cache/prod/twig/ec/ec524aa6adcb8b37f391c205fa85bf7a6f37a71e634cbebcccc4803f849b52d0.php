<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__478c9bfd78f051f9c43d5347a30b69638bd463b724a4c82c625892c38aee701e */
class __TwigTemplate_2bd2f42e2f99b4b4558ce65942bfcc998b8864d7845b48d11dfdc74df909cae5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "TOKUDEN POWERED BY SHINERAY SRGC600 GRASS CUTTER  WITH TK270 ,9HP PETROL ENGINE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__478c9bfd78f051f9c43d5347a30b69638bd463b724a4c82c625892c38aee701e";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__478c9bfd78f051f9c43d5347a30b69638bd463b724a4c82c625892c38aee701e", "");
    }
}
