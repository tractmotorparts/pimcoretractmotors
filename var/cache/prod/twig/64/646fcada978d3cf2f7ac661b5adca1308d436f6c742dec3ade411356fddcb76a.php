<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__4c85b2c313c7118bff4eaf7d09cb41c98328c9a396eb47ac9b46813fce84e111 */
class __TwigTemplate_c7820a764052a2576bbb6d4a82ad3cea69ce756d7ac0fb5923d44844d76a97e6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC 1LB 2.6MM SPOOL SQUARE CUT YELLOW TRIMMER LINE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__4c85b2c313c7118bff4eaf7d09cb41c98328c9a396eb47ac9b46813fce84e111";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__4c85b2c313c7118bff4eaf7d09cb41c98328c9a396eb47ac9b46813fce84e111", "");
    }
}
