<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:universalTrackingCode.html.twig */
class __TwigTemplate_329b92de4c80e7fe9ddf1d592d22b00758575e9ea28c05588ff5e0e9186af7cc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'beforeScript' => [$this, 'block_beforeScript'],
            'beforeInit' => [$this, 'block_beforeInit'],
            'trackInit' => [$this, 'block_trackInit'],
            'beforeTrack' => [$this, 'block_beforeTrack'],
            'track' => [$this, 'block_track'],
            'afterTrack' => [$this, 'block_afterTrack'],
            'afterScript' => [$this, 'block_afterScript'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
<script>
    ";
        // line 4
        $this->displayBlock('beforeScript', $context, $blocks);
        // line 5
        echo "
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ";
        // line 11
        $this->displayBlock('beforeInit', $context, $blocks);
        // line 12
        echo "
    ";
        // line 13
        $this->displayBlock('trackInit', $context, $blocks);
        // line 22
        echo "
    ";
        // line 23
        $this->displayBlock('beforeTrack', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('track', $context, $blocks);
        // line 36
        echo "
    ";
        // line 37
        $this->displayBlock('afterTrack', $context, $blocks);
        // line 38
        echo "    ";
        $this->displayBlock('afterScript', $context, $blocks);
        // line 39
        echo "</script>

";
        // line 41
        $this->displayBlock('afterScriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 4
    public function block_beforeScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScript", [], "any", false, false, false, 4);
    }

    // line 11
    public function block_beforeInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeInit", [], "any", false, false, false, 11);
    }

    // line 13
    public function block_trackInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        if (($context["universalConfiguration"] ?? null)) {
            // line 15
            echo "    ga('create', '";
            echo twig_escape_filter($this->env, ($context["trackId"] ?? null), "html", null, true);
            echo "', ";
            echo ($context["universalConfiguration"] ?? null);
            echo ");
    ";
        } else {
            // line 17
            echo "    ga('create', '";
            echo twig_escape_filter($this->env, ($context["trackId"] ?? null), "html", null, true);
            echo "');
    ";
        }
        // line 19
        echo "
    ga('set', 'anonymizeIp', true);
    ";
    }

    // line 23
    public function block_beforeTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeTrack", [], "any", false, false, false, 23);
    }

    // line 25
    public function block_track($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    if (typeof _gaqPageView != \"undefined\"){
        ga('send', 'pageview', _gaqPageView);
    } else {
        ";
        // line 29
        if (($context["defaultPath"] ?? null)) {
            // line 30
            echo "        ga('send', 'pageview', '";
            echo twig_escape_filter($this->env, ($context["defaultPath"] ?? null), "html", null, true);
            echo "');
        ";
        } else {
            // line 32
            echo "        ga('send', 'pageview');
        ";
        }
        // line 34
        echo "    }
    ";
    }

    // line 37
    public function block_afterTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterTrack", [], "any", false, false, false, 37);
    }

    // line 38
    public function block_afterScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScript", [], "any", false, false, false, 38);
    }

    // line 41
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 41);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:universalTrackingCode.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  190 => 41,  183 => 38,  176 => 37,  171 => 34,  167 => 32,  161 => 30,  159 => 29,  154 => 26,  150 => 25,  143 => 23,  137 => 19,  131 => 17,  123 => 15,  120 => 14,  116 => 13,  109 => 11,  102 => 4,  95 => 1,  91 => 41,  87 => 39,  84 => 38,  82 => 37,  79 => 36,  77 => 25,  74 => 24,  72 => 23,  69 => 22,  67 => 13,  64 => 12,  62 => 11,  54 => 5,  52 => 4,  48 => 2,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:universalTrackingCode.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Analytics/Tracking/Google/Analytics/universalTrackingCode.html.twig");
    }
}
