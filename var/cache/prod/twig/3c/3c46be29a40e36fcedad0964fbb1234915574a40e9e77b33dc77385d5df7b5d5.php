<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__95dbf2a0fc15ed69f355a0daa466177e33031810b183f57b38d6c635be7ca1fb */
class __TwigTemplate_86ca51097cb0093e5e983c0872ba6bc43eb2815f8d796a78b66477556c1b0317 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KANAZAWA G45 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__95dbf2a0fc15ed69f355a0daa466177e33031810b183f57b38d6c635be7ca1fb";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__95dbf2a0fc15ed69f355a0daa466177e33031810b183f57b38d6c635be7ca1fb", "");
    }
}
