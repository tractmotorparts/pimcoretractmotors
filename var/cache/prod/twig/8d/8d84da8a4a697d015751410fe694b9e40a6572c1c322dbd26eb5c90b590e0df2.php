<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__afea1a5b78abd57281d3e44e9d3ea5a6d8753416d0f96824ba7ca0be75bdb6e9 */
class __TwigTemplate_6318dad3e43a5ccc2b51e93eef13b1f77d3301d077ce219d95f42c8565fc22f3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC 5KG 2.6MM TRIMMER LINE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__afea1a5b78abd57281d3e44e9d3ea5a6d8753416d0f96824ba7ca0be75bdb6e9";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__afea1a5b78abd57281d3e44e9d3ea5a6d8753416d0f96824ba7ca0be75bdb6e9", "");
    }
}
