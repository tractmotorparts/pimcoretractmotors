<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2cbff761e0a4a3a1e24514745ac5cff47ebe5c03dd93585a2b7f95669d235097 */
class __TwigTemplate_b77113dfb381b36dc6d76b0cb842f0ea2150c6ea4f068e0e4633f7353694aab5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA TL33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2cbff761e0a4a3a1e24514745ac5cff47ebe5c03dd93585a2b7f95669d235097";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2cbff761e0a4a3a1e24514745ac5cff47ebe5c03dd93585a2b7f95669d235097", "");
    }
}
