<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__8e298e9e1e820be249ca323cc48787809a500c4a5e47d4974e3f356469c0d62c */
class __TwigTemplate_5eb4bf4632be72bf3151fe00434b433bb205620fdd21c7857df8b0ba34f02b05 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V338 BLUE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__8e298e9e1e820be249ca323cc48787809a500c4a5e47d4974e3f356469c0d62c";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__8e298e9e1e820be249ca323cc48787809a500c4a5e47d4974e3f356469c0d62c", "");
    }
}
