<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Google/TagManager:codeBody.html.twig */
class __TwigTemplate_5ec372a174409786be6d56730a295d076c435262b45832cc104ef6c19e237f53 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeNoscriptTag' => [$this, 'block_beforeNoscriptTag'],
            'code' => [$this, 'block_code'],
            'afterNoscriptTag' => [$this, 'block_afterNoscriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeNoscriptTag', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('code', $context, $blocks);
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('afterNoscriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeNoscriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeNoscriptTag", [], "any", false, false, false, 1);
    }

    // line 3
    public function block_code($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<!-- Google Tag Manager (noscript) -->
<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=";
        // line 5
        echo twig_escape_filter($this->env, ($context["containerId"] ?? null), "html", null, true);
        echo "\"
height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
";
    }

    // line 10
    public function block_afterNoscriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterNoscriptTag", [], "any", false, false, false, 10);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Google/TagManager:codeBody.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  76 => 10,  68 => 5,  65 => 4,  61 => 3,  54 => 1,  50 => 10,  47 => 9,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Google/TagManager:codeBody.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Google/TagManager/codeBody.html.twig");
    }
}
