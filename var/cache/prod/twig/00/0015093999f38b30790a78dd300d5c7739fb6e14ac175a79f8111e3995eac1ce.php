<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:asset.html.php */
class __TwigTemplate_08fc4bd1798fc0f2fae2557f6d1c4f03cd8fabd942a5f88d4a16fae442fe923f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
    /** @var \\Pimcore\\Templating\\PhpEngine \$view */
    /**
     * @var \\Pimcore\\Model\\Asset\\Image|\\Pimcore\\Model\\Asset\\Document|\\Pimcore\\Model\\Asset\\Video \$element
     */
    \$element = \$this->element;
    \$this->get(\"translate\")->setDomain(\"admin\");

    \$previewImage = null;
    \$params = [
        'id' => \$element->getId(),
        'treepreview' => true,
        'hdpi' => true,
    ];

    try {
        if (\$element instanceof \\Pimcore\\Model\\Asset\\Image) {
             \$previewImage = \$view->router()->path('pimcore_admin_asset_getimagethumbnail', \$params);
        }
        elseif (\$element instanceof \\Pimcore\\Model\\Asset\\Video && \\Pimcore\\Video::isAvailable()) {
            \$previewImage = \$view->router()->path('pimcore_admin_asset_getvideothumbnail', \$params);
        }
        if (\$element instanceof \\Pimcore\\Model\\Asset\\Document && \\Pimcore\\Document::isAvailable()) {
            \$previewImage = \$view->router()->path('pimcore_admin_asset_getdocumentthumbnail', \$params);
        }
    } catch (\\Exception \$e) {

    }
?>

<?php if(\$previewImage) {?>
    <div class=\"full-preview\">
        <img src=\"<?= \$previewImage ?>\" onload=\"this.parentNode.className += ' complete';\">
        <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
    </div>
<?php } else { ?>
    <div class=\"mega-icon <?= \$this->iconCls ?>\"></div>
    <?= \$this->render('PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:info-table.html.php', ['element' => \$element]) ?>
<?php } ?>

";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:asset.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:SearchAdmin/Search/Quicksearch:asset.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/SearchAdmin/Search/Quicksearch/asset.html.php");
    }
}
