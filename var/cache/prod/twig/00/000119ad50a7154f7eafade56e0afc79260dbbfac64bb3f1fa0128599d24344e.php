<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__b1de205d7334a53afa830256453c6038f524892619302a75d946c0ef887d3036 */
class __TwigTemplate_aaecdc8282b70f5869e0753cea4efbb83c6b02b58725dd20bd40c84f2b9820ad extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA M430 MIST BLOWER is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__b1de205d7334a53afa830256453c6038f524892619302a75d946c0ef887d3036";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__b1de205d7334a53afa830256453c6038f524892619302a75d946c0ef887d3036", "");
    }
}
