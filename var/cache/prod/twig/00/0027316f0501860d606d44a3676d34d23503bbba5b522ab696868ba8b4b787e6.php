<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__42fc66ea2ab64f9e9ac8ef6b131cbdf1d82b2087b8046a5808f71dc354521b0b */
class __TwigTemplate_4808fa334057511f893a5c70a88365da2d5a9d3da89e43a4414ac6948949914c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>ECO348</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>VICTA ECO348</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting For Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__42fc66ea2ab64f9e9ac8ef6b131cbdf1d82b2087b8046a5808f71dc354521b0b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__42fc66ea2ab64f9e9ac8ef6b131cbdf1d82b2087b8046a5808f71dc354521b0b", "");
    }
}
