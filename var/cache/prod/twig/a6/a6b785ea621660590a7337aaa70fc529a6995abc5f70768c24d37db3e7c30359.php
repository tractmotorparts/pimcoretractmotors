<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2f05758a620f244b3982b1544b2dfd7a307169b0dfdbeb80f3db5f82d5935f2a */
class __TwigTemplate_8fdb35d5f79cf60da0aaf8e99ed6eb68484e3439d774604504639e820a256c45 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA ECO348 is approved";
    }

    public function getTemplateName()
    {
        return "__string_template__2f05758a620f244b3982b1544b2dfd7a307169b0dfdbeb80f3db5f82d5935f2a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2f05758a620f244b3982b1544b2dfd7a307169b0dfdbeb80f3db5f82d5935f2a", "");
    }
}
