<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__fa218e238fd74e2dffab5c18c7bcaff9fbb913c42d7dce888ac9a397617ec13d */
class __TwigTemplate_b3f48465f4289f3cebcf3883bffb5050411e8c54a09eb1c9c1ee2721e11d3e5e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>SRGC600</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>TOKUDEN POWERED BY SHINERAY SRGC600 GRASS CUTTER  WITH TK270 ,9HP PETROL ENGINE</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__fa218e238fd74e2dffab5c18c7bcaff9fbb913c42d7dce888ac9a397617ec13d";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__fa218e238fd74e2dffab5c18c7bcaff9fbb913c42d7dce888ac9a397617ec13d", "");
    }
}
