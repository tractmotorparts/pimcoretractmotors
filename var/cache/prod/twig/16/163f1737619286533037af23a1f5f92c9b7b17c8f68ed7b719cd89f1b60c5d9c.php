<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__d38b3dde04b095e5668b73d9016397ef958bef0d33a0787af02fa072e9d7671b */
class __TwigTemplate_77575b8cac31a51750fae7b1494173e22598d859818fcaff2447dbf94bd8b133 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html>
<head><style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style></head>
<body>
                       <table style=\"width: 100%; border: 1px solid black;\">
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">SKU</th>
                              <td style=\"border: 1px solid black; padding: 5px;\">1NYLON</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Name</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">PROMAC 1KG 12\" PRECUT 2.6MM NYLON ROPE</td>
                            </tr>
<tr>
<th style=\"border: 1px solid black; padding: 5px; text-align: center;\">Status</th>
                               <td style=\"border: 1px solid black; padding: 5px;\">Awaiting For Approval</td>
                            </tr>
</table>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__d38b3dde04b095e5668b73d9016397ef958bef0d33a0787af02fa072e9d7671b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__d38b3dde04b095e5668b73d9016397ef958bef0d33a0787af02fa072e9d7671b", "");
    }
}
