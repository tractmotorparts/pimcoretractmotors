<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__4c7a39e31d05e4162da74be41ac2b9242759fd24f3228d7ab1888dcea08888fc */
class __TwigTemplate_f7c93da8ed6d4ffb63adf91d5f2714aeb6baed88e73d15dae470951b47bf30f5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA B16 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__4c7a39e31d05e4162da74be41ac2b9242759fd24f3228d7ab1888dcea08888fc";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__4c7a39e31d05e4162da74be41ac2b9242759fd24f3228d7ab1888dcea08888fc", "");
    }
}
