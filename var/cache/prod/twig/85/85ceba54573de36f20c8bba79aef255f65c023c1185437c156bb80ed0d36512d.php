<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :layouts:print_catalog.html.twig */
class __TwigTemplate_44f98a06ea820de2c0fc74338eb709c954425013fea103dc7addc402fec4ce3e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head_stylesheets' => [$this, 'block_head_stylesheets'],
            'headscripts' => [$this, 'block_headscripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"\">
    <head>
        ";
        // line 4
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 7
        echo "        <link href=\"https://fonts.googleapis.com/css?family=Hind+Guntur:300,400,500,600,700&display=swap\" rel=\"stylesheet\">
        
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"print\">
        <link rel=\"stylesheet\" type=\"text/css\"  href=\"/static/print/css/pdf-styles.css\"     media=\"screen\">
        
        ";
        // line 12
        if ((array_key_exists("printermarks", $context) && (($context["printermarks"] ?? null) == true))) {
            // line 13
            echo "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/pimcoreadmin/css/print/print-printermarks.css\" media=\"print\" />
        ";
        }
        // line 15
        echo "        
        ";
        // line 16
        if (($context["editmode"] ?? null)) {
            // line 17
            echo "            <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/print/css/print-edit.css\"  media=\"screen\" />
        ";
        }
        // line 19
        echo "        
        <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>
        
        ";
        // line 22
        $this->displayBlock('headscripts', $context, $blocks);
        // line 25
        echo "    </head>
    <body>
        <div class=\"site\">
        ";
        // line 28
        $this->displayBlock("_content", $context, $blocks);
        echo "
        </div>
    </body>
</html>

";
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
    }

    public function block_head_stylesheets($context, array $blocks = [])
    {
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->defer($this, 'head_stylesheets');
    }

    // line 4
    public function block_head_stylesheets_deferred($context, array $blocks = [])
    {
        // line 5
        echo "            ";
        echo call_user_func_array($this->env->getFunction('pimcore_head_link')->getCallable(), []);
        echo "
        ";
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
    }

    public function block_headscripts($context, array $blocks = [])
    {
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->defer($this, 'headscripts');
    }

    // line 22
    public function block_headscripts_deferred($context, array $blocks = [])
    {
        // line 23
        echo "            ";
        echo call_user_func_array($this->env->getFunction('pimcore_head_script')->getCallable(), []);
        echo "
        ";
        $this->env->getExtension('Phive\Twig\Extensions\Deferred\DeferredExtension')->resolve($this, $context, $blocks);
    }

    public function getTemplateName()
    {
        return ":layouts:print_catalog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 23,  112 => 22,  99 => 5,  96 => 4,  80 => 28,  75 => 25,  73 => 22,  68 => 19,  64 => 17,  62 => 16,  59 => 15,  55 => 13,  53 => 12,  46 => 7,  44 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":layouts:print_catalog.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/layouts/print_catalog.html.twig");
    }
}
