<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Document/Document:diff-versions-unsupported.html.php */
class __TwigTemplate_ff54a99f37e2712ea906541de909a72adf4b953586f645eea49a91c1387385f2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
<head lang=\"en\">
    <?php
        \$this->get(\"translate\")->setDomain(\"admin\");
    ?>
    <meta charset=\"UTF-8\">
    <style type=\"text/css\">

        html, body {
            height: 100%;
        }

        #message {
            position: absolute;
            width: 100%;
            left: 0;
            top: 45%;
            text-align: center;
            font-family: Arial , Tahoma, Verdana, sans-serif;
            font-size: 16px;
            color: darkred;
        }
    </style>
</head>
<body>

    <div id=\"message\">
        <?= \$this->translate(\"unsupported_feature\"); ?>
        <br>
        <br>
        <b>Wkhtmltoimage binary and PHP extension Imagick are required!</b>
    </div>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Document/Document:diff-versions-unsupported.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Document/Document:diff-versions-unsupported.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Document/Document/diff-versions-unsupported.html.php");
    }
}
