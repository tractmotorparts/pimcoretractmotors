<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :web2print:default_no_layout.html.twig */
class __TwigTemplate_7e0806ba648b5d7ecee3a74f4efd6b374fb3604224d419de9dd857700026ae68 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_content' => [$this, 'block__content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_content', $context, $blocks);
    }

    public function block__content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->extensions['AppBundle\Twig\Extension\PrintCatalogExtension']->createRegisterTitleStyling(twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_title"], "method", false, false, false, 2), twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_type"], "method", false, false, false, 2));
        // line 3
        echo "    <div class=\"page ";
        echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintCatalogExtension']->getRegisterName(twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_title"], "method", false, false, false, 3)), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "getProperty", [0 => "print_register_type"], "method", false, false, false, 3), "html", null, true);
        echo "\" id=\"documentpage-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["document"] ?? null), "id", [], "any", false, false, false, 3), "html", null, true);
        echo "\">
        <div>

           ";
        // line 18
        echo "
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":web2print:default_no_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  59 => 18,  48 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":web2print:default_no_layout.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/web2print/default_no_layout.html.twig");
    }
}
