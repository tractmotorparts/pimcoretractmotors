<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php */
class __TwigTemplate_1f818eb4f1f5740c86e18ab60b4ab9e2b222be0b9f2045bb02b9817270e0fb71 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/**
 * @var \\Pimcore\\Templating\\PhpEngine \$this
 * @var \\Pimcore\\Templating\\PhpEngine \$view
 * @var \\Pimcore\\Templating\\GlobalVariables \$app
 */

\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');
\$this->get(\"translate\")->setDomain(\"admin\");
?>



<?php if (\$this->error) { ?>
    <div class=\"text error\">
        <?= \$this->translate(\$this->error) ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your verification code\"); ?>
    </div>
<?php } ?>

<form method=\"post\" action=\"<?=\$this->url('pimcore_admin_2fa-verify')?>\">
    <input name=\"_auth_code\" id=\"_auth_code\" autocomplete=\"one-time-code\" type=\"password\" placeholder=\"<?= \$this->translate(\"2fa_code\"); ?>\" required autofocus>
    <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

    <button type=\"submit\"><?= \$this->translate(\"Login\"); ?></button>
</form>

<a href=\"<?= \$view->router()->path('pimcore_admin_logout') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Login:twoFactorAuthentication.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/twoFactorAuthentication.html.php");
    }
}
