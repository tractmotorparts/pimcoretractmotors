<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__13eb27e8a2d5eda7a233d77e9a01ddc5362d7d6fa7eb2ed40502c1100a9f512a */
class __TwigTemplate_d80d4fef7f2057b92dbf4f96ff6041dd20e50dcb011aea01f085b978a5c798a0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA T120 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__13eb27e8a2d5eda7a233d77e9a01ddc5362d7d6fa7eb2ed40502c1100a9f512a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__13eb27e8a2d5eda7a233d77e9a01ddc5362d7d6fa7eb2ed40502c1100a9f512a", "");
    }
}
