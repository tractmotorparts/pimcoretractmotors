<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__dfec164ebbd9dd0044a72a7a273906e58dc25ccb7a4dfbf500def91f9910efe2 */
class __TwigTemplate_e75ae251876981d291f675f0614a95be3500b807ea736562650eb7817262656a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V500BB is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__dfec164ebbd9dd0044a72a7a273906e58dc25ccb7a4dfbf500def91f9910efe2";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__dfec164ebbd9dd0044a72a7a273906e58dc25ccb7a4dfbf500def91f9910efe2", "");
    }
}
