<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__3451afc5b349bdd0f1f226081ccc1ed7d82e67c3c8b9f78b4c47e73ed6a315ef */
class __TwigTemplate_165f5e7a395552f6fefc7f97e80844c547217f46bfd71d59774c444f64b59a28 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>KDF8500Q3</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>KOOP KDF8500Q-3D  DIESEL GENERATOR SET 6KW ,30L FUEL TANK  ELEC ST SILENT , 3 PHASE / SINGLE PHASE </td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__3451afc5b349bdd0f1f226081ccc1ed7d82e67c3c8b9f78b4c47e73ed6a315ef";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__3451afc5b349bdd0f1f226081ccc1ed7d82e67c3c8b9f78b4c47e73ed6a315ef", "");
    }
}
