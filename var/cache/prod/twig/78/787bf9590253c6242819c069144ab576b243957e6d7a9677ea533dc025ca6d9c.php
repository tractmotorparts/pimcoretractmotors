<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:gtagTrackingCode.html.twig */
class __TwigTemplate_b7ca3febcab1455f6bbeedde89319602b4e869e488b2712c07e2665fdca19313 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'beforeScript' => [$this, 'block_beforeScript'],
            'beforeInit' => [$this, 'block_beforeInit'],
            'beforeTrack' => [$this, 'block_beforeTrack'],
            'track' => [$this, 'block_track'],
            'afterTrack' => [$this, 'block_afterTrack'],
            'afterScript' => [$this, 'block_afterScript'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
<script async src=\"https://www.googletagmanager.com/gtag/js?id=";
        // line 3
        echo twig_escape_filter($this->env, ($context["trackId"] ?? null), "html", null, true);
        echo "\"></script>
<script>
    ";
        // line 5
        $this->displayBlock('beforeScript', $context, $blocks);
        // line 6
        echo "    ";
        $this->displayBlock('beforeInit', $context, $blocks);
        // line 7
        echo "
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    ";
        // line 12
        $this->displayBlock('beforeTrack', $context, $blocks);
        // line 13
        echo "
    ";
        // line 14
        $this->displayBlock('track', $context, $blocks);
        // line 17
        echo "
    ";
        // line 18
        $this->displayBlock('afterTrack', $context, $blocks);
        // line 19
        echo "    ";
        $this->displayBlock('afterScript', $context, $blocks);
        // line 20
        echo "</script>

";
        // line 22
        $this->displayBlock('afterScriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 5
    public function block_beforeScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScript", [], "any", false, false, false, 5);
    }

    // line 6
    public function block_beforeInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeInit", [], "any", false, false, false, 6);
    }

    // line 12
    public function block_beforeTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeTrack", [], "any", false, false, false, 12);
    }

    // line 14
    public function block_track($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    gtag('config', '";
        echo twig_escape_filter($this->env, ($context["trackId"] ?? null), "html", null, true);
        echo "', ";
        echo json_encode(($context["gtagConfig"] ?? null));
        echo ");
    ";
    }

    // line 18
    public function block_afterTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterTrack", [], "any", false, false, false, 18);
    }

    // line 19
    public function block_afterScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScript", [], "any", false, false, false, 19);
    }

    // line 22
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 22);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:gtagTrackingCode.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 22,  138 => 19,  131 => 18,  122 => 15,  118 => 14,  111 => 12,  104 => 6,  97 => 5,  90 => 1,  86 => 22,  82 => 20,  79 => 19,  77 => 18,  74 => 17,  72 => 14,  69 => 13,  67 => 12,  60 => 7,  57 => 6,  55 => 5,  50 => 3,  47 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Analytics/Tracking/Google/Analytics:gtagTrackingCode.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Analytics/Tracking/Google/Analytics/gtagTrackingCode.html.twig");
    }
}
