<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2068cb270c3f49196f483659879af358b7930eb652a06f308577bc7fc671c4b6 */
class __TwigTemplate_6ea59225f0d073ff00a537c82d26556b44c8f12794a8f92c5eb46318e3d96acc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KANAZAWA TU43 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2068cb270c3f49196f483659879af358b7930eb652a06f308577bc7fc671c4b6";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2068cb270c3f49196f483659879af358b7930eb652a06f308577bc7fc671c4b6", "");
    }
}
