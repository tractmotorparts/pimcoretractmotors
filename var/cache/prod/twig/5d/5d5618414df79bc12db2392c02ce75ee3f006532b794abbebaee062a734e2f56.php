<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Analytics/Tracking/Piwik:trackingCode.html.twig */
class __TwigTemplate_5c0bacd0f4d768448cdfe11505e1a41845d494a26a61d7414e21699ee3126b10 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'beforeScript' => [$this, 'block_beforeScript'],
            'beforeTrack' => [$this, 'block_beforeTrack'],
            'track' => [$this, 'block_track'],
            'afterTrack' => [$this, 'block_afterTrack'],
            'asyncInit' => [$this, 'block_asyncInit'],
            'beforeAsync' => [$this, 'block_beforeAsync'],
            'async' => [$this, 'block_async'],
            'afterAsync' => [$this, 'block_afterAsync'],
            'afterScript' => [$this, 'block_afterScript'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
<script>
    ";
        // line 5
        echo "    ";
        $this->displayBlock('beforeScript', $context, $blocks);
        // line 6
        echo "
    window._paq = window._paq || [];

    ";
        // line 9
        $this->displayBlock('beforeTrack', $context, $blocks);
        // line 10
        echo "
    ";
        // line 11
        $this->displayBlock('track', $context, $blocks);
        // line 12
        echo "
    ";
        // line 13
        $this->displayBlock('afterTrack', $context, $blocks);
        // line 14
        echo "
    (function() {
        ";
        // line 16
        $this->displayBlock('asyncInit', $context, $blocks);
        // line 19
        echo "
        ";
        // line 20
        $this->displayBlock('beforeAsync', $context, $blocks);
        // line 21
        echo "
        ";
        // line 22
        $this->displayBlock('async', $context, $blocks);
        // line 26
        echo "
        ";
        // line 27
        $this->displayBlock('afterAsync', $context, $blocks);
        // line 28
        echo "    })();

    ";
        // line 30
        $this->displayBlock('afterScript', $context, $blocks);
        // line 31
        echo "    ";
        // line 32
        echo "</script>

";
        // line 34
        $this->displayBlock('afterScriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 5
    public function block_beforeScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScript", [], "any", false, false, false, 5);
    }

    // line 9
    public function block_beforeTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeTrack", [], "any", false, false, false, 9);
    }

    // line 11
    public function block_track($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "track", [], "any", false, false, false, 11);
    }

    // line 13
    public function block_afterTrack($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterTrack", [], "any", false, false, false, 13);
    }

    // line 16
    public function block_asyncInit($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "        var u='";
        echo twig_trim_filter(($context["piwikUrl"] ?? null), "/", "right");
        echo "/';
        ";
    }

    // line 20
    public function block_beforeAsync($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeAsync", [], "any", false, false, false, 20);
    }

    // line 22
    public function block_async($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        ";
    }

    // line 27
    public function block_afterAsync($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterAsync", [], "any", false, false, false, 27);
    }

    // line 30
    public function block_afterScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScript", [], "any", false, false, false, 30);
    }

    // line 34
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 34);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Analytics/Tracking/Piwik:trackingCode.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 34,  181 => 30,  174 => 27,  168 => 23,  164 => 22,  157 => 20,  150 => 17,  146 => 16,  139 => 13,  132 => 11,  125 => 9,  118 => 5,  111 => 1,  107 => 34,  103 => 32,  101 => 31,  99 => 30,  95 => 28,  93 => 27,  90 => 26,  88 => 22,  85 => 21,  83 => 20,  80 => 19,  78 => 16,  74 => 14,  72 => 13,  69 => 12,  67 => 11,  64 => 10,  62 => 9,  57 => 6,  54 => 5,  50 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Analytics/Tracking/Piwik:trackingCode.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Analytics/Tracking/Piwik/trackingCode.html.twig");
    }
}
