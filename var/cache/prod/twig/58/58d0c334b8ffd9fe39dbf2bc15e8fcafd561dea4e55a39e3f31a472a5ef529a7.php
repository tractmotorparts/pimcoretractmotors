<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__226c834aedf7ed1bb46390a76584a429835592c92a2f4e8170af3555e12d0d55 */
class __TwigTemplate_aabcd11be910c7675c141ebc07bde526d097b34e95dbfa389b63367aa07a742b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KANAZAWA TB33 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__226c834aedf7ed1bb46390a76584a429835592c92a2f4e8170af3555e12d0d55";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__226c834aedf7ed1bb46390a76584a429835592c92a2f4e8170af3555e12d0d55", "");
    }
}
