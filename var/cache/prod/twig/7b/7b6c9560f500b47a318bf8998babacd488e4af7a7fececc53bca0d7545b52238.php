<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__4d54aca75c65d6f354ceeae7dc543257901a9ad5609d2fa72b933b20b90d52a1 */
class __TwigTemplate_8e008d2d2d40608187eb231394a56b2fde5e9a2805eea1bbffb35fba3a97e8ec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA KB328 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__4d54aca75c65d6f354ceeae7dc543257901a9ad5609d2fa72b933b20b90d52a1";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__4d54aca75c65d6f354ceeae7dc543257901a9ad5609d2fa72b933b20b90d52a1", "");
    }
}
