<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Google/TagManager:codeHead.html.twig */
class __TwigTemplate_1a1d1d30c9db49bf9b8e715f67aa66911df37f113fa3d12592c4f37782ea41f5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'code' => [$this, 'block_code'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('code', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('afterScriptTag', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 3
    public function block_code($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','";
        // line 9
        echo twig_escape_filter($this->env, ($context["containerId"] ?? null), "html", null, true);
        echo "');</script>
<!-- End Google Tag Manager -->
";
    }

    // line 13
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 13);
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Google/TagManager:codeHead.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  79 => 13,  72 => 9,  65 => 4,  61 => 3,  54 => 1,  50 => 13,  47 => 12,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Google/TagManager:codeHead.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Google/TagManager/codeHead.html.twig");
    }
}
