<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e88293567ba4edcdbb21328162ee1dc6b6f4bc58d6365c9d0e9cc6696b9ca54a */
class __TwigTemplate_746abcb1697d54cc086ccd536b749b7c1732240877263868205b249b11704261 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V338 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__e88293567ba4edcdbb21328162ee1dc6b6f4bc58d6365c9d0e9cc6696b9ca54a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__e88293567ba4edcdbb21328162ee1dc6b6f4bc58d6365c9d0e9cc6696b9ca54a", "");
    }
}
