<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__61dae893f94d3ddce4cf60b2e83292b50e0212625f27eb1049d0a96e544ef252 */
class __TwigTemplate_414f5467faf9e50d4b2747635eff51f0025979ddc6d337cb610cd5b316d865ae extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V328 ORANGE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__61dae893f94d3ddce4cf60b2e83292b50e0212625f27eb1049d0a96e544ef252";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__61dae893f94d3ddce4cf60b2e83292b50e0212625f27eb1049d0a96e544ef252", "");
    }
}
