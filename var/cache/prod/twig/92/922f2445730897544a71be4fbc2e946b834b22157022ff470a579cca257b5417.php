<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-product-comparision:view.html.twig */
class __TwigTemplate_5c56dde19b26a3a3fb10716e28b2689fe6aa77d2a302eb0b80ee297a69c716aa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
       .table-pro-compare .img-placeholder img {
            width: 150px;
        }

        .table-pro-compare .img-placeholder {
            border: 1px solid;
            padding: 2px;
        }
</style>
<div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                ";
        // line 17
        echo "            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        
                        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding:15px;padding-left:0; padding-right: 0;\">
                            <tr>
                                <td><h2>";
        // line 31
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "product-compare-title");
        echo "</h2></td>
                            </tr>
                            ";
        // line 33
        if (($context["editmode"] ?? null)) {
            // line 34
            echo "                             <tr>
                                 <td>
                                    <div class=\"outPutconfig\">
                                        ";
            // line 37
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "reload" => true, "selectedClass" => "Product"]);
            // line 44
            echo "
                                        <strong style=\"color:red\"><span>NOTE:</span><span>Please select minimum 2 and maximum 3 product </span></strong>
                                    </div>
                                  </td>
                             </tr>
                            ";
        } else {
            // line 50
            echo "                            <tr>
                                <td>
                                    ";
            // line 52
            $context["configArray"] = $this->extensions['Web2PrintToolsBundle\Twig\OutputChannelExtension']->buildOutputDataConfig(twig_get_attribute($this->env, $this->source, $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable"), "getOutputChannel", [], "method", false, false, false, 52));
            // line 53
            echo "                                    ";
            $context["elements"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "productCompareTable");
            // line 54
            echo "                                    ";
            if (($context["elements"] ?? null)) {
                // line 55
                echo "                                        ";
                $context["productCount"] = twig_length_filter($this->env, ($context["elements"] ?? null));
                // line 56
                echo "                                        ";
                if ((($context["productCount"] ?? null) > 0)) {
                    // line 57
                    echo "                                        <table class=\"pdf-data-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                           <tr>
                                                <td style=\"border: 0;\"></td>
                                                ";
                    // line 60
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                        // line 61
                        echo "                                                <td class=\"column_1\">
                                                   <div class=\"img-placeholder\">
                                                        <img src=\"";
                        // line 63
                        echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 63)), "html", null, true);
                        echo "\" />   
                                                   </div>
                                                   <div class=\"product-content\">
                                                        <h2>";
                        // line 66
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 66), "html", null, true);
                        echo "</h2>
                                                        <p class=\"description\">";
                        // line 67
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 67), "html", null, true);
                        echo "</p>
                                                        ";
                        // line 68
                        $context["currency"] = twig_get_attribute($this->env, $this->source, $context["element"], "getCurrency", [], "method", false, false, false, 68);
                        // line 69
                        echo "                                                        ";
                        $context["defaultCurrency"] = "\$";
                        // line 70
                        echo "                                                        ";
                        if (($context["currency"] ?? null)) {
                            // line 71
                            echo "                                                        ";
                            $context["expldCurrency"] = twig_split_filter($this->env, ($context["currency"] ?? null), "-");
                            // line 72
                            echo "                                                        ";
                            // line 73
                            echo "                                                        ";
                            $context["defaultCurrency"] = ((((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["expldCurrency"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[1] ?? null) : null) != "")) ? ((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["expldCurrency"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[1] ?? null) : null)) : (($context["defaultCurrency"] ?? null)));
                            // line 74
                            echo "                                                        ";
                        }
                        // line 75
                        echo "                                                        <p class=\"price\">";
                        echo ($context["defaultCurrency"] ?? null);
                        echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, $context["element"], "getListPrice", [], "method", false, false, false, 75)), "html", null, true);
                        echo "</p>
                                                   </div>
                                                </td>
                                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 79
                    echo "                                            </tr>
                                            ";
                    // line 80
                    $context["productAttrList"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getProductCompareResult(($context["elements"] ?? null));
                    // line 81
                    echo "                                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["productAttrList"] ?? null));
                    foreach ($context['_seq'] as $context["title"] => $context["products"]) {
                        // line 82
                        echo "                                            <tr>
                                                <td style=\"text-align: left\">";
                        // line 83
                        echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                        echo "</td>
                                                ";
                        // line 84
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["products"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                            // line 85
                            echo "                                                <td style=\"text-align: left\">";
                            echo $context["value"];
                            echo "</td>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 87
                        echo "                                            </tr>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['title'], $context['products'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 89
                    echo "                                        </table>
                                        ";
                }
                // line 91
                echo "                                    ";
            }
            // line 92
            echo "                                </td>
                            </tr>
                            ";
        }
        // line 95
        echo "                        </table>
                        
                    </div>
                </td>
            </tr>
        </tbody>
</table>
 </div>
";
    }

    public function getTemplateName()
    {
        return ":Areas/print-product-comparision:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 95,  212 => 92,  209 => 91,  205 => 89,  198 => 87,  189 => 85,  185 => 84,  181 => 83,  178 => 82,  173 => 81,  171 => 80,  168 => 79,  156 => 75,  153 => 74,  150 => 73,  148 => 72,  145 => 71,  142 => 70,  139 => 69,  137 => 68,  133 => 67,  129 => 66,  123 => 63,  119 => 61,  115 => 60,  110 => 57,  107 => 56,  104 => 55,  101 => 54,  98 => 53,  96 => 52,  92 => 50,  84 => 44,  82 => 37,  77 => 34,  75 => 33,  70 => 31,  54 => 17,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-product-comparision:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-product-comparision/view.html.twig");
    }
}
