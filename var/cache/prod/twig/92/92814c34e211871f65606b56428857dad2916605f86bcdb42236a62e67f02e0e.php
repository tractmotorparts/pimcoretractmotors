<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Targeting:targetingCode.html.twig */
class __TwigTemplate_b84cec5361fa651e900bec80b2e83d149ce64b5b574a67e25a213ee907b848d7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'beforeScriptTag' => [$this, 'block_beforeScriptTag'],
            'beforeScript' => [$this, 'block_beforeScript'],
            'script' => [$this, 'block_script'],
            'afterScript' => [$this, 'block_afterScript'],
            'afterScriptTag' => [$this, 'block_afterScriptTag'],
            'targetingScript' => [$this, 'block_targetingScript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('beforeScriptTag', $context, $blocks);
        // line 2
        echo "
<script>
    ";
        // line 4
        $this->displayBlock('beforeScript', $context, $blocks);
        // line 5
        echo "
    ";
        // line 6
        $this->displayBlock('script', $context, $blocks);
        // line 20
        echo "
    ";
        // line 21
        $this->displayBlock('afterScript', $context, $blocks);
        // line 22
        echo "</script>

";
        // line 24
        $this->displayBlock('afterScriptTag', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('targetingScript', $context, $blocks);
    }

    // line 1
    public function block_beforeScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScriptTag", [], "any", false, false, false, 1);
    }

    // line 4
    public function block_beforeScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "beforeScript", [], "any", false, false, false, 4);
    }

    // line 6
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        // line 8
        echo "    var _ptg = _ptg || {};
    _ptg.options = _ptg.options || {};

    ";
        // line 11
        if (($context["inDebugMode"] ?? null)) {
            // line 12
            echo "    _ptg.options.log = true;
    ";
        }
        // line 14
        echo "
    ";
        // line 15
        if ( !twig_test_empty(($context["dataProviderKeys"] ?? null))) {
            // line 16
            echo "    _ptg.dataProviderKeys = ";
            echo json_encode(($context["dataProviderKeys"] ?? null));
            echo ";
    ";
        }
        // line 18
        echo "    ";
        // line 19
        echo "    ";
    }

    // line 21
    public function block_afterScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScript", [], "any", false, false, false, 21);
    }

    // line 24
    public function block_afterScriptTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_get_attribute($this->env, $this->source, ($context["blocks"] ?? null), "afterScriptTag", [], "any", false, false, false, 24);
    }

    // line 26
    public function block_targetingScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/pimcorecore/js/targeting.js"), "html", null, true);
        echo "\" async></script>
";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Targeting:targetingCode.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  140 => 27,  136 => 26,  129 => 24,  122 => 21,  118 => 19,  116 => 18,  110 => 16,  108 => 15,  105 => 14,  101 => 12,  99 => 11,  94 => 8,  92 => 7,  88 => 6,  81 => 4,  74 => 1,  70 => 26,  67 => 25,  65 => 24,  61 => 22,  59 => 21,  56 => 20,  54 => 6,  51 => 5,  49 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Targeting:targetingCode.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Targeting/targetingCode.html.twig");
    }
}
