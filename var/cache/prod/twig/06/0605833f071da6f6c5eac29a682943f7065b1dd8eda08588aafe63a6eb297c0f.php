<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-multiple-product:view.html.twig */
class __TwigTemplate_5180f957d006915bb24af4e884b08b4237811c6a55d4133fa0e31ba7f9ecba3c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"pdf-multiple-product\">
    <table style=\"width: 100%;\">
        <thead>
            <tr>
                <td>
                    <div class=\"page-head-row\">
                        <div class=\"page-head-left\">

                        </div>
                        <div class=\"page-head-right\">
                            <h2>";
        // line 11
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_sub_head_1", ["placeholder" => "Enter Category/Subcategory Name"]);
        echo "</h2>
                        </div>
                    </div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class=\"sub-heading\">";
        // line 20
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_brand_name", ["placeholder" => "Enter Brand Name"]);
        echo "</div>
                    <div class=\"pro-row\">
                        ";
        // line 22
        if (($context["editmode"] ?? null)) {
            // line 23
            echo "                            ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_1", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "type" => "Object", "selectedClass" => "Product", "reload" => true]);
            // line 31
            echo "
                        ";
        } else {
            // line 33
            echo "                            ";
            $context["productGridData_1"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_1");
            // line 34
            echo "                            ";
            if ((twig_length_filter($this->env, ($context["productGridData_1"] ?? null)) > 0)) {
                // line 35
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["productGridData_1"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 36
                    echo "                                <div class=\"pro-col\">
                                    <img src=\"";
                    // line 37
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 37)), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 37), "html", null, true);
                    echo "\" >
                                    <h3>";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 38), "html", null, true);
                    echo "</h3>
                                </div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "                            ";
            }
            // line 42
            echo "                        ";
        }
        // line 43
        echo "                    </div>
                    ";
        // line 44
        if ( !($context["editmode"] ?? null)) {
            // line 45
            echo "                        ";
            if ((twig_length_filter($this->env, ($context["productGridData_1"] ?? null)) > 0)) {
                // line 46
                echo "                        <div class=\"multi-prod-compare-table\">
                            <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                                <thead>
                                    <tr>
                                        <th style=\"width:20%; text-align: left;\">Model</th>
                                        ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["productGridData_1"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 52
                    echo "                                         <th style=\"width:12%\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 52), "html", null, true);
                    echo "</th>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 54
                echo "                                     </tr>
                                </thead>
                                <tbody>
                                    ";
                // line 57
                $context["productAttrList"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getTechnichleAttrMultipleProducts(($context["productGridData_1"] ?? null));
                // line 58
                echo "
                                    ";
                // line 59
                if (($context["productAttrList"] ?? null)) {
                    // line 60
                    echo "                                        ";
                    $context["titles"] = twig_get_attribute($this->env, $this->source, ($context["productAttrList"] ?? null), "title", [], "any", false, false, false, 60);
                    // line 61
                    echo "                                        ";
                    $context["techAttributes"] = twig_get_attribute($this->env, $this->source, ($context["productAttrList"] ?? null), "technichleItems", [], "any", false, false, false, 61);
                    // line 62
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["titles"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                        // line 63
                        echo "                                        <tr>
                                            <td style=\"text-align: left;\">";
                        // line 64
                        echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                        echo "</td>
                                            ";
                        // line 65
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["techAttributes"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["tattr"]) {
                            // line 66
                            echo "                                                <td>";
                            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["tattr"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["title"]] ?? null) : null), "html", null, true);
                            echo "</td>
                                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tattr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 68
                        echo "                                        </tr>
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "                                    ";
                } else {
                    // line 71
                    echo "                                        <tr>
                                            <td style=\"text-align: left;\">No Technicle Attributes Found</td>
                                        </tr>
                                    ";
                }
                // line 75
                echo "                                </tbody>
                            </table>
                        </div>
                        ";
            }
            // line 79
            echo "                    ";
        }
        // line 80
        echo "                    <p style=\"page-break-after: always;\">&nbsp;</p>
                    <div class=\"product-detail-text\">
                        <div class=\"prote-box\">
                            <h4>";
        // line 83
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_1", ["placeholder" => "Enter Title 1"]);
        echo "</h4>
                            ";
        // line 87
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_1", ["height" => 200]);
        // line 90
        echo "
                            <h4>";
        // line 91
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_2", ["placeholder" => "Enter Title 2"]);
        echo "</h4>
                            ";
        // line 95
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_2", ["height" => 200]);
        // line 98
        echo "
                            <h4>";
        // line 99
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_desc_head_3", ["placeholder" => "Enter Title 3"]);
        echo "</h4>
                            ";
        // line 103
        echo "                            ";
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "wysiwyg", "category_desc_3", ["height" => 200]);
        // line 106
        echo "
                        </div>
                    </div>
                    <div class=\"sub-heading\">";
        // line 109
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "input", "category_brand_name_2", ["placeholder" => "Enter Brand Name"]);
        echo "</div>
                    ";
        // line 110
        if ( !($context["editmode"] ?? null)) {
            echo "   
                    <div class=\"multi-prod-compare-table\">
                        ";
            // line 112
            $context["productGridData_2"] = $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_2");
            // line 113
            echo "                        ";
            if ((twig_length_filter($this->env, ($context["productGridData_2"] ?? null)) > 0)) {
                // line 114
                echo "                        <table class=\"multi-pro-com-table\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                            <thead>
                                <tr>
                                    <th style=\"width:20%; text-align: left;\">Model</th>
                                    ";
                // line 118
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["productGridData_2"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 119
                    echo "                                    <th style=\"width:12%\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 119), "html", null, true);
                    echo "</th>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 121
                echo "                                </tr>
                            </thead>
                            <tbody>
                                ";
                // line 124
                $context["productAttrList2"] = $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getTechnichleAttrMultipleProducts(($context["productGridData_2"] ?? null));
                // line 125
                echo "                                ";
                $context["titles2"] = twig_get_attribute($this->env, $this->source, ($context["productAttrList2"] ?? null), "title", [], "any", false, false, false, 125);
                // line 126
                echo "                                ";
                $context["techAttributes2"] = twig_get_attribute($this->env, $this->source, ($context["productAttrList2"] ?? null), "technichleItems", [], "any", false, false, false, 126);
                // line 127
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["titles2"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                    // line 128
                    echo "                                <tr>
                                    <td style=\"text-align: left;\">";
                    // line 129
                    echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                    echo "</td>
                                    ";
                    // line 130
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["techAttributes2"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["tattr"]) {
                        // line 131
                        echo "                                        <td>";
                        echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["tattr"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["title"]] ?? null) : null), "html", null, true);
                        echo "</td>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tattr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 133
                    echo "                                </tr>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 135
                echo "                            </tbody>
                        </table>
                        ";
            }
            // line 138
            echo "                    </div>
                    ";
        }
        // line 140
        echo "                    <div class=\"pro-row two-col-prod\">
                        ";
        // line 141
        if (($context["editmode"] ?? null)) {
            // line 142
            echo "                            ";
            echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "outputchanneltable", "multiple_product_grid_2", ["disableClassSelection" => true, "disableFavoriteOutputChannel" => true, "type" => "Object", "selectedClass" => "Product", "reload" => true]);
            // line 150
            echo "
                        ";
        } else {
            // line 152
            echo "                            ";
            if ((twig_length_filter($this->env, ($context["productGridData_2"] ?? null)) > 0)) {
                // line 153
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["productGridData_2"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 154
                    echo "                                <div class=\"pro-col\">
                                    <img src=\"";
                    // line 155
                    echo twig_escape_filter($this->env, $this->extensions['AppBundle\Twig\Extension\PrintDocument']->getImageUrl(twig_get_attribute($this->env, $this->source, $context["element"], "getImage", [], "method", false, false, false, 155)), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getProductName", [], "method", false, false, false, 155), "html", null, true);
                    echo "\">
                                    <h3>";
                    // line 156
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getModel", [], "method", false, false, false, 156), "html", null, true);
                    echo "</h3>
                                </div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 159
                echo "                            ";
            }
            // line 160
            echo "                        ";
        }
        // line 161
        echo "                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>

                </td>
            </tr>
        </tfoot>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return ":Areas/print-multiple-product:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  374 => 161,  371 => 160,  368 => 159,  359 => 156,  353 => 155,  350 => 154,  345 => 153,  342 => 152,  338 => 150,  335 => 142,  333 => 141,  330 => 140,  326 => 138,  321 => 135,  314 => 133,  305 => 131,  301 => 130,  297 => 129,  294 => 128,  289 => 127,  286 => 126,  283 => 125,  281 => 124,  276 => 121,  267 => 119,  263 => 118,  257 => 114,  254 => 113,  252 => 112,  247 => 110,  243 => 109,  238 => 106,  235 => 103,  231 => 99,  228 => 98,  225 => 95,  221 => 91,  218 => 90,  215 => 87,  211 => 83,  206 => 80,  203 => 79,  197 => 75,  191 => 71,  188 => 70,  181 => 68,  172 => 66,  168 => 65,  164 => 64,  161 => 63,  156 => 62,  153 => 61,  150 => 60,  148 => 59,  145 => 58,  143 => 57,  138 => 54,  129 => 52,  125 => 51,  118 => 46,  115 => 45,  113 => 44,  110 => 43,  107 => 42,  104 => 41,  95 => 38,  89 => 37,  86 => 36,  81 => 35,  78 => 34,  75 => 33,  71 => 31,  68 => 23,  66 => 22,  61 => 20,  49 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-multiple-product:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-multiple-product/view.html.twig");
    }
}
