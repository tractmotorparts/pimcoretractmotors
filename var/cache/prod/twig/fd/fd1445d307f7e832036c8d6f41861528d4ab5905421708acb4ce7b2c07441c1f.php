<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Profiler:target.svg.twig */
class __TwigTemplate_2141e05caca63a137fe790fbf8588187462b3059993b9c5b85da96cb78262bc1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<svg version=\"1.1\" id=\"Ebene_1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
     viewBox=\"0 0 48 48\" style=\"enable-background:new 0 0 48 48;\" xml:space=\"preserve\">
    <style type=\"text/css\">
        .st0 { fill: #eee; }
        .st1 { fill: #555; }
    </style>

    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"21\"/>
    <ellipse class=\"st1\" cx=\"24\" cy=\"24\" rx=\"14\" ry=\"14.2\"/>
    <circle class=\"st0\" cx=\"24\" cy=\"24\" r=\"7.1\"/>
</svg>
";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:target.svg.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Profiler:target.svg.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Profiler/target.svg.twig");
    }
}
