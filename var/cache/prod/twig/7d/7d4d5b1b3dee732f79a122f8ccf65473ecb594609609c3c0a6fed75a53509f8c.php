<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a6f0b1497e650a75d01e4f372cadcb8db774bd4319702e4f28cd12bbe4fcc911 */
class __TwigTemplate_f8b42b23f6c4798e6ece57f3585c3fdb83149043dcba2f5aadf44706d2cf9ef0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA TU43 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__a6f0b1497e650a75d01e4f372cadcb8db774bd4319702e4f28cd12bbe4fcc911";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a6f0b1497e650a75d01e4f372cadcb8db774bd4319702e4f28cd12bbe4fcc911", "");
    }
}
