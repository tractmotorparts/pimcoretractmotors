<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__7bf105c76ec94d81d97d6f16a58833c90ea7889a70e82814812e51ca71186f78 */
class __TwigTemplate_75efff5b57abb5a3964d6b2a771be88a5de227a23fafb86429324fea04cdc37d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "FARMATE HYT2W is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__7bf105c76ec94d81d97d6f16a58833c90ea7889a70e82814812e51ca71186f78";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__7bf105c76ec94d81d97d6f16a58833c90ea7889a70e82814812e51ca71186f78", "");
    }
}
