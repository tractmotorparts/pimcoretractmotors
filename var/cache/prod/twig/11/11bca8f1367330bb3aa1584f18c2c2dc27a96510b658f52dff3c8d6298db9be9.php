<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a062b7727549fcb9feed7f0f78d5e98248612262cf13d31ebd34688732836d59 */
class __TwigTemplate_0eeb92fdad203e45752ae0bca4bed43a9705d420d96f798ec566d73bba1b5a62 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>1NYLON1</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>PROMAC 1LB 2.6MM SPOOL SQUARE CUT YELLOW TRIMMER LINE</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Awaiting Approval</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__a062b7727549fcb9feed7f0f78d5e98248612262cf13d31ebd34688732836d59";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a062b7727549fcb9feed7f0f78d5e98248612262cf13d31ebd34688732836d59", "");
    }
}
