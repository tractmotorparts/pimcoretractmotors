<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Workflow/NotificationEmail:notificationEmail.html.twig */
class __TwigTemplate_517070c6633df29cef83e90b537893eff9dd0916b01e66ce35774fba64974c26 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
</head>
<body>
";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => "workflow_change_email_notification_text", 1 => [0 => ((($context["subjectType"] ?? null) . " ") . twig_get_attribute($this->env, $this->source, ($context["subject"] ?? null), "getFullPath", [], "method", false, false, false, 15)), 1 => twig_get_attribute($this->env, $this->source, ($context["subject"] ?? null), "getId", [], "method", false, false, false, 15), 2 => twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => ($context["action"] ?? null), 1 => [], 2 => "admin", 3 => ($context["lang"] ?? null)], "method", false, false, false, 15), 3 => twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => twig_get_attribute($this->env, $this->source, ($context["workflow"] ?? null), "getName", [], "method", false, false, false, 15), 1 => [], 2 => "admin", 3 => ($context["lang"] ?? null)], "method", false, false, false, 15)], 2 => "admin", 3 => ($context["lang"] ?? null)], "method", false, false, false, 15), "html", null, true);
        echo "<br>
";
        // line 16
        if ( !twig_test_empty(twig_trim_filter(($context["deeplink"] ?? null)))) {
            // line 17
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, ($context["deeplink"] ?? null), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => "workflow_change_email_notification_deeplink", 1 => [], 2 => "admin", 3 => ($context["lang"] ?? null)], "method", false, false, false, 17), "html", null, true);
            echo "</a><br>
";
        }
        // line 19
        echo "<br>

";
        // line 21
        if ( !twig_test_empty(twig_trim_filter(($context["note_description"] ?? null)))) {
            // line 22
            echo "    <strong>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["translator"] ?? null), "trans", [0 => "workflow_change_email_notification_note", 1 => [], 2 => "admin", 3 => ($context["lang"] ?? null)], "method", false, false, false, 22), "html", null, true);
            echo "</strong>
    <p>";
            // line 23
            echo twig_escape_filter($this->env, ($context["note_description"] ?? null), "html", null, true);
            echo "</p>
";
        }
        // line 25
        echo "</body>
</html>";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Workflow/NotificationEmail:notificationEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 25,  71 => 23,  66 => 22,  64 => 21,  60 => 19,  52 => 17,  50 => 16,  46 => 15,  37 => 8,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Workflow/NotificationEmail:notificationEmail.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Workflow/NotificationEmail/notificationEmail.html.twig");
    }
}
