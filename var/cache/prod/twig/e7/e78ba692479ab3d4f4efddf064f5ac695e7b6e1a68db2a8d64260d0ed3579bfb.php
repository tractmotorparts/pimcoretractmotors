<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__48fbdf368a8aa74e5746ad01214d64bc69587f6de4f95756f242e9b9939e94a3 */
class __TwigTemplate_2134d55657c237de17cb8c4590800cb43c1032c8d88484776bb27d199cb9d57c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA TB43 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__48fbdf368a8aa74e5746ad01214d64bc69587f6de4f95756f242e9b9939e94a3";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__48fbdf368a8aa74e5746ad01214d64bc69587f6de4f95756f242e9b9939e94a3", "");
    }
}
