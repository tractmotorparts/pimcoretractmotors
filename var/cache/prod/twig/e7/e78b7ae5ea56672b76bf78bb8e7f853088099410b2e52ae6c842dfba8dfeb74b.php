<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2336ad5fb8d41f1d82104ec82f79e445545ae0a138a3ec509bcdd0bbdde205bf */
class __TwigTemplate_fdf7381687e07b6b7ea164c64a8516ef96556f26b237e9cc7f7e8c1affad1d18 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC MULTI-PURPOSE APRON is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2336ad5fb8d41f1d82104ec82f79e445545ae0a138a3ec509bcdd0bbdde205bf";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2336ad5fb8d41f1d82104ec82f79e445545ae0a138a3ec509bcdd0bbdde205bf", "");
    }
}
