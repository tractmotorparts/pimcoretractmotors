<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__01682ff2475265bab3a12eaeb70345d4d844af7f931f143c81c57b152e7ea8d3 */
class __TwigTemplate_774c075881f3be40a05426b25104e5100a3482e49b0b0dc4f7eba082ce303317 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KABA TB52 is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__01682ff2475265bab3a12eaeb70345d4d844af7f931f143c81c57b152e7ea8d3";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__01682ff2475265bab3a12eaeb70345d4d844af7f931f143c81c57b152e7ea8d3", "");
    }
}
