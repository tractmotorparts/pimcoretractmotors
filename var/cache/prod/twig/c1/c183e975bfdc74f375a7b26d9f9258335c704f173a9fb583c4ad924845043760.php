<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreAdminBundle:Admin/Login:lostpassword.html.php */
class __TwigTemplate_594201723bdf1a0b0069d3a21a9847acb7bbf9a3b5ce1c0d28b39d166eff93eb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<?php
/** @var \\Pimcore\\Templating\\PhpEngine \$view */
\$view->extend('PimcoreAdminBundle:Admin/Login:layout.html.php');

\$this->get(\"translate\")->setDomain(\"admin\");

?>



<?php if (\$this->getRequest()->getMethod() === 'POST') { ?>
    <div class=\"text error\">
        <?= \$this->translate(\"A temporary login link has been sent to your email address.\"); ?>
        <br/>
        <?= \$this->translate(\"Please check your mailbox.\"); ?>
    </div>
<?php } else { ?>
    <div class=\"text info\">
        <?= \$this->translate(\"Enter your username and pimcore will send a login link to your email address\"); ?>
    </div>

    <form method=\"post\" action=\"<?= \$view->router()->path('pimcore_admin_login_lostpassword') ?>\">
        <input type=\"text\" name=\"username\" autocomplete=\"username\" placeholder=\"<?= \$this->translate(\"Username\"); ?>\" required autofocus>
        <input type=\"hidden\" name=\"csrfToken\" value=\"<?= \$this->csrfToken ?>\">

        <button type=\"submit\" name=\"submit\"><?= \$this->translate(\"Submit\"); ?></button>
    </form>
<?php } ?>

<a href=\"<?= \$view->router()->path('pimcore_admin_login') ?>\"><?= \$this->translate(\"Back to Login\"); ?></a>

<?= \$this->breachAttackRandomContent(); ?>


";
    }

    public function getTemplateName()
    {
        return "PimcoreAdminBundle:Admin/Login:lostpassword.html.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreAdminBundle:Admin/Login:lostpassword.html.php", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/AdminBundle/Resources/views/Admin/Login/lostpassword.html.php");
    }
}
