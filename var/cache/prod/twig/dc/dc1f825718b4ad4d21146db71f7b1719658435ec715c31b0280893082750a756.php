<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__73046139d7be658da5990e8feb42396b5fce115c0778e809a1136c383bf2752b */
class __TwigTemplate_15d0001a585880e3e89ea776babd1f641684c3acdc80cdc4a0280373262573e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "VICTA V1800 MIST BLOWER is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__73046139d7be658da5990e8feb42396b5fce115c0778e809a1136c383bf2752b";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__73046139d7be658da5990e8feb42396b5fce115c0778e809a1136c383bf2752b", "");
    }
}
