<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__d2ce546cdb30f58d1b8f3acf1469dac2347a3bbcc478e98a7d2083cec6b9249e */
class __TwigTemplate_4cbe6b17e00bfc45697b97244ff7cc2cb53c600bf5fbd304111667da882dde6c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "PROMAC 1KG 12\" PRECUT 2.6MM NYLON ROPE is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__d2ce546cdb30f58d1b8f3acf1469dac2347a3bbcc478e98a7d2083cec6b9249e";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__d2ce546cdb30f58d1b8f3acf1469dac2347a3bbcc478e98a7d2083cec6b9249e", "");
    }
}
