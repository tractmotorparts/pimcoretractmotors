<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__c9b6af12e309de73d583a88a3f5caaebf071ce0ccbbf93f1a3be73d021726f3f */
class __TwigTemplate_d4ea4c73c0f6afec8ac0aa888fca1e35793947e3283f5afea7cb7cecbd9cf487 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>V1800</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>VICTA V1800 MIST BLOWER</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Aprroved</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__c9b6af12e309de73d583a88a3f5caaebf071ce0ccbbf93f1a3be73d021726f3f";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__c9b6af12e309de73d583a88a3f5caaebf071ce0ccbbf93f1a3be73d021726f3f", "");
    }
}
