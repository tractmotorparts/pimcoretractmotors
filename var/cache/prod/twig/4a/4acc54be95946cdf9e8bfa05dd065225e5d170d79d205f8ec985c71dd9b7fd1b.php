<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2d73c0da7300307b001610290b8ad9ab2b75b13793159e7117a7732459ef0c2a */
class __TwigTemplate_51e7d5d699b4cd5a1bb680b1373a9fd94af56606efe674fbf2253fb082d35e30 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "KOOP KDF8500Q-3D  DIESEL GENERATOR SET 6KW ,30L FUEL TANK  ELEC ST SILENT , 3 PHASE / SINGLE PHASE  is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__2d73c0da7300307b001610290b8ad9ab2b75b13793159e7117a7732459ef0c2a";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__2d73c0da7300307b001610290b8ad9ab2b75b13793159e7117a7732459ef0c2a", "");
    }
}
