<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a9160603bf8999d4e380ca8a7bf652250234bb2ebca4235bf1345b48eede0393 */
class __TwigTemplate_e36b33b8691172b1866e8067879a995d124d86295073c31aa0ef60e24857069b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "OLA APS125 Automatic Self-priming Pump 0.125kw; Max. Flow:2m3h; Max. Head:24m; Max. Suction:8m; InletOutlet:1\" is awaiting for approval";
    }

    public function getTemplateName()
    {
        return "__string_template__a9160603bf8999d4e380ca8a7bf652250234bb2ebca4235bf1345b48eede0393";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a9160603bf8999d4e380ca8a7bf652250234bb2ebca4235bf1345b48eede0393", "");
    }
}
