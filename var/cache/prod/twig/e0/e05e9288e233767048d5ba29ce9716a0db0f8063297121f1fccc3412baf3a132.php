<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__b8e1ddf2e697c7fc7364acfacd3b20861ae2f516473fa1b92da64c93e06f89c7 */
class __TwigTemplate_4dc4c440d20e0fc731beff3660e7efe6304c345f555f4246c4c6a9dc4ee69021 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>T430</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>VICTA T430 MIST BLOWER TURBO</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Aprroved</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__b8e1ddf2e697c7fc7364acfacd3b20861ae2f516473fa1b92da64c93e06f89c7";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__b8e1ddf2e697c7fc7364acfacd3b20861ae2f516473fa1b92da64c93e06f89c7", "");
    }
}
