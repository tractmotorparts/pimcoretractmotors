<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a713bb92b183e8760fa2520f0bbbacd265872cfe5d2c338d7935bb3800819d53 */
class __TwigTemplate_547b84f484db11606d993213d4e27e342ff05c6b6a4e0f6c6a8eeab441f5b4cd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>M430</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>VICTA M430 MIST BLOWER</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>Aprroved</td>
                            </tr>
                       </table>
                    </body>
                </html>";
    }

    public function getTemplateName()
    {
        return "__string_template__a713bb92b183e8760fa2520f0bbbacd265872cfe5d2c338d7935bb3800819d53";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a713bb92b183e8760fa2520f0bbbacd265872cfe5d2c338d7935bb3800819d53", "");
    }
}
