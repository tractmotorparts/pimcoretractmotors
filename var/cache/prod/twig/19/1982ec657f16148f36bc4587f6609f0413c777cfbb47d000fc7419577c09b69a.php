<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Areabrick:wrapper.html.twig */
class __TwigTemplate_9957d159f2afef6fee1b1b79b131e818d052a9f81aa81635fea9714ad8dd3db9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'areabrickWrapper' => [$this, 'block_areabrickWrapper'],
            'areabrickOpenTag' => [$this, 'block_areabrickOpenTag'],
            'areabrickFrontend' => [$this, 'block_areabrickFrontend'],
            'areabrickCloseTag' => [$this, 'block_areabrickCloseTag'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
";
        // line 6
        echo "
";
        // line 9
        echo "
";
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('areabrickWrapper', $context, $blocks);
    }

    public function block_areabrickWrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $this->displayBlock('areabrickOpenTag', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        if ((twig_get_attribute($this->env, $this->source, ($context["brick"] ?? null), "hasEditTemplate", [], "method", false, false, false, 18) && ($context["editmode"] ?? null))) {
            // line 19
            echo "
            <div class=\"pimcore_area_edit_button\" data-name=\"";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["info"] ?? null), "tag", [], "any", false, false, false, 20), "name", [], "any", false, false, false, 20), "html", null, true);
            echo "\" data-real-name=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["info"] ?? null), "tag", [], "any", false, false, false, 20), "realName", [], "any", false, false, false, 20), "html", null, true);
            echo "\"></div>

        ";
        }
        // line 23
        echo "
        ";
        // line 24
        $this->displayBlock('areabrickFrontend', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        if (((twig_get_attribute($this->env, $this->source, ($context["brick"] ?? null), "hasEditTemplate", [], "method", false, false, false, 28) && ($context["editmode"] ?? null)) && ($context["editTemplate"] ?? null))) {
            // line 29
            echo "
            <div class=\"pimcore_area_editmode pimcore_area_editmode_hidden\" data-name=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["info"] ?? null), "tag", [], "any", false, false, false, 30), "name", [], "any", false, false, false, 30), "html", null, true);
            echo "\" data-real-name=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["info"] ?? null), "tag", [], "any", false, false, false, 30), "realName", [], "any", false, false, false, 30), "html", null, true);
            echo "\">
                ";
            // line 31
            echo twig_get_attribute($this->env, $this->source, ($context["templating"] ?? null), "render", [0 => ($context["editTemplate"] ?? null), 1 => ($context["editParameters"] ?? null)], "method", false, false, false, 31);
            echo "
            </div>

        ";
        }
        // line 35
        echo "
    ";
        // line 36
        $this->displayBlock('areabrickCloseTag', $context, $blocks);
    }

    // line 14
    public function block_areabrickOpenTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, ($context["brick"] ?? null), "htmlTagOpen", [0 => ($context["info"] ?? null)], "method", false, false, false, 15);
        echo "
    ";
    }

    // line 24
    public function block_areabrickFrontend($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "            ";
        echo twig_get_attribute($this->env, $this->source, ($context["templating"] ?? null), "render", [0 => ($context["viewTemplate"] ?? null), 1 => ($context["viewParameters"] ?? null)], "method", false, false, false, 25);
        echo "
        ";
    }

    // line 36
    public function block_areabrickCloseTag($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "        ";
        echo twig_get_attribute($this->env, $this->source, ($context["brick"] ?? null), "htmlTagClose", [0 => ($context["info"] ?? null)], "method", false, false, false, 37);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Areabrick:wrapper.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  138 => 37,  134 => 36,  127 => 25,  123 => 24,  116 => 15,  112 => 14,  108 => 36,  105 => 35,  98 => 31,  92 => 30,  89 => 29,  87 => 28,  84 => 27,  82 => 24,  79 => 23,  71 => 20,  68 => 19,  66 => 18,  63 => 17,  60 => 14,  53 => 13,  50 => 12,  47 => 9,  44 => 6,  41 => 4,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Areabrick:wrapper.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Areabrick/wrapper.html.twig");
    }
}
