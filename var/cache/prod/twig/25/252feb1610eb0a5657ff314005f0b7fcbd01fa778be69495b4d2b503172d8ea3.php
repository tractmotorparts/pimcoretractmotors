<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* PimcoreCoreBundle:Targeting/toolbar/icon:toolbar-collapse.svg.twig */
class __TwigTemplate_d6c014c67131d9b8309878537d691d6185f07dc4f3cc5e514775598f0ba0057d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "<svg version=\"1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 48 48\" enable-background=\"new 0 0 48 48\">
    <polygon fill=\"#AAAAAA\" points=\"30.9,43 34,39.9 18.1,24 34,8.1 30.9,5 12,24\"/>
</svg>
";
    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Targeting/toolbar/icon:toolbar-collapse.svg.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "PimcoreCoreBundle:Targeting/toolbar/icon:toolbar-collapse.svg.twig", "/usr/share/nginx/html/pimcore_tractmotors/vendor/pimcore/pimcore/bundles/CoreBundle/Resources/views/Targeting/toolbar/icon/toolbar-collapse.svg.twig");
    }
}
