<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* :Areas/print-single-product:view.html.twig */
class __TwigTemplate_43436af3025bb2e0ee939d4f3736901187f1ed171af726575ad2fa43fb89983b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
.product-list-price p {
    color: #333;
    font-size: 1.5em;
    padding-left: 15px;
    line-height: 23px;
    margin-top: 18px;
    font-weight: 600;
}
.product-list-price {
    display: flex;
    align-items: center;
}
</style>
        <div class=\"table-pro-compare\">
<table style=\"width: 100%;\">
        <thead>
        <tr>
            <td style=\"width: 100px; background-color: #cf0101; height: 100vh; position: fixed;\">
                ";
        // line 21
        echo "            </td>
            <td style=\"vertical-align: top;\">
                <div class=\"pdf-right-container\">    
                </div>
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan=\"2\" style=\"vertical-align: top;\">
                    <div class=\"pdf-right-container\">
                        ";
        // line 32
        echo $this->extensions['Pimcore\Twig\Extension\DocumentEditableExtension']->renderEditable($context, "renderlet", "product1", ["controller" => "web2print", "action" => "product", "className" => [0 => "Product"], "editmode" => true, "type" => "object", "title" => "Drag a product here", "reload" => true, "height" => 1000]);
        // line 43
        echo "
                     </div>
                </td>
            </tr>
        </tbody>
    </table>
        </div>
";
    }

    public function getTemplateName()
    {
        return ":Areas/print-single-product:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 43,  71 => 32,  58 => 21,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", ":Areas/print-single-product:view.html.twig", "/usr/share/nginx/html/pimcore_tractmotors/app/Resources/views/Areas/print-single-product/view.html.twig");
    }
}
