<?php 

return [
    "bundle" => [
        "Wvision\\Bundle\\DataDefinitionsBundle\\DataDefinitionsBundle" => FALSE,
        "OutputDataConfigToolkitBundle\\OutputDataConfigToolkitBundle" => TRUE,
        "Web2PrintToolsBundle\\Web2PrintToolsBundle" => TRUE
    ]
];
