<?php 

return [
    "enableInDefaultView" => TRUE,
    "generalTool" => "pdfreactor",
    "generalDocumentSaveMode" => "default",
    "pdfreactorVersion" => "10.0",
    "pdfreactorProtocol" => "http",
    "pdfreactorServer" => "localhost",
    "pdfreactorServerPort" => "9423",
    "pdfreactorBaseUrl" => "https://pim.tractmotor.com.my/",
    "pdfreactorApiKey" => "",
    "pdfreactorLicence" => "<license><licenseserialno>5418</licenseserialno><licensee><name>TRACTMOTOR PARTS (M) SDN BHD</name></licensee><product>PDFreactor</product><majorversion>10</majorversion><minorversion>0</minorversion><licensetype>CPU</licensetype><amount>4</amount><unit>CPU</unit><maintenanceexpirationdate>2022-06-15</maintenanceexpirationdate><purchasedate>2021-06-15</purchasedate><options><option>pdf</option></options><signatureinformation><signdate>2021-06-16 12:40</signdate><signature>302c02141695cfc65c6853dd292f8c88e82d7c5a7bba0b6f021435f369b0ff108ee52f70a65f61f02ccbe8da3a3b</signature><checksum>1460</checksum></signatureinformation></license>",
    "pdfreactorEnableLenientHttpsMode" => FALSE,
    "pdfreactorEnableDebugMode" => FALSE,
    "wkhtmltopdfBin" => "C:\\wkhtmltopdf\\bin\\wkhtmltopdf",
    "wkhtml2pdfOptions" => [
        "print-media-type" => "",
        "page-size" => "A4"
    ],
    "wkhtml2pdfHostname" => "http://project.tractmotors.com/"
];
