<?php 

return [
    1 => [
        "id" => 1,
        "name" => "USExchangeRate",
        "language" => "",
        "type" => "text",
        "data" => "1",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1629868828
    ],
    2 => [
        "id" => 2,
        "name" => "MiscExpenses",
        "language" => "",
        "type" => "text",
        "data" => "1",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1629350488
    ],
    3 => [
        "id" => 3,
        "name" => "SST",
        "language" => "",
        "type" => "text",
        "data" => "1.10",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1608801940
    ],
    4 => [
        "id" => 4,
        "name" => "ProfitMargin",
        "language" => "",
        "type" => "text",
        "data" => "1",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1629868833
    ],
    5 => [
        "id" => 5,
        "name" => "DefaultEndUserDiscount",
        "language" => "",
        "type" => "text",
        "data" => "30",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1626407213
    ],
    6 => [
        "id" => 6,
        "name" => "DefaultDealerDiscount",
        "language" => "",
        "type" => "text",
        "data" => "40",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1626407217
    ],
    7 => [
        "id" => 7,
        "name" => "DefaultLooseDiscount",
        "language" => "",
        "type" => "text",
        "data" => "50",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1626407220
    ],
    8 => [
        "id" => 8,
        "name" => "DefaultBulkDiscount",
        "language" => "",
        "type" => "text",
        "data" => "50",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1626407223
    ],
    9 => [
        "id" => 9,
        "name" => "selectLayoutIdOnApprove",
        "language" => "",
        "type" => "text",
        "data" => "2",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1620278944
    ],
    10 => [
        "id" => 10,
        "name" => "defaultLayoutId",
        "language" => "",
        "type" => "text",
        "data" => "0",
        "siteId" => 0,
        "creationDate" => 0,
        "modificationDate" => 1620296965
    ]
];
