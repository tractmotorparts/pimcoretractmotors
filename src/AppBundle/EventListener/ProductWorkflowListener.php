<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Pimcore\Model\DataObject;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Pimcore\Model\DataObject\Product;
use Pimcore\Tool;
class ProductWorkflowListener{
    public function onSentForApproval(TransitionEvent $event){
        $this->checkValidation($event);
        
        $product = $event->getSubject();
        $mail = new \Pimcore\Mail();
        $mail->addTo('musht99@gmail.com');
        
        
        //$mail->setDocument('/email/myemaildocument');
        //$mail->setBodyText("Product is sent for approval.");
        $productName=$product->getProductName();
        $productSku=$product->getSKU();
        $status="Awaiting Approval";
        $html=$this->prepareMailTemplate($productName,$productSku,$status);
        $subject=$productName. " is awaiting for approval";
        $mail->setSubject($subject);
        $mail->setBodyHtml($html);
        $mail->send();
    }

    public function onSentForPublish(TransitionEvent $event){
        $this->checkValidation($event);
        
        $product = $event->getSubject();
        
        $productOwnerId=$product->getUserOwner();
        $productOwner=\Pimcore\Model\User::getById($productOwnerId);
        $productOwnerEmail=$productOwner->getEmail();
        
        if(!empty($productOwnerEmail)){
            $mail = new \Pimcore\Mail();
            $mail->addTo($productOwnerEmail);
           //$mail->setDocument('/email/myemaildocument');
            //$mail->setBodyText("Product is sent for approval.");
            $productName=$product->getProductName();
            $productSku=$product->getSKU();
            $status="Aprroved";
            $html=$this->prepareMailTemplate($productName,$productSku,$status);
            $subject=$productName. " is approved";
            $mail->setSubject($subject);
            $mail->setBodyHtml($html);
            $mail->send();
        }
//        $mail = new \Pimcore\Mail();
//        $mail->addTo('praveen.jha@credencys.com');
//        //$mail->setDocument('/email/myemaildocument');
//        $mail->setBodyText("Product is sent for approval.");
//        $mail->send();
        /* $object = $event->getArgument("object");
        $obID = $object->getId();
    
        if($object instanceof Product) {
            $data = $event->getArgument("data");
            $db = \Pimcore\Db::get();		
            	$stm = $db->query('SELECT * from element_workflow_state WHERE cid = '.$obID);
            	$resJob = $stm->fetchOne();
                 $currentUser = \Pimcore\Tool\Admin::getCurrentUser();
                 $role = $currentUser->getRoles();
                
            if($resJob['place'] == 'approved') {
                echo "Here";    exit;
                $data = $this->doModifyCustomLayouts($data, $object, 1, [0]);
            }
             $event->setArgument("data", $data);
        } */
    }

    /*public function doModifyCustomLayouts($data, $object, $customLayoutToSelect = null, $layoutsToRemove = []) {
        
        if($customLayoutToSelect != null) {
            //set current layout to subcategory layout
            $data['currentLayoutId'] = $customLayoutToSelect;
            $customLayout = CustomLayout::getById($customLayoutToSelect);
            $data['layout'] = $customLayout->getLayoutDefinitions();
            \Pimcore\Model\DataObject\Service::enrichLayoutDefinition($data["layout"], $object);            
        }
        
        if(!empty($layoutsToRemove)) {
            //remove master layout from valid layouts
            $validLayouts = $data["validLayouts"];
            foreach($validLayouts as $key => $validLayout) {
                if(in_array($validLayout['id'], $layoutsToRemove)) {
                    unset($validLayouts[$key]);
                }
            }
            $data["validLayouts"] = array_values($validLayouts);            
        }

        return $data; 
    } */

    public function onRejectFromApproval(TransitionEvent $event){
        $product = $event->getSubject();
        $mail = new \Pimcore\Mail();
        
        $productOwnerId=$product->getUserOwner();
        $productOwner=\Pimcore\Model\User::getById($productOwnerId);
        //var_dump($productOwner);
        $productOwnerEmail=$productOwner->getEmail();
        if(!empty($productOwnerEmail)){
            $mail->addTo('praveen.jha@credencys.com');


            //$mail->setDocument('/email/myemaildocument');
            //$mail->setBodyText("Product is sent for approval.");
            $productName=$product->getProductName();
            $productSku=$product->getSKU();
            $status="Rejected";
            $html=$this->prepareMailTemplate($productName,$productSku,$status);
            $subject=$productName. "is rejected";
            $mail->setSubject($subject);
            $mail->setBodyHtml($html);
            $mail->send();
        }
    }

    public function onReOpen(TransitionEvent $event){
        //$this->checkValidation($event);
    }
    
    public function onResendInApproval(TransitionEvent $event){
        $this->checkValidation($event);
        $product = $event->getSubject();
        
        
        
        $mail = new \Pimcore\Mail();
        $mail->addTo('musht99@gmail.com');
        
        
        //$mail->setDocument('/email/myemaildocument');
        //$mail->setBodyText("Product is sent for approval.");
        $productName=$product->getProductName();
        $productSku=$product->getSKU();
        $status="Awaiting For Approval";
        $html=$this->prepareMailTemplate($productName,$productSku,$status);
        $subject=$productName. " is awaiting for approval";
        $mail->setSubject($subject);
        $mail->setBodyHtml($html);
        $mail->send();
    }
    
    public function onStartReapproval(TransitionEvent $event){
        $this->checkValidation($event);
        
        $product = $event->getSubject();
        
        $productOwnerId=$product->getUserOwner();
        $productOwner=\Pimcore\Model\User::getById($productOwnerId);
        //var_dump($productOwner);
        $productOwnerEmail=$productOwner->getEmail();
        if(!empty($productOwnerEmail)){
            $mail = new \Pimcore\Mail();
            $mail->addTo($productOwnerEmail);
           //$mail->setDocument('/email/myemaildocument');
            //$mail->setBodyText("Product is sent for approval.");
            $productName=$product->getProductName();
            $productSku=$product->getSKU();
            $status="Aprroved";
            $html=$this->prepareMailTemplate($productName,$productSku,$status);
            $subject=$productName. " is approved";
            $mail->setSubject($subject);
            $mail->setBodyHtml($html);
            $mail->send();
        }
    }

    function checkValidation($event){
	$product = $event->getSubject();
	$errorMessages = [];
	try{
            if (!$product->getSKU()) {
                $errorMessages[] = 'Please enter SKU.';
            }
            if (!$product->getProductName()) {
                $errorMessages[] = 'Please enter Product Name.';
            }
            if (!$product->getModel()) {
                $errorMessages[] = 'Please enter Model.';
            }
            if (!$product->getCategory()) {
                $errorMessages[] = 'Please enter Category.';
            }            
            if (!$product->getBrandName()) {
                $errorMessages[] = 'Please enter BrandName.';
            }
//            if (!$product->getMachineType()) {
//                $errorMessages[] = 'Please enter MachineType.';
//            }
            if (!$product->getCountry()) {
                $errorMessages[] = 'Please enter Country.';
            }
            if (!$product->getDescription()) {
                $errorMessages[] = 'Please enter Description.';
            }
//            if (!$product->getHeight()) {
//                $errorMessages[] = 'Please enter Height.';
//            }
            if (!$product->getGrossWeight()) {
                $errorMessages[] = 'Please enter GrossWeight.';
            }
            if (!$product->getNetWeight()) {
                $errorMessages[] = 'Please enter NetWeight.';
            }
            if (!$product->getPackingDimensionHeight()) {
                $errorMessages[] = 'Please enter Packing Dimension Height.';
            }
            if (!$product->getPackingDimensionLength()) {
                $errorMessages[] = 'Please enter Packing Dimension Length.';
            }
            if (!$product->getPackingDimensionWidth()) {
                $errorMessages[] = 'Please enter Packing Dimension Width.';
            }
            if (!$product->getProfitMargin()) {
                $errorMessages[] = 'Please Enter Profit Margin.';
            }else{
                if($product->getProfitMargin()<0.45){
                    $errorMessages[] = 'Minimum profit margin should be 0.75.';
                }
            }

            if (count($errorMessages) > 0) {
                throw new \Pimcore\Model\Element\ValidationException(implode("<br>", $errorMessages));
            }
        }
        catch (\Exception $ex) {
            throw new \Pimcore\Model\Element\ValidationException($ex->getMessage());
	}
    }   
    
    
    /**
     * 
     * Preapare Mail Template
     * 
    */
    private function  prepareMailTemplate($productName,$productSku,$status){
        $html="<!DOCTYPE html>
                <html>
                    <head>
                     <style>
                     table{width:100%}
                     table, th, td {
                        border: 1px solid black;
                      }
                      th, td {
                        padding: 5px;
                      }
                      th {
                        text-align: center;
                      }
                     </style>
                    </head>
                    <body>
                       <table>
                            <tr>
                              <th>SKU</th>
                              <td>".$productSku."</td>
                            </tr>
                            <tr>
                               <th>Name</th>
                               <td>".$productName."</td>
                            </tr>
                            <tr>
                               <th>Status</th>
                               <td>".$status."</td>
                            </tr>
                       </table>
                    </body>
                </html>";
        return $html;
    }
}
