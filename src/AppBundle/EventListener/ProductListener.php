<?php
namespace AppBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Service;

use Pimcore\Model\DataObject\Product;
use Pimcore\Model\DataObject\Parts;
use Pimcore\Model\DataObject\Category;

use AppBundle\Utility\ObjectHelper;
class ProductListener {

    public function onObjectPreUpdate (ElementEventInterface $event) {
         if($event instanceof DataObjectEvent) {
            if($event->getObject() instanceof Product || $event->getObject() instanceof Parts ){
                $object = $event->getObject();
                $id = $object->getId();
                //$this->createCategoryFolder($object);
                $product = $object;
                ObjectHelper::calculatePricingOnSaveObject($product);
            }
        }
    }
    /**
     * Add object in respective category folder
     * 
     * 
     */
    private function createCategoryFolder(&$object){
       
        $parentFolder="";
        if($object instanceof Product ){
            $parentFolder="Product";
        }else if($object instanceof Parts){
            $parentFolder="Parts";
        }
        $catgId=$object->getCategory();
        if(!empty($catgId)){
            $categoryObject=\Pimcore\Model\DataObject\Category::getById($catgId);
             $catPath=$categoryObject->getPath();
             $catPath=str_replace('/Category/',"",$catPath);
            $catPath=$parentFolder."/".$catPath.$categoryObject->getCategoryName();
            $folder = Service::createFolderByPath($catPath);
            $parentId = $folder->getId();
            $object->setParentId($parentId);
        }
    }
}
