<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Pimcore\Model\DataObject;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Pimcore\Model\DataObject\Product;

class PartsWorkflowListener
{
	public function onSentForApproval(TransitionEvent $event)
	{
        $this->checkValidation($event);
    }

    public function onSentForPublish(TransitionEvent $event)
	{
        $this->checkValidation($event);
    }

    public function onRejectFromApproval(TransitionEvent $event)
	{
        // $this->checkValidation($event);
    }

    public function onReOpen(TransitionEvent $event)
	{
        // $this->checkValidation($event);
    }

    function checkValidation($event){
		
        $parts = $event->getSubject();
		$errorMessages = [];
		try{
            if (!$parts->getSKU()) {
                $errorMessages[] = 'Please enter SKU.';
            }
            /*if (!$parts->getProductName()) {
                $errorMessages[] = 'Please enter Product Name.';
            }
            if (!$parts->getModel()) {
                $errorMessages[] = 'Please enter Model.';
            }
            if (!$parts->getCategory()) {
                $errorMessages[] = 'Please enter Category.';
            }            
            if (!$parts->getBrandName()) {
                $errorMessages[] = 'Please enter BrandName.';
            }
            if (!$parts->getMachineType()) {
                $errorMessages[] = 'Please enter MachineType.';
            }
            if (!$parts->getCountry()) {
                $errorMessages[] = 'Please enter Country.';
            } */
            if (!$parts->getDescription()) {
                $errorMessages[] = 'Please enter Description.';
            }
            
            if (!$parts->getProfitMargin()) {
                $errorMessages[] = 'Please Enter Profit Margin.';
            }else{
                if($parts->getProfitMargin()<0.45){
                    $errorMessages[] = 'Minimum profit margin should be 0.45.';
                }
            }
            /*
            if (!$parts->getHeight()) {
                $errorMessages[] = 'Please enter Height.';
            }
            if (!$parts->getGrossWeight()) {
                $errorMessages[] = 'Please enter GrossWeight.';
            }
            if (!$parts->getNetWeight()) {
                $errorMessages[] = 'Please enter NetWeight.';
            }
            if (!$parts->getPackingDimensionHeight()) {
                $errorMessages[] = 'Please enter Packing Dimension Height.';
            }
            if (!$parts->getPackingDimensionLength()) {
                $errorMessages[] = 'Please enter Packing Dimension Length.';
            }
            if (!$parts->getPackingDimensionWidth()) {
                $errorMessages[] = 'Please enter Packing Dimension Width.';
            } */

            if (count($errorMessages) > 0) {
                throw new \Pimcore\Model\Element\ValidationException(implode("<br>", $errorMessages));
            }
        }
        catch (\Exception $ex) {
			throw new \Pimcore\Model\Element\ValidationException($ex->getMessage());
		}
    }    
}
