<?php
namespace AppBundle\EventListener;


use Pimcore\Model\DataObject\ClassDefinition\CustomLayout;
use Pimcore\Model\DataObject\Product;

use Pimcore\Model\DataObject;
use Symfony\Component\EventDispatcher\GenericEvent;


class LayoutChangeListener {

    public function selectCustomLayout(GenericEvent $event) {
        
//        $defaultLayoutId=\Pimcore\Model\WebsiteSetting::getByName('defaultLayoutId');
//        $selectLayoutIdOnApprove=\Pimcore\Model\WebsiteSetting::getByName('selectLayoutIdOnApprove');
//        if(empty($defaultLayoutId)){
//            throw new Exception("'defaultLayoutId' is not set in website settings.");
//        }
//        if(empty($selectLayoutIdOnApprove)){
//            throw new Exception("'selectLayoutIdOnApprove' is not set in website settings.");
//        }
//        $object = $event->getArgument("object");
//        $obID = $object->getId();
//    
//        if($object instanceof Product) {
//            $data = $event->getArgument("data");
//            $db = \Pimcore\Db::get();		
//            $stm = $db->query('SELECT * from element_workflow_state WHERE cid = '.$obID);
//            $resJob = $stm->fetch();
//            $currentUser = \Pimcore\Tool\Admin::getCurrentUser();
//            $role = $currentUser->getRoles();
//               
//            if(($resJob['place'] == 'approved')) {
//                $data = $this->doModifyCustomLayouts($data, $object, $selectLayoutIdOnApprove->getData(), []);
//            }
//            $event->setArgument("data", $data);
//        }

    }
    
    /**
    * 
    */
    public function doModifyCustomLayouts($data, $object, $customLayoutToSelect = null, $layoutsToRemove = []) {
        
        if($customLayoutToSelect != null) {
            //set current layout to subcategory layout
            $data['currentLayoutId'] = $customLayoutToSelect;
            $customLayout = CustomLayout::getById($customLayoutToSelect);
            $data['layout'] = $customLayout->getLayoutDefinitions();
            \Pimcore\Model\DataObject\Service::enrichLayoutDefinition($data["layout"], $object);            
        }
        
        if(!empty($layoutsToRemove)) {
            //remove master layout from valid layouts
            $validLayouts = $data["validLayouts"];
            foreach($validLayouts as $key => $validLayout) {
                if(in_array($validLayout['id'], $layoutsToRemove)) {
                    unset($validLayouts[$key]);
                }
            }
            $data["validLayouts"] = array_values($validLayouts);            
        }

        return $data; 
    }

}
