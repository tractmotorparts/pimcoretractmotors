<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Pimcore\Model\DataObject;
use Symfony\Component\Workflow\Event\TransitionEvent;
use Pimcore\Model\DataObject\Product;
//use Pimcore\Event\DataObjectImportEvents;
use Pimcore\Model\DataObject\Service;
use Pimcore\Event\Model\DataObjectImportEvent;
use AppBundle\Utility\CurrencyHelper;
class  DataImportConfigListner
{
    public function onObjectPreSave (DataObjectImportEvent $event) {
        if($event->getObject() instanceof \Pimcore\Model\DataObject\Product) {
            $this->checkValidation($event);
            
            $DefaultEndUserDiscount = \Pimcore\Model\WebsiteSetting::getByName('DefaultEndUserDiscount')->getData();
            $DefaultDealerDiscount  = \Pimcore\Model\WebsiteSetting::getByName('DefaultDealerDiscount')->getData();
            $DefaultLooseDiscount   = \Pimcore\Model\WebsiteSetting::getByName('DefaultLooseDiscount')->getData();
            $DefaultBulkDiscount    = \Pimcore\Model\WebsiteSetting::getByName('DefaultBulkDiscount')->getData();
            
            $uSExchangeRatesetting = \Pimcore\Model\WebsiteSetting::getByName('USExchangeRate');
            $profitMarginsetting = \Pimcore\Model\WebsiteSetting::getByName('ProfitMargin');
            $sstsetting = \Pimcore\Model\WebsiteSetting::getByName('SST');
            $miscExpensessetting = \Pimcore\Model\WebsiteSetting::getByName('MiscExpenses');
            
            
            $object = $event->getObject();

            $productSku=$object->getSKU();
            $product= \Pimcore\Model\DataObject\Product::getBySKU($productSku,1);
            if(!empty($product)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Product With  SKU : " .$productSku . " Already Exist." );
            }

            $purchasePrice=$object->getPurchasePrice();
            if(empty($purchasePrice)){
                $purchasePrice=0;
            }
            
            $product = $object;
            
            $categoryName=$object->getCategory();
            $categoryName= strtolower($categoryName);
            $categoryObject=\Pimcore\Model\DataObject\Category::getByCategoryName($categoryName,1);
            if(empty($categoryObject)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Category Found For Product With With SKU : " .$productSku );
            }
            
            //var_dump($categoryObject);
            $catPath=$categoryObject->getPath();
            $catPath=ltrim($catPath,"/Category");
            $catPath="Products/".$catPath.$categoryObject->getCategoryName();
            $folder = Service::createFolderByPath($catPath);
            $parentId = $folder->getId();
            $product->setParentId($parentId);

            $product->setCategory($categoryObject->getId());

            $brandName=$object->getBrandName();
            $brandName=strtolower($brandName);
            $brandObject=\Pimcore\Model\DataObject\Brand::getByBrand($brandName,1);
            if(empty($brandObject)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Brand Found For Product With  SKU : " .$productSku );
            }
            $product->setBrandName($brandObject->getId());

            $machineType=$object->getMachineType();
            if(!empty($machineType)){
                $machineType=strtolower($machineType);
                $machineTypeObject=\Pimcore\Model\DataObject\MachineType::getByMachineType($machineType,1);
                if(empty($machineTypeObject)){
                    throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Machine Found For Product With  SKU : " .$productSku );
                }
                $product->setMachineType($machineTypeObject->getId());
            }
            
            $currency=$object->getCurrency();
            $currencyValue=CurrencyHelper::getByName($currency);
            if(!$currencyValue){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Invalid Currency For Product With  SKU : " .$productSku );
            }
            $product->setCurrency($currencyValue);
            $productName=trim($object->getProductName(),'');
	    $productName=str_replace('/','-',$productName);
            $product->setKey($productName);
	    
            
            $purchasePrice = $product->getPurchasePrice();
            $isSst= $product->getSST();

            
            
            $uSExchangeRate = $uSExchangeRatesetting->getData();
            $miscExpenses = $miscExpensessetting->getData();
            $sst = $sstsetting->getData();
            $profitMargin = $profitMarginsetting->getData();
            
            if(!empty($product->getProfitMargin())){
                $profitMargin=$product->getProfitMargin();
            }
            $product->setProfitMargin($profitMargin);
            $purchasePrice = $product->getPurchasePrice();
            $isSst = $product->getSST();
            if($isSst){
                $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses*$sst);
            }else{
                $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses);
            }

            //$cost=sprintf('%.4f', $cost);
            $cost=round($cost);
            $product->setCost($cost);
            
            $listPrice = (($cost/$profitMargin)*2);
            //$listPrice=sprintf('%.4f', $listPrice);
            $listPrice=round($listPrice);
            $product->setListPrice($listPrice);
            
            //Calculate Discount for End User Price
            $euDiscountPercent = $product->getDiscountforEU();
            if(empty($euDiscountPercent)){
                $euDiscountPercent=$DefaultEndUserDiscount;
            }
            if($euDiscountPercent > 0){
                $userDiscountedPrice = $listPrice - ($listPrice * ($euDiscountPercent / 100));
            }else{
                $userDiscountedPrice = $listPrice;
            }
            
            //Calculate Additional Discount For End User Price
            $euAddDiscountPercent = $product->getAddDiscountforEU();
            if($euAddDiscountPercent > 0){
                $userDiscountedPrice = $userDiscountedPrice - ($userDiscountedPrice * ($euAddDiscountPercent / 100));
            }
            //$userDiscountedPrice=sprintf('%.4f', $userDiscountedPrice);
            $userDiscountedPrice=round($userDiscountedPrice);
            $product->setEndUserPrice($userDiscountedPrice);
            $product->setDiscountforEU($euDiscountPercent);


            //Calculate Discount for Dealer

            $dealerDiscountPercent = $product->getDiscountforDealer();
            if(empty($dealerDiscountPercent)){
                $dealerDiscountPercent=$DefaultDealerDiscount;
            }
            if($dealerDiscountPercent > 0){
                $dealerDiscountedPrice = $listPrice - ($listPrice * ($dealerDiscountPercent / 100));
            }else{
                $dealerDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For Dealer
            $dealerAddDiscountPercent = $product->getAddDiscountforDealer();
            if($dealerAddDiscountPercent > 0){
                $dealerDiscountedPrice = $dealerDiscountedPrice - ($dealerDiscountedPrice * ($dealerAddDiscountPercent / 100));
            }
            //$dealerDiscountedPrice=sprintf('%.4f', $dealerDiscountedPrice);
            $dealerDiscountedPrice=round($dealerDiscountedPrice);
            $product->setDealerPrice($dealerDiscountedPrice);
            $product->setDiscountforDealer($dealerDiscountPercent);

            //Calculate Discount for loose

            $looseDiscountPercent = $product->getDiscountforLoose();
            if(empty($looseDiscountPercent)){
                $looseDiscountPercent=$DefaultLooseDiscount;
            }
            if($looseDiscountPercent > 0){
                $looseDiscountedPrice = $listPrice - ($listPrice * ($looseDiscountPercent / 100));
            }else{
                $looseDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For loose
            $looseAddDiscountPercent = $product->getAddDiscountforLoose();
            if($looseAddDiscountPercent > 0){
                $looseDiscountedPrice = $looseDiscountedPrice - ($looseDiscountedPrice * ($looseAddDiscountPercent / 100));
            }

            //$looseDiscountedPrice=sprintf('%.4f', $looseDiscountedPrice);
            $looseDiscountedPrice=round($looseDiscountedPrice);
            $product->setLoosePrice($looseDiscountedPrice);
            $product->setDiscountforLoose($looseDiscountPercent);

            //Calculate Discount for Bulk

            $bulkDiscountPercent = $product->getDiscountforBulk();
            if(empty($bulkDiscountPercent)){
                $bulkDiscountPercent=$DefaultBulkDiscount;
            }
            if($bulkDiscountPercent > 0){
                $bulkDiscountedPrice = $listPrice - ($listPrice * ($bulkDiscountPercent / 100));
            }else{
                $bulkDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For loose
            $bulkAddDiscountPercent = $product->getAddDiscountforBulk();
            if($bulkDiscountedPrice > 0){
                $bulkDiscountedPrice = $bulkDiscountedPrice - ($bulkDiscountedPrice * ($bulkAddDiscountPercent / 100));
            }
            //$bulkDiscountedPrice=sprintf('%.4f', $bulkDiscountedPrice);
            $bulkDiscountedPrice=round($bulkDiscountedPrice);
            $product->setBulkPrice($bulkDiscountedPrice);
            $product->setDiscountforBulk($bulkAddDiscountPercent);
        }
        
        if($event->getObject() instanceof \Pimcore\Model\DataObject\Parts) {
            $this->importParts($event);
        }
    }
    
    private function importParts($event){
        $DefaultEndUserDiscount = \Pimcore\Model\WebsiteSetting::getByName('DefaultEndUserDiscount')->getData();
        $DefaultDealerDiscount  = \Pimcore\Model\WebsiteSetting::getByName('DefaultDealerDiscount')->getData();
        $DefaultLooseDiscount   = \Pimcore\Model\WebsiteSetting::getByName('DefaultLooseDiscount')->getData();
        $DefaultBulkDiscount    = \Pimcore\Model\WebsiteSetting::getByName('DefaultBulkDiscount')->getData();

        $uSExchangeRatesetting = \Pimcore\Model\WebsiteSetting::getByName('USExchangeRate');
        $profitMarginsetting = \Pimcore\Model\WebsiteSetting::getByName('ProfitMargin');
        $sstsetting = \Pimcore\Model\WebsiteSetting::getByName('SST');
        $miscExpensessetting = \Pimcore\Model\WebsiteSetting::getByName('MiscExpenses');


        $object = $event->getObject();

        $productSku=$object->getSKU();
            $product= \Pimcore\Model\DataObject\Parts::getBySKU($productSku,1);
            if(!empty($product)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Parts With  SKU : " .$productSku . " Already Exist." );
            }

            $purchasePrice=$object->getPurchasePrice();
            if(empty($purchasePrice)){
                $purchasePrice=0;
            }
            
            $product = $object;
            
            $categoryName=$object->getCategory();
            $categoryName= strtolower($categoryName);
            $categoryObject=\Pimcore\Model\DataObject\Category::getByCategoryName($categoryName,1);
            if(empty($categoryObject)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Category Found For Product With With SKU : " .$productSku );
            }
            
            //var_dump($categoryObject);
            $catPath=$categoryObject->getPath();
            $catPath=ltrim($catPath,"/Category");
            $catPath="Parts/".$catPath.$categoryObject->getCategoryName();
            $folder = Service::createFolderByPath($catPath);
            $parentId = $folder->getId();
            $product->setParentId($parentId);

            $product->setCategory($categoryObject->getId());

            $brandName=$object->getBrandName();
            $brandName=strtolower($brandName);
            $brandObject=\Pimcore\Model\DataObject\Brand::getByBrand($brandName,1);
            if(empty($brandObject)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Brand Found For Product With  SKU : " .$productSku );
            }
            $product->setBrandName($brandObject->getId());

            $machineType=$object->getMachineType();
            if(!empty($machineType)){
                $machineType=strtolower($machineType);
                $machineTypeObject=\Pimcore\Model\DataObject\MachineType::getByMachineType($machineType,1);
                if(empty($machineTypeObject)){
                    throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Machine Found For Product With  SKU : " .$productSku );
                }
                $product->setMachineType($machineTypeObject->getId());
            }
            
            $currency=$object->getCurrency();
            $currencyValue=CurrencyHelper::getByName($currency);
            if(!$currencyValue){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Invalid Currency For Product With  SKU : " .$productSku );
            }
            $product->setCurrency($currencyValue);
            
            $product->setKey(trim($object->getProductName()),'');
            
            $purchasePrice = $product->getPurchasePrice();
            $isSst= $product->getSST();

            
            
            $uSExchangeRate = $uSExchangeRatesetting->getData();
            $miscExpenses = $miscExpensessetting->getData();
            $sst = $sstsetting->getData();
            $profitMargin = $profitMarginsetting->getData();
            
            if(!empty($product->getProfitMargin())){
                $profitMargin=$product->getProfitMargin();
            }
            $product->setProfitMargin($profitMargin);
            $purchasePrice = $product->getPurchasePrice();
            $isSst = $product->getSST();
            if($isSst){
                $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses*$sst);
            }else{
                $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses);
            }

            //$cost=sprintf('%.4f', $cost);
            $cost=round($cost);
            $product->setCost($cost);
            
            $listPrice = (($cost/$profitMargin)*2);
            //$listPrice=sprintf('%.4f', $listPrice);
            $listPrice=round($listPrice);
            $product->setListPrice($listPrice);
            
            //Calculate Discount for End User Price
            $euDiscountPercent = $product->getDiscountforEU();
            if(empty($euDiscountPercent)){
                $euDiscountPercent=$DefaultEndUserDiscount;
            }
            if($euDiscountPercent > 0){
                $userDiscountedPrice = $listPrice - ($listPrice * ($euDiscountPercent / 100));
            }else{
                $userDiscountedPrice = $listPrice;
            }
            
            //Calculate Additional Discount For End User Price
            $euAddDiscountPercent = $product->getAddDiscountforEU();
            if($euAddDiscountPercent > 0){
                $userDiscountedPrice = $userDiscountedPrice - ($userDiscountedPrice * ($euAddDiscountPercent / 100));
            }
            //$userDiscountedPrice=sprintf('%.4f', $userDiscountedPrice);
            $userDiscountedPrice=round($userDiscountedPrice);
            $product->setEndUserPrice($userDiscountedPrice);
            $product->setDiscountforEU($euDiscountPercent);


            //Calculate Discount for Dealer

            $dealerDiscountPercent = $product->getDiscountforDealer();
            if(empty($dealerDiscountPercent)){
                $dealerDiscountPercent=$DefaultDealerDiscount;
            }
            if($dealerDiscountPercent > 0){
                $dealerDiscountedPrice = $listPrice - ($listPrice * ($dealerDiscountPercent / 100));
            }else{
                $dealerDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For Dealer
            $dealerAddDiscountPercent = $product->getAddDiscountforDealer();
            if($dealerAddDiscountPercent > 0){
                $dealerDiscountedPrice = $dealerDiscountedPrice - ($dealerDiscountedPrice * ($dealerAddDiscountPercent / 100));
            }
            //$dealerDiscountedPrice=sprintf('%.4f', $dealerDiscountedPrice);
            $dealerDiscountedPrice=round($dealerDiscountedPrice);
            $product->setDealerPrice($dealerDiscountedPrice);
            $product->setDiscountforDealer($dealerDiscountPercent);

            //Calculate Discount for loose

            $looseDiscountPercent = $product->getDiscountforLoose();
            if(empty($looseDiscountPercent)){
                $looseDiscountPercent=$DefaultLooseDiscount;
            }
            if($looseDiscountPercent > 0){
                $looseDiscountedPrice = $listPrice - ($listPrice * ($looseDiscountPercent / 100));
            }else{
                $looseDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For loose
            $looseAddDiscountPercent = $product->getAddDiscountforLoose();
            if($looseAddDiscountPercent > 0){
                $looseDiscountedPrice = $looseDiscountedPrice - ($looseDiscountedPrice * ($looseAddDiscountPercent / 100));
            }

            //$looseDiscountedPrice=sprintf('%.4f', $looseDiscountedPrice);
            $looseDiscountedPrice=round($looseDiscountedPrice);
            $product->setLoosePrice($looseDiscountedPrice);
            $product->setDiscountforLoose($looseDiscountPercent);

            //Calculate Discount for Bulk

            $bulkDiscountPercent = $product->getDiscountforBulk();
            if(empty($bulkDiscountPercent)){
                $bulkDiscountPercent=$DefaultBulkDiscount;
            }
            if($bulkDiscountPercent > 0){
                $bulkDiscountedPrice = $listPrice - ($listPrice * ($bulkDiscountPercent / 100));
            }else{
                $bulkDiscountedPrice = $listPrice;
            }

            //Calculate Additional Discount For loose
            $bulkAddDiscountPercent = $product->getAddDiscountforBulk();
            if($bulkDiscountedPrice > 0){
                $bulkDiscountedPrice = $bulkDiscountedPrice - ($bulkDiscountedPrice * ($bulkAddDiscountPercent / 100));
            }
            //$bulkDiscountedPrice=sprintf('%.4f', $bulkDiscountedPrice);
            $bulkDiscountedPrice=round($bulkDiscountedPrice);
            $product->setBulkPrice($bulkDiscountedPrice);
            $product->setDiscountforBulk($bulkDiscountPercent);
    }
    
    Private function checkValidation($event){
	$product = $event->getObject();
	$errorMessages = [];
	try{
            if (!$product->getSKU()) {
                $errorMessages[] = 'SKU Not Found.';
            }
            if (!$product->getProductName()) {
                $errorMessages[] = 'Product Name Not Found.';
            }
            if (!$product->getModel()) {
                $errorMessages[] = 'Model Name Not Found.';
            }
            if (!$product->getCategory()) {
                $errorMessages[] = 'Category Name Not Found';
            }            
            if (!$product->getBrandName()) {
                $errorMessages[] = 'Brand Name Not Found.';
            }
//            if (!$product->getMachineType()) {
//                $errorMessages[] = 'Please enter MachineType.';
//            }
            if (!$product->getCountry()) {
                $errorMessages[] = 'Country Name Not Found.';
            }
            if (!$product->getDescription()) {
                $errorMessages[] = 'No Product Description Found.';
            }
            if (!$product->getHeight()) {
                $errorMessages[] = 'No Height Found.';
            }
            if (!$product->getGrossWeight()) {
                $errorMessages[] = 'No GrossWeight Found.';
            }
            if (!$product->getNetWeight()) {
                $errorMessages[] = 'No NetWeight Found For.';
            }
            if (!$product->getPackingDimensionHeight()) {
                $errorMessages[] = 'No Packing Dimension Height Found .';
            }
            if (!$product->getPackingDimensionLength()) {
                $errorMessages[] = 'No Packing Dimension Length Found.';
            }
            if (!$product->getPackingDimensionWidth()) {
                $errorMessages[] = 'No Packing Dimension Width Found.';
            }
//            if (!$product->getProfitMargin()) {
//                $errorMessages[] = 'No Profit Margin Found.';
//            }
            if (!$product->getPurchasePrice()) {
                $errorMessages[] = 'No Purchase Price Found.';
            }

            if (count($errorMessages) > 0) {
               throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException(implode("<br>", $errorMessages));
            }
        }catch(\Exception $ex) {
            throw new \Pimcore\Model\Element\ValidationException($ex->getMessage());
	}
    } 
}
