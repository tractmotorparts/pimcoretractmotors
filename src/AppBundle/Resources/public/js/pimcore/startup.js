pimcore.registerNS("pimcore.plugin.AppBundle");

pimcore.plugin.AppBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.AppBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },
    postOpenObject: function (object, type) {
        if (object.data.general.o_className == "Product" || object.data.general.o_className == "Parts") {
            var catComponent = object.edit.dataFields.Category.component;
            catComponent.addListener('change', function (obj) {
                setTimeout(function(){
                    let category = obj.value;
                    Ext.Ajax.request({
                        url: '/updateclassificationstore',
                        params: {
                            class : object.data.general.o_className,
                            productId:object.id,
                            categoryId:obj.value
                        },
                        success: function (response) {
                            var respObj = JSON.parse(response.responseText);
                            if(respObj.success == true){
                                object.save(undefined,undefined,function(){
                                    object.reload();
                                });
                            }
                        }
                    })
                },0)

            });

        }


    },
    postSaveObject:function(object, type){
        if (object.data.general.o_className == "Product" || object.data.general.o_className == "Parts") {
            object.reload();
        }
    }
});
new pimcore.plugin.AppBundle();
