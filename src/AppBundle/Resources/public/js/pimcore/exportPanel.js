pimcore.registerNS("pimcore.plugin.exportPanel");

pimcore.plugin.exportPanel = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.exportPanel";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
        //this.navEl = Ext.get('pimcore_menu_search').insertSibling('<li id="pimcore_menu_mds" data-menu-tooltip="mds Erweiterungen" class="pimcore_menu_item pimcore_menu_needs_children">EP</li>', 'after');
        if(pimcore.currentuser.permissions.indexOf("custom_export_products") >= 0) {
            this.navEl = Ext.get('pimcore_menu_search').insertSibling('<li id="pimcore_menu_mds" data-menu-tooltip="Export Product" class="pimcore_menu_item pimcore_menu_needs_children"><img src="https://cdn0.iconfinder.com/data/icons/typicons-2/24/export-outline-512.png" style="background-color: white!important;"></li>', 'after');
            this.menu = new Ext.menu.Menu({
                items: [{
                    text: "Export Product",
                    iconCls: "pimcore_icon_apply",
                    handler: function () {
                        Ext.Ajax.request({
                            url: '/admin/getCategories',
                            params: {
                                type: 'category'
                            },
                            success: function (response) {

                                var responseObj = JSON.parse(response.responseText);
                                if(responseObj.success == true){
                                    var store = Ext.create('Ext.data.Store', {
                                        fields: ['id', 'name', 'parentId'],
                                        data: responseObj.data
                                    });
                                    catSelectBox = Ext.create('Ext.ux.form.MultiSelect', {
                                        name: "category",
                                        triggerAction: "all",
                                        editable: false,
                                        fieldLabel: t("Category"),
                                        width: 400,
                                        maxHeight: 200,
                                        store: store,
                                        displayField: "name",
                                        valueField: "id"
                                    });
                                    // var catSelectBox = new Ext.form.ComboBox({
                                    //     fieldLabel: 'Category',
                                    //     name: 'category',
                                    //     store: store ,
                                    //     valueField:'id',
                                    //     displayField:'name',
                                    //     allowBlank:false,
                                    //     forceSelection : true,
                                    //     multiSelect:true,
                                    //     width:200,
                                    //
                                    // });
                                    var arrCheckBoxes = [];
                                    arrCheckBoxes.push({boxLabel : 'Base Attributes', name: 'baseAttributes' });
                                    arrCheckBoxes.push({boxLabel : 'Media Attributes', name: 'mediaAttributes' });
                                    arrCheckBoxes.push({boxLabel : 'Purchase Price', name: 'purchasePriceAttributes' });
                                    arrCheckBoxes.push({boxLabel : 'Sales Price', name: 'salesPriceAttributes' });
                                    arrCheckBoxes.push({boxLabel : 'Technical Attributes', name: 'technicalAttributes' });
                                    var checkboxes = new Ext.form.CheckboxGroup({
                                        id:'pricebookType',
                                        xtype: 'checkboxgroup',
                                        fieldLabel: 'Attributes',
                                        itemCls: 'x-check-group-alt pricebookType',
                                        columns: 2,
                                        items: arrCheckBoxes
                                    });
                                    var configPanel = new Ext.Panel({
                                        layout: "form",
                                        bodyStyle: "padding: 10px;",
                                        autoScroll: true,
                                        items: [catSelectBox, checkboxes],
                                        buttons: [{
                                            text: t("Export"),
                                            iconCls: "pimcore_icon_save",
                                            handler: function () {
                                                var postData = {};
                                                postData.attributes = checkboxes.getValue();
                                                postData.categories = catSelectBox.getValue();

                                                Ext.Ajax.request({
                                                    url: '/download-product',
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/json'
                                                    },
                                                    jsonData: postData,
                                                    success: function (response) {
                                                        //response.responseText = '{"success":true,"fileName":"http:\/\/tractmotors.local\/Product_Report\/sql_mdt5.xls"}';
                                                        var responseObj = JSON.parse(response.responseText);
                                                        if(responseObj.success == true){
                                                            var fileUri = responseObj.fileName;
                                                            Ext.DomHelper.append(Ext.getBody(), {
                                                                tag:          'iframe',
                                                                frameBorder:  0,
                                                                width:        0,
                                                                height:       0,
                                                                css:          'display:none;visibility:hidden;height:0px;',
                                                                src:          fileUri
                                                            });
                                                        }else{
                                                            alert('No Data Found');
                                                        }


                                                    }
                                                });
                                                console.log(postData);
                                            }.bind(this),
                                        }]
                                    });
                                    this.saveWindow = new Ext.Window({
                                        width: 600,
                                        height: 500,
                                        top:0,
                                        modal: true,
                                        title: t('Export Product'),
                                        layout: "fit",
                                        items: [configPanel]
                                    });

                                    this.saveWindow.show();
                                }
                            }
                        });
                    }
                }],
                cls: "pimcore_navigation_flyout"
            });
            pimcore.layout.toolbar.prototype.mdsMenu = this.menu;
        }


    },
    pimcoreReady: function (params, broker) {
        if(pimcore.currentuser.permissions.indexOf("custom_export_products") >= 0) {
            var toolbar = pimcore.globalmanager.get("layout_toolbar");
            this.navEl.on("mousedown", toolbar.showSubMenu.bind(toolbar.mdsMenu));
            pimcore.plugin.broker.fireEvent("mdsMenuReady", toolbar.mdsMenu);
        }
    }

});
new pimcore.plugin.exportPanel();
