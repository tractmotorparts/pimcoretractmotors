<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class AppBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/app/js/pimcore/startup.js',
            '/bundles/app/js/pimcore/exportPanel.js'
        ];
    }
}
