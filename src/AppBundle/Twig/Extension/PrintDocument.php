<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Twig\Extension;

use Symfony\Component\Intl\Intl;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Pimcore\Model\Document;
 use Pimcore\Db;
 use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;
use Symfony\Component\Yaml\Yaml;
class PrintDocument extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('pimcore_document_by_path', [$this, 'getDocumentByPath']),
            new TwigFunction('pimcore_get_object_tags', [$this, 'getElementTags']),
            new TwigFunction('pimcore_generate_image_url', [$this, 'getImageUrl']),
            new TwigFunction('pimcore_get_current_route_name', [$this, 'getCurrentRouteName']),
            new TwigFunction('pimcore_append_style', [$this, 'getStyle']),
            new TwigFunction('pimcore_get_compare_result', [$this, 'getProductCompareResult']),
            new TwigFunction('pimcore_get_technicleAttribute', [$this, 'getTechnichleAttrMultipleProducts']),
            new TwigFunction('product_grid_output_elm', [$this, 'prepareElmentArray']),
            new TwigFunction('product_grid_pagination', [$this, 'preParePagination']),
        ];
    }
    /*
     * @param string $path
     * 
     * @return boolean
     */
    
    public function getDocumentByPath($path){
         return Document::getByPath($path);
    }
    
    /*
     * @param string $type
     * @param string elementId
     * 
     * @return string
     */
    public function getElementTags($type,$elementId){
        $tagArray=array();
        $tags = \Pimcore\Model\Element\Tag::getTagsForElement($type,$elementId);
        $tagArray=array();
        foreach($tags as $tag){
            $tagArray[]= ucfirst($tag->getName());
        }
        return implode(",",$tagArray);
    }
    
    /*
     * @param Object Image
     * @param string elementId
     * 
     * @return string url
     */
    public function getImageUrl($imageObject){
        $imgUrl="";
        if($imageObject){
            $imgPath=$imageObject->getPath();
            $imgFileName=$imageObject->getFileName();
            $imgUrl=\Pimcore\Tool::getHostUrl().$imgPath.$imgFileName;
        }
        return $imgUrl;
    }
    
    public function getStyle($path){
        return file_get_contents($path);
    }
    
    public function getProductCompareResult($productArray){
        $fields=$this->getProductCompareFields();
        $tableHeading=array();
        $productList=array();
        $product=$productArray->getElements()[0];
        $definations=$product->getClass()->getFieldDefinitions();
//        foreach($definations as $k=>$v){
//            $tableHeading[$v->getTitle()]=$v->getName();
//        }
        foreach($fields as $k=>$v){
            $tableHeading[$k]=$v;
        }
        foreach($tableHeading as $title=>$name){
            foreach($productArray as $prod){
                if(!in_array($name, ['Image','Gallery','Video','TechnicalSpecification','Description','ShortDescription'])){
                    $getter= "get".ucfirst($name);
                    if($name=='MachineType'){
                        $machinTypeId=$product->getMachineType();
                        if($machinTypeId){
                            $machinType= \Pimcore\Model\DataObject\MachineType::getById($machinTypeId,1);
                            $productList[$title][]=$machinType->getMachineType();
                        }
                    }elseif($name=='Category'){
                         $categoryId=$product->getCategory();
                         if($categoryId){
                            $category= \Pimcore\Model\DataObject\Category::getById($categoryId,1);
                            $productList[$title][]=$category->getCategoryName();
                         }
                    }elseif($name=='BrandName'){
                         $brandId=$product->getBrandName();
                         if($brandId){
                            $brand= \Pimcore\Model\DataObject\Brand::getById($brandId,1);
                            $productList[$title][]=$brand->getBrand();
                         }

                    }elseif($name=='ListPrice'){
                        $explodedCurrency=explode('-',$prod->getCurrency());
                        $productList[$title][]=utf8_encode($explodedCurrency[1]).$product->getListPrice()." ";
                    }elseif($name=='Color'){
                        if(!empty($prod->getColor())){
                             $productList[$title][]="<div style='width:120px;height:20px;background-color:".$prod->getColor().";'></div>";
                        }else{
                            $productList[$title][]='-';
                        }
                    }elseif($name=='Color2'){
                        if(!empty($prod->getColor2())){
                             $productList[$title][]="<div style='width:120px;height:20px;background-color:".$prod->getColor2().";'></div>";
                        }else{
                            $productList[$title][]='-';
                        }
                    }elseif($name=='Color3'){
                        if(!empty($prod->getColor3())){
                             $productList[$title][]="<div style='width:120px;height:20px;background-color:".$prod->getColor3().";'></div>";
                        }else{
                            $productList[$title][]='-';
                        }
                    }elseif($name=='Color4'){
                        if(!empty($prod->getColor4())){
                             $productList[$title][]="<div style='width:120px;height:20px;background-color:".$prod->getColor4().";'></div>";
                        }else{
                            $productList[$title][]='-';
                        }
                    }elseif($name=='Color5'){
                        if(!empty($prod->getColor5())){
                             $productList[$title][]="<div style='width:120px;height:20px;background-color:".$prod->getColor5().";'></div>";
                        }else{
                            $productList[$title][]='-';
                        }
                    }else{
                        $productList[$title][]=$prod->$getter();
                    }
                }else if($name=='TechnicalSpecification'){
                    $this->getTechnichleAttributes($prod,$productList);
                }
            }
        }
        return $productList;
    }
    
    public function getTechnichleAttributes($product, &$productList){
        $techniCleSpec=$product->getTechnicalSpecification();
        $activeGroups=$techniCleSpec->getActiveGroups();
        $techniCalGroup=array();
        if(!empty($activeGroups)){
            $activeGroupIds=array_keys($activeGroups);
            foreach($techniCleSpec->getItems() as $k=>$item){
                foreach($item as  $ke=>$it){
                    $classStoreItem[$ke]=$it['default'];
                }
            }
            $imploddedActiveGroup=implode(",",$activeGroupIds);
            if($classStoreItem!=null){
                $classStore=array_keys($classStoreItem);
            }
            $db = Db::get();
//            echo $sqlQuery="SELECT keyId, title FROM classificationstore_relations 
//                            left join  classificationstore_keys 
//                            on classificationstore_keys.id=classificationstore_relations.keyId
//                            where groupId in (".$imploddedActiveGroup.");";
            $sqlQuery="SELECT id,title FROM classificationstore_keys where id "
                       . " in(SELECT keyId FROM classificationstore_relations "
                       . "  where  groupId in (".$imploddedActiveGroup."));";
            $groupList = $db->fetchAll($sqlQuery);
            if(count($groupList) > 0){
                foreach($groupList as $group){
                    if(isset($classStoreItem[$group['id']])){
                        $productList[$group['title']][] = $classStoreItem[$group['id']];
                    }else{
                        $productList[$group['title']][] = "-";
                    }
                }
            }
        }
        
    }
    
    public function prepareElmentArray($elmArray,$outputElementArr,$brandName){
        $outputElementArr[$brandName]=$elmArray;
        return $outputElementArr;
    }
    
    public function preParePagination($outputElementArr){
        $counter=0;
        var_dump($outputElementArr);
//        foreach()
    }
    
    public function getTechnichleAttrMultipleProducts($productArray){
        $productArray2=array();
        $title=array();
        $isTitleSet=false;
        foreach($productArray as $product){
            $techniCleSpec=$product->getTechnicalSpecification();
            $activeGroups=$techniCleSpec->getActiveGroups();
            $techniCalGroup=array();
            $titleArray=array();
            if(!empty($activeGroups)){
                $activeGroupIds=array_keys($activeGroups);
                foreach($techniCleSpec->getItems() as $k=>$item){
                    foreach($item as  $ke=>$it){
                        $classStoreItem[$ke]=$it['default'];
                    }
                }
                $imploddedActiveGroup=implode(",",$activeGroupIds);
                if($classStoreItem!=null){
                    $classStore=array_keys($classStoreItem);
                }
                $db = Db::get();
                $sqlQuery="SELECT id,title FROM classificationstore_keys where id "
                           . " in(SELECT keyId FROM classificationstore_relations "
                           . "  where  groupId in (".$imploddedActiveGroup."));";
                $groupList = $db->fetchAll($sqlQuery);
                if(count($groupList) > 0){
                    foreach($groupList as $group){
                        if(!$isTitleSet){
                            $title[]=$group['title'];
                        }
                        if(isset($classStoreItem[$group['id']])){
                            
                            $techniCalGroup[$group['title']] = $classStoreItem[$group['id']];
                        }else{
                            $techniCalGroup[$group['title']] = "-";
                        }
                    }
                    $isTitleSet=true;
                }
            }
            $productArray2['technichleItems'][]=$techniCalGroup;
        }
        if(!empty($title)){
            $productArray2['title']=$title;
        }
//        echo '<pre>';
//        print_r($productArray2);die();
        return $productArray2;
    }
    
    private  function getProductCompareFields() {
        $fields=array(
                    'SKU'=>'SKU',
                    'Product  Name'=>'ProductName',
                    'Model'=>'Model',
                    'Category'=>'Category',
                    'Brand Name'=>'BrandName',
                    'Machine Type'=>'MachineType',
                    'Country'=>'Country',
                    'Color 1'=>'Color',
                    'Color 2'=>'Color2',
                    'Color 3'=>'Color3',
                    'Color 4'=>'Color4',
                    'Color 5'=>'Color5',
                    'Height (cm)'=>'Height',
                    'Gross Weight (kg)'=>'GrossWeight',
                    'Net Weight (kg)'=>'NetWeight',
                    'Packing Dimension Height (mm)'=>'PackingDimensionHeight',
                    'Packing Dimension Length (mm)'=>'PackingDimensionLength',
                    'Packing Dimension Width (mm)'=>'PackingDimensionWidth',
                    'List Price'=>'ListPrice',
                    'TechnicalSpecification'=>'TechnicalSpecification'
                );
        return $fields;
    }
    
}
