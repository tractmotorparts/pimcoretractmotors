<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject\Product;
use Pimcore\Model\DataObject\Parts;
use Pimcore\Model\DataObject\Category;
use Pimcore\Db;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Pimcore\Model\DataObject\ProductExportConfig;
use AppBundle\OptionsProviders\ProExportConfigOptionProvider;
class DefaultController extends FrontendController
{
    public function defaultAction(Request $request)
    {
    }
	/**
    @Route("/getproducts")
     */
	/*public function getProductsAction(Request $request){
		$categoryData = Category::getById(11);
		$productList = Product::getByCategory($categoryData);
		
		if($productList->getCount()>0){
				$productData = $productList->getData();
				foreach($productData as $product){
					echo $product->getSKU();
					echo $product->getProductName();
					echo $product->getModel();
					echo $product->getCategory();
					echo $product->getBrandName();
					echo $product->getMachineType();

				}
                 

		}
		//dump($productData);
		exit;

	}*/

    //export csv function//
    /**
    @Route("/export_to_csv_download")
     */

/*function export_to_csv_download(Request $request) {
    $categoryData = Category::getById(11);
    $productList = Product::getByCategory($categoryData);
    $productData = $productList->getData();

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=.$filename; ";"');
    $delimiter = ";";
    $f = fopen("export.csv", "w");
    foreach ($productData as $line) {
    fputcsv($f, $line, $delimiter);
}
    
    //dump($productData);
    //exit;
}*/
	 
    /**
    @Route("/updateclassificationstore", name="dashboard")
     */
    public function updateclassificationstoreAction(Request $request)
    {
        $class = $request->get('class');
        $productId = $request->get('productId');
        $categoryId = $request->get('categoryId');

        if($class == 'Product' || $class == 'Parts'){
            if($class =='Product'){
                $productData = Product::getById($productId);
            }else{
                $productData = Parts::getById($productId);
            }
            
            $categoryData = Category::getById($categoryId);
            //$categoryData = Category::getByPath($categoryId);
            $cattegoryName = $categoryData->getCategoryName();
            $cattegoryName=trim($cattegoryName,'');
            $attributes = $productData->getTechnicalSpecification();
            //$activeGroups = $attributes->getActiveGroups();
            $productData->getTechnicalSpecification()->setActiveGroups([]);
            $collectionMapping = $attributes->getGroupCollectionMappings();
            $db = Db::get();
            $sqlQuery = "SELECT groupId FROM classificationstore_collectionrelations WHERE colId = (SELECT id FROM classificationstore_collections WHERE NAME = '".$cattegoryName."' AND storeId  = (SELECT id FROM classificationstore_stores WHERE NAME = 'Attributes'))";
            $groupList = $db->fetchAll($sqlQuery);
            if(count($groupList) > 0){
                foreach($groupList as $group){
                    $activeGroups[$group['groupId']] = true;
                }
                $productData->getTechnicalSpecification()->setActiveGroups($activeGroups);
            }

            $productData->setCategory($categoryData->getId());
            $productData->setOmitMandatoryCheck(true);
            $productData->save();
            //$activeGroups = $productData->getAttributes()->getActiveGroups();

            echo json_encode(array('errors' => [], 'success' => true));
            exit;
        }
        exit;
    }
    
    /**
    @Route("/download-product", name="download_prod_by_cat")
    */
    public function downLoadProduct(Request $request){
        $request_body = file_get_contents('php://input');
        $requestObj= json_decode($request_body);
        
        $categories=$requestObj->categories;
        $attributes=$requestObj->attributes;
       
        
        //Listing Of Selected Categories
        $categoryDataSet=array();
        $categoryListing=new Category\Listing();
        $categoryListing->setCondition("o_id IN (?)", [$categories]);
        $categories=$categoryListing->load();
        
        if(count($categories)<1){
            return $this->json(["success" => false, "error" => "No Category Found."]);
        }
        
        //Fetch All Ids Of Category With Name as Respective Key
        $catArray=array();
        foreach($categories as $category){
            $tree = array();
            $isParent=$category->hasChildren();
            if($isParent){
                $tree[$category->getCategoryName()]=$category->getId();
                $this->getChildCategoryId($category,$tree);
                $catArray[]=$tree;
            }else{
               $tree[$category->getCategoryName()]=$category->getId();
               $catArray[]=$tree;
            }
             
        }
        
        //Merge All Category Id
        $mergeArray=array();
        foreach($catArray as $key=>$val){
           $mergeArray=array_merge($mergeArray,$val);
        }
        $mergedCategory=$mergeArray;
        
        //Fetch All Category id For Pass In Sql
        $mergeArray=array_values($mergeArray);
        
        
        $dataSet=array();
        $productListing=new Product\Listing();
        $productListing->setCondition("Category IN (?)", [$mergeArray]);
        //$productListing->setUnpublished(true);
        $prodcuts=$productListing->load();
        
//        echo "<pre>";
//        print_r($prodcuts);
        $proExpCnfg=ProductExportConfig::getByExportFor('Product',1);
        if(empty($proExpCnfg)){
            return $this->json(["success" => false, "error" => "Please Set Download Configuration First."]);
        }
        $baseAttr= $proExpCnfg->getBaseAttribute();
        if(empty($baseAttr)){
            return $this->json(["success" => false, "error" => "Please Set Download Configuration First."]);
        }
        $mediaAttr= $proExpCnfg->getMediaAttribute();
        if(empty($mediaAttr)){
            return $this->json(["success" => false, "error" => "Please Set Download Configuration First."]);
        }
        $salesAttr= $proExpCnfg->getSalesAttribute();
        if(empty($salesAttr)){
            return $this->json(["success" => false, "error" => "Please Set Download Configuration First."]);
        }
        $purchaseAttr=$proExpCnfg->getPurchaseAttribute();
        if(empty($purchaseAttr)){
            return $this->json(["success" => false, "error" => "Please Set Download Configuration First."]);
        }

        
        $excelColumns=$this->getExcelColumnTitles();
        
        if(count($prodcuts)>0){
            foreach($prodcuts as $product){
                //print_r($product);
                $dataSet2=array();
                if(isset($attributes->baseAttributes)){
                    foreach($baseAttr as $base){
                        $attname= ucfirst($base);
                        $getter="get".$attname;
                        if(!empty($product->$getter())){
                            if($attname=='MachineType'){
                                $machinTypeId=$product->getMachineType();
                                if($machinTypeId){
                                    $machinType= \Pimcore\Model\DataObject\MachineType::getById($machinTypeId,1);
                                    $dataSet2[$excelColumns[$attname]]=$machinType->getMachineType();
                                }
                            }elseif($attname=='Category'){
                                 $categoryId=$product->getCategory();
                                 if($categoryId){
                                    $category= \Pimcore\Model\DataObject\Category::getById($categoryId,1);
                                    $dataSet2[$excelColumns[$attname]]=$category->getCategoryName();
                                 }
                            }elseif($attname=='BrandName'){
                                 $brandId=$product->getBrandName();
                                 if($brandId){
                                    $brand= \Pimcore\Model\DataObject\Brand::getById($brandId,1);
                                    $dataSet2[$excelColumns[$attname]]=$brand->getBrand();
                                 }
                                
                            }else{
                                 $dataSet2[$excelColumns[$attname]]=$product->$getter();
                            }
                        }else{
                             $dataSet2[$excelColumns[$attname]]='';
                        }
                    }
                }
                if(isset($attributes->mediaAttributes)){
                    foreach($mediaAttr as $media){
                        $attname= ucfirst($media);
                        $getter="get".$attname;
                        if(!empty($product->$getter())){
                            if($attname=='Image'){
                                $imageObject=$product->$getter();
                                if(!empty($imageObject)){
                                    $imagePath=\Pimcore\Tool::getHostUrl().$imageObject->getPath().$imageObject->getFileName();
                                    $dataSet2[$excelColumns[$attname]]=$imagePath;
                                }
                            }elseif($attname=='Gallery'){
                                $galleryItem=$product->$getter()->getItems();
                                $paths="";
                                if(!empty($galleryItem)){
                                    foreach($galleryItem as $gI){
                                        if(!empty($gI)){
                                            $hotSpotImage=$gI->getImage();
                                            $paths.=\Pimcore\Tool::getHostUrl().$hotSpotImage->getPath().$hotSpotImage->getFileName()."  , ";
                                        }

                                    }
                                    $dataSet2[$excelColumns[$attname]]=$paths;
                                }
                                
                            }elseif($attname=='Video'){
                                $videotem=$product->$getter();
                                if(!empty($videotem)){
                                    $videoUrl="";
                                    foreach($videotem as $vi){
                                        if(!empty($vi)){
                                            $videoTypes=$vi['Video']->getData()->getType();
                                            if($videoTypes=="youtube"){
                                                $youtubeBase="https://www.youtube.com/watch?v=";
                                                $videoUrl.=$youtubeBase.$vi['Video']->getData()->getData();
                                            }
                                            if($videoTypes=="dailymotion"){
                                                $dailymotion="https://www.dailymotion.com/video/";
                                                $videoUrl.=$dailymotion.$vi['Video']->getData()->getData();
                                            }
                                            if($videoTypes=="asset"){
                                                //$dailymotion="https://www.dailymotion.com/video";
                                                //$videoUrl.=$dailymotion.$vi['Video']->getData()->getData();
                                            }
                                        }
                                    }
                                    $dataSet2[$excelColumns[$attname]]=$videoUrl;
                                }
                            }
                        }else{
                             $dataSet2[$excelColumns[$attname]]='';
                        }

                    }
                }
                if(isset($attributes->salesPriceAttributes)){
                    foreach($salesAttr as $salse){
                        $attname= ucfirst($salse);
                        $getter="get".$attname;
                        if(!empty($product->$getter())){
                             $dataSet2[$excelColumns[$attname]]=$product->$getter();
                        }else{
                             $dataSet2[$excelColumns[$attname]]='';
                        }

                    }
                }
                if(isset($attributes->purchasePriceAttributes)){
                    foreach($purchaseAttr as $purch){
                        $attname= ucfirst($purch);
                        $getter="get".$attname;
                        if(!empty($product->$getter())){
                             $dataSet2[$excelColumns[$attname]]=$product->$getter();
                        }else{
                             $dataSet2[$excelColumns[$attname]]='';
                        }

                    }
                }
                if(isset($attributes->technicalAttributes)){
                    //if(!empty($product->getTechnicalSpecification()->getItems())){
                        $this->getTechnichleAttributes($product,$dataSet2);
                    //}
                }
                $dataSet[$category->getCategoryName()][]=$dataSet2;
            }
           
            $fileName = 'Products_'.time().'.xls';

            $sheetIndex =0;
            $spreadsheet = new Spreadsheet();
            foreach($mergedCategory as $key=>$val){
                $sheet=$spreadsheet->createSheet($sheetIndex);
                $sheet->setTitle($key);
                $categoryProducts=$dataSet[$key];
                if(isset($categoryProducts) && count($categoryProducts)>0){
                    $this->setHeader($categoryProducts[0],$sheet);
                    
                    $r=2;
                    foreach($categoryProducts as $cp){
                        
                        $c=1;
                        foreach($cp as $p){
                            $sheet->setCellValueByColumnAndRow($c,$r,$p);
                            $c++;
                        }
                        $r++;
                        
                    }
                }else{
                    $sheet->setCellValue('A1', "No Product Found For Category ".$key);
                }
                $sheetIndex++;
            }
            $spreadsheet->setActiveSheetIndex(0);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save('Product_Report/'.$fileName);
            $url=\Pimcore\Tool::getHostUrl()."/Product_Report/";
            return $this->json(["success" => true, "fileName" => $url.$fileName]);
        }else{
            return $this->json(["success" => false, "error" => "No Product Found."]);
        }
    }
    private function getChildCategoryId($parentCategory,&$tree){
        
        //if($category->hasChildren()){
            $children=$parentCategory->getChildren();
            foreach($children as $c){
                $isParent=$c->hasChildren();
                if($isParent){
                    $tree[$c->getCategoryName()]=$c->getId();
                    $this->getChildCategoryId($c,$tree);
                }else{
                    $tree[$c->getCategoryName()]=$c->getId();
                }
            }
        //}
        
    }
    
    private function setHeader($firstRow,$sheetObject){
        $columnas=array_keys($firstRow);
        $i=1;
        $j=1;
        foreach($columnas as $c){
            $sheetObject->setCellValueByColumnAndRow($i,$j,$c);
            //$sheetObject->getStyle($i,$j)->getFont()->setBold(true);
            //$sheetObject->getStyle($i,$j)->setCellWidth(200);
            $i++;
        }
        
    }
    public function getTechnichleAttributes($product,&$dataSet2){
        $techniCleSpec=$product->getTechnicalSpecification();
        $activeGroups=$product->getTechnicalSpecification()->getActiveGroups();
        if(!empty($activeGroups)){
            $activeGroupIds=array_keys($activeGroups);
            foreach($techniCleSpec->getItems() as $k=>$item){
                foreach($item as  $ke=>$it){
                    $classStoreItem[$ke]=$it['default'];
                }
            }
            $imploddedActiveGroup=implode(",",$activeGroupIds);
            $classStore=array_keys($classStoreItem);
            $storeId=implode(',',$classStore);
            $db = Db::get();
            $sqlQuery="SELECT keyId, title FROM classificationstore_relations 
                            left join  classificationstore_keys 
                            on classificationstore_keys.id=classificationstore_relations.keyId
                            where groupId in (".$imploddedActiveGroup.");";
            $groupList = $db->fetchAll($sqlQuery);
            if(count($groupList) > 0){
                foreach($groupList as $group){
                    if(isset($classStoreItem[$group['keyId']])){
                        $dataSet2[$group['title']] = $classStoreItem[$group['keyId']];
                    }else{
                        $dataSet2[$group['title']] = "";
                    }
                }
            }
        }
    }
    
    private function getExcelColumnTitles(){
        $list=array();
        
        $productListing=new Product\Listing();
        $productListing->setLimit(1);
        $productListing->setUnpublished(true);
        $productobj=$productListing->load();

        $productobj=$productobj[0];
        $fieldDefination=$productobj->getClass()->getFieldDefinitions();
        foreach($fieldDefination as $k=>$v){
            $list[$v->getName()]= $v->getTitle();
        }
        return $list;
       
    }
    
}
