<?php


namespace AppBundle\Controller;


use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Sabre\Event\Loop\instance;

/**
 * Class AdminController
 * @Route("/admin")
 */
class AdminController extends \Pimcore\Bundle\AdminBundle\Controller\AdminController
{

    /**
     * @Route("/getCategories", methods={"GET","HEAD"})
     */
    public function getCategories (Request $request){

    $categoryListData = Category::getList();
        if($categoryListData->getCount() > 0){
            $categories = [];
            foreach($categoryListData->getData() as $category){
                $categories[$category->getParentId()][] = array("id"=>$category->getId(), "name"=>$category->getCategoryName(),"parentId"=>$category->getParentId());
            }
            $cats = [];
            $parentCats = [];
            foreach($categories as $key=>$catGroup){
                $obj = DataObject::getById($key);
                if($obj instanceof DataObject\Folder){
                    $parentCats = $catGroup;
                }
            }
            foreach($parentCats as $parent){
                array_push($cats,$parent);
                //dump($categories[$parent['id']]);
                if(is_array($categories[$parent['id']])){
                    foreach($categories[$parent['id']] as $child){
                        $child['name'] = $parent['name'].' - '. $child['name'];
                        array_push($cats,$child);
                    }
                }

            }
            $response = new Response();
            $response->setContent(json_encode(array('success' => true, 'errors' => [],'data'=>$cats)));
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }else{
            $response = new Response();
            $response->setContent(json_encode(array('success' => false, 'errors' => ['Data Not Found'],'data'=>[])));
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

}

