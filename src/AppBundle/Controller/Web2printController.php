<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 *  @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 *  @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Controller;

use Pimcore\Model\DataObject\Product;
use AppBundle\Model\Product\Car;
use Pimcore\Model\DataObject\AbstractObject;
use Pimcore\Model\Document\Hardlink;
use Pimcore\Web2Print\Processor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Model\DataObject\ProductExportConfig;
use Pimcore\Model\DataObject\Category;
 use Symfony\Component\HttpFoundation\Session\Session;
 use Pimcore\Db;
class Web2printController extends BaseController
{
    
    public function defaultAction(Request $request){
        $categoryListData = Category::getList();
        $paramsBag = [];
        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        $paramsBag = array_merge($this->view->getAllParameters(), $paramsBag);
        return $this->render('web2print/default.html.twig', $paramsBag);
        
    }
    public function ProductGridViewAction(Request $request)
    {
        $categoryListData = Category::getList();
        $paramsBag = [];
        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        $paramsBag = array_merge($this->view->getAllParameters(), $paramsBag);
        return $this->render('web2print/product_grid_catalogue.html.twig', $paramsBag);
        
    }
    
    public function ProductCompareViewAction(Request $request)
    {
        $categoryListData = Category::getList();
        $paramsBag = [];
        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        $paramsBag = array_merge($this->view->getAllParameters(), $paramsBag);
        return $this->render('web2print/product_comparison_catalogue.html.twig', $paramsBag);
        
    }
    
    public function SingleProductDetailViewAction(Request $request)
    {
        $categoryListData = Category::getList();
        $paramsBag = [];
        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        $paramsBag = array_merge($this->view->getAllParameters(), $paramsBag);
        return $this->render('web2print/product_detail_catalogue.html.twig', $paramsBag);
        
    }
    
    public function MultipleProductViewAction(Request $request)
    {
        $categoryListData = Category::getList();
        $paramsBag = [];
        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        $paramsBag = array_merge($this->view->getAllParameters(), $paramsBag);
        return $this->render('web2print/multiple_product_catalogue.html.twig', $paramsBag);
        
    }
    
    public function containerAction(Request $request)
    {
        $paramsBag = [];

        foreach ($request->attributes as $key => $value) {
            $paramsBag[$key] = $value;
        }
        
        $allChildren = [];
       
        
        //prepare children for include
        foreach ($this->document->getAllChildren() as $child) {
            if ($child instanceof Hardlink) {
                $child = Hardlink\Service::wrap($child);
            }

            $child->setProperty('hide-layout', 'bool', true);

            $allChildren[] = $child;
        }
        $paramsBag['allChildren'] = $allChildren;
        return $paramsBag;
    }

    public function productAction(Request $request){
        //AbstractObject::setGetInheritedValues(true);
        $fields=$this->getProductViewFields();
        $product = Product::getById($request->get('id'));
        $definations=$product->getClass()->getFieldDefinitions();
        $list=array();
        foreach($fields as $k=>$v){
             if(!in_array($v, ['Image','Gallery','Video','Description','ShortDescription'])){
                if($v=='TechnicalSpecification'){
                    $this->getTechnichleAttributes($product,$list);
                }else{
                    if($v=='MachineType'){
                        $machinTypeId=$product->getMachineType();
                        if($machinTypeId){
                            $machinType= \Pimcore\Model\DataObject\MachineType::getById($machinTypeId,1);
                            $list[$k]=$machinType->getMachineType();
                        }
                    }elseif($v=='Category'){
                         $categoryId=$product->getCategory();
                         if($categoryId){
                            $category= \Pimcore\Model\DataObject\Category::getById($categoryId,1);
                            $list[$k]=$category->getCategoryName();
                         }
                    }elseif($v=='BrandName'){
                         $brandId=$product->getBrandName();
                         if($brandId){
                            $brand= \Pimcore\Model\DataObject\Brand::getById($brandId,1);
                            $list[$k]=$brand->getBrand();
                         }
                    }elseif($v=='ListPrice'){
                        $explodedCurrency=explode('-',$product->getCurrency());
                        $list[$k]=utf8_encode($explodedCurrency[1]).$product->getListPrice()." ";
                        
                    }elseif($v=='Color'){
                        if(!empty($product->getColor())){
                            $list[$k]="<div style='width:120px;height:20px;background-color:".$product->getColor() .";'></div>";
                        }
                    }elseif($v=='Color2' ){
                        if(!empty($product->getColor2())){
                            $list[$k]="<div style='width:120px;height:20px;background-color:".$product->getColor2() .";'></div>";
                        }
                    }elseif($v=='Color3'){
                        if(!empty($product->getColor3())){
                            $list[$k]="<div style='width:120px;height:20px;background-color:".$product->getColor3() .";'></div>";
                        }
                    }elseif($v=='Color4'){
                       if(!empty($product->getColor4())){
                            $list[$k]="<div style='width:120px;height:20px;background-color:".$product->getColor4() .";'></div>";
                        }
                    }elseif($v=='Color5'){
                        if(!empty($product->getColor5())){
                            $list[$k]="<div style='width:120px;height:20px;background-color:".$product->getColor5() .";'></div>";
                        }
                    }else{
                        $getter= "get".ucfirst($v);
                        $list[$k]=$product->$getter();
                    }
                    
                }
            }
        }
        $paramsBag['product'] = $product;
        $paramsBag['productList'] = $list;
        return $paramsBag;
    }
    
    public function getTechnichleAttributes($product, &$list){
        $techniCleSpec=$product->getTechnicalSpecification();
        $activeGroups=$techniCleSpec->getActiveGroups();
        $techniCalGroup=array();
        if(!empty($activeGroups)){
            $activeGroupIds=array_keys($activeGroups);
            foreach($techniCleSpec->getItems() as $k=>$item){
                foreach($item as  $ke=>$it){
                    $classStoreItem[$ke]=$it['default'];
                }
            }
            $imploddedActiveGroup=implode(",",$activeGroupIds);
            if($classStoreItem!=null){
                $classStore=array_keys($classStoreItem);
            }
            $db = Db::get();
//            echo $sqlQuery="SELECT keyId, title FROM classificationstore_relations 
//                            left join  classificationstore_keys 
//                            on classificationstore_keys.id=classificationstore_relations.keyId
//                            where groupId in (".$imploddedActiveGroup.");";
            $sqlQuery="SELECT id,title FROM classificationstore_keys where id "
                       . " in(SELECT keyId FROM classificationstore_relations "
                       . "  where  groupId in (".$imploddedActiveGroup."));";
            $groupList = $db->fetchAll($sqlQuery);
            if(count($groupList) > 0){
                foreach($groupList as $group){
                    if(isset($classStoreItem[$group['id']])){
                        $list[$group['title']] = $classStoreItem[$group['id']];
                    }
                    // else{
                    //     $list[$group['title']] = "-";
                    // }
                }
            }
        }
        
    }
    
    private  function getProductViewFields() {
        $fields=array(
                    'SKU'=>'SKU',
                    'Product  Name'=>'ProductName',
                    'Model'=>'Model',
                    'Category'=>'Category',
                    'Brand Name'=>'BrandName',
                    'Machine Type'=>'MachineType',
                    'Country'=>'Country',
                    'Color 1'=>'Color',
                    'Color 2'=>'Color2',
                    'Color 3'=>'Color3',
                    'Color 4'=>'Color4',
                    'Color 5'=>'Color5',
                    'Height (cm)'=>'Height',
                    'Gross Weight (kg)'=>'GrossWeight',
                    'Net Weight (kg)'=>'NetWeight',
                    'Packing Dimension Height (mm)'=>'PackingDimensionHeight',
                    'Packing Dimension Length (mm)'=>'PackingDimensionLength',
                    'Packing Dimension Width (mm)'=>'PackingDimensionWidth',
                    'TechnicalSpecification'=>'TechnicalSpecification'
                );
        return $fields;
    }
}
