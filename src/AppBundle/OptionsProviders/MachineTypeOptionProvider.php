<?php

namespace AppBundle\OptionsProviders;

use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
//use Pimcore\Model\DataObject\Brand;
use Pimcore\Model\DataObject\MachineType;

class MachineTypeOptionProvider implements SelectOptionsProviderInterface
{
    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
     */
    public function getOptions($context, $fieldDefinition) {
        
       
        $listing = new MachineType\Listing();
        foreach ($listing as $attribute) {

            $data[] = [
                'key' => $attribute->getMachineType(),
                'value' => $attribute->getId()
            ];
        }
        return $data;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        return null;
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return false;
    }

}
