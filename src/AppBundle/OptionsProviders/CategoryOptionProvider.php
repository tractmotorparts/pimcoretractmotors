<?php

namespace AppBundle\OptionsProviders;

use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
//use Pimcore\Model\DataObject\Brand;
use Pimcore\Model\DataObject\Category;
use Pimcore\Model\DataObject;
class CategoryOptionProvider implements SelectOptionsProviderInterface
{
    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
     */
    public function getOptions($context, $fieldDefinition) {
        
        $categoryListData = Category::getList();
        $categories = [];
        foreach($categoryListData->getData() as $category){
            $categories[$category->getParentId()][] = array("id"=>$category->getId(), "name"=>$category->getCategoryName(),"parentId"=>$category->getParentId());
        }
        $cats = [];
        $parentCats = [];
        foreach($categories as $key=>$catGroup){
            $obj = DataObject::getById($key);
            if($obj instanceof DataObject\Folder){
                $parentCats = $catGroup;
            }
        }
        foreach($parentCats as $parent){
            array_push($cats,$parent);
            $data[]= [
                        'key' => $parent['name'],
                        'value' => $parent['id']
                    ];
            if(is_array($categories[$parent['id']])){
                foreach($categories[$parent['id']] as $child){
                    $data[] = [
                                'key' => $parent['name'].' - '. $child['name'],
                                'value' => $child['id']
                            ];
                }
            }
        }
        return $data;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        return null;
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return false;
    }

}
