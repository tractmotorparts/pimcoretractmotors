<?php

namespace AppBundle\OptionsProviders;

use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use Pimcore\Model\DataObject\Product;

class ProExportConfigOptionProvider implements SelectOptionsProviderInterface
{
    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
    */
    public function getOptions($context, $fieldDefinition) {
        $list = [];
        $providerClass = $fieldDefinition->getOptionsProviderData();
        if (class_exists($providerClass) && !empty($providerClass))  {
            $masterClassName = (new \ReflectionClass($providerClass))->getShortName();
            $className=$providerClass.'\Listing';
            $productListing=new $className();
            $productListing->setLimit(1);
            $productListing->setUnpublished(true);
            $productobj=$productListing->load();
//            print_r($productobj);
            $productobj=$productobj[0];
            $fieldDefination=$productobj->getClass()->getFieldDefinitions();
            foreach($fieldDefination as $k=>$v){
                array_push($list,["key" => $v->getTitle(), "value" => $v->getName()]);
                
            }
        }
        
        return $list;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return false;
    }
    

}
