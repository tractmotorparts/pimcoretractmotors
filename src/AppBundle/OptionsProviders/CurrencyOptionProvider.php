<?php

namespace AppBundle\OptionsProviders;

use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use Pimcore\Model\DataObject\Brand;
use AppBundle\Utility\CurrencyHelper;
class CurrencyOptionProvider implements SelectOptionsProviderInterface
{
    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
     */
    public function getOptions($context, $fieldDefinition) {
        $currency=CurrencyHelper::getAll();
        foreach ($currency as $key=> $value) {

            $data[] = [
                'key' => $key,
                'value' =>$key."-".$value 
            ];
        }
        return $data;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition) {
        //return '&#36;'.'-'.'USD';
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition) {
        return false;
    }

}
