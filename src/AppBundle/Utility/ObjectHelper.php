<?php
/*
 * UtilsHelper
 */

namespace AppBundle\Utility;




/**
 * CurrencyHelper contains the function need to print currency
 *
 * @author Pimcore
*/
class ObjectHelper {
    /*
     * calculate pricing of product and parts on save
     * 
     */
    public static function calculatePricingOnSaveObject(&$object){
        $uSExchangeRatesetting = \Pimcore\Model\WebsiteSetting::getByName('USExchangeRate');
        $uSExchangeRate = $uSExchangeRatesetting->getData();

        $miscExpensessetting = \Pimcore\Model\WebsiteSetting::getByName('MiscExpenses');
        $miscExpenses = $miscExpensessetting->getData();

        $sstsetting = \Pimcore\Model\WebsiteSetting::getByName('SST');
        $sst = $sstsetting->getData();

        $profitMarginsetting = \Pimcore\Model\WebsiteSetting::getByName('ProfitMargin');
        $profitMargin = $profitMarginsetting->getData();

        if(!empty($object->getProfitMargin())){
            $profitMargin=$object->getProfitMargin();
        }
       
        $purchasePrice = $object->getPurchasePrice();
        $isSst = $object->getSST();
        if($isSst){
            $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses*$sst);
        }else{
            $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses);
        }
        //$cost=sprintf('%.4f', $cost);
        $cost=round($cost);
        $object->setCost($cost);
        $listPrice = (($cost/$profitMargin)*2);

        //$listPrice=sprintf('%.4f', $listPrice);
        $listPrice=round($listPrice);
        $object->setListPrice($listPrice);
        //Calculate Discount for End User Price

        $euDiscountPercent = $object->getDiscountforEU();
        if($euDiscountPercent > 0){
            $userDiscountedPrice = $listPrice - ($listPrice * ($euDiscountPercent / 100));
        }else{
            $userDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For End User Price
        $euAddDiscountPercent = $object->getAddDiscountforEU();
        if($euAddDiscountPercent > 0){
            $userDiscountedPrice = $userDiscountedPrice - ($userDiscountedPrice * ($euAddDiscountPercent / 100));
        }
        $userDiscountedPrice=round($userDiscountedPrice);
        //$userDiscountedPrice=sprintf('%.4f', $userDiscountedPrice);
        $object->setEndUserPrice($userDiscountedPrice);


        //Calculate Discount for Dealer

        $dealerDiscountPercent = $object->getDiscountforDealer();
        if($dealerDiscountPercent > 0){
            $dealerDiscountedPrice = $listPrice - ($listPrice * ($dealerDiscountPercent / 100));
        }else{
            $dealerDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For Dealer
        $dealerAddDiscountPercent = $object->getAddDiscountforDealer();
        if($dealerAddDiscountPercent > 0){
            $dealerDiscountedPrice = $dealerDiscountedPrice - ($dealerDiscountedPrice * ($dealerAddDiscountPercent / 100));
        }
        $dealerDiscountedPrice=round($dealerDiscountedPrice);
        //$dealerDiscountedPrice=sprintf('%.4f', $dealerDiscountedPrice);
        $object->setDealerPrice($dealerDiscountedPrice);

        //Calculate Discount for loose

        $looseDiscountPercent = $object->getDiscountforLoose();
        if($looseDiscountPercent > 0){
            $looseDiscountedPrice = $listPrice - ($listPrice * ($looseDiscountPercent / 100));
        }else{
            $looseDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For loose
        $looseAddDiscountPercent = $object->getAddDiscountforLoose();
        if($looseAddDiscountPercent > 0){
            $looseDiscountedPrice = $looseDiscountedPrice - ($looseDiscountedPrice * ($looseAddDiscountPercent / 100));
        }

        //$looseDiscountedPrice=sprintf('%.4f', $looseDiscountedPrice);
        $looseDiscountedPrice=round($looseDiscountedPrice);
        $object->setLoosePrice($looseDiscountedPrice);

        //Calculate Discount for Bulk

        $bulkDiscountPercent = $object->getDiscountforBulk();
        if($bulkDiscountPercent > 0){
            $bulkDiscountedPrice = $listPrice - ($listPrice * ($bulkDiscountPercent / 100));
        }else{
            $bulkDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For loose
        $bulkAddDiscountPercent = $object->getAddDiscountforBulk();
        if($bulkDiscountedPrice > 0){
            $bulkDiscountedPrice = $bulkDiscountedPrice - ($bulkDiscountedPrice * ($bulkAddDiscountPercent / 100));
        }
        //$bulkDiscountedPrice=sprintf('%.4f', $bulkDiscountedPrice);
        $bulkDiscountedPrice=round($bulkDiscountedPrice);
        $object->setBulkPrice($bulkDiscountedPrice);
    }
    /*
     * Calculate Pricing On Import Of Product
     * 
     */
    public static function calculatePricingOnImportObject(&$object){
        $DefaultEndUserDiscount = \Pimcore\Model\WebsiteSetting::getByName('DefaultEndUserDiscount')->getData();
        $DefaultDealerDiscount  = \Pimcore\Model\WebsiteSetting::getByName('DefaultDealerDiscount')->getData();
        $DefaultLooseDiscount   = \Pimcore\Model\WebsiteSetting::getByName('DefaultLooseDiscount')->getData();
        $DefaultBulkDiscount    = \Pimcore\Model\WebsiteSetting::getByName('DefaultBulkDiscount')->getData();

        $uSExchangeRatesetting = \Pimcore\Model\WebsiteSetting::getByName('USExchangeRate');
        $profitMarginsetting = \Pimcore\Model\WebsiteSetting::getByName('ProfitMargin');
        $sstsetting = \Pimcore\Model\WebsiteSetting::getByName('SST');
        $miscExpensessetting = \Pimcore\Model\WebsiteSetting::getByName('MiscExpenses');
            
            
        

        $productSku=$object->getSKU();
        $product= \Pimcore\Model\DataObject\Product::getBySKU($productSku,1);
        if(!empty($product)){
            throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Product With  SKU : " .$productSku . " Already Exist." );
        }

        $purchasePrice=$object->getPurchasePrice();
        if(empty($purchasePrice)){
            $purchasePrice=0;
        }

        $product = $object;

        $categoryName=$object->getCategory();
        $categoryName= strtolower($categoryName);
        $categoryObject=\Pimcore\Model\DataObject\Category::getByCategoryName($categoryName,1);
        if(empty($categoryObject)){
            throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Category Found For Product With With SKU : " .$productSku );
        }

        //var_dump($categoryObject);
        $catPath=$categoryObject->getPath();
        $catPath=ltrim($catPath,"/Category");
        $catPath="Product/".$catPath.$categoryObject->getCategoryName();
        $folder = Service::createFolderByPath($catPath);
        $parentId = $folder->getId();
        $product->setParentId($parentId);

        $product->setCategory($categoryObject->getId());

        $brandName=$object->getBrandName();
        $brandName=strtolower($brandName);
        $brandObject=\Pimcore\Model\DataObject\Brand::getByBrand($brandName,1);
        if(empty($brandObject)){
            throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Brand Found For Product With  SKU : " .$productSku );
        }
        $product->setBrandName($brandObject->getId());

        $machineType=$object->getMachineType();
        if(!empty($machineType)){
            $machineType=strtolower($machineType);
            $machineTypeObject=\Pimcore\Model\DataObject\MachineType::getByMachineType($machineType,1);
            if(empty($machineTypeObject)){
                throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("No Such Machine Found For Product With  SKU : " .$productSku );
            }
            $product->setMachineType($machineTypeObject->getId());
        }

        $currency=$object->getCurrency();
        $currencyValue=CurrencyHelper::getByName($currency);
        if(!$currencyValue){
            throw new \Pimcore\DataObject\Import\Resolver\ImportErrorException("Invalid Currency For Product With  SKU : " .$productSku );
        }
        $product->setCurrency($currencyValue);

        $product->setKey($object->getProductName());

        $purchasePrice = $product->getPurchasePrice();
        $isSst= $product->getSST();



        $uSExchangeRate = $uSExchangeRatesetting->getData();
        $miscExpenses = $miscExpensessetting->getData();
        $sst = $sstsetting->getData();
        $profitMargin = $profitMarginsetting->getData();

        if(!empty($product->getProfitMargin())){
            $profitMargin=$product->getProfitMargin();
        }
        $product->setProfitMargin($profitMargin);
        $purchasePrice = $product->getPurchasePrice();
        $isSst = $product->getSST();
        if($isSst){
            $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses*$sst);
        }else{
            $cost = ($purchasePrice*$uSExchangeRate*$miscExpenses);
        }

        //$cost=sprintf('%.4f', $cost);
        $cost=round($cost);
        $product->setCost($cost);

        $listPrice = (($cost/$profitMargin)*2);
        //$listPrice=sprintf('%.4f', $listPrice);
        $listPrice=round($listPrice);
        $product->setListPrice($listPrice);

        //Calculate Discount for End User Price
        $euDiscountPercent = $product->getDiscountforEU();
        if(empty($euDiscountPercent)){
            $euDiscountPercent=$DefaultEndUserDiscount;
        }
        if($euDiscountPercent > 0){
            $userDiscountedPrice = $listPrice - ($listPrice * ($euDiscountPercent / 100));
        }else{
            $userDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For End User Price
        $euAddDiscountPercent = $product->getAddDiscountforEU();
        if($euAddDiscountPercent > 0){
            $userDiscountedPrice = $userDiscountedPrice - ($userDiscountedPrice * ($euAddDiscountPercent / 100));
        }
        //$userDiscountedPrice=sprintf('%.4f', $userDiscountedPrice);
        $userDiscountedPrice=round($userDiscountedPrice);
        $product->setEndUserPrice($userDiscountedPrice);


        //Calculate Discount for Dealer

        $dealerDiscountPercent = $product->getDiscountforDealer();
        if(empty($dealerDiscountPercent)){
            $dealerDiscountPercent=$DefaultDealerDiscount;
        }
        if($dealerDiscountPercent > 0){
            $dealerDiscountedPrice = $listPrice - ($listPrice * ($dealerDiscountPercent / 100));
        }else{
            $dealerDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For Dealer
        $dealerAddDiscountPercent = $product->getAddDiscountforDealer();
        if($dealerAddDiscountPercent > 0){
            $dealerDiscountedPrice = $dealerDiscountedPrice - ($dealerDiscountedPrice * ($dealerAddDiscountPercent / 100));
        }
        //$dealerDiscountedPrice=sprintf('%.4f', $dealerDiscountedPrice);
        $dealerDiscountedPrice=round($dealerDiscountedPrice);
        $product->setDealerPrice($dealerDiscountedPrice);

        //Calculate Discount for loose

        $looseDiscountPercent = $product->getDiscountforLoose();
        if(empty($looseDiscountPercent)){
            $looseDiscountPercent=$DefaultLooseDiscount;
        }
        if($looseDiscountPercent > 0){
            $looseDiscountedPrice = $listPrice - ($listPrice * ($looseDiscountPercent / 100));
        }else{
            $looseDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For loose
        $looseAddDiscountPercent = $product->getAddDiscountforLoose();
        if($looseAddDiscountPercent > 0){
            $looseDiscountedPrice = $looseDiscountedPrice - ($looseDiscountedPrice * ($looseAddDiscountPercent / 100));
        }

        //$looseDiscountedPrice=sprintf('%.4f', $looseDiscountedPrice);
        $looseDiscountedPrice=round($looseDiscountedPrice);
        $product->setLoosePrice($looseDiscountedPrice);

        //Calculate Discount for Bulk

        $bulkDiscountPercent = $product->getDiscountforBulk();
        if(empty($bulkDiscountPercent)){
            $bulkDiscountPercent=$DefaultBulkDiscount;
        }
        if($bulkDiscountPercent > 0){
            $bulkDiscountedPrice = $listPrice - ($listPrice * ($bulkDiscountPercent / 100));
        }else{
            $bulkDiscountedPrice = $listPrice;
        }

        //Calculate Additional Discount For loose
        $bulkAddDiscountPercent = $product->getAddDiscountforBulk();
        if($bulkDiscountedPrice > 0){
            $bulkDiscountedPrice = $bulkDiscountedPrice - ($bulkDiscountedPrice * ($bulkAddDiscountPercent / 100));
        }
        //$bulkDiscountedPrice=sprintf('%.4f', $bulkDiscountedPrice);
        $bulkDiscountedPrice=round($bulkDiscountedPrice);
        $product->setBulkPrice($bulkDiscountedPrice);
    }
    
}
